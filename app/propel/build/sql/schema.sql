
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- cms_articles
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cms_articles`;

CREATE TABLE `cms_articles`
(
    `id_article` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(100),
    `slug` VARCHAR(100),
    `pubdate` DATE,
    `description` VARCHAR(255),
    `body` TEXT,
    `created` DATETIME,
    `modified` DATETIME,
    `user_modified` INTEGER,
    `state` VARCHAR(20) DEFAULT 'ACTIVE',
    `id_page` int(11) unsigned,
    `id_user` INTEGER COMMENT 'Usuario que creo el artículo',
    `number_visits` INTEGER DEFAULT 0,
    `year` INTEGER(4),
    `others` TEXT COMMENT 'Campo donde se guardará información extra del artículo',
    `id_enterprise` INTEGER,
    PRIMARY KEY (`id_article`),
    INDEX `id_page` (`id_page`),
    INDEX `id_user` (`id_user`),
    INDEX `user_modified` (`user_modified`),
    INDEX `cms_articles_fk3` (`id_enterprise`),
    CONSTRAINT `cms_articles_fk3`
        FOREIGN KEY (`id_enterprise`)
        REFERENCES `sys_enterprises` (`id_enterprise`),
    CONSTRAINT `cms_articles_fk`
        FOREIGN KEY (`id_page`)
        REFERENCES `cms_pages` (`id_page`),
    CONSTRAINT `cms_articles_fk1`
        FOREIGN KEY (`id_user`)
        REFERENCES `sys_users` (`id_user`),
    CONSTRAINT `cms_articles_fk2`
        FOREIGN KEY (`user_modified`)
        REFERENCES `sys_users` (`id_user`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cms_favorites
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cms_favorites`;

CREATE TABLE `cms_favorites`
(
    `id_user` INTEGER NOT NULL,
    `id_article` INTEGER NOT NULL,
    PRIMARY KEY (`id_user`,`id_article`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cms_files_x_article
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cms_files_x_article`;

CREATE TABLE `cms_files_x_article`
(
    `id_file` int(11) unsigned NOT NULL,
    `id_article` int(11) unsigned NOT NULL,
    `type` VARCHAR(20),
    `description` TEXT,
    `primary` VARCHAR(50) DEFAULT 'NO',
    `state` VARCHAR(10) DEFAULT 'ACTIVE',
    `order` INTEGER DEFAULT 0,
    PRIMARY KEY (`id_file`),
    INDEX `id_file` (`id_file`),
    INDEX `id_article` (`id_article`),
    CONSTRAINT `cms_files_x_article_fk`
        FOREIGN KEY (`id_file`)
        REFERENCES `sys_files` (`id_file`),
    CONSTRAINT `cms_files_x_article_fk1`
        FOREIGN KEY (`id_article`)
        REFERENCES `cms_articles` (`id_article`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cms_files_x_page
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cms_files_x_page`;

CREATE TABLE `cms_files_x_page`
(
    `id_file` int(11) unsigned NOT NULL,
    `id_page` int(11) unsigned NOT NULL,
    `type` VARCHAR(20),
    `description` TEXT,
    `primary` VARCHAR(50) DEFAULT 'NO',
    `state` VARCHAR(10) DEFAULT 'ACTIVE',
    PRIMARY KEY (`id_file`),
    INDEX `id_file` (`id_file`),
    INDEX `id_page` (`id_page`),
    CONSTRAINT `cms_files_x_page_fk`
        FOREIGN KEY (`id_file`)
        REFERENCES `sys_files` (`id_file`),
    CONSTRAINT `cms_files_x_page_fk1`
        FOREIGN KEY (`id_page`)
        REFERENCES `cms_pages` (`id_page`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cms_pages
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cms_pages`;

CREATE TABLE `cms_pages`
(
    `id_page` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(100) NOT NULL,
    `slug` VARCHAR(100) NOT NULL,
    `order` INTEGER NOT NULL,
    `id_parent` INTEGER DEFAULT 0,
    `state` VARCHAR(10) DEFAULT 'ACTIVE',
    `template` VARCHAR(100) NOT NULL,
    `body` TEXT,
    `visible` VARCHAR(10) DEFAULT 'YES',
    `type` VARCHAR(10) DEFAULT 'CMS',
    `created` DATETIME,
    `modified` DATETIME,
    `user_modified` INTEGER,
    `id_enterprise` INTEGER,
    PRIMARY KEY (`id_page`),
    INDEX `template` (`template`),
    INDEX `template_2` (`template`),
    INDEX `template_3` (`template`),
    INDEX `user_modified` (`user_modified`),
    INDEX `cms_pages_fk2` (`id_enterprise`),
    CONSTRAINT `cms_pages_fk2`
        FOREIGN KEY (`id_enterprise`)
        REFERENCES `sys_enterprises` (`id_enterprise`),
    CONSTRAINT `cms_pages_fk`
        FOREIGN KEY (`template`)
        REFERENCES `cms_templates` (`template`),
    CONSTRAINT `cms_pages_fk1`
        FOREIGN KEY (`user_modified`)
        REFERENCES `sys_users` (`id_user`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cms_tags
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cms_tags`;

CREATE TABLE `cms_tags`
(
    `id_tag` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50),
    `description` VARCHAR(255),
    `id_file` int(11) unsigned,
    `state` VARCHAR(10) DEFAULT 'ACTIVE',
    `id_enterprise` INTEGER,
    PRIMARY KEY (`id_tag`),
    INDEX `id_file` (`id_file`),
    INDEX `cms_tags_fk1` (`id_enterprise`),
    CONSTRAINT `cms_tags_fk1`
        FOREIGN KEY (`id_enterprise`)
        REFERENCES `sys_enterprises` (`id_enterprise`),
    CONSTRAINT `cms_tags_fk`
        FOREIGN KEY (`id_file`)
        REFERENCES `sys_files` (`id_file`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cms_tags_x_article
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cms_tags_x_article`;

CREATE TABLE `cms_tags_x_article`
(
    `id_tag` INTEGER NOT NULL,
    `id_article` int(11) unsigned NOT NULL,
    PRIMARY KEY (`id_tag`,`id_article`),
    INDEX `id_tag` (`id_tag`),
    INDEX `id_article` (`id_article`),
    CONSTRAINT `cms_tags_x_article_fk`
        FOREIGN KEY (`id_tag`)
        REFERENCES `cms_tags` (`id_tag`),
    CONSTRAINT `cms_tags_x_article_fk1`
        FOREIGN KEY (`id_article`)
        REFERENCES `cms_articles` (`id_article`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cms_templates
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cms_templates`;

CREATE TABLE `cms_templates`
(
    `template` VARCHAR(100) NOT NULL,
    `name` VARCHAR(100),
    `description` VARCHAR(255),
    `type` VARCHAR(5) DEFAULT 'CMS',
    PRIMARY KEY (`template`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cms_tmp_files
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cms_tmp_files`;

CREATE TABLE `cms_tmp_files`
(
    `id_tmp_file` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `id_file` int(11) unsigned DEFAULT 0 NOT NULL,
    `type` VARCHAR(20),
    `description` VARCHAR(100),
    `id_user` INTEGER,
    PRIMARY KEY (`id_tmp_file`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cms_videos_x_article
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cms_videos_x_article`;

CREATE TABLE `cms_videos_x_article`
(
    `id_video` INTEGER NOT NULL AUTO_INCREMENT,
    `id_article` int(11) unsigned NOT NULL,
    `url` VARCHAR(255) NOT NULL,
    `type` VARCHAR(10) NOT NULL,
    `description` TEXT,
    `primary` VARCHAR(5) DEFAULT 'NO' NOT NULL,
    `state` VARCHAR(10) DEFAULT 'ACTIVE' NOT NULL,
    `created` DATETIME,
    `modified` DATETIME,
    `user_modified` INTEGER,
    PRIMARY KEY (`id_video`),
    INDEX `id_article` (`id_article`),
    INDEX `user_modified` (`user_modified`),
    CONSTRAINT `cms_videos_x_article_fk`
        FOREIGN KEY (`user_modified`)
        REFERENCES `sys_users` (`id_user`),
    CONSTRAINT `cms_videos_x_articulo_fk`
        FOREIGN KEY (`id_article`)
        REFERENCES `cms_articles` (`id_article`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cms_videos_x_page
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cms_videos_x_page`;

CREATE TABLE `cms_videos_x_page`
(
    `id_video` INTEGER NOT NULL AUTO_INCREMENT,
    `id_page` int(11) unsigned NOT NULL,
    `url` VARCHAR(255) NOT NULL,
    `type` VARCHAR(10) NOT NULL,
    `description` TEXT,
    `primary` VARCHAR(5) DEFAULT 'NO' NOT NULL,
    `state` VARCHAR(10) DEFAULT 'ACTIVE' NOT NULL,
    `created` DATETIME,
    `modified` DATETIME,
    `user_modified` INTEGER,
    PRIMARY KEY (`id_video`),
    INDEX `id_page` (`id_page`),
    INDEX `user_modified` (`user_modified`),
    CONSTRAINT `cms_videos_x_articulo_fk_new`
        FOREIGN KEY (`id_page`)
        REFERENCES `cms_pages` (`id_page`),
    CONSTRAINT `cms_videos_x_page_fk`
        FOREIGN KEY (`user_modified`)
        REFERENCES `sys_users` (`id_user`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- smi_churches
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `smi_churches`;

CREATE TABLE `smi_churches`
(
    `id_church` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(100),
    `address` VARCHAR(255),
    `constancy_type` VARCHAR(100) COMMENT 'Tipo de construcción de la iglesia',
    `area` INTEGER COMMENT 'Área medidos en metros cuadrados',
    `department` VARCHAR(50),
    `id_city` INTEGER,
    `latitude` DECIMAL(11,0),
    `length` DECIMAL(11,0),
    `phone` VARCHAR(50),
    `email` VARCHAR(100),
    `state` VARCHAR(10) DEFAULT 'ACTIVE',
    `id_country` INTEGER DEFAULT 30,
    `id_enterprise` INTEGER,
    PRIMARY KEY (`id_church`),
    INDEX `smi_churches_fk` (`id_enterprise`),
    CONSTRAINT `smi_churches_fk`
        FOREIGN KEY (`id_enterprise`)
        REFERENCES `sys_enterprises` (`id_enterprise`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- sys_chats
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sys_chats`;

CREATE TABLE `sys_chats`
(
    `id_chat` INTEGER NOT NULL AUTO_INCREMENT,
    `id_sender` INTEGER COMMENT 'id_user del emisor del mensaje de chat',
    `id_receiver` INTEGER COMMENT 'id_user del receptor del mensaje de chat',
    `message` VARCHAR(255),
    `created` DATETIME,
    `modified` DATETIME,
    PRIMARY KEY (`id_chat`),
    INDEX `id_sender` (`id_sender`),
    INDEX `id_receiver` (`id_receiver`),
    CONSTRAINT `sys_chats_fk`
        FOREIGN KEY (`id_sender`)
        REFERENCES `sys_users` (`id_user`),
    CONSTRAINT `sys_chats_fk1`
        FOREIGN KEY (`id_receiver`)
        REFERENCES `sys_users` (`id_user`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- sys_cities
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sys_cities`;

CREATE TABLE `sys_cities`
(
    `id_city` INTEGER NOT NULL AUTO_INCREMENT,
    `id_country` INTEGER NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `code` VARCHAR(10),
    `region_code` VARCHAR(50),
    `region_name` VARCHAR(255),
    `region_type` VARCHAR(50),
    `coordinates` VARCHAR(50),
    `state` VARCHAR(20) DEFAULT 'ACTIVE' NOT NULL,
    PRIMARY KEY (`id_city`),
    INDEX `ID_PAIS` (`id_country`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- sys_countries
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sys_countries`;

CREATE TABLE `sys_countries`
(
    `id_country` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(200) NOT NULL,
    `code` VARCHAR(50),
    `state` VARCHAR(20) DEFAULT 'ACTIVE' NOT NULL,
    PRIMARY KEY (`id_country`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- sys_enterprises
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sys_enterprises`;

CREATE TABLE `sys_enterprises`
(
    `id_enterprise` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(100),
    `number_register` VARCHAR(20),
    `address` VARCHAR(100),
    `phone` VARCHAR(20),
    `fax` VARCHAR(20),
    `email` VARCHAR(50),
    `web_page` VARCHAR(100),
    `state` VARCHAR(100) DEFAULT 'ACTIVE',
    `created` DATETIME,
    `modified` DATETIME,
    `user_modified` INTEGER,
    PRIMARY KEY (`id_enterprise`),
    INDEX `user_modified` (`user_modified`),
    CONSTRAINT `FK__sys_users`
        FOREIGN KEY (`user_modified`)
        REFERENCES `sys_users` (`id_user`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- sys_files
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sys_files`;

CREATE TABLE `sys_files`
(
    `id_file` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `filename` VARCHAR(255),
    `title` VARCHAR(100),
    `type` VARCHAR(100),
    `fullpath` VARCHAR(255),
    `size` DECIMAL(20,0) DEFAULT 0,
    `image_width` INTEGER DEFAULT 0,
    `image_height` INTEGER DEFAULT 0,
    `image_type` VARCHAR(20),
    `is_image` VARCHAR(20) DEFAULT 'NO',
    `created` DATETIME,
    `modified` DATETIME,
    `user_modified` INTEGER,
    PRIMARY KEY (`id_file`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- sys_lang
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sys_lang`;

CREATE TABLE `sys_lang`
(
    `id_lang` INTEGER NOT NULL AUTO_INCREMENT,
    `key` VARCHAR(100) NOT NULL,
    `english` VARCHAR(255),
    `spanish` VARCHAR(255),
    `portuguese` VARCHAR(255),
    PRIMARY KEY (`id_lang`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- sys_notifications
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sys_notifications`;

CREATE TABLE `sys_notifications`
(
    `id_notification` INTEGER NOT NULL AUTO_INCREMENT,
    `id_sender` INTEGER COMMENT 'id_user del emisor de la notificación',
    `id_receiver` INTEGER COMMENT 'id_user del receptor de la notificación',
    `type` VARCHAR(20) COMMENT 'tipo de notificación: ALERT, MESSAGE, CHAT, INVITATION',
    `title` VARCHAR(255),
    `message` TEXT,
    `state` VARCHAR(20) DEFAULT 'CREATED' COMMENT 'Estado de la notifiación: CREATED, VIEWED, READ',
    `created` DATETIME,
    `modified` DATETIME,
    PRIMARY KEY (`id_notification`),
    INDEX `id_sender` (`id_sender`),
    INDEX `id_receiver` (`id_receiver`),
    CONSTRAINT `sys_notifications_fk`
        FOREIGN KEY (`id_sender`)
        REFERENCES `sys_users` (`id_user`),
    CONSTRAINT `sys_notifications_fk1`
        FOREIGN KEY (`id_receiver`)
        REFERENCES `sys_users` (`id_user`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- sys_pages
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sys_pages`;

CREATE TABLE `sys_pages`
(
    `id_page` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(100) NOT NULL,
    `slug` VARCHAR(100) NOT NULL,
    `order` int(11) unsigned NOT NULL,
    `id_module` int(11) unsigned DEFAULT 0,
    `id_section` INTEGER DEFAULT 0,
    `state` VARCHAR(10) DEFAULT 'ACTIVE',
    `visible` VARCHAR(10) DEFAULT 'YES',
    `created` DATETIME,
    `modified` DATETIME,
    `user_modified` INTEGER,
    PRIMARY KEY (`id_page`),
    INDEX `user_modified` (`user_modified`),
    CONSTRAINT `sys_pages_fk`
        FOREIGN KEY (`user_modified`)
        REFERENCES `sys_users` (`id_user`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- sys_parameters
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sys_parameters`;

CREATE TABLE `sys_parameters`
(
    `name` VARCHAR(50) NOT NULL,
    `value` VARCHAR(255),
    `title` VARCHAR(100),
    `description` TEXT,
    PRIMARY KEY (`name`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- sys_permissions
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sys_permissions`;

CREATE TABLE `sys_permissions`
(
    `id_page` int(11) unsigned NOT NULL,
    `id_rol` int(11) unsigned NOT NULL,
    `create` VARCHAR(3) DEFAULT 'NO',
    `read` VARCHAR(3) DEFAULT 'NO',
    `update` VARCHAR(3) DEFAULT 'NO',
    `delete` VARCHAR(3) DEFAULT 'NO',
    PRIMARY KEY (`id_page`,`id_rol`),
    INDEX `id_page` (`id_page`),
    INDEX `id_rol` (`id_rol`),
    CONSTRAINT `sys_permissions_fk`
        FOREIGN KEY (`id_page`)
        REFERENCES `sys_pages` (`id_page`),
    CONSTRAINT `sys_permissions_fk1`
        FOREIGN KEY (`id_rol`)
        REFERENCES `sys_roles` (`id_rol`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- sys_roles
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sys_roles`;

CREATE TABLE `sys_roles`
(
    `id_rol` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50),
    `description` VARCHAR(255),
    PRIMARY KEY (`id_rol`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- sys_sessions
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sys_sessions`;

CREATE TABLE `sys_sessions`
(
    `session_id` VARCHAR(40) DEFAULT '0' NOT NULL,
    `ip_address` VARCHAR(45) DEFAULT '0' NOT NULL,
    `user_agent` VARCHAR(120) NOT NULL,
    `last_activity` int(10) unsigned DEFAULT 0 NOT NULL,
    `user_data` TEXT NOT NULL,
    PRIMARY KEY (`session_id`),
    INDEX `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- sys_time_zone
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sys_time_zone`;

CREATE TABLE `sys_time_zone`
(
    `id_time_zone` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `region` VARCHAR(255),
    `country` VARCHAR(255),
    `city` VARCHAR(255),
    `state` VARCHAR(50) DEFAULT 'ACTIVE',
    PRIMARY KEY (`id_time_zone`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- sys_users
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sys_users`;

CREATE TABLE `sys_users`
(
    `id_user` INTEGER NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(50) COMMENT 'Se puede usar CI como username',
    `password` VARCHAR(255),
    `email` VARCHAR(100),
    `first_name` VARCHAR(50),
    `last_name` VARCHAR(100),
    `state` VARCHAR(20) DEFAULT 'CREATED',
    `id_rol` int(11) unsigned,
    `id_photo` int(11) unsigned,
    `created` DATETIME,
    `modified` DATETIME,
    `user_modified` INTEGER,
    `lang_code` VARCHAR(10) DEFAULT 'spanish',
    `phone` VARCHAR(20),
    `mobile` VARCHAR(20),
    `id_time_zone` INTEGER,
    `id_city_birthday` INTEGER,
    `id_country_birthday` INTEGER,
    `id_city_address` INTEGER,
    `id_country_address` INTEGER,
    `gender` VARCHAR(5),
    `birthday` DATE,
    `birthplace` VARCHAR(100),
    `hash` VARCHAR(100),
    `tour` VARCHAR(5) DEFAULT 'YES',
    `marital_status` VARCHAR(20),
    `level_education` VARCHAR(20),
    `occupation` VARCHAR(50),
    `department_address` VARCHAR(50),
    `department_birthday` VARCHAR(50),
    `address` VARCHAR(255),
    `id_church` INTEGER,
    `member` VARCHAR(5) DEFAULT 'NO' COMMENT 'Condición del asociado BAUTIZADO/SIMPATIZANTE',
    `pastor_baptism` VARCHAR(100) COMMENT 'Pastor por el que fué bautizado',
    `date_baptism` DATETIME COMMENT 'Fecha del bautizmo',
    `id_enterprise` INTEGER COMMENT 'ID de la empresa a la que pertenece el usuario',
    PRIMARY KEY (`id_user`),
    UNIQUE INDEX `email` (`email`),
    INDEX `id_rol` (`id_rol`),
    INDEX `id_photo` (`id_photo`),
    INDEX `id_church` (`id_church`),
    INDEX `user_modified` (`user_modified`),
    INDEX `sys_users_fk4` (`id_enterprise`),
    CONSTRAINT `sys_users_fk4`
        FOREIGN KEY (`id_enterprise`)
        REFERENCES `sys_enterprises` (`id_enterprise`),
    CONSTRAINT `sys_users_fk`
        FOREIGN KEY (`id_rol`)
        REFERENCES `sys_roles` (`id_rol`),
    CONSTRAINT `sys_users_fk1`
        FOREIGN KEY (`id_photo`)
        REFERENCES `sys_files` (`id_file`),
    CONSTRAINT `sys_users_fk3`
        FOREIGN KEY (`user_modified`)
        REFERENCES `sys_users` (`id_user`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;

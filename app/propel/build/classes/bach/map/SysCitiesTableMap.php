<?php



/**
 * This class defines the structure of the 'sys_cities' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bach.map
 */
class SysCitiesTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bach.map.SysCitiesTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sys_cities');
        $this->setPhpName('SysCities');
        $this->setClassname('SysCities');
        $this->setPackage('bach');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_city', 'IdCity', 'INTEGER', true, null, null);
        $this->addColumn('id_country', 'IdCountry', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('code', 'Code', 'VARCHAR', false, 10, null);
        $this->addColumn('region_code', 'RegionCode', 'VARCHAR', false, 50, null);
        $this->addColumn('region_name', 'RegionName', 'VARCHAR', false, 255, null);
        $this->addColumn('region_type', 'RegionType', 'VARCHAR', false, 50, null);
        $this->addColumn('coordinates', 'Coordinates', 'VARCHAR', false, 50, null);
        $this->addColumn('state', 'State', 'VARCHAR', true, 20, 'ACTIVE');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // SysCitiesTableMap

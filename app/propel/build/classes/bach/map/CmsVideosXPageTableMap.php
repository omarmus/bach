<?php



/**
 * This class defines the structure of the 'cms_videos_x_page' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bach.map
 */
class CmsVideosXPageTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bach.map.CmsVideosXPageTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cms_videos_x_page');
        $this->setPhpName('CmsVideosXPage');
        $this->setClassname('CmsVideosXPage');
        $this->setPackage('bach');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_video', 'IdVideo', 'INTEGER', true, null, null);
        $this->addForeignKey('id_page', 'IdPage', 'INTEGER', 'cms_pages', 'id_page', true, null, null);
        $this->addColumn('url', 'Url', 'VARCHAR', true, 255, null);
        $this->addColumn('type', 'Type', 'VARCHAR', true, 10, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('primary', 'Primary', 'VARCHAR', true, 5, 'NO');
        $this->addColumn('state', 'State', 'VARCHAR', true, 10, 'ACTIVE');
        $this->addColumn('created', 'Created', 'TIMESTAMP', false, null, null);
        $this->addColumn('modified', 'Modified', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('user_modified', 'UserModified', 'INTEGER', 'sys_users', 'id_user', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CmsPages', 'CmsPages', RelationMap::MANY_TO_ONE, array('id_page' => 'id_page', ), null, null);
        $this->addRelation('SysUsers', 'SysUsers', RelationMap::MANY_TO_ONE, array('user_modified' => 'id_user', ), null, null);
    } // buildRelations()

} // CmsVideosXPageTableMap

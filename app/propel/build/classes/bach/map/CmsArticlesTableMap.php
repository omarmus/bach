<?php



/**
 * This class defines the structure of the 'cms_articles' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bach.map
 */
class CmsArticlesTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bach.map.CmsArticlesTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cms_articles');
        $this->setPhpName('CmsArticles');
        $this->setClassname('CmsArticles');
        $this->setPackage('bach');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_article', 'IdArticle', 'INTEGER', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 100, null);
        $this->addColumn('slug', 'Slug', 'VARCHAR', false, 100, null);
        $this->addColumn('pubdate', 'Pubdate', 'DATE', false, null, null);
        $this->addColumn('description', 'Description', 'VARCHAR', false, 255, null);
        $this->addColumn('body', 'Body', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created', 'Created', 'TIMESTAMP', false, null, null);
        $this->addColumn('modified', 'Modified', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('user_modified', 'UserModified', 'INTEGER', 'sys_users', 'id_user', false, null, null);
        $this->addColumn('state', 'State', 'VARCHAR', false, 20, 'ACTIVE');
        $this->addForeignKey('id_page', 'IdPage', 'INTEGER', 'cms_pages', 'id_page', false, null, null);
        $this->addForeignKey('id_user', 'IdUser', 'INTEGER', 'sys_users', 'id_user', false, null, null);
        $this->addColumn('number_visits', 'NumberVisits', 'INTEGER', false, null, 0);
        $this->addColumn('year', 'Year', 'INTEGER', false, 4, null);
        $this->addColumn('others', 'Others', 'LONGVARCHAR', false, null, null);
        $this->addForeignKey('id_enterprise', 'IdEnterprise', 'INTEGER', 'sys_enterprises', 'id_enterprise', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SysEnterprises', 'SysEnterprises', RelationMap::MANY_TO_ONE, array('id_enterprise' => 'id_enterprise', ), null, null);
        $this->addRelation('CmsPages', 'CmsPages', RelationMap::MANY_TO_ONE, array('id_page' => 'id_page', ), null, null);
        $this->addRelation('SysUsersRelatedByIdUser', 'SysUsers', RelationMap::MANY_TO_ONE, array('id_user' => 'id_user', ), null, null);
        $this->addRelation('SysUsersRelatedByUserModified', 'SysUsers', RelationMap::MANY_TO_ONE, array('user_modified' => 'id_user', ), null, null);
        $this->addRelation('CmsFilesXArticle', 'CmsFilesXArticle', RelationMap::ONE_TO_MANY, array('id_article' => 'id_article', ), null, null, 'CmsFilesXArticles');
        $this->addRelation('CmsTagsXArticle', 'CmsTagsXArticle', RelationMap::ONE_TO_MANY, array('id_article' => 'id_article', ), null, null, 'CmsTagsXArticles');
        $this->addRelation('CmsVideosXArticle', 'CmsVideosXArticle', RelationMap::ONE_TO_MANY, array('id_article' => 'id_article', ), null, null, 'CmsVideosXArticles');
    } // buildRelations()

} // CmsArticlesTableMap

<?php



/**
 * This class defines the structure of the 'sys_enterprises' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bach.map
 */
class SysEnterprisesTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bach.map.SysEnterprisesTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sys_enterprises');
        $this->setPhpName('SysEnterprises');
        $this->setClassname('SysEnterprises');
        $this->setPackage('bach');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_enterprise', 'IdEnterprise', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 100, null);
        $this->addColumn('number_register', 'NumberRegister', 'VARCHAR', false, 20, null);
        $this->addColumn('address', 'Address', 'VARCHAR', false, 100, null);
        $this->addColumn('phone', 'Phone', 'VARCHAR', false, 20, null);
        $this->addColumn('fax', 'Fax', 'VARCHAR', false, 20, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 50, null);
        $this->addColumn('web_page', 'WebPage', 'VARCHAR', false, 100, null);
        $this->addColumn('state', 'State', 'VARCHAR', false, 100, 'ACTIVE');
        $this->addColumn('created', 'Created', 'TIMESTAMP', false, null, null);
        $this->addColumn('modified', 'Modified', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('user_modified', 'UserModified', 'INTEGER', 'sys_users', 'id_user', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SysUsersRelatedByUserModified', 'SysUsers', RelationMap::MANY_TO_ONE, array('user_modified' => 'id_user', ), null, null);
        $this->addRelation('CmsArticles', 'CmsArticles', RelationMap::ONE_TO_MANY, array('id_enterprise' => 'id_enterprise', ), null, null, 'CmsArticless');
        $this->addRelation('CmsPages', 'CmsPages', RelationMap::ONE_TO_MANY, array('id_enterprise' => 'id_enterprise', ), null, null, 'CmsPagess');
        $this->addRelation('CmsTags', 'CmsTags', RelationMap::ONE_TO_MANY, array('id_enterprise' => 'id_enterprise', ), null, null, 'CmsTagss');
        $this->addRelation('SmiChurches', 'SmiChurches', RelationMap::ONE_TO_MANY, array('id_enterprise' => 'id_enterprise', ), null, null, 'SmiChurchess');
        $this->addRelation('SysUsersRelatedByIdEnterprise', 'SysUsers', RelationMap::ONE_TO_MANY, array('id_enterprise' => 'id_enterprise', ), null, null, 'SysUserssRelatedByIdEnterprise');
    } // buildRelations()

} // SysEnterprisesTableMap

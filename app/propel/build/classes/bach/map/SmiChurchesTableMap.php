<?php



/**
 * This class defines the structure of the 'smi_churches' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bach.map
 */
class SmiChurchesTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bach.map.SmiChurchesTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('smi_churches');
        $this->setPhpName('SmiChurches');
        $this->setClassname('SmiChurches');
        $this->setPackage('bach');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_church', 'IdChurch', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 100, null);
        $this->addColumn('address', 'Address', 'VARCHAR', false, 255, null);
        $this->addColumn('constancy_type', 'ConstancyType', 'VARCHAR', false, 100, null);
        $this->addColumn('area', 'Area', 'INTEGER', false, null, null);
        $this->addColumn('department', 'Department', 'VARCHAR', false, 50, null);
        $this->addColumn('id_city', 'IdCity', 'INTEGER', false, null, null);
        $this->addColumn('latitude', 'Latitude', 'DECIMAL', false, 11, null);
        $this->addColumn('length', 'Length', 'DECIMAL', false, 11, null);
        $this->addColumn('phone', 'Phone', 'VARCHAR', false, 50, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 100, null);
        $this->addColumn('state', 'State', 'VARCHAR', false, 10, 'ACTIVE');
        $this->addColumn('id_country', 'IdCountry', 'INTEGER', false, null, 30);
        $this->addForeignKey('id_enterprise', 'IdEnterprise', 'INTEGER', 'sys_enterprises', 'id_enterprise', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SysEnterprises', 'SysEnterprises', RelationMap::MANY_TO_ONE, array('id_enterprise' => 'id_enterprise', ), null, null);
    } // buildRelations()

} // SmiChurchesTableMap

<?php



/**
 * This class defines the structure of the 'cms_tags_x_article' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bach.map
 */
class CmsTagsXArticleTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bach.map.CmsTagsXArticleTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cms_tags_x_article');
        $this->setPhpName('CmsTagsXArticle');
        $this->setClassname('CmsTagsXArticle');
        $this->setPackage('bach');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('id_tag', 'IdTag', 'INTEGER' , 'cms_tags', 'id_tag', true, null, null);
        $this->addForeignPrimaryKey('id_article', 'IdArticle', 'INTEGER' , 'cms_articles', 'id_article', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CmsTags', 'CmsTags', RelationMap::MANY_TO_ONE, array('id_tag' => 'id_tag', ), null, null);
        $this->addRelation('CmsArticles', 'CmsArticles', RelationMap::MANY_TO_ONE, array('id_article' => 'id_article', ), null, null);
    } // buildRelations()

} // CmsTagsXArticleTableMap

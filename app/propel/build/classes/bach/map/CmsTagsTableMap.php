<?php



/**
 * This class defines the structure of the 'cms_tags' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bach.map
 */
class CmsTagsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bach.map.CmsTagsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cms_tags');
        $this->setPhpName('CmsTags');
        $this->setClassname('CmsTags');
        $this->setPackage('bach');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_tag', 'IdTag', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 50, null);
        $this->addColumn('description', 'Description', 'VARCHAR', false, 255, null);
        $this->addForeignKey('id_file', 'IdFile', 'INTEGER', 'sys_files', 'id_file', false, null, null);
        $this->addColumn('state', 'State', 'VARCHAR', false, 10, 'ACTIVE');
        $this->addForeignKey('id_enterprise', 'IdEnterprise', 'INTEGER', 'sys_enterprises', 'id_enterprise', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SysEnterprises', 'SysEnterprises', RelationMap::MANY_TO_ONE, array('id_enterprise' => 'id_enterprise', ), null, null);
        $this->addRelation('SysFiles', 'SysFiles', RelationMap::MANY_TO_ONE, array('id_file' => 'id_file', ), null, null);
        $this->addRelation('CmsTagsXArticle', 'CmsTagsXArticle', RelationMap::ONE_TO_MANY, array('id_tag' => 'id_tag', ), null, null, 'CmsTagsXArticles');
    } // buildRelations()

} // CmsTagsTableMap

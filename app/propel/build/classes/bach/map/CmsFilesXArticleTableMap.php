<?php



/**
 * This class defines the structure of the 'cms_files_x_article' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bach.map
 */
class CmsFilesXArticleTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bach.map.CmsFilesXArticleTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cms_files_x_article');
        $this->setPhpName('CmsFilesXArticle');
        $this->setClassname('CmsFilesXArticle');
        $this->setPackage('bach');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('id_file', 'IdFile', 'INTEGER' , 'sys_files', 'id_file', true, null, null);
        $this->addForeignKey('id_article', 'IdArticle', 'INTEGER', 'cms_articles', 'id_article', true, null, null);
        $this->addColumn('type', 'Type', 'VARCHAR', false, 20, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('primary', 'Primary', 'VARCHAR', false, 50, 'NO');
        $this->addColumn('state', 'State', 'VARCHAR', false, 10, 'ACTIVE');
        $this->addColumn('order', 'Order', 'INTEGER', false, null, 0);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SysFiles', 'SysFiles', RelationMap::MANY_TO_ONE, array('id_file' => 'id_file', ), null, null);
        $this->addRelation('CmsArticles', 'CmsArticles', RelationMap::MANY_TO_ONE, array('id_article' => 'id_article', ), null, null);
    } // buildRelations()

} // CmsFilesXArticleTableMap

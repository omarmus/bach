<?php


/**
 * Base class that represents a query for the 'sys_cities' table.
 *
 *
 *
 * @method SysCitiesQuery orderByIdCity($order = Criteria::ASC) Order by the id_city column
 * @method SysCitiesQuery orderByIdCountry($order = Criteria::ASC) Order by the id_country column
 * @method SysCitiesQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method SysCitiesQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method SysCitiesQuery orderByRegionCode($order = Criteria::ASC) Order by the region_code column
 * @method SysCitiesQuery orderByRegionName($order = Criteria::ASC) Order by the region_name column
 * @method SysCitiesQuery orderByRegionType($order = Criteria::ASC) Order by the region_type column
 * @method SysCitiesQuery orderByCoordinates($order = Criteria::ASC) Order by the coordinates column
 * @method SysCitiesQuery orderByState($order = Criteria::ASC) Order by the state column
 *
 * @method SysCitiesQuery groupByIdCity() Group by the id_city column
 * @method SysCitiesQuery groupByIdCountry() Group by the id_country column
 * @method SysCitiesQuery groupByName() Group by the name column
 * @method SysCitiesQuery groupByCode() Group by the code column
 * @method SysCitiesQuery groupByRegionCode() Group by the region_code column
 * @method SysCitiesQuery groupByRegionName() Group by the region_name column
 * @method SysCitiesQuery groupByRegionType() Group by the region_type column
 * @method SysCitiesQuery groupByCoordinates() Group by the coordinates column
 * @method SysCitiesQuery groupByState() Group by the state column
 *
 * @method SysCitiesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SysCitiesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SysCitiesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SysCities findOne(PropelPDO $con = null) Return the first SysCities matching the query
 * @method SysCities findOneOrCreate(PropelPDO $con = null) Return the first SysCities matching the query, or a new SysCities object populated from the query conditions when no match is found
 *
 * @method SysCities findOneByIdCountry(int $id_country) Return the first SysCities filtered by the id_country column
 * @method SysCities findOneByName(string $name) Return the first SysCities filtered by the name column
 * @method SysCities findOneByCode(string $code) Return the first SysCities filtered by the code column
 * @method SysCities findOneByRegionCode(string $region_code) Return the first SysCities filtered by the region_code column
 * @method SysCities findOneByRegionName(string $region_name) Return the first SysCities filtered by the region_name column
 * @method SysCities findOneByRegionType(string $region_type) Return the first SysCities filtered by the region_type column
 * @method SysCities findOneByCoordinates(string $coordinates) Return the first SysCities filtered by the coordinates column
 * @method SysCities findOneByState(string $state) Return the first SysCities filtered by the state column
 *
 * @method array findByIdCity(int $id_city) Return SysCities objects filtered by the id_city column
 * @method array findByIdCountry(int $id_country) Return SysCities objects filtered by the id_country column
 * @method array findByName(string $name) Return SysCities objects filtered by the name column
 * @method array findByCode(string $code) Return SysCities objects filtered by the code column
 * @method array findByRegionCode(string $region_code) Return SysCities objects filtered by the region_code column
 * @method array findByRegionName(string $region_name) Return SysCities objects filtered by the region_name column
 * @method array findByRegionType(string $region_type) Return SysCities objects filtered by the region_type column
 * @method array findByCoordinates(string $coordinates) Return SysCities objects filtered by the coordinates column
 * @method array findByState(string $state) Return SysCities objects filtered by the state column
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseSysCitiesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSysCitiesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bach';
        }
        if (null === $modelName) {
            $modelName = 'SysCities';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SysCitiesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SysCitiesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SysCitiesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SysCitiesQuery) {
            return $criteria;
        }
        $query = new SysCitiesQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SysCities|SysCities[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SysCitiesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SysCitiesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SysCities A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdCity($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SysCities A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_city`, `id_country`, `name`, `code`, `region_code`, `region_name`, `region_type`, `coordinates`, `state` FROM `sys_cities` WHERE `id_city` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SysCities();
            $obj->hydrate($row);
            SysCitiesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SysCities|SysCities[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SysCities[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SysCitiesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SysCitiesPeer::ID_CITY, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SysCitiesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SysCitiesPeer::ID_CITY, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_city column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCity(1234); // WHERE id_city = 1234
     * $query->filterByIdCity(array(12, 34)); // WHERE id_city IN (12, 34)
     * $query->filterByIdCity(array('min' => 12)); // WHERE id_city >= 12
     * $query->filterByIdCity(array('max' => 12)); // WHERE id_city <= 12
     * </code>
     *
     * @param     mixed $idCity The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysCitiesQuery The current query, for fluid interface
     */
    public function filterByIdCity($idCity = null, $comparison = null)
    {
        if (is_array($idCity)) {
            $useMinMax = false;
            if (isset($idCity['min'])) {
                $this->addUsingAlias(SysCitiesPeer::ID_CITY, $idCity['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCity['max'])) {
                $this->addUsingAlias(SysCitiesPeer::ID_CITY, $idCity['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysCitiesPeer::ID_CITY, $idCity, $comparison);
    }

    /**
     * Filter the query on the id_country column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCountry(1234); // WHERE id_country = 1234
     * $query->filterByIdCountry(array(12, 34)); // WHERE id_country IN (12, 34)
     * $query->filterByIdCountry(array('min' => 12)); // WHERE id_country >= 12
     * $query->filterByIdCountry(array('max' => 12)); // WHERE id_country <= 12
     * </code>
     *
     * @param     mixed $idCountry The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysCitiesQuery The current query, for fluid interface
     */
    public function filterByIdCountry($idCountry = null, $comparison = null)
    {
        if (is_array($idCountry)) {
            $useMinMax = false;
            if (isset($idCountry['min'])) {
                $this->addUsingAlias(SysCitiesPeer::ID_COUNTRY, $idCountry['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCountry['max'])) {
                $this->addUsingAlias(SysCitiesPeer::ID_COUNTRY, $idCountry['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysCitiesPeer::ID_COUNTRY, $idCountry, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysCitiesQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysCitiesPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysCitiesQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysCitiesPeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the region_code column
     *
     * Example usage:
     * <code>
     * $query->filterByRegionCode('fooValue');   // WHERE region_code = 'fooValue'
     * $query->filterByRegionCode('%fooValue%'); // WHERE region_code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $regionCode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysCitiesQuery The current query, for fluid interface
     */
    public function filterByRegionCode($regionCode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($regionCode)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $regionCode)) {
                $regionCode = str_replace('*', '%', $regionCode);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysCitiesPeer::REGION_CODE, $regionCode, $comparison);
    }

    /**
     * Filter the query on the region_name column
     *
     * Example usage:
     * <code>
     * $query->filterByRegionName('fooValue');   // WHERE region_name = 'fooValue'
     * $query->filterByRegionName('%fooValue%'); // WHERE region_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $regionName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysCitiesQuery The current query, for fluid interface
     */
    public function filterByRegionName($regionName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($regionName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $regionName)) {
                $regionName = str_replace('*', '%', $regionName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysCitiesPeer::REGION_NAME, $regionName, $comparison);
    }

    /**
     * Filter the query on the region_type column
     *
     * Example usage:
     * <code>
     * $query->filterByRegionType('fooValue');   // WHERE region_type = 'fooValue'
     * $query->filterByRegionType('%fooValue%'); // WHERE region_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $regionType The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysCitiesQuery The current query, for fluid interface
     */
    public function filterByRegionType($regionType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($regionType)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $regionType)) {
                $regionType = str_replace('*', '%', $regionType);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysCitiesPeer::REGION_TYPE, $regionType, $comparison);
    }

    /**
     * Filter the query on the coordinates column
     *
     * Example usage:
     * <code>
     * $query->filterByCoordinates('fooValue');   // WHERE coordinates = 'fooValue'
     * $query->filterByCoordinates('%fooValue%'); // WHERE coordinates LIKE '%fooValue%'
     * </code>
     *
     * @param     string $coordinates The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysCitiesQuery The current query, for fluid interface
     */
    public function filterByCoordinates($coordinates = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($coordinates)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $coordinates)) {
                $coordinates = str_replace('*', '%', $coordinates);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysCitiesPeer::COORDINATES, $coordinates, $comparison);
    }

    /**
     * Filter the query on the state column
     *
     * Example usage:
     * <code>
     * $query->filterByState('fooValue');   // WHERE state = 'fooValue'
     * $query->filterByState('%fooValue%'); // WHERE state LIKE '%fooValue%'
     * </code>
     *
     * @param     string $state The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysCitiesQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($state)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $state)) {
                $state = str_replace('*', '%', $state);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysCitiesPeer::STATE, $state, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   SysCities $sysCities Object to remove from the list of results
     *
     * @return SysCitiesQuery The current query, for fluid interface
     */
    public function prune($sysCities = null)
    {
        if ($sysCities) {
            $this->addUsingAlias(SysCitiesPeer::ID_CITY, $sysCities->getIdCity(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

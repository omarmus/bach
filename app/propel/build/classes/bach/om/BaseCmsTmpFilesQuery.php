<?php


/**
 * Base class that represents a query for the 'cms_tmp_files' table.
 *
 *
 *
 * @method CmsTmpFilesQuery orderByIdTmpFile($order = Criteria::ASC) Order by the id_tmp_file column
 * @method CmsTmpFilesQuery orderByIdFile($order = Criteria::ASC) Order by the id_file column
 * @method CmsTmpFilesQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method CmsTmpFilesQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method CmsTmpFilesQuery orderByIdUser($order = Criteria::ASC) Order by the id_user column
 *
 * @method CmsTmpFilesQuery groupByIdTmpFile() Group by the id_tmp_file column
 * @method CmsTmpFilesQuery groupByIdFile() Group by the id_file column
 * @method CmsTmpFilesQuery groupByType() Group by the type column
 * @method CmsTmpFilesQuery groupByDescription() Group by the description column
 * @method CmsTmpFilesQuery groupByIdUser() Group by the id_user column
 *
 * @method CmsTmpFilesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CmsTmpFilesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CmsTmpFilesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CmsTmpFiles findOne(PropelPDO $con = null) Return the first CmsTmpFiles matching the query
 * @method CmsTmpFiles findOneOrCreate(PropelPDO $con = null) Return the first CmsTmpFiles matching the query, or a new CmsTmpFiles object populated from the query conditions when no match is found
 *
 * @method CmsTmpFiles findOneByIdFile(int $id_file) Return the first CmsTmpFiles filtered by the id_file column
 * @method CmsTmpFiles findOneByType(string $type) Return the first CmsTmpFiles filtered by the type column
 * @method CmsTmpFiles findOneByDescription(string $description) Return the first CmsTmpFiles filtered by the description column
 * @method CmsTmpFiles findOneByIdUser(int $id_user) Return the first CmsTmpFiles filtered by the id_user column
 *
 * @method array findByIdTmpFile(int $id_tmp_file) Return CmsTmpFiles objects filtered by the id_tmp_file column
 * @method array findByIdFile(int $id_file) Return CmsTmpFiles objects filtered by the id_file column
 * @method array findByType(string $type) Return CmsTmpFiles objects filtered by the type column
 * @method array findByDescription(string $description) Return CmsTmpFiles objects filtered by the description column
 * @method array findByIdUser(int $id_user) Return CmsTmpFiles objects filtered by the id_user column
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseCmsTmpFilesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCmsTmpFilesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bach';
        }
        if (null === $modelName) {
            $modelName = 'CmsTmpFiles';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CmsTmpFilesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CmsTmpFilesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CmsTmpFilesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CmsTmpFilesQuery) {
            return $criteria;
        }
        $query = new CmsTmpFilesQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CmsTmpFiles|CmsTmpFiles[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CmsTmpFilesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CmsTmpFilesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CmsTmpFiles A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdTmpFile($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CmsTmpFiles A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_tmp_file`, `id_file`, `type`, `description`, `id_user` FROM `cms_tmp_files` WHERE `id_tmp_file` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CmsTmpFiles();
            $obj->hydrate($row);
            CmsTmpFilesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CmsTmpFiles|CmsTmpFiles[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CmsTmpFiles[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CmsTmpFilesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CmsTmpFilesPeer::ID_TMP_FILE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CmsTmpFilesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CmsTmpFilesPeer::ID_TMP_FILE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_tmp_file column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTmpFile(1234); // WHERE id_tmp_file = 1234
     * $query->filterByIdTmpFile(array(12, 34)); // WHERE id_tmp_file IN (12, 34)
     * $query->filterByIdTmpFile(array('min' => 12)); // WHERE id_tmp_file >= 12
     * $query->filterByIdTmpFile(array('max' => 12)); // WHERE id_tmp_file <= 12
     * </code>
     *
     * @param     mixed $idTmpFile The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsTmpFilesQuery The current query, for fluid interface
     */
    public function filterByIdTmpFile($idTmpFile = null, $comparison = null)
    {
        if (is_array($idTmpFile)) {
            $useMinMax = false;
            if (isset($idTmpFile['min'])) {
                $this->addUsingAlias(CmsTmpFilesPeer::ID_TMP_FILE, $idTmpFile['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTmpFile['max'])) {
                $this->addUsingAlias(CmsTmpFilesPeer::ID_TMP_FILE, $idTmpFile['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsTmpFilesPeer::ID_TMP_FILE, $idTmpFile, $comparison);
    }

    /**
     * Filter the query on the id_file column
     *
     * Example usage:
     * <code>
     * $query->filterByIdFile(1234); // WHERE id_file = 1234
     * $query->filterByIdFile(array(12, 34)); // WHERE id_file IN (12, 34)
     * $query->filterByIdFile(array('min' => 12)); // WHERE id_file >= 12
     * $query->filterByIdFile(array('max' => 12)); // WHERE id_file <= 12
     * </code>
     *
     * @param     mixed $idFile The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsTmpFilesQuery The current query, for fluid interface
     */
    public function filterByIdFile($idFile = null, $comparison = null)
    {
        if (is_array($idFile)) {
            $useMinMax = false;
            if (isset($idFile['min'])) {
                $this->addUsingAlias(CmsTmpFilesPeer::ID_FILE, $idFile['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idFile['max'])) {
                $this->addUsingAlias(CmsTmpFilesPeer::ID_FILE, $idFile['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsTmpFilesPeer::ID_FILE, $idFile, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%'); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsTmpFilesQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $type)) {
                $type = str_replace('*', '%', $type);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsTmpFilesPeer::TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsTmpFilesQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsTmpFilesPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the id_user column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUser(1234); // WHERE id_user = 1234
     * $query->filterByIdUser(array(12, 34)); // WHERE id_user IN (12, 34)
     * $query->filterByIdUser(array('min' => 12)); // WHERE id_user >= 12
     * $query->filterByIdUser(array('max' => 12)); // WHERE id_user <= 12
     * </code>
     *
     * @param     mixed $idUser The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsTmpFilesQuery The current query, for fluid interface
     */
    public function filterByIdUser($idUser = null, $comparison = null)
    {
        if (is_array($idUser)) {
            $useMinMax = false;
            if (isset($idUser['min'])) {
                $this->addUsingAlias(CmsTmpFilesPeer::ID_USER, $idUser['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUser['max'])) {
                $this->addUsingAlias(CmsTmpFilesPeer::ID_USER, $idUser['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsTmpFilesPeer::ID_USER, $idUser, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CmsTmpFiles $cmsTmpFiles Object to remove from the list of results
     *
     * @return CmsTmpFilesQuery The current query, for fluid interface
     */
    public function prune($cmsTmpFiles = null)
    {
        if ($cmsTmpFiles) {
            $this->addUsingAlias(CmsTmpFilesPeer::ID_TMP_FILE, $cmsTmpFiles->getIdTmpFile(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

<?php


/**
 * Base class that represents a query for the 'cms_pages' table.
 *
 *
 *
 * @method CmsPagesQuery orderByIdPage($order = Criteria::ASC) Order by the id_page column
 * @method CmsPagesQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method CmsPagesQuery orderBySlug($order = Criteria::ASC) Order by the slug column
 * @method CmsPagesQuery orderByOrder($order = Criteria::ASC) Order by the order column
 * @method CmsPagesQuery orderByIdParent($order = Criteria::ASC) Order by the id_parent column
 * @method CmsPagesQuery orderByState($order = Criteria::ASC) Order by the state column
 * @method CmsPagesQuery orderByTemplate($order = Criteria::ASC) Order by the template column
 * @method CmsPagesQuery orderByBody($order = Criteria::ASC) Order by the body column
 * @method CmsPagesQuery orderByVisible($order = Criteria::ASC) Order by the visible column
 * @method CmsPagesQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method CmsPagesQuery orderByCreated($order = Criteria::ASC) Order by the created column
 * @method CmsPagesQuery orderByModified($order = Criteria::ASC) Order by the modified column
 * @method CmsPagesQuery orderByUserModified($order = Criteria::ASC) Order by the user_modified column
 * @method CmsPagesQuery orderByIdEnterprise($order = Criteria::ASC) Order by the id_enterprise column
 *
 * @method CmsPagesQuery groupByIdPage() Group by the id_page column
 * @method CmsPagesQuery groupByTitle() Group by the title column
 * @method CmsPagesQuery groupBySlug() Group by the slug column
 * @method CmsPagesQuery groupByOrder() Group by the order column
 * @method CmsPagesQuery groupByIdParent() Group by the id_parent column
 * @method CmsPagesQuery groupByState() Group by the state column
 * @method CmsPagesQuery groupByTemplate() Group by the template column
 * @method CmsPagesQuery groupByBody() Group by the body column
 * @method CmsPagesQuery groupByVisible() Group by the visible column
 * @method CmsPagesQuery groupByType() Group by the type column
 * @method CmsPagesQuery groupByCreated() Group by the created column
 * @method CmsPagesQuery groupByModified() Group by the modified column
 * @method CmsPagesQuery groupByUserModified() Group by the user_modified column
 * @method CmsPagesQuery groupByIdEnterprise() Group by the id_enterprise column
 *
 * @method CmsPagesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CmsPagesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CmsPagesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CmsPagesQuery leftJoinSysEnterprises($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysEnterprises relation
 * @method CmsPagesQuery rightJoinSysEnterprises($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysEnterprises relation
 * @method CmsPagesQuery innerJoinSysEnterprises($relationAlias = null) Adds a INNER JOIN clause to the query using the SysEnterprises relation
 *
 * @method CmsPagesQuery leftJoinCmsTemplates($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsTemplates relation
 * @method CmsPagesQuery rightJoinCmsTemplates($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsTemplates relation
 * @method CmsPagesQuery innerJoinCmsTemplates($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsTemplates relation
 *
 * @method CmsPagesQuery leftJoinSysUsers($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysUsers relation
 * @method CmsPagesQuery rightJoinSysUsers($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysUsers relation
 * @method CmsPagesQuery innerJoinSysUsers($relationAlias = null) Adds a INNER JOIN clause to the query using the SysUsers relation
 *
 * @method CmsPagesQuery leftJoinCmsArticles($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsArticles relation
 * @method CmsPagesQuery rightJoinCmsArticles($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsArticles relation
 * @method CmsPagesQuery innerJoinCmsArticles($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsArticles relation
 *
 * @method CmsPagesQuery leftJoinCmsFilesXPage($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsFilesXPage relation
 * @method CmsPagesQuery rightJoinCmsFilesXPage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsFilesXPage relation
 * @method CmsPagesQuery innerJoinCmsFilesXPage($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsFilesXPage relation
 *
 * @method CmsPagesQuery leftJoinCmsVideosXPage($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsVideosXPage relation
 * @method CmsPagesQuery rightJoinCmsVideosXPage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsVideosXPage relation
 * @method CmsPagesQuery innerJoinCmsVideosXPage($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsVideosXPage relation
 *
 * @method CmsPages findOne(PropelPDO $con = null) Return the first CmsPages matching the query
 * @method CmsPages findOneOrCreate(PropelPDO $con = null) Return the first CmsPages matching the query, or a new CmsPages object populated from the query conditions when no match is found
 *
 * @method CmsPages findOneByTitle(string $title) Return the first CmsPages filtered by the title column
 * @method CmsPages findOneBySlug(string $slug) Return the first CmsPages filtered by the slug column
 * @method CmsPages findOneByOrder(int $order) Return the first CmsPages filtered by the order column
 * @method CmsPages findOneByIdParent(int $id_parent) Return the first CmsPages filtered by the id_parent column
 * @method CmsPages findOneByState(string $state) Return the first CmsPages filtered by the state column
 * @method CmsPages findOneByTemplate(string $template) Return the first CmsPages filtered by the template column
 * @method CmsPages findOneByBody(string $body) Return the first CmsPages filtered by the body column
 * @method CmsPages findOneByVisible(string $visible) Return the first CmsPages filtered by the visible column
 * @method CmsPages findOneByType(string $type) Return the first CmsPages filtered by the type column
 * @method CmsPages findOneByCreated(string $created) Return the first CmsPages filtered by the created column
 * @method CmsPages findOneByModified(string $modified) Return the first CmsPages filtered by the modified column
 * @method CmsPages findOneByUserModified(int $user_modified) Return the first CmsPages filtered by the user_modified column
 * @method CmsPages findOneByIdEnterprise(int $id_enterprise) Return the first CmsPages filtered by the id_enterprise column
 *
 * @method array findByIdPage(int $id_page) Return CmsPages objects filtered by the id_page column
 * @method array findByTitle(string $title) Return CmsPages objects filtered by the title column
 * @method array findBySlug(string $slug) Return CmsPages objects filtered by the slug column
 * @method array findByOrder(int $order) Return CmsPages objects filtered by the order column
 * @method array findByIdParent(int $id_parent) Return CmsPages objects filtered by the id_parent column
 * @method array findByState(string $state) Return CmsPages objects filtered by the state column
 * @method array findByTemplate(string $template) Return CmsPages objects filtered by the template column
 * @method array findByBody(string $body) Return CmsPages objects filtered by the body column
 * @method array findByVisible(string $visible) Return CmsPages objects filtered by the visible column
 * @method array findByType(string $type) Return CmsPages objects filtered by the type column
 * @method array findByCreated(string $created) Return CmsPages objects filtered by the created column
 * @method array findByModified(string $modified) Return CmsPages objects filtered by the modified column
 * @method array findByUserModified(int $user_modified) Return CmsPages objects filtered by the user_modified column
 * @method array findByIdEnterprise(int $id_enterprise) Return CmsPages objects filtered by the id_enterprise column
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseCmsPagesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCmsPagesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bach';
        }
        if (null === $modelName) {
            $modelName = 'CmsPages';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CmsPagesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CmsPagesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CmsPagesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CmsPagesQuery) {
            return $criteria;
        }
        $query = new CmsPagesQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CmsPages|CmsPages[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CmsPagesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CmsPages A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdPage($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CmsPages A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_page`, `title`, `slug`, `order`, `id_parent`, `state`, `template`, `body`, `visible`, `type`, `created`, `modified`, `user_modified`, `id_enterprise` FROM `cms_pages` WHERE `id_page` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CmsPages();
            $obj->hydrate($row);
            CmsPagesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CmsPages|CmsPages[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CmsPages[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CmsPagesPeer::ID_PAGE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CmsPagesPeer::ID_PAGE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_page column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPage(1234); // WHERE id_page = 1234
     * $query->filterByIdPage(array(12, 34)); // WHERE id_page IN (12, 34)
     * $query->filterByIdPage(array('min' => 12)); // WHERE id_page >= 12
     * $query->filterByIdPage(array('max' => 12)); // WHERE id_page <= 12
     * </code>
     *
     * @param     mixed $idPage The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function filterByIdPage($idPage = null, $comparison = null)
    {
        if (is_array($idPage)) {
            $useMinMax = false;
            if (isset($idPage['min'])) {
                $this->addUsingAlias(CmsPagesPeer::ID_PAGE, $idPage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPage['max'])) {
                $this->addUsingAlias(CmsPagesPeer::ID_PAGE, $idPage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsPagesPeer::ID_PAGE, $idPage, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsPagesPeer::TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the slug column
     *
     * Example usage:
     * <code>
     * $query->filterBySlug('fooValue');   // WHERE slug = 'fooValue'
     * $query->filterBySlug('%fooValue%'); // WHERE slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slug The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function filterBySlug($slug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slug)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $slug)) {
                $slug = str_replace('*', '%', $slug);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsPagesPeer::SLUG, $slug, $comparison);
    }

    /**
     * Filter the query on the order column
     *
     * Example usage:
     * <code>
     * $query->filterByOrder(1234); // WHERE order = 1234
     * $query->filterByOrder(array(12, 34)); // WHERE order IN (12, 34)
     * $query->filterByOrder(array('min' => 12)); // WHERE order >= 12
     * $query->filterByOrder(array('max' => 12)); // WHERE order <= 12
     * </code>
     *
     * @param     mixed $order The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function filterByOrder($order = null, $comparison = null)
    {
        if (is_array($order)) {
            $useMinMax = false;
            if (isset($order['min'])) {
                $this->addUsingAlias(CmsPagesPeer::ORDER, $order['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($order['max'])) {
                $this->addUsingAlias(CmsPagesPeer::ORDER, $order['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsPagesPeer::ORDER, $order, $comparison);
    }

    /**
     * Filter the query on the id_parent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdParent(1234); // WHERE id_parent = 1234
     * $query->filterByIdParent(array(12, 34)); // WHERE id_parent IN (12, 34)
     * $query->filterByIdParent(array('min' => 12)); // WHERE id_parent >= 12
     * $query->filterByIdParent(array('max' => 12)); // WHERE id_parent <= 12
     * </code>
     *
     * @param     mixed $idParent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function filterByIdParent($idParent = null, $comparison = null)
    {
        if (is_array($idParent)) {
            $useMinMax = false;
            if (isset($idParent['min'])) {
                $this->addUsingAlias(CmsPagesPeer::ID_PARENT, $idParent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idParent['max'])) {
                $this->addUsingAlias(CmsPagesPeer::ID_PARENT, $idParent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsPagesPeer::ID_PARENT, $idParent, $comparison);
    }

    /**
     * Filter the query on the state column
     *
     * Example usage:
     * <code>
     * $query->filterByState('fooValue');   // WHERE state = 'fooValue'
     * $query->filterByState('%fooValue%'); // WHERE state LIKE '%fooValue%'
     * </code>
     *
     * @param     string $state The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($state)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $state)) {
                $state = str_replace('*', '%', $state);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsPagesPeer::STATE, $state, $comparison);
    }

    /**
     * Filter the query on the template column
     *
     * Example usage:
     * <code>
     * $query->filterByTemplate('fooValue');   // WHERE template = 'fooValue'
     * $query->filterByTemplate('%fooValue%'); // WHERE template LIKE '%fooValue%'
     * </code>
     *
     * @param     string $template The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function filterByTemplate($template = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($template)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $template)) {
                $template = str_replace('*', '%', $template);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsPagesPeer::TEMPLATE, $template, $comparison);
    }

    /**
     * Filter the query on the body column
     *
     * Example usage:
     * <code>
     * $query->filterByBody('fooValue');   // WHERE body = 'fooValue'
     * $query->filterByBody('%fooValue%'); // WHERE body LIKE '%fooValue%'
     * </code>
     *
     * @param     string $body The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function filterByBody($body = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($body)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $body)) {
                $body = str_replace('*', '%', $body);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsPagesPeer::BODY, $body, $comparison);
    }

    /**
     * Filter the query on the visible column
     *
     * Example usage:
     * <code>
     * $query->filterByVisible('fooValue');   // WHERE visible = 'fooValue'
     * $query->filterByVisible('%fooValue%'); // WHERE visible LIKE '%fooValue%'
     * </code>
     *
     * @param     string $visible The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function filterByVisible($visible = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($visible)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $visible)) {
                $visible = str_replace('*', '%', $visible);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsPagesPeer::VISIBLE, $visible, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%'); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $type)) {
                $type = str_replace('*', '%', $type);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsPagesPeer::TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the created column
     *
     * Example usage:
     * <code>
     * $query->filterByCreated('2011-03-14'); // WHERE created = '2011-03-14'
     * $query->filterByCreated('now'); // WHERE created = '2011-03-14'
     * $query->filterByCreated(array('max' => 'yesterday')); // WHERE created < '2011-03-13'
     * </code>
     *
     * @param     mixed $created The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function filterByCreated($created = null, $comparison = null)
    {
        if (is_array($created)) {
            $useMinMax = false;
            if (isset($created['min'])) {
                $this->addUsingAlias(CmsPagesPeer::CREATED, $created['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($created['max'])) {
                $this->addUsingAlias(CmsPagesPeer::CREATED, $created['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsPagesPeer::CREATED, $created, $comparison);
    }

    /**
     * Filter the query on the modified column
     *
     * Example usage:
     * <code>
     * $query->filterByModified('2011-03-14'); // WHERE modified = '2011-03-14'
     * $query->filterByModified('now'); // WHERE modified = '2011-03-14'
     * $query->filterByModified(array('max' => 'yesterday')); // WHERE modified < '2011-03-13'
     * </code>
     *
     * @param     mixed $modified The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function filterByModified($modified = null, $comparison = null)
    {
        if (is_array($modified)) {
            $useMinMax = false;
            if (isset($modified['min'])) {
                $this->addUsingAlias(CmsPagesPeer::MODIFIED, $modified['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modified['max'])) {
                $this->addUsingAlias(CmsPagesPeer::MODIFIED, $modified['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsPagesPeer::MODIFIED, $modified, $comparison);
    }

    /**
     * Filter the query on the user_modified column
     *
     * Example usage:
     * <code>
     * $query->filterByUserModified(1234); // WHERE user_modified = 1234
     * $query->filterByUserModified(array(12, 34)); // WHERE user_modified IN (12, 34)
     * $query->filterByUserModified(array('min' => 12)); // WHERE user_modified >= 12
     * $query->filterByUserModified(array('max' => 12)); // WHERE user_modified <= 12
     * </code>
     *
     * @see       filterBySysUsers()
     *
     * @param     mixed $userModified The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function filterByUserModified($userModified = null, $comparison = null)
    {
        if (is_array($userModified)) {
            $useMinMax = false;
            if (isset($userModified['min'])) {
                $this->addUsingAlias(CmsPagesPeer::USER_MODIFIED, $userModified['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userModified['max'])) {
                $this->addUsingAlias(CmsPagesPeer::USER_MODIFIED, $userModified['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsPagesPeer::USER_MODIFIED, $userModified, $comparison);
    }

    /**
     * Filter the query on the id_enterprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEnterprise(1234); // WHERE id_enterprise = 1234
     * $query->filterByIdEnterprise(array(12, 34)); // WHERE id_enterprise IN (12, 34)
     * $query->filterByIdEnterprise(array('min' => 12)); // WHERE id_enterprise >= 12
     * $query->filterByIdEnterprise(array('max' => 12)); // WHERE id_enterprise <= 12
     * </code>
     *
     * @see       filterBySysEnterprises()
     *
     * @param     mixed $idEnterprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function filterByIdEnterprise($idEnterprise = null, $comparison = null)
    {
        if (is_array($idEnterprise)) {
            $useMinMax = false;
            if (isset($idEnterprise['min'])) {
                $this->addUsingAlias(CmsPagesPeer::ID_ENTERPRISE, $idEnterprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEnterprise['max'])) {
                $this->addUsingAlias(CmsPagesPeer::ID_ENTERPRISE, $idEnterprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsPagesPeer::ID_ENTERPRISE, $idEnterprise, $comparison);
    }

    /**
     * Filter the query by a related SysEnterprises object
     *
     * @param   SysEnterprises|PropelObjectCollection $sysEnterprises The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsPagesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysEnterprises($sysEnterprises, $comparison = null)
    {
        if ($sysEnterprises instanceof SysEnterprises) {
            return $this
                ->addUsingAlias(CmsPagesPeer::ID_ENTERPRISE, $sysEnterprises->getIdEnterprise(), $comparison);
        } elseif ($sysEnterprises instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CmsPagesPeer::ID_ENTERPRISE, $sysEnterprises->toKeyValue('PrimaryKey', 'IdEnterprise'), $comparison);
        } else {
            throw new PropelException('filterBySysEnterprises() only accepts arguments of type SysEnterprises or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysEnterprises relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function joinSysEnterprises($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysEnterprises');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysEnterprises');
        }

        return $this;
    }

    /**
     * Use the SysEnterprises relation SysEnterprises object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysEnterprisesQuery A secondary query class using the current class as primary query
     */
    public function useSysEnterprisesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysEnterprises($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysEnterprises', 'SysEnterprisesQuery');
    }

    /**
     * Filter the query by a related CmsTemplates object
     *
     * @param   CmsTemplates|PropelObjectCollection $cmsTemplates The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsPagesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsTemplates($cmsTemplates, $comparison = null)
    {
        if ($cmsTemplates instanceof CmsTemplates) {
            return $this
                ->addUsingAlias(CmsPagesPeer::TEMPLATE, $cmsTemplates->getTemplate(), $comparison);
        } elseif ($cmsTemplates instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CmsPagesPeer::TEMPLATE, $cmsTemplates->toKeyValue('PrimaryKey', 'Template'), $comparison);
        } else {
            throw new PropelException('filterByCmsTemplates() only accepts arguments of type CmsTemplates or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsTemplates relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function joinCmsTemplates($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsTemplates');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsTemplates');
        }

        return $this;
    }

    /**
     * Use the CmsTemplates relation CmsTemplates object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsTemplatesQuery A secondary query class using the current class as primary query
     */
    public function useCmsTemplatesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCmsTemplates($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsTemplates', 'CmsTemplatesQuery');
    }

    /**
     * Filter the query by a related SysUsers object
     *
     * @param   SysUsers|PropelObjectCollection $sysUsers The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsPagesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysUsers($sysUsers, $comparison = null)
    {
        if ($sysUsers instanceof SysUsers) {
            return $this
                ->addUsingAlias(CmsPagesPeer::USER_MODIFIED, $sysUsers->getIdUser(), $comparison);
        } elseif ($sysUsers instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CmsPagesPeer::USER_MODIFIED, $sysUsers->toKeyValue('PrimaryKey', 'IdUser'), $comparison);
        } else {
            throw new PropelException('filterBySysUsers() only accepts arguments of type SysUsers or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysUsers relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function joinSysUsers($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysUsers');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysUsers');
        }

        return $this;
    }

    /**
     * Use the SysUsers relation SysUsers object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysUsersQuery A secondary query class using the current class as primary query
     */
    public function useSysUsersQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysUsers($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysUsers', 'SysUsersQuery');
    }

    /**
     * Filter the query by a related CmsArticles object
     *
     * @param   CmsArticles|PropelObjectCollection $cmsArticles  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsPagesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsArticles($cmsArticles, $comparison = null)
    {
        if ($cmsArticles instanceof CmsArticles) {
            return $this
                ->addUsingAlias(CmsPagesPeer::ID_PAGE, $cmsArticles->getIdPage(), $comparison);
        } elseif ($cmsArticles instanceof PropelObjectCollection) {
            return $this
                ->useCmsArticlesQuery()
                ->filterByPrimaryKeys($cmsArticles->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCmsArticles() only accepts arguments of type CmsArticles or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsArticles relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function joinCmsArticles($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsArticles');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsArticles');
        }

        return $this;
    }

    /**
     * Use the CmsArticles relation CmsArticles object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsArticlesQuery A secondary query class using the current class as primary query
     */
    public function useCmsArticlesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCmsArticles($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsArticles', 'CmsArticlesQuery');
    }

    /**
     * Filter the query by a related CmsFilesXPage object
     *
     * @param   CmsFilesXPage|PropelObjectCollection $cmsFilesXPage  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsPagesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsFilesXPage($cmsFilesXPage, $comparison = null)
    {
        if ($cmsFilesXPage instanceof CmsFilesXPage) {
            return $this
                ->addUsingAlias(CmsPagesPeer::ID_PAGE, $cmsFilesXPage->getIdPage(), $comparison);
        } elseif ($cmsFilesXPage instanceof PropelObjectCollection) {
            return $this
                ->useCmsFilesXPageQuery()
                ->filterByPrimaryKeys($cmsFilesXPage->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCmsFilesXPage() only accepts arguments of type CmsFilesXPage or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsFilesXPage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function joinCmsFilesXPage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsFilesXPage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsFilesXPage');
        }

        return $this;
    }

    /**
     * Use the CmsFilesXPage relation CmsFilesXPage object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsFilesXPageQuery A secondary query class using the current class as primary query
     */
    public function useCmsFilesXPageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCmsFilesXPage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsFilesXPage', 'CmsFilesXPageQuery');
    }

    /**
     * Filter the query by a related CmsVideosXPage object
     *
     * @param   CmsVideosXPage|PropelObjectCollection $cmsVideosXPage  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsPagesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsVideosXPage($cmsVideosXPage, $comparison = null)
    {
        if ($cmsVideosXPage instanceof CmsVideosXPage) {
            return $this
                ->addUsingAlias(CmsPagesPeer::ID_PAGE, $cmsVideosXPage->getIdPage(), $comparison);
        } elseif ($cmsVideosXPage instanceof PropelObjectCollection) {
            return $this
                ->useCmsVideosXPageQuery()
                ->filterByPrimaryKeys($cmsVideosXPage->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCmsVideosXPage() only accepts arguments of type CmsVideosXPage or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsVideosXPage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function joinCmsVideosXPage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsVideosXPage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsVideosXPage');
        }

        return $this;
    }

    /**
     * Use the CmsVideosXPage relation CmsVideosXPage object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsVideosXPageQuery A secondary query class using the current class as primary query
     */
    public function useCmsVideosXPageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCmsVideosXPage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsVideosXPage', 'CmsVideosXPageQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CmsPages $cmsPages Object to remove from the list of results
     *
     * @return CmsPagesQuery The current query, for fluid interface
     */
    public function prune($cmsPages = null)
    {
        if ($cmsPages) {
            $this->addUsingAlias(CmsPagesPeer::ID_PAGE, $cmsPages->getIdPage(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

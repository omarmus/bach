<?php


/**
 * Base class that represents a query for the 'cms_articles' table.
 *
 *
 *
 * @method CmsArticlesQuery orderByIdArticle($order = Criteria::ASC) Order by the id_article column
 * @method CmsArticlesQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method CmsArticlesQuery orderBySlug($order = Criteria::ASC) Order by the slug column
 * @method CmsArticlesQuery orderByPubdate($order = Criteria::ASC) Order by the pubdate column
 * @method CmsArticlesQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method CmsArticlesQuery orderByBody($order = Criteria::ASC) Order by the body column
 * @method CmsArticlesQuery orderByCreated($order = Criteria::ASC) Order by the created column
 * @method CmsArticlesQuery orderByModified($order = Criteria::ASC) Order by the modified column
 * @method CmsArticlesQuery orderByUserModified($order = Criteria::ASC) Order by the user_modified column
 * @method CmsArticlesQuery orderByState($order = Criteria::ASC) Order by the state column
 * @method CmsArticlesQuery orderByIdPage($order = Criteria::ASC) Order by the id_page column
 * @method CmsArticlesQuery orderByIdUser($order = Criteria::ASC) Order by the id_user column
 * @method CmsArticlesQuery orderByNumberVisits($order = Criteria::ASC) Order by the number_visits column
 * @method CmsArticlesQuery orderByYear($order = Criteria::ASC) Order by the year column
 * @method CmsArticlesQuery orderByOthers($order = Criteria::ASC) Order by the others column
 * @method CmsArticlesQuery orderByIdEnterprise($order = Criteria::ASC) Order by the id_enterprise column
 *
 * @method CmsArticlesQuery groupByIdArticle() Group by the id_article column
 * @method CmsArticlesQuery groupByTitle() Group by the title column
 * @method CmsArticlesQuery groupBySlug() Group by the slug column
 * @method CmsArticlesQuery groupByPubdate() Group by the pubdate column
 * @method CmsArticlesQuery groupByDescription() Group by the description column
 * @method CmsArticlesQuery groupByBody() Group by the body column
 * @method CmsArticlesQuery groupByCreated() Group by the created column
 * @method CmsArticlesQuery groupByModified() Group by the modified column
 * @method CmsArticlesQuery groupByUserModified() Group by the user_modified column
 * @method CmsArticlesQuery groupByState() Group by the state column
 * @method CmsArticlesQuery groupByIdPage() Group by the id_page column
 * @method CmsArticlesQuery groupByIdUser() Group by the id_user column
 * @method CmsArticlesQuery groupByNumberVisits() Group by the number_visits column
 * @method CmsArticlesQuery groupByYear() Group by the year column
 * @method CmsArticlesQuery groupByOthers() Group by the others column
 * @method CmsArticlesQuery groupByIdEnterprise() Group by the id_enterprise column
 *
 * @method CmsArticlesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CmsArticlesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CmsArticlesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CmsArticlesQuery leftJoinSysEnterprises($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysEnterprises relation
 * @method CmsArticlesQuery rightJoinSysEnterprises($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysEnterprises relation
 * @method CmsArticlesQuery innerJoinSysEnterprises($relationAlias = null) Adds a INNER JOIN clause to the query using the SysEnterprises relation
 *
 * @method CmsArticlesQuery leftJoinCmsPages($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsPages relation
 * @method CmsArticlesQuery rightJoinCmsPages($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsPages relation
 * @method CmsArticlesQuery innerJoinCmsPages($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsPages relation
 *
 * @method CmsArticlesQuery leftJoinSysUsersRelatedByIdUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysUsersRelatedByIdUser relation
 * @method CmsArticlesQuery rightJoinSysUsersRelatedByIdUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysUsersRelatedByIdUser relation
 * @method CmsArticlesQuery innerJoinSysUsersRelatedByIdUser($relationAlias = null) Adds a INNER JOIN clause to the query using the SysUsersRelatedByIdUser relation
 *
 * @method CmsArticlesQuery leftJoinSysUsersRelatedByUserModified($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysUsersRelatedByUserModified relation
 * @method CmsArticlesQuery rightJoinSysUsersRelatedByUserModified($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysUsersRelatedByUserModified relation
 * @method CmsArticlesQuery innerJoinSysUsersRelatedByUserModified($relationAlias = null) Adds a INNER JOIN clause to the query using the SysUsersRelatedByUserModified relation
 *
 * @method CmsArticlesQuery leftJoinCmsFilesXArticle($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsFilesXArticle relation
 * @method CmsArticlesQuery rightJoinCmsFilesXArticle($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsFilesXArticle relation
 * @method CmsArticlesQuery innerJoinCmsFilesXArticle($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsFilesXArticle relation
 *
 * @method CmsArticlesQuery leftJoinCmsTagsXArticle($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsTagsXArticle relation
 * @method CmsArticlesQuery rightJoinCmsTagsXArticle($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsTagsXArticle relation
 * @method CmsArticlesQuery innerJoinCmsTagsXArticle($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsTagsXArticle relation
 *
 * @method CmsArticlesQuery leftJoinCmsVideosXArticle($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsVideosXArticle relation
 * @method CmsArticlesQuery rightJoinCmsVideosXArticle($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsVideosXArticle relation
 * @method CmsArticlesQuery innerJoinCmsVideosXArticle($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsVideosXArticle relation
 *
 * @method CmsArticles findOne(PropelPDO $con = null) Return the first CmsArticles matching the query
 * @method CmsArticles findOneOrCreate(PropelPDO $con = null) Return the first CmsArticles matching the query, or a new CmsArticles object populated from the query conditions when no match is found
 *
 * @method CmsArticles findOneByTitle(string $title) Return the first CmsArticles filtered by the title column
 * @method CmsArticles findOneBySlug(string $slug) Return the first CmsArticles filtered by the slug column
 * @method CmsArticles findOneByPubdate(string $pubdate) Return the first CmsArticles filtered by the pubdate column
 * @method CmsArticles findOneByDescription(string $description) Return the first CmsArticles filtered by the description column
 * @method CmsArticles findOneByBody(string $body) Return the first CmsArticles filtered by the body column
 * @method CmsArticles findOneByCreated(string $created) Return the first CmsArticles filtered by the created column
 * @method CmsArticles findOneByModified(string $modified) Return the first CmsArticles filtered by the modified column
 * @method CmsArticles findOneByUserModified(int $user_modified) Return the first CmsArticles filtered by the user_modified column
 * @method CmsArticles findOneByState(string $state) Return the first CmsArticles filtered by the state column
 * @method CmsArticles findOneByIdPage(int $id_page) Return the first CmsArticles filtered by the id_page column
 * @method CmsArticles findOneByIdUser(int $id_user) Return the first CmsArticles filtered by the id_user column
 * @method CmsArticles findOneByNumberVisits(int $number_visits) Return the first CmsArticles filtered by the number_visits column
 * @method CmsArticles findOneByYear(int $year) Return the first CmsArticles filtered by the year column
 * @method CmsArticles findOneByOthers(string $others) Return the first CmsArticles filtered by the others column
 * @method CmsArticles findOneByIdEnterprise(int $id_enterprise) Return the first CmsArticles filtered by the id_enterprise column
 *
 * @method array findByIdArticle(int $id_article) Return CmsArticles objects filtered by the id_article column
 * @method array findByTitle(string $title) Return CmsArticles objects filtered by the title column
 * @method array findBySlug(string $slug) Return CmsArticles objects filtered by the slug column
 * @method array findByPubdate(string $pubdate) Return CmsArticles objects filtered by the pubdate column
 * @method array findByDescription(string $description) Return CmsArticles objects filtered by the description column
 * @method array findByBody(string $body) Return CmsArticles objects filtered by the body column
 * @method array findByCreated(string $created) Return CmsArticles objects filtered by the created column
 * @method array findByModified(string $modified) Return CmsArticles objects filtered by the modified column
 * @method array findByUserModified(int $user_modified) Return CmsArticles objects filtered by the user_modified column
 * @method array findByState(string $state) Return CmsArticles objects filtered by the state column
 * @method array findByIdPage(int $id_page) Return CmsArticles objects filtered by the id_page column
 * @method array findByIdUser(int $id_user) Return CmsArticles objects filtered by the id_user column
 * @method array findByNumberVisits(int $number_visits) Return CmsArticles objects filtered by the number_visits column
 * @method array findByYear(int $year) Return CmsArticles objects filtered by the year column
 * @method array findByOthers(string $others) Return CmsArticles objects filtered by the others column
 * @method array findByIdEnterprise(int $id_enterprise) Return CmsArticles objects filtered by the id_enterprise column
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseCmsArticlesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCmsArticlesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bach';
        }
        if (null === $modelName) {
            $modelName = 'CmsArticles';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CmsArticlesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CmsArticlesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CmsArticlesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CmsArticlesQuery) {
            return $criteria;
        }
        $query = new CmsArticlesQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CmsArticles|CmsArticles[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CmsArticlesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CmsArticles A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdArticle($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CmsArticles A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_article`, `title`, `slug`, `pubdate`, `description`, `body`, `created`, `modified`, `user_modified`, `state`, `id_page`, `id_user`, `number_visits`, `year`, `others`, `id_enterprise` FROM `cms_articles` WHERE `id_article` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CmsArticles();
            $obj->hydrate($row);
            CmsArticlesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CmsArticles|CmsArticles[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CmsArticles[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CmsArticlesPeer::ID_ARTICLE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CmsArticlesPeer::ID_ARTICLE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_article column
     *
     * Example usage:
     * <code>
     * $query->filterByIdArticle(1234); // WHERE id_article = 1234
     * $query->filterByIdArticle(array(12, 34)); // WHERE id_article IN (12, 34)
     * $query->filterByIdArticle(array('min' => 12)); // WHERE id_article >= 12
     * $query->filterByIdArticle(array('max' => 12)); // WHERE id_article <= 12
     * </code>
     *
     * @param     mixed $idArticle The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterByIdArticle($idArticle = null, $comparison = null)
    {
        if (is_array($idArticle)) {
            $useMinMax = false;
            if (isset($idArticle['min'])) {
                $this->addUsingAlias(CmsArticlesPeer::ID_ARTICLE, $idArticle['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idArticle['max'])) {
                $this->addUsingAlias(CmsArticlesPeer::ID_ARTICLE, $idArticle['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsArticlesPeer::ID_ARTICLE, $idArticle, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsArticlesPeer::TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the slug column
     *
     * Example usage:
     * <code>
     * $query->filterBySlug('fooValue');   // WHERE slug = 'fooValue'
     * $query->filterBySlug('%fooValue%'); // WHERE slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slug The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterBySlug($slug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slug)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $slug)) {
                $slug = str_replace('*', '%', $slug);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsArticlesPeer::SLUG, $slug, $comparison);
    }

    /**
     * Filter the query on the pubdate column
     *
     * Example usage:
     * <code>
     * $query->filterByPubdate('2011-03-14'); // WHERE pubdate = '2011-03-14'
     * $query->filterByPubdate('now'); // WHERE pubdate = '2011-03-14'
     * $query->filterByPubdate(array('max' => 'yesterday')); // WHERE pubdate < '2011-03-13'
     * </code>
     *
     * @param     mixed $pubdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterByPubdate($pubdate = null, $comparison = null)
    {
        if (is_array($pubdate)) {
            $useMinMax = false;
            if (isset($pubdate['min'])) {
                $this->addUsingAlias(CmsArticlesPeer::PUBDATE, $pubdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pubdate['max'])) {
                $this->addUsingAlias(CmsArticlesPeer::PUBDATE, $pubdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsArticlesPeer::PUBDATE, $pubdate, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsArticlesPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the body column
     *
     * Example usage:
     * <code>
     * $query->filterByBody('fooValue');   // WHERE body = 'fooValue'
     * $query->filterByBody('%fooValue%'); // WHERE body LIKE '%fooValue%'
     * </code>
     *
     * @param     string $body The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterByBody($body = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($body)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $body)) {
                $body = str_replace('*', '%', $body);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsArticlesPeer::BODY, $body, $comparison);
    }

    /**
     * Filter the query on the created column
     *
     * Example usage:
     * <code>
     * $query->filterByCreated('2011-03-14'); // WHERE created = '2011-03-14'
     * $query->filterByCreated('now'); // WHERE created = '2011-03-14'
     * $query->filterByCreated(array('max' => 'yesterday')); // WHERE created < '2011-03-13'
     * </code>
     *
     * @param     mixed $created The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterByCreated($created = null, $comparison = null)
    {
        if (is_array($created)) {
            $useMinMax = false;
            if (isset($created['min'])) {
                $this->addUsingAlias(CmsArticlesPeer::CREATED, $created['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($created['max'])) {
                $this->addUsingAlias(CmsArticlesPeer::CREATED, $created['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsArticlesPeer::CREATED, $created, $comparison);
    }

    /**
     * Filter the query on the modified column
     *
     * Example usage:
     * <code>
     * $query->filterByModified('2011-03-14'); // WHERE modified = '2011-03-14'
     * $query->filterByModified('now'); // WHERE modified = '2011-03-14'
     * $query->filterByModified(array('max' => 'yesterday')); // WHERE modified < '2011-03-13'
     * </code>
     *
     * @param     mixed $modified The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterByModified($modified = null, $comparison = null)
    {
        if (is_array($modified)) {
            $useMinMax = false;
            if (isset($modified['min'])) {
                $this->addUsingAlias(CmsArticlesPeer::MODIFIED, $modified['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modified['max'])) {
                $this->addUsingAlias(CmsArticlesPeer::MODIFIED, $modified['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsArticlesPeer::MODIFIED, $modified, $comparison);
    }

    /**
     * Filter the query on the user_modified column
     *
     * Example usage:
     * <code>
     * $query->filterByUserModified(1234); // WHERE user_modified = 1234
     * $query->filterByUserModified(array(12, 34)); // WHERE user_modified IN (12, 34)
     * $query->filterByUserModified(array('min' => 12)); // WHERE user_modified >= 12
     * $query->filterByUserModified(array('max' => 12)); // WHERE user_modified <= 12
     * </code>
     *
     * @see       filterBySysUsersRelatedByUserModified()
     *
     * @param     mixed $userModified The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterByUserModified($userModified = null, $comparison = null)
    {
        if (is_array($userModified)) {
            $useMinMax = false;
            if (isset($userModified['min'])) {
                $this->addUsingAlias(CmsArticlesPeer::USER_MODIFIED, $userModified['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userModified['max'])) {
                $this->addUsingAlias(CmsArticlesPeer::USER_MODIFIED, $userModified['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsArticlesPeer::USER_MODIFIED, $userModified, $comparison);
    }

    /**
     * Filter the query on the state column
     *
     * Example usage:
     * <code>
     * $query->filterByState('fooValue');   // WHERE state = 'fooValue'
     * $query->filterByState('%fooValue%'); // WHERE state LIKE '%fooValue%'
     * </code>
     *
     * @param     string $state The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($state)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $state)) {
                $state = str_replace('*', '%', $state);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsArticlesPeer::STATE, $state, $comparison);
    }

    /**
     * Filter the query on the id_page column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPage(1234); // WHERE id_page = 1234
     * $query->filterByIdPage(array(12, 34)); // WHERE id_page IN (12, 34)
     * $query->filterByIdPage(array('min' => 12)); // WHERE id_page >= 12
     * $query->filterByIdPage(array('max' => 12)); // WHERE id_page <= 12
     * </code>
     *
     * @see       filterByCmsPages()
     *
     * @param     mixed $idPage The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterByIdPage($idPage = null, $comparison = null)
    {
        if (is_array($idPage)) {
            $useMinMax = false;
            if (isset($idPage['min'])) {
                $this->addUsingAlias(CmsArticlesPeer::ID_PAGE, $idPage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPage['max'])) {
                $this->addUsingAlias(CmsArticlesPeer::ID_PAGE, $idPage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsArticlesPeer::ID_PAGE, $idPage, $comparison);
    }

    /**
     * Filter the query on the id_user column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUser(1234); // WHERE id_user = 1234
     * $query->filterByIdUser(array(12, 34)); // WHERE id_user IN (12, 34)
     * $query->filterByIdUser(array('min' => 12)); // WHERE id_user >= 12
     * $query->filterByIdUser(array('max' => 12)); // WHERE id_user <= 12
     * </code>
     *
     * @see       filterBySysUsersRelatedByIdUser()
     *
     * @param     mixed $idUser The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterByIdUser($idUser = null, $comparison = null)
    {
        if (is_array($idUser)) {
            $useMinMax = false;
            if (isset($idUser['min'])) {
                $this->addUsingAlias(CmsArticlesPeer::ID_USER, $idUser['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUser['max'])) {
                $this->addUsingAlias(CmsArticlesPeer::ID_USER, $idUser['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsArticlesPeer::ID_USER, $idUser, $comparison);
    }

    /**
     * Filter the query on the number_visits column
     *
     * Example usage:
     * <code>
     * $query->filterByNumberVisits(1234); // WHERE number_visits = 1234
     * $query->filterByNumberVisits(array(12, 34)); // WHERE number_visits IN (12, 34)
     * $query->filterByNumberVisits(array('min' => 12)); // WHERE number_visits >= 12
     * $query->filterByNumberVisits(array('max' => 12)); // WHERE number_visits <= 12
     * </code>
     *
     * @param     mixed $numberVisits The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterByNumberVisits($numberVisits = null, $comparison = null)
    {
        if (is_array($numberVisits)) {
            $useMinMax = false;
            if (isset($numberVisits['min'])) {
                $this->addUsingAlias(CmsArticlesPeer::NUMBER_VISITS, $numberVisits['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numberVisits['max'])) {
                $this->addUsingAlias(CmsArticlesPeer::NUMBER_VISITS, $numberVisits['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsArticlesPeer::NUMBER_VISITS, $numberVisits, $comparison);
    }

    /**
     * Filter the query on the year column
     *
     * Example usage:
     * <code>
     * $query->filterByYear(1234); // WHERE year = 1234
     * $query->filterByYear(array(12, 34)); // WHERE year IN (12, 34)
     * $query->filterByYear(array('min' => 12)); // WHERE year >= 12
     * $query->filterByYear(array('max' => 12)); // WHERE year <= 12
     * </code>
     *
     * @param     mixed $year The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterByYear($year = null, $comparison = null)
    {
        if (is_array($year)) {
            $useMinMax = false;
            if (isset($year['min'])) {
                $this->addUsingAlias(CmsArticlesPeer::YEAR, $year['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($year['max'])) {
                $this->addUsingAlias(CmsArticlesPeer::YEAR, $year['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsArticlesPeer::YEAR, $year, $comparison);
    }

    /**
     * Filter the query on the others column
     *
     * Example usage:
     * <code>
     * $query->filterByOthers('fooValue');   // WHERE others = 'fooValue'
     * $query->filterByOthers('%fooValue%'); // WHERE others LIKE '%fooValue%'
     * </code>
     *
     * @param     string $others The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterByOthers($others = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($others)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $others)) {
                $others = str_replace('*', '%', $others);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsArticlesPeer::OTHERS, $others, $comparison);
    }

    /**
     * Filter the query on the id_enterprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEnterprise(1234); // WHERE id_enterprise = 1234
     * $query->filterByIdEnterprise(array(12, 34)); // WHERE id_enterprise IN (12, 34)
     * $query->filterByIdEnterprise(array('min' => 12)); // WHERE id_enterprise >= 12
     * $query->filterByIdEnterprise(array('max' => 12)); // WHERE id_enterprise <= 12
     * </code>
     *
     * @see       filterBySysEnterprises()
     *
     * @param     mixed $idEnterprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function filterByIdEnterprise($idEnterprise = null, $comparison = null)
    {
        if (is_array($idEnterprise)) {
            $useMinMax = false;
            if (isset($idEnterprise['min'])) {
                $this->addUsingAlias(CmsArticlesPeer::ID_ENTERPRISE, $idEnterprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEnterprise['max'])) {
                $this->addUsingAlias(CmsArticlesPeer::ID_ENTERPRISE, $idEnterprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsArticlesPeer::ID_ENTERPRISE, $idEnterprise, $comparison);
    }

    /**
     * Filter the query by a related SysEnterprises object
     *
     * @param   SysEnterprises|PropelObjectCollection $sysEnterprises The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsArticlesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysEnterprises($sysEnterprises, $comparison = null)
    {
        if ($sysEnterprises instanceof SysEnterprises) {
            return $this
                ->addUsingAlias(CmsArticlesPeer::ID_ENTERPRISE, $sysEnterprises->getIdEnterprise(), $comparison);
        } elseif ($sysEnterprises instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CmsArticlesPeer::ID_ENTERPRISE, $sysEnterprises->toKeyValue('PrimaryKey', 'IdEnterprise'), $comparison);
        } else {
            throw new PropelException('filterBySysEnterprises() only accepts arguments of type SysEnterprises or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysEnterprises relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function joinSysEnterprises($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysEnterprises');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysEnterprises');
        }

        return $this;
    }

    /**
     * Use the SysEnterprises relation SysEnterprises object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysEnterprisesQuery A secondary query class using the current class as primary query
     */
    public function useSysEnterprisesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysEnterprises($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysEnterprises', 'SysEnterprisesQuery');
    }

    /**
     * Filter the query by a related CmsPages object
     *
     * @param   CmsPages|PropelObjectCollection $cmsPages The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsArticlesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsPages($cmsPages, $comparison = null)
    {
        if ($cmsPages instanceof CmsPages) {
            return $this
                ->addUsingAlias(CmsArticlesPeer::ID_PAGE, $cmsPages->getIdPage(), $comparison);
        } elseif ($cmsPages instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CmsArticlesPeer::ID_PAGE, $cmsPages->toKeyValue('PrimaryKey', 'IdPage'), $comparison);
        } else {
            throw new PropelException('filterByCmsPages() only accepts arguments of type CmsPages or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsPages relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function joinCmsPages($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsPages');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsPages');
        }

        return $this;
    }

    /**
     * Use the CmsPages relation CmsPages object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsPagesQuery A secondary query class using the current class as primary query
     */
    public function useCmsPagesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCmsPages($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsPages', 'CmsPagesQuery');
    }

    /**
     * Filter the query by a related SysUsers object
     *
     * @param   SysUsers|PropelObjectCollection $sysUsers The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsArticlesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysUsersRelatedByIdUser($sysUsers, $comparison = null)
    {
        if ($sysUsers instanceof SysUsers) {
            return $this
                ->addUsingAlias(CmsArticlesPeer::ID_USER, $sysUsers->getIdUser(), $comparison);
        } elseif ($sysUsers instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CmsArticlesPeer::ID_USER, $sysUsers->toKeyValue('PrimaryKey', 'IdUser'), $comparison);
        } else {
            throw new PropelException('filterBySysUsersRelatedByIdUser() only accepts arguments of type SysUsers or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysUsersRelatedByIdUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function joinSysUsersRelatedByIdUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysUsersRelatedByIdUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysUsersRelatedByIdUser');
        }

        return $this;
    }

    /**
     * Use the SysUsersRelatedByIdUser relation SysUsers object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysUsersQuery A secondary query class using the current class as primary query
     */
    public function useSysUsersRelatedByIdUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysUsersRelatedByIdUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysUsersRelatedByIdUser', 'SysUsersQuery');
    }

    /**
     * Filter the query by a related SysUsers object
     *
     * @param   SysUsers|PropelObjectCollection $sysUsers The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsArticlesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysUsersRelatedByUserModified($sysUsers, $comparison = null)
    {
        if ($sysUsers instanceof SysUsers) {
            return $this
                ->addUsingAlias(CmsArticlesPeer::USER_MODIFIED, $sysUsers->getIdUser(), $comparison);
        } elseif ($sysUsers instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CmsArticlesPeer::USER_MODIFIED, $sysUsers->toKeyValue('PrimaryKey', 'IdUser'), $comparison);
        } else {
            throw new PropelException('filterBySysUsersRelatedByUserModified() only accepts arguments of type SysUsers or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysUsersRelatedByUserModified relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function joinSysUsersRelatedByUserModified($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysUsersRelatedByUserModified');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysUsersRelatedByUserModified');
        }

        return $this;
    }

    /**
     * Use the SysUsersRelatedByUserModified relation SysUsers object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysUsersQuery A secondary query class using the current class as primary query
     */
    public function useSysUsersRelatedByUserModifiedQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysUsersRelatedByUserModified($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysUsersRelatedByUserModified', 'SysUsersQuery');
    }

    /**
     * Filter the query by a related CmsFilesXArticle object
     *
     * @param   CmsFilesXArticle|PropelObjectCollection $cmsFilesXArticle  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsArticlesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsFilesXArticle($cmsFilesXArticle, $comparison = null)
    {
        if ($cmsFilesXArticle instanceof CmsFilesXArticle) {
            return $this
                ->addUsingAlias(CmsArticlesPeer::ID_ARTICLE, $cmsFilesXArticle->getIdArticle(), $comparison);
        } elseif ($cmsFilesXArticle instanceof PropelObjectCollection) {
            return $this
                ->useCmsFilesXArticleQuery()
                ->filterByPrimaryKeys($cmsFilesXArticle->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCmsFilesXArticle() only accepts arguments of type CmsFilesXArticle or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsFilesXArticle relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function joinCmsFilesXArticle($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsFilesXArticle');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsFilesXArticle');
        }

        return $this;
    }

    /**
     * Use the CmsFilesXArticle relation CmsFilesXArticle object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsFilesXArticleQuery A secondary query class using the current class as primary query
     */
    public function useCmsFilesXArticleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCmsFilesXArticle($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsFilesXArticle', 'CmsFilesXArticleQuery');
    }

    /**
     * Filter the query by a related CmsTagsXArticle object
     *
     * @param   CmsTagsXArticle|PropelObjectCollection $cmsTagsXArticle  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsArticlesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsTagsXArticle($cmsTagsXArticle, $comparison = null)
    {
        if ($cmsTagsXArticle instanceof CmsTagsXArticle) {
            return $this
                ->addUsingAlias(CmsArticlesPeer::ID_ARTICLE, $cmsTagsXArticle->getIdArticle(), $comparison);
        } elseif ($cmsTagsXArticle instanceof PropelObjectCollection) {
            return $this
                ->useCmsTagsXArticleQuery()
                ->filterByPrimaryKeys($cmsTagsXArticle->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCmsTagsXArticle() only accepts arguments of type CmsTagsXArticle or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsTagsXArticle relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function joinCmsTagsXArticle($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsTagsXArticle');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsTagsXArticle');
        }

        return $this;
    }

    /**
     * Use the CmsTagsXArticle relation CmsTagsXArticle object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsTagsXArticleQuery A secondary query class using the current class as primary query
     */
    public function useCmsTagsXArticleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCmsTagsXArticle($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsTagsXArticle', 'CmsTagsXArticleQuery');
    }

    /**
     * Filter the query by a related CmsVideosXArticle object
     *
     * @param   CmsVideosXArticle|PropelObjectCollection $cmsVideosXArticle  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsArticlesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsVideosXArticle($cmsVideosXArticle, $comparison = null)
    {
        if ($cmsVideosXArticle instanceof CmsVideosXArticle) {
            return $this
                ->addUsingAlias(CmsArticlesPeer::ID_ARTICLE, $cmsVideosXArticle->getIdArticle(), $comparison);
        } elseif ($cmsVideosXArticle instanceof PropelObjectCollection) {
            return $this
                ->useCmsVideosXArticleQuery()
                ->filterByPrimaryKeys($cmsVideosXArticle->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCmsVideosXArticle() only accepts arguments of type CmsVideosXArticle or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsVideosXArticle relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function joinCmsVideosXArticle($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsVideosXArticle');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsVideosXArticle');
        }

        return $this;
    }

    /**
     * Use the CmsVideosXArticle relation CmsVideosXArticle object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsVideosXArticleQuery A secondary query class using the current class as primary query
     */
    public function useCmsVideosXArticleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCmsVideosXArticle($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsVideosXArticle', 'CmsVideosXArticleQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CmsArticles $cmsArticles Object to remove from the list of results
     *
     * @return CmsArticlesQuery The current query, for fluid interface
     */
    public function prune($cmsArticles = null)
    {
        if ($cmsArticles) {
            $this->addUsingAlias(CmsArticlesPeer::ID_ARTICLE, $cmsArticles->getIdArticle(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

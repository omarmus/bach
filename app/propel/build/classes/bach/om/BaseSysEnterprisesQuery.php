<?php


/**
 * Base class that represents a query for the 'sys_enterprises' table.
 *
 *
 *
 * @method SysEnterprisesQuery orderByIdEnterprise($order = Criteria::ASC) Order by the id_enterprise column
 * @method SysEnterprisesQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method SysEnterprisesQuery orderByNumberRegister($order = Criteria::ASC) Order by the number_register column
 * @method SysEnterprisesQuery orderByAddress($order = Criteria::ASC) Order by the address column
 * @method SysEnterprisesQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method SysEnterprisesQuery orderByFax($order = Criteria::ASC) Order by the fax column
 * @method SysEnterprisesQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method SysEnterprisesQuery orderByWebPage($order = Criteria::ASC) Order by the web_page column
 * @method SysEnterprisesQuery orderByState($order = Criteria::ASC) Order by the state column
 * @method SysEnterprisesQuery orderByCreated($order = Criteria::ASC) Order by the created column
 * @method SysEnterprisesQuery orderByModified($order = Criteria::ASC) Order by the modified column
 * @method SysEnterprisesQuery orderByUserModified($order = Criteria::ASC) Order by the user_modified column
 *
 * @method SysEnterprisesQuery groupByIdEnterprise() Group by the id_enterprise column
 * @method SysEnterprisesQuery groupByName() Group by the name column
 * @method SysEnterprisesQuery groupByNumberRegister() Group by the number_register column
 * @method SysEnterprisesQuery groupByAddress() Group by the address column
 * @method SysEnterprisesQuery groupByPhone() Group by the phone column
 * @method SysEnterprisesQuery groupByFax() Group by the fax column
 * @method SysEnterprisesQuery groupByEmail() Group by the email column
 * @method SysEnterprisesQuery groupByWebPage() Group by the web_page column
 * @method SysEnterprisesQuery groupByState() Group by the state column
 * @method SysEnterprisesQuery groupByCreated() Group by the created column
 * @method SysEnterprisesQuery groupByModified() Group by the modified column
 * @method SysEnterprisesQuery groupByUserModified() Group by the user_modified column
 *
 * @method SysEnterprisesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SysEnterprisesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SysEnterprisesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SysEnterprisesQuery leftJoinSysUsersRelatedByUserModified($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysUsersRelatedByUserModified relation
 * @method SysEnterprisesQuery rightJoinSysUsersRelatedByUserModified($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysUsersRelatedByUserModified relation
 * @method SysEnterprisesQuery innerJoinSysUsersRelatedByUserModified($relationAlias = null) Adds a INNER JOIN clause to the query using the SysUsersRelatedByUserModified relation
 *
 * @method SysEnterprisesQuery leftJoinCmsArticles($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsArticles relation
 * @method SysEnterprisesQuery rightJoinCmsArticles($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsArticles relation
 * @method SysEnterprisesQuery innerJoinCmsArticles($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsArticles relation
 *
 * @method SysEnterprisesQuery leftJoinCmsPages($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsPages relation
 * @method SysEnterprisesQuery rightJoinCmsPages($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsPages relation
 * @method SysEnterprisesQuery innerJoinCmsPages($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsPages relation
 *
 * @method SysEnterprisesQuery leftJoinCmsTags($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsTags relation
 * @method SysEnterprisesQuery rightJoinCmsTags($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsTags relation
 * @method SysEnterprisesQuery innerJoinCmsTags($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsTags relation
 *
 * @method SysEnterprisesQuery leftJoinSmiChurches($relationAlias = null) Adds a LEFT JOIN clause to the query using the SmiChurches relation
 * @method SysEnterprisesQuery rightJoinSmiChurches($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SmiChurches relation
 * @method SysEnterprisesQuery innerJoinSmiChurches($relationAlias = null) Adds a INNER JOIN clause to the query using the SmiChurches relation
 *
 * @method SysEnterprisesQuery leftJoinSysUsersRelatedByIdEnterprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysUsersRelatedByIdEnterprise relation
 * @method SysEnterprisesQuery rightJoinSysUsersRelatedByIdEnterprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysUsersRelatedByIdEnterprise relation
 * @method SysEnterprisesQuery innerJoinSysUsersRelatedByIdEnterprise($relationAlias = null) Adds a INNER JOIN clause to the query using the SysUsersRelatedByIdEnterprise relation
 *
 * @method SysEnterprises findOne(PropelPDO $con = null) Return the first SysEnterprises matching the query
 * @method SysEnterprises findOneOrCreate(PropelPDO $con = null) Return the first SysEnterprises matching the query, or a new SysEnterprises object populated from the query conditions when no match is found
 *
 * @method SysEnterprises findOneByName(string $name) Return the first SysEnterprises filtered by the name column
 * @method SysEnterprises findOneByNumberRegister(string $number_register) Return the first SysEnterprises filtered by the number_register column
 * @method SysEnterprises findOneByAddress(string $address) Return the first SysEnterprises filtered by the address column
 * @method SysEnterprises findOneByPhone(string $phone) Return the first SysEnterprises filtered by the phone column
 * @method SysEnterprises findOneByFax(string $fax) Return the first SysEnterprises filtered by the fax column
 * @method SysEnterprises findOneByEmail(string $email) Return the first SysEnterprises filtered by the email column
 * @method SysEnterprises findOneByWebPage(string $web_page) Return the first SysEnterprises filtered by the web_page column
 * @method SysEnterprises findOneByState(string $state) Return the first SysEnterprises filtered by the state column
 * @method SysEnterprises findOneByCreated(string $created) Return the first SysEnterprises filtered by the created column
 * @method SysEnterprises findOneByModified(string $modified) Return the first SysEnterprises filtered by the modified column
 * @method SysEnterprises findOneByUserModified(int $user_modified) Return the first SysEnterprises filtered by the user_modified column
 *
 * @method array findByIdEnterprise(int $id_enterprise) Return SysEnterprises objects filtered by the id_enterprise column
 * @method array findByName(string $name) Return SysEnterprises objects filtered by the name column
 * @method array findByNumberRegister(string $number_register) Return SysEnterprises objects filtered by the number_register column
 * @method array findByAddress(string $address) Return SysEnterprises objects filtered by the address column
 * @method array findByPhone(string $phone) Return SysEnterprises objects filtered by the phone column
 * @method array findByFax(string $fax) Return SysEnterprises objects filtered by the fax column
 * @method array findByEmail(string $email) Return SysEnterprises objects filtered by the email column
 * @method array findByWebPage(string $web_page) Return SysEnterprises objects filtered by the web_page column
 * @method array findByState(string $state) Return SysEnterprises objects filtered by the state column
 * @method array findByCreated(string $created) Return SysEnterprises objects filtered by the created column
 * @method array findByModified(string $modified) Return SysEnterprises objects filtered by the modified column
 * @method array findByUserModified(int $user_modified) Return SysEnterprises objects filtered by the user_modified column
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseSysEnterprisesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSysEnterprisesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bach';
        }
        if (null === $modelName) {
            $modelName = 'SysEnterprises';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SysEnterprisesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SysEnterprisesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SysEnterprisesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SysEnterprisesQuery) {
            return $criteria;
        }
        $query = new SysEnterprisesQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SysEnterprises|SysEnterprises[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SysEnterprisesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SysEnterprisesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SysEnterprises A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdEnterprise($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SysEnterprises A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_enterprise`, `name`, `number_register`, `address`, `phone`, `fax`, `email`, `web_page`, `state`, `created`, `modified`, `user_modified` FROM `sys_enterprises` WHERE `id_enterprise` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SysEnterprises();
            $obj->hydrate($row);
            SysEnterprisesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SysEnterprises|SysEnterprises[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SysEnterprises[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SysEnterprisesPeer::ID_ENTERPRISE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SysEnterprisesPeer::ID_ENTERPRISE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_enterprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEnterprise(1234); // WHERE id_enterprise = 1234
     * $query->filterByIdEnterprise(array(12, 34)); // WHERE id_enterprise IN (12, 34)
     * $query->filterByIdEnterprise(array('min' => 12)); // WHERE id_enterprise >= 12
     * $query->filterByIdEnterprise(array('max' => 12)); // WHERE id_enterprise <= 12
     * </code>
     *
     * @param     mixed $idEnterprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function filterByIdEnterprise($idEnterprise = null, $comparison = null)
    {
        if (is_array($idEnterprise)) {
            $useMinMax = false;
            if (isset($idEnterprise['min'])) {
                $this->addUsingAlias(SysEnterprisesPeer::ID_ENTERPRISE, $idEnterprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEnterprise['max'])) {
                $this->addUsingAlias(SysEnterprisesPeer::ID_ENTERPRISE, $idEnterprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysEnterprisesPeer::ID_ENTERPRISE, $idEnterprise, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysEnterprisesPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the number_register column
     *
     * Example usage:
     * <code>
     * $query->filterByNumberRegister('fooValue');   // WHERE number_register = 'fooValue'
     * $query->filterByNumberRegister('%fooValue%'); // WHERE number_register LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numberRegister The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function filterByNumberRegister($numberRegister = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numberRegister)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numberRegister)) {
                $numberRegister = str_replace('*', '%', $numberRegister);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysEnterprisesPeer::NUMBER_REGISTER, $numberRegister, $comparison);
    }

    /**
     * Filter the query on the address column
     *
     * Example usage:
     * <code>
     * $query->filterByAddress('fooValue');   // WHERE address = 'fooValue'
     * $query->filterByAddress('%fooValue%'); // WHERE address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $address The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function filterByAddress($address = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($address)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $address)) {
                $address = str_replace('*', '%', $address);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysEnterprisesPeer::ADDRESS, $address, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE phone = 'fooValue'
     * $query->filterByPhone('%fooValue%'); // WHERE phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phone)) {
                $phone = str_replace('*', '%', $phone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysEnterprisesPeer::PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the fax column
     *
     * Example usage:
     * <code>
     * $query->filterByFax('fooValue');   // WHERE fax = 'fooValue'
     * $query->filterByFax('%fooValue%'); // WHERE fax LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fax The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function filterByFax($fax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fax)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $fax)) {
                $fax = str_replace('*', '%', $fax);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysEnterprisesPeer::FAX, $fax, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysEnterprisesPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the web_page column
     *
     * Example usage:
     * <code>
     * $query->filterByWebPage('fooValue');   // WHERE web_page = 'fooValue'
     * $query->filterByWebPage('%fooValue%'); // WHERE web_page LIKE '%fooValue%'
     * </code>
     *
     * @param     string $webPage The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function filterByWebPage($webPage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($webPage)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $webPage)) {
                $webPage = str_replace('*', '%', $webPage);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysEnterprisesPeer::WEB_PAGE, $webPage, $comparison);
    }

    /**
     * Filter the query on the state column
     *
     * Example usage:
     * <code>
     * $query->filterByState('fooValue');   // WHERE state = 'fooValue'
     * $query->filterByState('%fooValue%'); // WHERE state LIKE '%fooValue%'
     * </code>
     *
     * @param     string $state The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($state)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $state)) {
                $state = str_replace('*', '%', $state);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysEnterprisesPeer::STATE, $state, $comparison);
    }

    /**
     * Filter the query on the created column
     *
     * Example usage:
     * <code>
     * $query->filterByCreated('2011-03-14'); // WHERE created = '2011-03-14'
     * $query->filterByCreated('now'); // WHERE created = '2011-03-14'
     * $query->filterByCreated(array('max' => 'yesterday')); // WHERE created < '2011-03-13'
     * </code>
     *
     * @param     mixed $created The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function filterByCreated($created = null, $comparison = null)
    {
        if (is_array($created)) {
            $useMinMax = false;
            if (isset($created['min'])) {
                $this->addUsingAlias(SysEnterprisesPeer::CREATED, $created['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($created['max'])) {
                $this->addUsingAlias(SysEnterprisesPeer::CREATED, $created['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysEnterprisesPeer::CREATED, $created, $comparison);
    }

    /**
     * Filter the query on the modified column
     *
     * Example usage:
     * <code>
     * $query->filterByModified('2011-03-14'); // WHERE modified = '2011-03-14'
     * $query->filterByModified('now'); // WHERE modified = '2011-03-14'
     * $query->filterByModified(array('max' => 'yesterday')); // WHERE modified < '2011-03-13'
     * </code>
     *
     * @param     mixed $modified The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function filterByModified($modified = null, $comparison = null)
    {
        if (is_array($modified)) {
            $useMinMax = false;
            if (isset($modified['min'])) {
                $this->addUsingAlias(SysEnterprisesPeer::MODIFIED, $modified['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modified['max'])) {
                $this->addUsingAlias(SysEnterprisesPeer::MODIFIED, $modified['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysEnterprisesPeer::MODIFIED, $modified, $comparison);
    }

    /**
     * Filter the query on the user_modified column
     *
     * Example usage:
     * <code>
     * $query->filterByUserModified(1234); // WHERE user_modified = 1234
     * $query->filterByUserModified(array(12, 34)); // WHERE user_modified IN (12, 34)
     * $query->filterByUserModified(array('min' => 12)); // WHERE user_modified >= 12
     * $query->filterByUserModified(array('max' => 12)); // WHERE user_modified <= 12
     * </code>
     *
     * @see       filterBySysUsersRelatedByUserModified()
     *
     * @param     mixed $userModified The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function filterByUserModified($userModified = null, $comparison = null)
    {
        if (is_array($userModified)) {
            $useMinMax = false;
            if (isset($userModified['min'])) {
                $this->addUsingAlias(SysEnterprisesPeer::USER_MODIFIED, $userModified['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userModified['max'])) {
                $this->addUsingAlias(SysEnterprisesPeer::USER_MODIFIED, $userModified['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysEnterprisesPeer::USER_MODIFIED, $userModified, $comparison);
    }

    /**
     * Filter the query by a related SysUsers object
     *
     * @param   SysUsers|PropelObjectCollection $sysUsers The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysEnterprisesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysUsersRelatedByUserModified($sysUsers, $comparison = null)
    {
        if ($sysUsers instanceof SysUsers) {
            return $this
                ->addUsingAlias(SysEnterprisesPeer::USER_MODIFIED, $sysUsers->getIdUser(), $comparison);
        } elseif ($sysUsers instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SysEnterprisesPeer::USER_MODIFIED, $sysUsers->toKeyValue('PrimaryKey', 'IdUser'), $comparison);
        } else {
            throw new PropelException('filterBySysUsersRelatedByUserModified() only accepts arguments of type SysUsers or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysUsersRelatedByUserModified relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function joinSysUsersRelatedByUserModified($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysUsersRelatedByUserModified');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysUsersRelatedByUserModified');
        }

        return $this;
    }

    /**
     * Use the SysUsersRelatedByUserModified relation SysUsers object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysUsersQuery A secondary query class using the current class as primary query
     */
    public function useSysUsersRelatedByUserModifiedQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysUsersRelatedByUserModified($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysUsersRelatedByUserModified', 'SysUsersQuery');
    }

    /**
     * Filter the query by a related CmsArticles object
     *
     * @param   CmsArticles|PropelObjectCollection $cmsArticles  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysEnterprisesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsArticles($cmsArticles, $comparison = null)
    {
        if ($cmsArticles instanceof CmsArticles) {
            return $this
                ->addUsingAlias(SysEnterprisesPeer::ID_ENTERPRISE, $cmsArticles->getIdEnterprise(), $comparison);
        } elseif ($cmsArticles instanceof PropelObjectCollection) {
            return $this
                ->useCmsArticlesQuery()
                ->filterByPrimaryKeys($cmsArticles->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCmsArticles() only accepts arguments of type CmsArticles or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsArticles relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function joinCmsArticles($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsArticles');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsArticles');
        }

        return $this;
    }

    /**
     * Use the CmsArticles relation CmsArticles object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsArticlesQuery A secondary query class using the current class as primary query
     */
    public function useCmsArticlesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCmsArticles($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsArticles', 'CmsArticlesQuery');
    }

    /**
     * Filter the query by a related CmsPages object
     *
     * @param   CmsPages|PropelObjectCollection $cmsPages  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysEnterprisesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsPages($cmsPages, $comparison = null)
    {
        if ($cmsPages instanceof CmsPages) {
            return $this
                ->addUsingAlias(SysEnterprisesPeer::ID_ENTERPRISE, $cmsPages->getIdEnterprise(), $comparison);
        } elseif ($cmsPages instanceof PropelObjectCollection) {
            return $this
                ->useCmsPagesQuery()
                ->filterByPrimaryKeys($cmsPages->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCmsPages() only accepts arguments of type CmsPages or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsPages relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function joinCmsPages($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsPages');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsPages');
        }

        return $this;
    }

    /**
     * Use the CmsPages relation CmsPages object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsPagesQuery A secondary query class using the current class as primary query
     */
    public function useCmsPagesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCmsPages($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsPages', 'CmsPagesQuery');
    }

    /**
     * Filter the query by a related CmsTags object
     *
     * @param   CmsTags|PropelObjectCollection $cmsTags  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysEnterprisesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsTags($cmsTags, $comparison = null)
    {
        if ($cmsTags instanceof CmsTags) {
            return $this
                ->addUsingAlias(SysEnterprisesPeer::ID_ENTERPRISE, $cmsTags->getIdEnterprise(), $comparison);
        } elseif ($cmsTags instanceof PropelObjectCollection) {
            return $this
                ->useCmsTagsQuery()
                ->filterByPrimaryKeys($cmsTags->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCmsTags() only accepts arguments of type CmsTags or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsTags relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function joinCmsTags($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsTags');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsTags');
        }

        return $this;
    }

    /**
     * Use the CmsTags relation CmsTags object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsTagsQuery A secondary query class using the current class as primary query
     */
    public function useCmsTagsQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCmsTags($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsTags', 'CmsTagsQuery');
    }

    /**
     * Filter the query by a related SmiChurches object
     *
     * @param   SmiChurches|PropelObjectCollection $smiChurches  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysEnterprisesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySmiChurches($smiChurches, $comparison = null)
    {
        if ($smiChurches instanceof SmiChurches) {
            return $this
                ->addUsingAlias(SysEnterprisesPeer::ID_ENTERPRISE, $smiChurches->getIdEnterprise(), $comparison);
        } elseif ($smiChurches instanceof PropelObjectCollection) {
            return $this
                ->useSmiChurchesQuery()
                ->filterByPrimaryKeys($smiChurches->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySmiChurches() only accepts arguments of type SmiChurches or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SmiChurches relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function joinSmiChurches($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SmiChurches');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SmiChurches');
        }

        return $this;
    }

    /**
     * Use the SmiChurches relation SmiChurches object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SmiChurchesQuery A secondary query class using the current class as primary query
     */
    public function useSmiChurchesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSmiChurches($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SmiChurches', 'SmiChurchesQuery');
    }

    /**
     * Filter the query by a related SysUsers object
     *
     * @param   SysUsers|PropelObjectCollection $sysUsers  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysEnterprisesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysUsersRelatedByIdEnterprise($sysUsers, $comparison = null)
    {
        if ($sysUsers instanceof SysUsers) {
            return $this
                ->addUsingAlias(SysEnterprisesPeer::ID_ENTERPRISE, $sysUsers->getIdEnterprise(), $comparison);
        } elseif ($sysUsers instanceof PropelObjectCollection) {
            return $this
                ->useSysUsersRelatedByIdEnterpriseQuery()
                ->filterByPrimaryKeys($sysUsers->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySysUsersRelatedByIdEnterprise() only accepts arguments of type SysUsers or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysUsersRelatedByIdEnterprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function joinSysUsersRelatedByIdEnterprise($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysUsersRelatedByIdEnterprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysUsersRelatedByIdEnterprise');
        }

        return $this;
    }

    /**
     * Use the SysUsersRelatedByIdEnterprise relation SysUsers object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysUsersQuery A secondary query class using the current class as primary query
     */
    public function useSysUsersRelatedByIdEnterpriseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysUsersRelatedByIdEnterprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysUsersRelatedByIdEnterprise', 'SysUsersQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SysEnterprises $sysEnterprises Object to remove from the list of results
     *
     * @return SysEnterprisesQuery The current query, for fluid interface
     */
    public function prune($sysEnterprises = null)
    {
        if ($sysEnterprises) {
            $this->addUsingAlias(SysEnterprisesPeer::ID_ENTERPRISE, $sysEnterprises->getIdEnterprise(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

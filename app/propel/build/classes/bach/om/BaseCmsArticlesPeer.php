<?php


/**
 * Base static class for performing query and update operations on the 'cms_articles' table.
 *
 *
 *
 * @package propel.generator.bach.om
 */
abstract class BaseCmsArticlesPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'bach';

    /** the table name for this class */
    const TABLE_NAME = 'cms_articles';

    /** the related Propel class for this table */
    const OM_CLASS = 'CmsArticles';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CmsArticlesTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 16;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 16;

    /** the column name for the id_article field */
    const ID_ARTICLE = 'cms_articles.id_article';

    /** the column name for the title field */
    const TITLE = 'cms_articles.title';

    /** the column name for the slug field */
    const SLUG = 'cms_articles.slug';

    /** the column name for the pubdate field */
    const PUBDATE = 'cms_articles.pubdate';

    /** the column name for the description field */
    const DESCRIPTION = 'cms_articles.description';

    /** the column name for the body field */
    const BODY = 'cms_articles.body';

    /** the column name for the created field */
    const CREATED = 'cms_articles.created';

    /** the column name for the modified field */
    const MODIFIED = 'cms_articles.modified';

    /** the column name for the user_modified field */
    const USER_MODIFIED = 'cms_articles.user_modified';

    /** the column name for the state field */
    const STATE = 'cms_articles.state';

    /** the column name for the id_page field */
    const ID_PAGE = 'cms_articles.id_page';

    /** the column name for the id_user field */
    const ID_USER = 'cms_articles.id_user';

    /** the column name for the number_visits field */
    const NUMBER_VISITS = 'cms_articles.number_visits';

    /** the column name for the year field */
    const YEAR = 'cms_articles.year';

    /** the column name for the others field */
    const OTHERS = 'cms_articles.others';

    /** the column name for the id_enterprise field */
    const ID_ENTERPRISE = 'cms_articles.id_enterprise';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CmsArticles objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CmsArticles[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CmsArticlesPeer::$fieldNames[CmsArticlesPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('IdArticle', 'Title', 'Slug', 'Pubdate', 'Description', 'Body', 'Created', 'Modified', 'UserModified', 'State', 'IdPage', 'IdUser', 'NumberVisits', 'Year', 'Others', 'IdEnterprise', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idArticle', 'title', 'slug', 'pubdate', 'description', 'body', 'created', 'modified', 'userModified', 'state', 'idPage', 'idUser', 'numberVisits', 'year', 'others', 'idEnterprise', ),
        BasePeer::TYPE_COLNAME => array (CmsArticlesPeer::ID_ARTICLE, CmsArticlesPeer::TITLE, CmsArticlesPeer::SLUG, CmsArticlesPeer::PUBDATE, CmsArticlesPeer::DESCRIPTION, CmsArticlesPeer::BODY, CmsArticlesPeer::CREATED, CmsArticlesPeer::MODIFIED, CmsArticlesPeer::USER_MODIFIED, CmsArticlesPeer::STATE, CmsArticlesPeer::ID_PAGE, CmsArticlesPeer::ID_USER, CmsArticlesPeer::NUMBER_VISITS, CmsArticlesPeer::YEAR, CmsArticlesPeer::OTHERS, CmsArticlesPeer::ID_ENTERPRISE, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_ARTICLE', 'TITLE', 'SLUG', 'PUBDATE', 'DESCRIPTION', 'BODY', 'CREATED', 'MODIFIED', 'USER_MODIFIED', 'STATE', 'ID_PAGE', 'ID_USER', 'NUMBER_VISITS', 'YEAR', 'OTHERS', 'ID_ENTERPRISE', ),
        BasePeer::TYPE_FIELDNAME => array ('id_article', 'title', 'slug', 'pubdate', 'description', 'body', 'created', 'modified', 'user_modified', 'state', 'id_page', 'id_user', 'number_visits', 'year', 'others', 'id_enterprise', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CmsArticlesPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('IdArticle' => 0, 'Title' => 1, 'Slug' => 2, 'Pubdate' => 3, 'Description' => 4, 'Body' => 5, 'Created' => 6, 'Modified' => 7, 'UserModified' => 8, 'State' => 9, 'IdPage' => 10, 'IdUser' => 11, 'NumberVisits' => 12, 'Year' => 13, 'Others' => 14, 'IdEnterprise' => 15, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idArticle' => 0, 'title' => 1, 'slug' => 2, 'pubdate' => 3, 'description' => 4, 'body' => 5, 'created' => 6, 'modified' => 7, 'userModified' => 8, 'state' => 9, 'idPage' => 10, 'idUser' => 11, 'numberVisits' => 12, 'year' => 13, 'others' => 14, 'idEnterprise' => 15, ),
        BasePeer::TYPE_COLNAME => array (CmsArticlesPeer::ID_ARTICLE => 0, CmsArticlesPeer::TITLE => 1, CmsArticlesPeer::SLUG => 2, CmsArticlesPeer::PUBDATE => 3, CmsArticlesPeer::DESCRIPTION => 4, CmsArticlesPeer::BODY => 5, CmsArticlesPeer::CREATED => 6, CmsArticlesPeer::MODIFIED => 7, CmsArticlesPeer::USER_MODIFIED => 8, CmsArticlesPeer::STATE => 9, CmsArticlesPeer::ID_PAGE => 10, CmsArticlesPeer::ID_USER => 11, CmsArticlesPeer::NUMBER_VISITS => 12, CmsArticlesPeer::YEAR => 13, CmsArticlesPeer::OTHERS => 14, CmsArticlesPeer::ID_ENTERPRISE => 15, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_ARTICLE' => 0, 'TITLE' => 1, 'SLUG' => 2, 'PUBDATE' => 3, 'DESCRIPTION' => 4, 'BODY' => 5, 'CREATED' => 6, 'MODIFIED' => 7, 'USER_MODIFIED' => 8, 'STATE' => 9, 'ID_PAGE' => 10, 'ID_USER' => 11, 'NUMBER_VISITS' => 12, 'YEAR' => 13, 'OTHERS' => 14, 'ID_ENTERPRISE' => 15, ),
        BasePeer::TYPE_FIELDNAME => array ('id_article' => 0, 'title' => 1, 'slug' => 2, 'pubdate' => 3, 'description' => 4, 'body' => 5, 'created' => 6, 'modified' => 7, 'user_modified' => 8, 'state' => 9, 'id_page' => 10, 'id_user' => 11, 'number_visits' => 12, 'year' => 13, 'others' => 14, 'id_enterprise' => 15, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CmsArticlesPeer::getFieldNames($toType);
        $key = isset(CmsArticlesPeer::$fieldKeys[$fromType][$name]) ? CmsArticlesPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CmsArticlesPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CmsArticlesPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CmsArticlesPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CmsArticlesPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CmsArticlesPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CmsArticlesPeer::ID_ARTICLE);
            $criteria->addSelectColumn(CmsArticlesPeer::TITLE);
            $criteria->addSelectColumn(CmsArticlesPeer::SLUG);
            $criteria->addSelectColumn(CmsArticlesPeer::PUBDATE);
            $criteria->addSelectColumn(CmsArticlesPeer::DESCRIPTION);
            $criteria->addSelectColumn(CmsArticlesPeer::BODY);
            $criteria->addSelectColumn(CmsArticlesPeer::CREATED);
            $criteria->addSelectColumn(CmsArticlesPeer::MODIFIED);
            $criteria->addSelectColumn(CmsArticlesPeer::USER_MODIFIED);
            $criteria->addSelectColumn(CmsArticlesPeer::STATE);
            $criteria->addSelectColumn(CmsArticlesPeer::ID_PAGE);
            $criteria->addSelectColumn(CmsArticlesPeer::ID_USER);
            $criteria->addSelectColumn(CmsArticlesPeer::NUMBER_VISITS);
            $criteria->addSelectColumn(CmsArticlesPeer::YEAR);
            $criteria->addSelectColumn(CmsArticlesPeer::OTHERS);
            $criteria->addSelectColumn(CmsArticlesPeer::ID_ENTERPRISE);
        } else {
            $criteria->addSelectColumn($alias . '.id_article');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.slug');
            $criteria->addSelectColumn($alias . '.pubdate');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.body');
            $criteria->addSelectColumn($alias . '.created');
            $criteria->addSelectColumn($alias . '.modified');
            $criteria->addSelectColumn($alias . '.user_modified');
            $criteria->addSelectColumn($alias . '.state');
            $criteria->addSelectColumn($alias . '.id_page');
            $criteria->addSelectColumn($alias . '.id_user');
            $criteria->addSelectColumn($alias . '.number_visits');
            $criteria->addSelectColumn($alias . '.year');
            $criteria->addSelectColumn($alias . '.others');
            $criteria->addSelectColumn($alias . '.id_enterprise');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsArticlesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsArticlesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return CmsArticles
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CmsArticlesPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CmsArticlesPeer::populateObjects(CmsArticlesPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CmsArticlesPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param CmsArticles $obj A CmsArticles object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getIdArticle();
            } // if key === null
            CmsArticlesPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CmsArticles object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CmsArticles) {
                $key = (string) $value->getIdArticle();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CmsArticles object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CmsArticlesPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return CmsArticles Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CmsArticlesPeer::$instances[$key])) {
                return CmsArticlesPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CmsArticlesPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CmsArticlesPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to cms_articles
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CmsArticlesPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CmsArticlesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CmsArticlesPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CmsArticlesPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CmsArticles object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CmsArticlesPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CmsArticlesPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CmsArticlesPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CmsArticlesPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CmsArticlesPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related SysEnterprises table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSysEnterprises(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsArticlesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsArticlesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CmsArticlesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CmsPages table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCmsPages(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsArticlesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsArticlesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CmsArticlesPeer::ID_PAGE, CmsPagesPeer::ID_PAGE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SysUsersRelatedByIdUser table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSysUsersRelatedByIdUser(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsArticlesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsArticlesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CmsArticlesPeer::ID_USER, SysUsersPeer::ID_USER, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SysUsersRelatedByUserModified table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSysUsersRelatedByUserModified(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsArticlesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsArticlesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CmsArticlesPeer::USER_MODIFIED, SysUsersPeer::ID_USER, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CmsArticles objects pre-filled with their SysEnterprises objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CmsArticles objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSysEnterprises(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);
        }

        CmsArticlesPeer::addSelectColumns($criteria);
        $startcol = CmsArticlesPeer::NUM_HYDRATE_COLUMNS;
        SysEnterprisesPeer::addSelectColumns($criteria);

        $criteria->addJoin(CmsArticlesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CmsArticlesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CmsArticlesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CmsArticlesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CmsArticlesPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SysEnterprisesPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SysEnterprisesPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SysEnterprisesPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SysEnterprisesPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CmsArticles) to $obj2 (SysEnterprises)
                $obj2->addCmsArticles($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CmsArticles objects pre-filled with their CmsPages objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CmsArticles objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCmsPages(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);
        }

        CmsArticlesPeer::addSelectColumns($criteria);
        $startcol = CmsArticlesPeer::NUM_HYDRATE_COLUMNS;
        CmsPagesPeer::addSelectColumns($criteria);

        $criteria->addJoin(CmsArticlesPeer::ID_PAGE, CmsPagesPeer::ID_PAGE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CmsArticlesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CmsArticlesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CmsArticlesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CmsArticlesPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CmsPagesPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CmsPagesPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CmsPagesPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CmsPagesPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CmsArticles) to $obj2 (CmsPages)
                $obj2->addCmsArticles($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CmsArticles objects pre-filled with their SysUsers objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CmsArticles objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSysUsersRelatedByIdUser(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);
        }

        CmsArticlesPeer::addSelectColumns($criteria);
        $startcol = CmsArticlesPeer::NUM_HYDRATE_COLUMNS;
        SysUsersPeer::addSelectColumns($criteria);

        $criteria->addJoin(CmsArticlesPeer::ID_USER, SysUsersPeer::ID_USER, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CmsArticlesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CmsArticlesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CmsArticlesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CmsArticlesPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SysUsersPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SysUsersPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SysUsersPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SysUsersPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CmsArticles) to $obj2 (SysUsers)
                $obj2->addCmsArticlesRelatedByIdUser($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CmsArticles objects pre-filled with their SysUsers objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CmsArticles objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSysUsersRelatedByUserModified(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);
        }

        CmsArticlesPeer::addSelectColumns($criteria);
        $startcol = CmsArticlesPeer::NUM_HYDRATE_COLUMNS;
        SysUsersPeer::addSelectColumns($criteria);

        $criteria->addJoin(CmsArticlesPeer::USER_MODIFIED, SysUsersPeer::ID_USER, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CmsArticlesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CmsArticlesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CmsArticlesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CmsArticlesPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SysUsersPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SysUsersPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SysUsersPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SysUsersPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CmsArticles) to $obj2 (SysUsers)
                $obj2->addCmsArticlesRelatedByUserModified($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsArticlesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsArticlesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CmsArticlesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::ID_PAGE, CmsPagesPeer::ID_PAGE, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::ID_USER, SysUsersPeer::ID_USER, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::USER_MODIFIED, SysUsersPeer::ID_USER, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CmsArticles objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CmsArticles objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);
        }

        CmsArticlesPeer::addSelectColumns($criteria);
        $startcol2 = CmsArticlesPeer::NUM_HYDRATE_COLUMNS;

        SysEnterprisesPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SysEnterprisesPeer::NUM_HYDRATE_COLUMNS;

        CmsPagesPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CmsPagesPeer::NUM_HYDRATE_COLUMNS;

        SysUsersPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SysUsersPeer::NUM_HYDRATE_COLUMNS;

        SysUsersPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + SysUsersPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CmsArticlesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::ID_PAGE, CmsPagesPeer::ID_PAGE, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::ID_USER, SysUsersPeer::ID_USER, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::USER_MODIFIED, SysUsersPeer::ID_USER, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CmsArticlesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CmsArticlesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CmsArticlesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CmsArticlesPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined SysEnterprises rows

            $key2 = SysEnterprisesPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = SysEnterprisesPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SysEnterprisesPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SysEnterprisesPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CmsArticles) to the collection in $obj2 (SysEnterprises)
                $obj2->addCmsArticles($obj1);
            } // if joined row not null

            // Add objects for joined CmsPages rows

            $key3 = CmsPagesPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CmsPagesPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = CmsPagesPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CmsPagesPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CmsArticles) to the collection in $obj3 (CmsPages)
                $obj3->addCmsArticles($obj1);
            } // if joined row not null

            // Add objects for joined SysUsers rows

            $key4 = SysUsersPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = SysUsersPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = SysUsersPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SysUsersPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (CmsArticles) to the collection in $obj4 (SysUsers)
                $obj4->addCmsArticlesRelatedByIdUser($obj1);
            } // if joined row not null

            // Add objects for joined SysUsers rows

            $key5 = SysUsersPeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = SysUsersPeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = SysUsersPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    SysUsersPeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (CmsArticles) to the collection in $obj5 (SysUsers)
                $obj5->addCmsArticlesRelatedByUserModified($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SysEnterprises table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSysEnterprises(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsArticlesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsArticlesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CmsArticlesPeer::ID_PAGE, CmsPagesPeer::ID_PAGE, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::ID_USER, SysUsersPeer::ID_USER, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::USER_MODIFIED, SysUsersPeer::ID_USER, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CmsPages table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCmsPages(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsArticlesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsArticlesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CmsArticlesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::ID_USER, SysUsersPeer::ID_USER, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::USER_MODIFIED, SysUsersPeer::ID_USER, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SysUsersRelatedByIdUser table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSysUsersRelatedByIdUser(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsArticlesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsArticlesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CmsArticlesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::ID_PAGE, CmsPagesPeer::ID_PAGE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SysUsersRelatedByUserModified table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSysUsersRelatedByUserModified(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsArticlesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsArticlesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CmsArticlesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::ID_PAGE, CmsPagesPeer::ID_PAGE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CmsArticles objects pre-filled with all related objects except SysEnterprises.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CmsArticles objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSysEnterprises(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);
        }

        CmsArticlesPeer::addSelectColumns($criteria);
        $startcol2 = CmsArticlesPeer::NUM_HYDRATE_COLUMNS;

        CmsPagesPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CmsPagesPeer::NUM_HYDRATE_COLUMNS;

        SysUsersPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SysUsersPeer::NUM_HYDRATE_COLUMNS;

        SysUsersPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SysUsersPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CmsArticlesPeer::ID_PAGE, CmsPagesPeer::ID_PAGE, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::ID_USER, SysUsersPeer::ID_USER, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::USER_MODIFIED, SysUsersPeer::ID_USER, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CmsArticlesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CmsArticlesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CmsArticlesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CmsArticlesPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CmsPages rows

                $key2 = CmsPagesPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CmsPagesPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CmsPagesPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CmsPagesPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CmsArticles) to the collection in $obj2 (CmsPages)
                $obj2->addCmsArticles($obj1);

            } // if joined row is not null

                // Add objects for joined SysUsers rows

                $key3 = SysUsersPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SysUsersPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = SysUsersPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SysUsersPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CmsArticles) to the collection in $obj3 (SysUsers)
                $obj3->addCmsArticlesRelatedByIdUser($obj1);

            } // if joined row is not null

                // Add objects for joined SysUsers rows

                $key4 = SysUsersPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = SysUsersPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = SysUsersPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SysUsersPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CmsArticles) to the collection in $obj4 (SysUsers)
                $obj4->addCmsArticlesRelatedByUserModified($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CmsArticles objects pre-filled with all related objects except CmsPages.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CmsArticles objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCmsPages(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);
        }

        CmsArticlesPeer::addSelectColumns($criteria);
        $startcol2 = CmsArticlesPeer::NUM_HYDRATE_COLUMNS;

        SysEnterprisesPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SysEnterprisesPeer::NUM_HYDRATE_COLUMNS;

        SysUsersPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SysUsersPeer::NUM_HYDRATE_COLUMNS;

        SysUsersPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SysUsersPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CmsArticlesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::ID_USER, SysUsersPeer::ID_USER, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::USER_MODIFIED, SysUsersPeer::ID_USER, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CmsArticlesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CmsArticlesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CmsArticlesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CmsArticlesPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined SysEnterprises rows

                $key2 = SysEnterprisesPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SysEnterprisesPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = SysEnterprisesPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SysEnterprisesPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CmsArticles) to the collection in $obj2 (SysEnterprises)
                $obj2->addCmsArticles($obj1);

            } // if joined row is not null

                // Add objects for joined SysUsers rows

                $key3 = SysUsersPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SysUsersPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = SysUsersPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SysUsersPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CmsArticles) to the collection in $obj3 (SysUsers)
                $obj3->addCmsArticlesRelatedByIdUser($obj1);

            } // if joined row is not null

                // Add objects for joined SysUsers rows

                $key4 = SysUsersPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = SysUsersPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = SysUsersPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SysUsersPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CmsArticles) to the collection in $obj4 (SysUsers)
                $obj4->addCmsArticlesRelatedByUserModified($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CmsArticles objects pre-filled with all related objects except SysUsersRelatedByIdUser.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CmsArticles objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSysUsersRelatedByIdUser(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);
        }

        CmsArticlesPeer::addSelectColumns($criteria);
        $startcol2 = CmsArticlesPeer::NUM_HYDRATE_COLUMNS;

        SysEnterprisesPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SysEnterprisesPeer::NUM_HYDRATE_COLUMNS;

        CmsPagesPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CmsPagesPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CmsArticlesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::ID_PAGE, CmsPagesPeer::ID_PAGE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CmsArticlesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CmsArticlesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CmsArticlesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CmsArticlesPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined SysEnterprises rows

                $key2 = SysEnterprisesPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SysEnterprisesPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = SysEnterprisesPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SysEnterprisesPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CmsArticles) to the collection in $obj2 (SysEnterprises)
                $obj2->addCmsArticles($obj1);

            } // if joined row is not null

                // Add objects for joined CmsPages rows

                $key3 = CmsPagesPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CmsPagesPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CmsPagesPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CmsPagesPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CmsArticles) to the collection in $obj3 (CmsPages)
                $obj3->addCmsArticles($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CmsArticles objects pre-filled with all related objects except SysUsersRelatedByUserModified.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CmsArticles objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSysUsersRelatedByUserModified(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);
        }

        CmsArticlesPeer::addSelectColumns($criteria);
        $startcol2 = CmsArticlesPeer::NUM_HYDRATE_COLUMNS;

        SysEnterprisesPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SysEnterprisesPeer::NUM_HYDRATE_COLUMNS;

        CmsPagesPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CmsPagesPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CmsArticlesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $criteria->addJoin(CmsArticlesPeer::ID_PAGE, CmsPagesPeer::ID_PAGE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CmsArticlesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CmsArticlesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CmsArticlesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CmsArticlesPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined SysEnterprises rows

                $key2 = SysEnterprisesPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SysEnterprisesPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = SysEnterprisesPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SysEnterprisesPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CmsArticles) to the collection in $obj2 (SysEnterprises)
                $obj2->addCmsArticles($obj1);

            } // if joined row is not null

                // Add objects for joined CmsPages rows

                $key3 = CmsPagesPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CmsPagesPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CmsPagesPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CmsPagesPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CmsArticles) to the collection in $obj3 (CmsPages)
                $obj3->addCmsArticles($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CmsArticlesPeer::DATABASE_NAME)->getTable(CmsArticlesPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCmsArticlesPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCmsArticlesPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new \CmsArticlesTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CmsArticlesPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CmsArticles or Criteria object.
     *
     * @param      mixed $values Criteria or CmsArticles object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CmsArticles object
        }

        if ($criteria->containsKey(CmsArticlesPeer::ID_ARTICLE) && $criteria->keyContainsValue(CmsArticlesPeer::ID_ARTICLE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CmsArticlesPeer::ID_ARTICLE.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CmsArticles or Criteria object.
     *
     * @param      mixed $values Criteria or CmsArticles object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CmsArticlesPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CmsArticlesPeer::ID_ARTICLE);
            $value = $criteria->remove(CmsArticlesPeer::ID_ARTICLE);
            if ($value) {
                $selectCriteria->add(CmsArticlesPeer::ID_ARTICLE, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CmsArticlesPeer::TABLE_NAME);
            }

        } else { // $values is CmsArticles object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the cms_articles table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CmsArticlesPeer::TABLE_NAME, $con, CmsArticlesPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CmsArticlesPeer::clearInstancePool();
            CmsArticlesPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CmsArticles or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CmsArticles object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CmsArticlesPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CmsArticles) { // it's a model object
            // invalidate the cache for this single object
            CmsArticlesPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CmsArticlesPeer::DATABASE_NAME);
            $criteria->add(CmsArticlesPeer::ID_ARTICLE, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CmsArticlesPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CmsArticlesPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CmsArticlesPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CmsArticles object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param CmsArticles $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CmsArticlesPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CmsArticlesPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CmsArticlesPeer::DATABASE_NAME, CmsArticlesPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CmsArticles
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CmsArticlesPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CmsArticlesPeer::DATABASE_NAME);
        $criteria->add(CmsArticlesPeer::ID_ARTICLE, $pk);

        $v = CmsArticlesPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CmsArticles[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CmsArticlesPeer::DATABASE_NAME);
            $criteria->add(CmsArticlesPeer::ID_ARTICLE, $pks, Criteria::IN);
            $objs = CmsArticlesPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCmsArticlesPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCmsArticlesPeer::buildTableMap();


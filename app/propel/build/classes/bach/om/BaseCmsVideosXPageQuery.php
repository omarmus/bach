<?php


/**
 * Base class that represents a query for the 'cms_videos_x_page' table.
 *
 *
 *
 * @method CmsVideosXPageQuery orderByIdVideo($order = Criteria::ASC) Order by the id_video column
 * @method CmsVideosXPageQuery orderByIdPage($order = Criteria::ASC) Order by the id_page column
 * @method CmsVideosXPageQuery orderByUrl($order = Criteria::ASC) Order by the url column
 * @method CmsVideosXPageQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method CmsVideosXPageQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method CmsVideosXPageQuery orderByPrimary($order = Criteria::ASC) Order by the primary column
 * @method CmsVideosXPageQuery orderByState($order = Criteria::ASC) Order by the state column
 * @method CmsVideosXPageQuery orderByCreated($order = Criteria::ASC) Order by the created column
 * @method CmsVideosXPageQuery orderByModified($order = Criteria::ASC) Order by the modified column
 * @method CmsVideosXPageQuery orderByUserModified($order = Criteria::ASC) Order by the user_modified column
 *
 * @method CmsVideosXPageQuery groupByIdVideo() Group by the id_video column
 * @method CmsVideosXPageQuery groupByIdPage() Group by the id_page column
 * @method CmsVideosXPageQuery groupByUrl() Group by the url column
 * @method CmsVideosXPageQuery groupByType() Group by the type column
 * @method CmsVideosXPageQuery groupByDescription() Group by the description column
 * @method CmsVideosXPageQuery groupByPrimary() Group by the primary column
 * @method CmsVideosXPageQuery groupByState() Group by the state column
 * @method CmsVideosXPageQuery groupByCreated() Group by the created column
 * @method CmsVideosXPageQuery groupByModified() Group by the modified column
 * @method CmsVideosXPageQuery groupByUserModified() Group by the user_modified column
 *
 * @method CmsVideosXPageQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CmsVideosXPageQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CmsVideosXPageQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CmsVideosXPageQuery leftJoinCmsPages($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsPages relation
 * @method CmsVideosXPageQuery rightJoinCmsPages($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsPages relation
 * @method CmsVideosXPageQuery innerJoinCmsPages($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsPages relation
 *
 * @method CmsVideosXPageQuery leftJoinSysUsers($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysUsers relation
 * @method CmsVideosXPageQuery rightJoinSysUsers($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysUsers relation
 * @method CmsVideosXPageQuery innerJoinSysUsers($relationAlias = null) Adds a INNER JOIN clause to the query using the SysUsers relation
 *
 * @method CmsVideosXPage findOne(PropelPDO $con = null) Return the first CmsVideosXPage matching the query
 * @method CmsVideosXPage findOneOrCreate(PropelPDO $con = null) Return the first CmsVideosXPage matching the query, or a new CmsVideosXPage object populated from the query conditions when no match is found
 *
 * @method CmsVideosXPage findOneByIdPage(int $id_page) Return the first CmsVideosXPage filtered by the id_page column
 * @method CmsVideosXPage findOneByUrl(string $url) Return the first CmsVideosXPage filtered by the url column
 * @method CmsVideosXPage findOneByType(string $type) Return the first CmsVideosXPage filtered by the type column
 * @method CmsVideosXPage findOneByDescription(string $description) Return the first CmsVideosXPage filtered by the description column
 * @method CmsVideosXPage findOneByPrimary(string $primary) Return the first CmsVideosXPage filtered by the primary column
 * @method CmsVideosXPage findOneByState(string $state) Return the first CmsVideosXPage filtered by the state column
 * @method CmsVideosXPage findOneByCreated(string $created) Return the first CmsVideosXPage filtered by the created column
 * @method CmsVideosXPage findOneByModified(string $modified) Return the first CmsVideosXPage filtered by the modified column
 * @method CmsVideosXPage findOneByUserModified(int $user_modified) Return the first CmsVideosXPage filtered by the user_modified column
 *
 * @method array findByIdVideo(int $id_video) Return CmsVideosXPage objects filtered by the id_video column
 * @method array findByIdPage(int $id_page) Return CmsVideosXPage objects filtered by the id_page column
 * @method array findByUrl(string $url) Return CmsVideosXPage objects filtered by the url column
 * @method array findByType(string $type) Return CmsVideosXPage objects filtered by the type column
 * @method array findByDescription(string $description) Return CmsVideosXPage objects filtered by the description column
 * @method array findByPrimary(string $primary) Return CmsVideosXPage objects filtered by the primary column
 * @method array findByState(string $state) Return CmsVideosXPage objects filtered by the state column
 * @method array findByCreated(string $created) Return CmsVideosXPage objects filtered by the created column
 * @method array findByModified(string $modified) Return CmsVideosXPage objects filtered by the modified column
 * @method array findByUserModified(int $user_modified) Return CmsVideosXPage objects filtered by the user_modified column
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseCmsVideosXPageQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCmsVideosXPageQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bach';
        }
        if (null === $modelName) {
            $modelName = 'CmsVideosXPage';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CmsVideosXPageQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CmsVideosXPageQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CmsVideosXPageQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CmsVideosXPageQuery) {
            return $criteria;
        }
        $query = new CmsVideosXPageQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CmsVideosXPage|CmsVideosXPage[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CmsVideosXPagePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CmsVideosXPagePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CmsVideosXPage A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdVideo($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CmsVideosXPage A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_video`, `id_page`, `url`, `type`, `description`, `primary`, `state`, `created`, `modified`, `user_modified` FROM `cms_videos_x_page` WHERE `id_video` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CmsVideosXPage();
            $obj->hydrate($row);
            CmsVideosXPagePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CmsVideosXPage|CmsVideosXPage[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CmsVideosXPage[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CmsVideosXPageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CmsVideosXPagePeer::ID_VIDEO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CmsVideosXPageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CmsVideosXPagePeer::ID_VIDEO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_video column
     *
     * Example usage:
     * <code>
     * $query->filterByIdVideo(1234); // WHERE id_video = 1234
     * $query->filterByIdVideo(array(12, 34)); // WHERE id_video IN (12, 34)
     * $query->filterByIdVideo(array('min' => 12)); // WHERE id_video >= 12
     * $query->filterByIdVideo(array('max' => 12)); // WHERE id_video <= 12
     * </code>
     *
     * @param     mixed $idVideo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsVideosXPageQuery The current query, for fluid interface
     */
    public function filterByIdVideo($idVideo = null, $comparison = null)
    {
        if (is_array($idVideo)) {
            $useMinMax = false;
            if (isset($idVideo['min'])) {
                $this->addUsingAlias(CmsVideosXPagePeer::ID_VIDEO, $idVideo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idVideo['max'])) {
                $this->addUsingAlias(CmsVideosXPagePeer::ID_VIDEO, $idVideo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsVideosXPagePeer::ID_VIDEO, $idVideo, $comparison);
    }

    /**
     * Filter the query on the id_page column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPage(1234); // WHERE id_page = 1234
     * $query->filterByIdPage(array(12, 34)); // WHERE id_page IN (12, 34)
     * $query->filterByIdPage(array('min' => 12)); // WHERE id_page >= 12
     * $query->filterByIdPage(array('max' => 12)); // WHERE id_page <= 12
     * </code>
     *
     * @see       filterByCmsPages()
     *
     * @param     mixed $idPage The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsVideosXPageQuery The current query, for fluid interface
     */
    public function filterByIdPage($idPage = null, $comparison = null)
    {
        if (is_array($idPage)) {
            $useMinMax = false;
            if (isset($idPage['min'])) {
                $this->addUsingAlias(CmsVideosXPagePeer::ID_PAGE, $idPage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPage['max'])) {
                $this->addUsingAlias(CmsVideosXPagePeer::ID_PAGE, $idPage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsVideosXPagePeer::ID_PAGE, $idPage, $comparison);
    }

    /**
     * Filter the query on the url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE url = 'fooValue'
     * $query->filterByUrl('%fooValue%'); // WHERE url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsVideosXPageQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $url)) {
                $url = str_replace('*', '%', $url);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsVideosXPagePeer::URL, $url, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%'); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsVideosXPageQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $type)) {
                $type = str_replace('*', '%', $type);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsVideosXPagePeer::TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsVideosXPageQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsVideosXPagePeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the primary column
     *
     * Example usage:
     * <code>
     * $query->filterByPrimary('fooValue');   // WHERE primary = 'fooValue'
     * $query->filterByPrimary('%fooValue%'); // WHERE primary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $primary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsVideosXPageQuery The current query, for fluid interface
     */
    public function filterByPrimary($primary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($primary)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $primary)) {
                $primary = str_replace('*', '%', $primary);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsVideosXPagePeer::PRIMARY, $primary, $comparison);
    }

    /**
     * Filter the query on the state column
     *
     * Example usage:
     * <code>
     * $query->filterByState('fooValue');   // WHERE state = 'fooValue'
     * $query->filterByState('%fooValue%'); // WHERE state LIKE '%fooValue%'
     * </code>
     *
     * @param     string $state The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsVideosXPageQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($state)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $state)) {
                $state = str_replace('*', '%', $state);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsVideosXPagePeer::STATE, $state, $comparison);
    }

    /**
     * Filter the query on the created column
     *
     * Example usage:
     * <code>
     * $query->filterByCreated('2011-03-14'); // WHERE created = '2011-03-14'
     * $query->filterByCreated('now'); // WHERE created = '2011-03-14'
     * $query->filterByCreated(array('max' => 'yesterday')); // WHERE created < '2011-03-13'
     * </code>
     *
     * @param     mixed $created The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsVideosXPageQuery The current query, for fluid interface
     */
    public function filterByCreated($created = null, $comparison = null)
    {
        if (is_array($created)) {
            $useMinMax = false;
            if (isset($created['min'])) {
                $this->addUsingAlias(CmsVideosXPagePeer::CREATED, $created['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($created['max'])) {
                $this->addUsingAlias(CmsVideosXPagePeer::CREATED, $created['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsVideosXPagePeer::CREATED, $created, $comparison);
    }

    /**
     * Filter the query on the modified column
     *
     * Example usage:
     * <code>
     * $query->filterByModified('2011-03-14'); // WHERE modified = '2011-03-14'
     * $query->filterByModified('now'); // WHERE modified = '2011-03-14'
     * $query->filterByModified(array('max' => 'yesterday')); // WHERE modified < '2011-03-13'
     * </code>
     *
     * @param     mixed $modified The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsVideosXPageQuery The current query, for fluid interface
     */
    public function filterByModified($modified = null, $comparison = null)
    {
        if (is_array($modified)) {
            $useMinMax = false;
            if (isset($modified['min'])) {
                $this->addUsingAlias(CmsVideosXPagePeer::MODIFIED, $modified['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modified['max'])) {
                $this->addUsingAlias(CmsVideosXPagePeer::MODIFIED, $modified['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsVideosXPagePeer::MODIFIED, $modified, $comparison);
    }

    /**
     * Filter the query on the user_modified column
     *
     * Example usage:
     * <code>
     * $query->filterByUserModified(1234); // WHERE user_modified = 1234
     * $query->filterByUserModified(array(12, 34)); // WHERE user_modified IN (12, 34)
     * $query->filterByUserModified(array('min' => 12)); // WHERE user_modified >= 12
     * $query->filterByUserModified(array('max' => 12)); // WHERE user_modified <= 12
     * </code>
     *
     * @see       filterBySysUsers()
     *
     * @param     mixed $userModified The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsVideosXPageQuery The current query, for fluid interface
     */
    public function filterByUserModified($userModified = null, $comparison = null)
    {
        if (is_array($userModified)) {
            $useMinMax = false;
            if (isset($userModified['min'])) {
                $this->addUsingAlias(CmsVideosXPagePeer::USER_MODIFIED, $userModified['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userModified['max'])) {
                $this->addUsingAlias(CmsVideosXPagePeer::USER_MODIFIED, $userModified['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsVideosXPagePeer::USER_MODIFIED, $userModified, $comparison);
    }

    /**
     * Filter the query by a related CmsPages object
     *
     * @param   CmsPages|PropelObjectCollection $cmsPages The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsVideosXPageQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsPages($cmsPages, $comparison = null)
    {
        if ($cmsPages instanceof CmsPages) {
            return $this
                ->addUsingAlias(CmsVideosXPagePeer::ID_PAGE, $cmsPages->getIdPage(), $comparison);
        } elseif ($cmsPages instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CmsVideosXPagePeer::ID_PAGE, $cmsPages->toKeyValue('PrimaryKey', 'IdPage'), $comparison);
        } else {
            throw new PropelException('filterByCmsPages() only accepts arguments of type CmsPages or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsPages relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsVideosXPageQuery The current query, for fluid interface
     */
    public function joinCmsPages($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsPages');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsPages');
        }

        return $this;
    }

    /**
     * Use the CmsPages relation CmsPages object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsPagesQuery A secondary query class using the current class as primary query
     */
    public function useCmsPagesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCmsPages($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsPages', 'CmsPagesQuery');
    }

    /**
     * Filter the query by a related SysUsers object
     *
     * @param   SysUsers|PropelObjectCollection $sysUsers The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsVideosXPageQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysUsers($sysUsers, $comparison = null)
    {
        if ($sysUsers instanceof SysUsers) {
            return $this
                ->addUsingAlias(CmsVideosXPagePeer::USER_MODIFIED, $sysUsers->getIdUser(), $comparison);
        } elseif ($sysUsers instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CmsVideosXPagePeer::USER_MODIFIED, $sysUsers->toKeyValue('PrimaryKey', 'IdUser'), $comparison);
        } else {
            throw new PropelException('filterBySysUsers() only accepts arguments of type SysUsers or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysUsers relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsVideosXPageQuery The current query, for fluid interface
     */
    public function joinSysUsers($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysUsers');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysUsers');
        }

        return $this;
    }

    /**
     * Use the SysUsers relation SysUsers object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysUsersQuery A secondary query class using the current class as primary query
     */
    public function useSysUsersQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysUsers($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysUsers', 'SysUsersQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CmsVideosXPage $cmsVideosXPage Object to remove from the list of results
     *
     * @return CmsVideosXPageQuery The current query, for fluid interface
     */
    public function prune($cmsVideosXPage = null)
    {
        if ($cmsVideosXPage) {
            $this->addUsingAlias(CmsVideosXPagePeer::ID_VIDEO, $cmsVideosXPage->getIdVideo(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

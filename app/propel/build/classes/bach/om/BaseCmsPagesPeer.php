<?php


/**
 * Base static class for performing query and update operations on the 'cms_pages' table.
 *
 *
 *
 * @package propel.generator.bach.om
 */
abstract class BaseCmsPagesPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'bach';

    /** the table name for this class */
    const TABLE_NAME = 'cms_pages';

    /** the related Propel class for this table */
    const OM_CLASS = 'CmsPages';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CmsPagesTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 14;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 14;

    /** the column name for the id_page field */
    const ID_PAGE = 'cms_pages.id_page';

    /** the column name for the title field */
    const TITLE = 'cms_pages.title';

    /** the column name for the slug field */
    const SLUG = 'cms_pages.slug';

    /** the column name for the order field */
    const ORDER = 'cms_pages.order';

    /** the column name for the id_parent field */
    const ID_PARENT = 'cms_pages.id_parent';

    /** the column name for the state field */
    const STATE = 'cms_pages.state';

    /** the column name for the template field */
    const TEMPLATE = 'cms_pages.template';

    /** the column name for the body field */
    const BODY = 'cms_pages.body';

    /** the column name for the visible field */
    const VISIBLE = 'cms_pages.visible';

    /** the column name for the type field */
    const TYPE = 'cms_pages.type';

    /** the column name for the created field */
    const CREATED = 'cms_pages.created';

    /** the column name for the modified field */
    const MODIFIED = 'cms_pages.modified';

    /** the column name for the user_modified field */
    const USER_MODIFIED = 'cms_pages.user_modified';

    /** the column name for the id_enterprise field */
    const ID_ENTERPRISE = 'cms_pages.id_enterprise';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CmsPages objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CmsPages[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CmsPagesPeer::$fieldNames[CmsPagesPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('IdPage', 'Title', 'Slug', 'Order', 'IdParent', 'State', 'Template', 'Body', 'Visible', 'Type', 'Created', 'Modified', 'UserModified', 'IdEnterprise', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idPage', 'title', 'slug', 'order', 'idParent', 'state', 'template', 'body', 'visible', 'type', 'created', 'modified', 'userModified', 'idEnterprise', ),
        BasePeer::TYPE_COLNAME => array (CmsPagesPeer::ID_PAGE, CmsPagesPeer::TITLE, CmsPagesPeer::SLUG, CmsPagesPeer::ORDER, CmsPagesPeer::ID_PARENT, CmsPagesPeer::STATE, CmsPagesPeer::TEMPLATE, CmsPagesPeer::BODY, CmsPagesPeer::VISIBLE, CmsPagesPeer::TYPE, CmsPagesPeer::CREATED, CmsPagesPeer::MODIFIED, CmsPagesPeer::USER_MODIFIED, CmsPagesPeer::ID_ENTERPRISE, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_PAGE', 'TITLE', 'SLUG', 'ORDER', 'ID_PARENT', 'STATE', 'TEMPLATE', 'BODY', 'VISIBLE', 'TYPE', 'CREATED', 'MODIFIED', 'USER_MODIFIED', 'ID_ENTERPRISE', ),
        BasePeer::TYPE_FIELDNAME => array ('id_page', 'title', 'slug', 'order', 'id_parent', 'state', 'template', 'body', 'visible', 'type', 'created', 'modified', 'user_modified', 'id_enterprise', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CmsPagesPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('IdPage' => 0, 'Title' => 1, 'Slug' => 2, 'Order' => 3, 'IdParent' => 4, 'State' => 5, 'Template' => 6, 'Body' => 7, 'Visible' => 8, 'Type' => 9, 'Created' => 10, 'Modified' => 11, 'UserModified' => 12, 'IdEnterprise' => 13, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idPage' => 0, 'title' => 1, 'slug' => 2, 'order' => 3, 'idParent' => 4, 'state' => 5, 'template' => 6, 'body' => 7, 'visible' => 8, 'type' => 9, 'created' => 10, 'modified' => 11, 'userModified' => 12, 'idEnterprise' => 13, ),
        BasePeer::TYPE_COLNAME => array (CmsPagesPeer::ID_PAGE => 0, CmsPagesPeer::TITLE => 1, CmsPagesPeer::SLUG => 2, CmsPagesPeer::ORDER => 3, CmsPagesPeer::ID_PARENT => 4, CmsPagesPeer::STATE => 5, CmsPagesPeer::TEMPLATE => 6, CmsPagesPeer::BODY => 7, CmsPagesPeer::VISIBLE => 8, CmsPagesPeer::TYPE => 9, CmsPagesPeer::CREATED => 10, CmsPagesPeer::MODIFIED => 11, CmsPagesPeer::USER_MODIFIED => 12, CmsPagesPeer::ID_ENTERPRISE => 13, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_PAGE' => 0, 'TITLE' => 1, 'SLUG' => 2, 'ORDER' => 3, 'ID_PARENT' => 4, 'STATE' => 5, 'TEMPLATE' => 6, 'BODY' => 7, 'VISIBLE' => 8, 'TYPE' => 9, 'CREATED' => 10, 'MODIFIED' => 11, 'USER_MODIFIED' => 12, 'ID_ENTERPRISE' => 13, ),
        BasePeer::TYPE_FIELDNAME => array ('id_page' => 0, 'title' => 1, 'slug' => 2, 'order' => 3, 'id_parent' => 4, 'state' => 5, 'template' => 6, 'body' => 7, 'visible' => 8, 'type' => 9, 'created' => 10, 'modified' => 11, 'user_modified' => 12, 'id_enterprise' => 13, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CmsPagesPeer::getFieldNames($toType);
        $key = isset(CmsPagesPeer::$fieldKeys[$fromType][$name]) ? CmsPagesPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CmsPagesPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CmsPagesPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CmsPagesPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CmsPagesPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CmsPagesPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CmsPagesPeer::ID_PAGE);
            $criteria->addSelectColumn(CmsPagesPeer::TITLE);
            $criteria->addSelectColumn(CmsPagesPeer::SLUG);
            $criteria->addSelectColumn(CmsPagesPeer::ORDER);
            $criteria->addSelectColumn(CmsPagesPeer::ID_PARENT);
            $criteria->addSelectColumn(CmsPagesPeer::STATE);
            $criteria->addSelectColumn(CmsPagesPeer::TEMPLATE);
            $criteria->addSelectColumn(CmsPagesPeer::BODY);
            $criteria->addSelectColumn(CmsPagesPeer::VISIBLE);
            $criteria->addSelectColumn(CmsPagesPeer::TYPE);
            $criteria->addSelectColumn(CmsPagesPeer::CREATED);
            $criteria->addSelectColumn(CmsPagesPeer::MODIFIED);
            $criteria->addSelectColumn(CmsPagesPeer::USER_MODIFIED);
            $criteria->addSelectColumn(CmsPagesPeer::ID_ENTERPRISE);
        } else {
            $criteria->addSelectColumn($alias . '.id_page');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.slug');
            $criteria->addSelectColumn($alias . '.order');
            $criteria->addSelectColumn($alias . '.id_parent');
            $criteria->addSelectColumn($alias . '.state');
            $criteria->addSelectColumn($alias . '.template');
            $criteria->addSelectColumn($alias . '.body');
            $criteria->addSelectColumn($alias . '.visible');
            $criteria->addSelectColumn($alias . '.type');
            $criteria->addSelectColumn($alias . '.created');
            $criteria->addSelectColumn($alias . '.modified');
            $criteria->addSelectColumn($alias . '.user_modified');
            $criteria->addSelectColumn($alias . '.id_enterprise');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsPagesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsPagesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CmsPagesPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return CmsPages
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CmsPagesPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CmsPagesPeer::populateObjects(CmsPagesPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CmsPagesPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param CmsPages $obj A CmsPages object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getIdPage();
            } // if key === null
            CmsPagesPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CmsPages object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CmsPages) {
                $key = (string) $value->getIdPage();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CmsPages object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CmsPagesPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return CmsPages Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CmsPagesPeer::$instances[$key])) {
                return CmsPagesPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CmsPagesPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CmsPagesPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to cms_pages
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CmsPagesPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CmsPagesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CmsPagesPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CmsPagesPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CmsPages object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CmsPagesPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CmsPagesPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CmsPagesPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CmsPagesPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CmsPagesPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related SysEnterprises table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSysEnterprises(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsPagesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsPagesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CmsPagesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CmsTemplates table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCmsTemplates(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsPagesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsPagesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CmsPagesPeer::TEMPLATE, CmsTemplatesPeer::TEMPLATE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SysUsers table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSysUsers(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsPagesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsPagesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CmsPagesPeer::USER_MODIFIED, SysUsersPeer::ID_USER, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CmsPages objects pre-filled with their SysEnterprises objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CmsPages objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSysEnterprises(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);
        }

        CmsPagesPeer::addSelectColumns($criteria);
        $startcol = CmsPagesPeer::NUM_HYDRATE_COLUMNS;
        SysEnterprisesPeer::addSelectColumns($criteria);

        $criteria->addJoin(CmsPagesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CmsPagesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CmsPagesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CmsPagesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CmsPagesPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SysEnterprisesPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SysEnterprisesPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SysEnterprisesPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SysEnterprisesPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CmsPages) to $obj2 (SysEnterprises)
                $obj2->addCmsPages($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CmsPages objects pre-filled with their CmsTemplates objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CmsPages objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCmsTemplates(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);
        }

        CmsPagesPeer::addSelectColumns($criteria);
        $startcol = CmsPagesPeer::NUM_HYDRATE_COLUMNS;
        CmsTemplatesPeer::addSelectColumns($criteria);

        $criteria->addJoin(CmsPagesPeer::TEMPLATE, CmsTemplatesPeer::TEMPLATE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CmsPagesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CmsPagesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CmsPagesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CmsPagesPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CmsTemplatesPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CmsTemplatesPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CmsTemplatesPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CmsTemplatesPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CmsPages) to $obj2 (CmsTemplates)
                $obj2->addCmsPages($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CmsPages objects pre-filled with their SysUsers objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CmsPages objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSysUsers(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);
        }

        CmsPagesPeer::addSelectColumns($criteria);
        $startcol = CmsPagesPeer::NUM_HYDRATE_COLUMNS;
        SysUsersPeer::addSelectColumns($criteria);

        $criteria->addJoin(CmsPagesPeer::USER_MODIFIED, SysUsersPeer::ID_USER, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CmsPagesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CmsPagesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CmsPagesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CmsPagesPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SysUsersPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SysUsersPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SysUsersPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SysUsersPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CmsPages) to $obj2 (SysUsers)
                $obj2->addCmsPages($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsPagesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsPagesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CmsPagesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $criteria->addJoin(CmsPagesPeer::TEMPLATE, CmsTemplatesPeer::TEMPLATE, $join_behavior);

        $criteria->addJoin(CmsPagesPeer::USER_MODIFIED, SysUsersPeer::ID_USER, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CmsPages objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CmsPages objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);
        }

        CmsPagesPeer::addSelectColumns($criteria);
        $startcol2 = CmsPagesPeer::NUM_HYDRATE_COLUMNS;

        SysEnterprisesPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SysEnterprisesPeer::NUM_HYDRATE_COLUMNS;

        CmsTemplatesPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CmsTemplatesPeer::NUM_HYDRATE_COLUMNS;

        SysUsersPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SysUsersPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CmsPagesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $criteria->addJoin(CmsPagesPeer::TEMPLATE, CmsTemplatesPeer::TEMPLATE, $join_behavior);

        $criteria->addJoin(CmsPagesPeer::USER_MODIFIED, SysUsersPeer::ID_USER, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CmsPagesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CmsPagesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CmsPagesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CmsPagesPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined SysEnterprises rows

            $key2 = SysEnterprisesPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = SysEnterprisesPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SysEnterprisesPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SysEnterprisesPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CmsPages) to the collection in $obj2 (SysEnterprises)
                $obj2->addCmsPages($obj1);
            } // if joined row not null

            // Add objects for joined CmsTemplates rows

            $key3 = CmsTemplatesPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CmsTemplatesPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = CmsTemplatesPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CmsTemplatesPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CmsPages) to the collection in $obj3 (CmsTemplates)
                $obj3->addCmsPages($obj1);
            } // if joined row not null

            // Add objects for joined SysUsers rows

            $key4 = SysUsersPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = SysUsersPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = SysUsersPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SysUsersPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (CmsPages) to the collection in $obj4 (SysUsers)
                $obj4->addCmsPages($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SysEnterprises table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSysEnterprises(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsPagesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsPagesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CmsPagesPeer::TEMPLATE, CmsTemplatesPeer::TEMPLATE, $join_behavior);

        $criteria->addJoin(CmsPagesPeer::USER_MODIFIED, SysUsersPeer::ID_USER, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CmsTemplates table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCmsTemplates(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsPagesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsPagesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CmsPagesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $criteria->addJoin(CmsPagesPeer::USER_MODIFIED, SysUsersPeer::ID_USER, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SysUsers table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSysUsers(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CmsPagesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CmsPagesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CmsPagesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $criteria->addJoin(CmsPagesPeer::TEMPLATE, CmsTemplatesPeer::TEMPLATE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CmsPages objects pre-filled with all related objects except SysEnterprises.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CmsPages objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSysEnterprises(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);
        }

        CmsPagesPeer::addSelectColumns($criteria);
        $startcol2 = CmsPagesPeer::NUM_HYDRATE_COLUMNS;

        CmsTemplatesPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CmsTemplatesPeer::NUM_HYDRATE_COLUMNS;

        SysUsersPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SysUsersPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CmsPagesPeer::TEMPLATE, CmsTemplatesPeer::TEMPLATE, $join_behavior);

        $criteria->addJoin(CmsPagesPeer::USER_MODIFIED, SysUsersPeer::ID_USER, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CmsPagesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CmsPagesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CmsPagesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CmsPagesPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CmsTemplates rows

                $key2 = CmsTemplatesPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CmsTemplatesPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CmsTemplatesPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CmsTemplatesPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CmsPages) to the collection in $obj2 (CmsTemplates)
                $obj2->addCmsPages($obj1);

            } // if joined row is not null

                // Add objects for joined SysUsers rows

                $key3 = SysUsersPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SysUsersPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = SysUsersPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SysUsersPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CmsPages) to the collection in $obj3 (SysUsers)
                $obj3->addCmsPages($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CmsPages objects pre-filled with all related objects except CmsTemplates.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CmsPages objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCmsTemplates(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);
        }

        CmsPagesPeer::addSelectColumns($criteria);
        $startcol2 = CmsPagesPeer::NUM_HYDRATE_COLUMNS;

        SysEnterprisesPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SysEnterprisesPeer::NUM_HYDRATE_COLUMNS;

        SysUsersPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SysUsersPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CmsPagesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $criteria->addJoin(CmsPagesPeer::USER_MODIFIED, SysUsersPeer::ID_USER, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CmsPagesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CmsPagesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CmsPagesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CmsPagesPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined SysEnterprises rows

                $key2 = SysEnterprisesPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SysEnterprisesPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = SysEnterprisesPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SysEnterprisesPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CmsPages) to the collection in $obj2 (SysEnterprises)
                $obj2->addCmsPages($obj1);

            } // if joined row is not null

                // Add objects for joined SysUsers rows

                $key3 = SysUsersPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SysUsersPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = SysUsersPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SysUsersPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CmsPages) to the collection in $obj3 (SysUsers)
                $obj3->addCmsPages($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CmsPages objects pre-filled with all related objects except SysUsers.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CmsPages objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSysUsers(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);
        }

        CmsPagesPeer::addSelectColumns($criteria);
        $startcol2 = CmsPagesPeer::NUM_HYDRATE_COLUMNS;

        SysEnterprisesPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SysEnterprisesPeer::NUM_HYDRATE_COLUMNS;

        CmsTemplatesPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CmsTemplatesPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CmsPagesPeer::ID_ENTERPRISE, SysEnterprisesPeer::ID_ENTERPRISE, $join_behavior);

        $criteria->addJoin(CmsPagesPeer::TEMPLATE, CmsTemplatesPeer::TEMPLATE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CmsPagesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CmsPagesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CmsPagesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CmsPagesPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined SysEnterprises rows

                $key2 = SysEnterprisesPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SysEnterprisesPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = SysEnterprisesPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SysEnterprisesPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CmsPages) to the collection in $obj2 (SysEnterprises)
                $obj2->addCmsPages($obj1);

            } // if joined row is not null

                // Add objects for joined CmsTemplates rows

                $key3 = CmsTemplatesPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CmsTemplatesPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CmsTemplatesPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CmsTemplatesPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CmsPages) to the collection in $obj3 (CmsTemplates)
                $obj3->addCmsPages($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CmsPagesPeer::DATABASE_NAME)->getTable(CmsPagesPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCmsPagesPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCmsPagesPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new \CmsPagesTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CmsPagesPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CmsPages or Criteria object.
     *
     * @param      mixed $values Criteria or CmsPages object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CmsPages object
        }

        if ($criteria->containsKey(CmsPagesPeer::ID_PAGE) && $criteria->keyContainsValue(CmsPagesPeer::ID_PAGE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CmsPagesPeer::ID_PAGE.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CmsPages or Criteria object.
     *
     * @param      mixed $values Criteria or CmsPages object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CmsPagesPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CmsPagesPeer::ID_PAGE);
            $value = $criteria->remove(CmsPagesPeer::ID_PAGE);
            if ($value) {
                $selectCriteria->add(CmsPagesPeer::ID_PAGE, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CmsPagesPeer::TABLE_NAME);
            }

        } else { // $values is CmsPages object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the cms_pages table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CmsPagesPeer::TABLE_NAME, $con, CmsPagesPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CmsPagesPeer::clearInstancePool();
            CmsPagesPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CmsPages or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CmsPages object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CmsPagesPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CmsPages) { // it's a model object
            // invalidate the cache for this single object
            CmsPagesPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CmsPagesPeer::DATABASE_NAME);
            $criteria->add(CmsPagesPeer::ID_PAGE, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CmsPagesPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CmsPagesPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CmsPagesPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CmsPages object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param CmsPages $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CmsPagesPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CmsPagesPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CmsPagesPeer::DATABASE_NAME, CmsPagesPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CmsPages
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CmsPagesPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CmsPagesPeer::DATABASE_NAME);
        $criteria->add(CmsPagesPeer::ID_PAGE, $pk);

        $v = CmsPagesPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CmsPages[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CmsPagesPeer::DATABASE_NAME);
            $criteria->add(CmsPagesPeer::ID_PAGE, $pks, Criteria::IN);
            $objs = CmsPagesPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCmsPagesPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCmsPagesPeer::buildTableMap();


<?php


/**
 * Base class that represents a query for the 'cms_files_x_article' table.
 *
 *
 *
 * @method CmsFilesXArticleQuery orderByIdFile($order = Criteria::ASC) Order by the id_file column
 * @method CmsFilesXArticleQuery orderByIdArticle($order = Criteria::ASC) Order by the id_article column
 * @method CmsFilesXArticleQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method CmsFilesXArticleQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method CmsFilesXArticleQuery orderByPrimary($order = Criteria::ASC) Order by the primary column
 * @method CmsFilesXArticleQuery orderByState($order = Criteria::ASC) Order by the state column
 * @method CmsFilesXArticleQuery orderByOrder($order = Criteria::ASC) Order by the order column
 *
 * @method CmsFilesXArticleQuery groupByIdFile() Group by the id_file column
 * @method CmsFilesXArticleQuery groupByIdArticle() Group by the id_article column
 * @method CmsFilesXArticleQuery groupByType() Group by the type column
 * @method CmsFilesXArticleQuery groupByDescription() Group by the description column
 * @method CmsFilesXArticleQuery groupByPrimary() Group by the primary column
 * @method CmsFilesXArticleQuery groupByState() Group by the state column
 * @method CmsFilesXArticleQuery groupByOrder() Group by the order column
 *
 * @method CmsFilesXArticleQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CmsFilesXArticleQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CmsFilesXArticleQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CmsFilesXArticleQuery leftJoinSysFiles($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysFiles relation
 * @method CmsFilesXArticleQuery rightJoinSysFiles($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysFiles relation
 * @method CmsFilesXArticleQuery innerJoinSysFiles($relationAlias = null) Adds a INNER JOIN clause to the query using the SysFiles relation
 *
 * @method CmsFilesXArticleQuery leftJoinCmsArticles($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsArticles relation
 * @method CmsFilesXArticleQuery rightJoinCmsArticles($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsArticles relation
 * @method CmsFilesXArticleQuery innerJoinCmsArticles($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsArticles relation
 *
 * @method CmsFilesXArticle findOne(PropelPDO $con = null) Return the first CmsFilesXArticle matching the query
 * @method CmsFilesXArticle findOneOrCreate(PropelPDO $con = null) Return the first CmsFilesXArticle matching the query, or a new CmsFilesXArticle object populated from the query conditions when no match is found
 *
 * @method CmsFilesXArticle findOneByIdArticle(int $id_article) Return the first CmsFilesXArticle filtered by the id_article column
 * @method CmsFilesXArticle findOneByType(string $type) Return the first CmsFilesXArticle filtered by the type column
 * @method CmsFilesXArticle findOneByDescription(string $description) Return the first CmsFilesXArticle filtered by the description column
 * @method CmsFilesXArticle findOneByPrimary(string $primary) Return the first CmsFilesXArticle filtered by the primary column
 * @method CmsFilesXArticle findOneByState(string $state) Return the first CmsFilesXArticle filtered by the state column
 * @method CmsFilesXArticle findOneByOrder(int $order) Return the first CmsFilesXArticle filtered by the order column
 *
 * @method array findByIdFile(int $id_file) Return CmsFilesXArticle objects filtered by the id_file column
 * @method array findByIdArticle(int $id_article) Return CmsFilesXArticle objects filtered by the id_article column
 * @method array findByType(string $type) Return CmsFilesXArticle objects filtered by the type column
 * @method array findByDescription(string $description) Return CmsFilesXArticle objects filtered by the description column
 * @method array findByPrimary(string $primary) Return CmsFilesXArticle objects filtered by the primary column
 * @method array findByState(string $state) Return CmsFilesXArticle objects filtered by the state column
 * @method array findByOrder(int $order) Return CmsFilesXArticle objects filtered by the order column
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseCmsFilesXArticleQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCmsFilesXArticleQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bach';
        }
        if (null === $modelName) {
            $modelName = 'CmsFilesXArticle';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CmsFilesXArticleQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CmsFilesXArticleQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CmsFilesXArticleQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CmsFilesXArticleQuery) {
            return $criteria;
        }
        $query = new CmsFilesXArticleQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CmsFilesXArticle|CmsFilesXArticle[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CmsFilesXArticlePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CmsFilesXArticlePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CmsFilesXArticle A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdFile($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CmsFilesXArticle A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_file`, `id_article`, `type`, `description`, `primary`, `state`, `order` FROM `cms_files_x_article` WHERE `id_file` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CmsFilesXArticle();
            $obj->hydrate($row);
            CmsFilesXArticlePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CmsFilesXArticle|CmsFilesXArticle[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CmsFilesXArticle[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CmsFilesXArticleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CmsFilesXArticlePeer::ID_FILE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CmsFilesXArticleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CmsFilesXArticlePeer::ID_FILE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_file column
     *
     * Example usage:
     * <code>
     * $query->filterByIdFile(1234); // WHERE id_file = 1234
     * $query->filterByIdFile(array(12, 34)); // WHERE id_file IN (12, 34)
     * $query->filterByIdFile(array('min' => 12)); // WHERE id_file >= 12
     * $query->filterByIdFile(array('max' => 12)); // WHERE id_file <= 12
     * </code>
     *
     * @see       filterBySysFiles()
     *
     * @param     mixed $idFile The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsFilesXArticleQuery The current query, for fluid interface
     */
    public function filterByIdFile($idFile = null, $comparison = null)
    {
        if (is_array($idFile)) {
            $useMinMax = false;
            if (isset($idFile['min'])) {
                $this->addUsingAlias(CmsFilesXArticlePeer::ID_FILE, $idFile['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idFile['max'])) {
                $this->addUsingAlias(CmsFilesXArticlePeer::ID_FILE, $idFile['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsFilesXArticlePeer::ID_FILE, $idFile, $comparison);
    }

    /**
     * Filter the query on the id_article column
     *
     * Example usage:
     * <code>
     * $query->filterByIdArticle(1234); // WHERE id_article = 1234
     * $query->filterByIdArticle(array(12, 34)); // WHERE id_article IN (12, 34)
     * $query->filterByIdArticle(array('min' => 12)); // WHERE id_article >= 12
     * $query->filterByIdArticle(array('max' => 12)); // WHERE id_article <= 12
     * </code>
     *
     * @see       filterByCmsArticles()
     *
     * @param     mixed $idArticle The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsFilesXArticleQuery The current query, for fluid interface
     */
    public function filterByIdArticle($idArticle = null, $comparison = null)
    {
        if (is_array($idArticle)) {
            $useMinMax = false;
            if (isset($idArticle['min'])) {
                $this->addUsingAlias(CmsFilesXArticlePeer::ID_ARTICLE, $idArticle['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idArticle['max'])) {
                $this->addUsingAlias(CmsFilesXArticlePeer::ID_ARTICLE, $idArticle['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsFilesXArticlePeer::ID_ARTICLE, $idArticle, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%'); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsFilesXArticleQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $type)) {
                $type = str_replace('*', '%', $type);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsFilesXArticlePeer::TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsFilesXArticleQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsFilesXArticlePeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the primary column
     *
     * Example usage:
     * <code>
     * $query->filterByPrimary('fooValue');   // WHERE primary = 'fooValue'
     * $query->filterByPrimary('%fooValue%'); // WHERE primary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $primary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsFilesXArticleQuery The current query, for fluid interface
     */
    public function filterByPrimary($primary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($primary)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $primary)) {
                $primary = str_replace('*', '%', $primary);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsFilesXArticlePeer::PRIMARY, $primary, $comparison);
    }

    /**
     * Filter the query on the state column
     *
     * Example usage:
     * <code>
     * $query->filterByState('fooValue');   // WHERE state = 'fooValue'
     * $query->filterByState('%fooValue%'); // WHERE state LIKE '%fooValue%'
     * </code>
     *
     * @param     string $state The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsFilesXArticleQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($state)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $state)) {
                $state = str_replace('*', '%', $state);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsFilesXArticlePeer::STATE, $state, $comparison);
    }

    /**
     * Filter the query on the order column
     *
     * Example usage:
     * <code>
     * $query->filterByOrder(1234); // WHERE order = 1234
     * $query->filterByOrder(array(12, 34)); // WHERE order IN (12, 34)
     * $query->filterByOrder(array('min' => 12)); // WHERE order >= 12
     * $query->filterByOrder(array('max' => 12)); // WHERE order <= 12
     * </code>
     *
     * @param     mixed $order The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsFilesXArticleQuery The current query, for fluid interface
     */
    public function filterByOrder($order = null, $comparison = null)
    {
        if (is_array($order)) {
            $useMinMax = false;
            if (isset($order['min'])) {
                $this->addUsingAlias(CmsFilesXArticlePeer::ORDER, $order['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($order['max'])) {
                $this->addUsingAlias(CmsFilesXArticlePeer::ORDER, $order['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsFilesXArticlePeer::ORDER, $order, $comparison);
    }

    /**
     * Filter the query by a related SysFiles object
     *
     * @param   SysFiles|PropelObjectCollection $sysFiles The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsFilesXArticleQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysFiles($sysFiles, $comparison = null)
    {
        if ($sysFiles instanceof SysFiles) {
            return $this
                ->addUsingAlias(CmsFilesXArticlePeer::ID_FILE, $sysFiles->getIdFile(), $comparison);
        } elseif ($sysFiles instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CmsFilesXArticlePeer::ID_FILE, $sysFiles->toKeyValue('PrimaryKey', 'IdFile'), $comparison);
        } else {
            throw new PropelException('filterBySysFiles() only accepts arguments of type SysFiles or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysFiles relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsFilesXArticleQuery The current query, for fluid interface
     */
    public function joinSysFiles($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysFiles');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysFiles');
        }

        return $this;
    }

    /**
     * Use the SysFiles relation SysFiles object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysFilesQuery A secondary query class using the current class as primary query
     */
    public function useSysFilesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSysFiles($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysFiles', 'SysFilesQuery');
    }

    /**
     * Filter the query by a related CmsArticles object
     *
     * @param   CmsArticles|PropelObjectCollection $cmsArticles The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsFilesXArticleQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsArticles($cmsArticles, $comparison = null)
    {
        if ($cmsArticles instanceof CmsArticles) {
            return $this
                ->addUsingAlias(CmsFilesXArticlePeer::ID_ARTICLE, $cmsArticles->getIdArticle(), $comparison);
        } elseif ($cmsArticles instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CmsFilesXArticlePeer::ID_ARTICLE, $cmsArticles->toKeyValue('PrimaryKey', 'IdArticle'), $comparison);
        } else {
            throw new PropelException('filterByCmsArticles() only accepts arguments of type CmsArticles or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsArticles relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsFilesXArticleQuery The current query, for fluid interface
     */
    public function joinCmsArticles($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsArticles');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsArticles');
        }

        return $this;
    }

    /**
     * Use the CmsArticles relation CmsArticles object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsArticlesQuery A secondary query class using the current class as primary query
     */
    public function useCmsArticlesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCmsArticles($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsArticles', 'CmsArticlesQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CmsFilesXArticle $cmsFilesXArticle Object to remove from the list of results
     *
     * @return CmsFilesXArticleQuery The current query, for fluid interface
     */
    public function prune($cmsFilesXArticle = null)
    {
        if ($cmsFilesXArticle) {
            $this->addUsingAlias(CmsFilesXArticlePeer::ID_FILE, $cmsFilesXArticle->getIdFile(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

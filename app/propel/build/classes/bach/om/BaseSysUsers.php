<?php


/**
 * Base class that represents a row from the 'sys_users' table.
 *
 *
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseSysUsers extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'SysUsersPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        SysUsersPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_user field.
     * @var        int
     */
    protected $id_user;

    /**
     * The value for the username field.
     * @var        string
     */
    protected $username;

    /**
     * The value for the password field.
     * @var        string
     */
    protected $password;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * The value for the first_name field.
     * @var        string
     */
    protected $first_name;

    /**
     * The value for the last_name field.
     * @var        string
     */
    protected $last_name;

    /**
     * The value for the state field.
     * Note: this column has a database default value of: 'CREATED'
     * @var        string
     */
    protected $state;

    /**
     * The value for the id_rol field.
     * @var        int
     */
    protected $id_rol;

    /**
     * The value for the id_photo field.
     * @var        int
     */
    protected $id_photo;

    /**
     * The value for the created field.
     * @var        string
     */
    protected $created;

    /**
     * The value for the modified field.
     * @var        string
     */
    protected $modified;

    /**
     * The value for the user_modified field.
     * @var        int
     */
    protected $user_modified;

    /**
     * The value for the lang_code field.
     * Note: this column has a database default value of: 'spanish'
     * @var        string
     */
    protected $lang_code;

    /**
     * The value for the phone field.
     * @var        string
     */
    protected $phone;

    /**
     * The value for the mobile field.
     * @var        string
     */
    protected $mobile;

    /**
     * The value for the id_time_zone field.
     * @var        int
     */
    protected $id_time_zone;

    /**
     * The value for the id_city_birthday field.
     * @var        int
     */
    protected $id_city_birthday;

    /**
     * The value for the id_country_birthday field.
     * @var        int
     */
    protected $id_country_birthday;

    /**
     * The value for the id_city_address field.
     * @var        int
     */
    protected $id_city_address;

    /**
     * The value for the id_country_address field.
     * @var        int
     */
    protected $id_country_address;

    /**
     * The value for the gender field.
     * @var        string
     */
    protected $gender;

    /**
     * The value for the birthday field.
     * @var        string
     */
    protected $birthday;

    /**
     * The value for the birthplace field.
     * @var        string
     */
    protected $birthplace;

    /**
     * The value for the hash field.
     * @var        string
     */
    protected $hash;

    /**
     * The value for the tour field.
     * Note: this column has a database default value of: 'YES'
     * @var        string
     */
    protected $tour;

    /**
     * The value for the marital_status field.
     * @var        string
     */
    protected $marital_status;

    /**
     * The value for the level_education field.
     * @var        string
     */
    protected $level_education;

    /**
     * The value for the occupation field.
     * @var        string
     */
    protected $occupation;

    /**
     * The value for the department_address field.
     * @var        string
     */
    protected $department_address;

    /**
     * The value for the department_birthday field.
     * @var        string
     */
    protected $department_birthday;

    /**
     * The value for the address field.
     * @var        string
     */
    protected $address;

    /**
     * The value for the id_church field.
     * @var        int
     */
    protected $id_church;

    /**
     * The value for the member field.
     * Note: this column has a database default value of: 'NO'
     * @var        string
     */
    protected $member;

    /**
     * The value for the pastor_baptism field.
     * @var        string
     */
    protected $pastor_baptism;

    /**
     * The value for the date_baptism field.
     * @var        string
     */
    protected $date_baptism;

    /**
     * The value for the id_enterprise field.
     * @var        int
     */
    protected $id_enterprise;

    /**
     * @var        SysEnterprises
     */
    protected $aSysEnterprisesRelatedByIdEnterprise;

    /**
     * @var        SysRoles
     */
    protected $aSysRoles;

    /**
     * @var        SysFiles
     */
    protected $aSysFiles;

    /**
     * @var        SysUsers
     */
    protected $aSysUsersRelatedByUserModified;

    /**
     * @var        PropelObjectCollection|CmsArticles[] Collection to store aggregation of CmsArticles objects.
     */
    protected $collCmsArticlessRelatedByIdUser;
    protected $collCmsArticlessRelatedByIdUserPartial;

    /**
     * @var        PropelObjectCollection|CmsArticles[] Collection to store aggregation of CmsArticles objects.
     */
    protected $collCmsArticlessRelatedByUserModified;
    protected $collCmsArticlessRelatedByUserModifiedPartial;

    /**
     * @var        PropelObjectCollection|CmsPages[] Collection to store aggregation of CmsPages objects.
     */
    protected $collCmsPagess;
    protected $collCmsPagessPartial;

    /**
     * @var        PropelObjectCollection|CmsVideosXArticle[] Collection to store aggregation of CmsVideosXArticle objects.
     */
    protected $collCmsVideosXArticles;
    protected $collCmsVideosXArticlesPartial;

    /**
     * @var        PropelObjectCollection|CmsVideosXPage[] Collection to store aggregation of CmsVideosXPage objects.
     */
    protected $collCmsVideosXPages;
    protected $collCmsVideosXPagesPartial;

    /**
     * @var        PropelObjectCollection|SysChats[] Collection to store aggregation of SysChats objects.
     */
    protected $collSysChatssRelatedByIdSender;
    protected $collSysChatssRelatedByIdSenderPartial;

    /**
     * @var        PropelObjectCollection|SysChats[] Collection to store aggregation of SysChats objects.
     */
    protected $collSysChatssRelatedByIdReceiver;
    protected $collSysChatssRelatedByIdReceiverPartial;

    /**
     * @var        PropelObjectCollection|SysEnterprises[] Collection to store aggregation of SysEnterprises objects.
     */
    protected $collSysEnterprisessRelatedByUserModified;
    protected $collSysEnterprisessRelatedByUserModifiedPartial;

    /**
     * @var        PropelObjectCollection|SysNotifications[] Collection to store aggregation of SysNotifications objects.
     */
    protected $collSysNotificationssRelatedByIdSender;
    protected $collSysNotificationssRelatedByIdSenderPartial;

    /**
     * @var        PropelObjectCollection|SysNotifications[] Collection to store aggregation of SysNotifications objects.
     */
    protected $collSysNotificationssRelatedByIdReceiver;
    protected $collSysNotificationssRelatedByIdReceiverPartial;

    /**
     * @var        PropelObjectCollection|SysPages[] Collection to store aggregation of SysPages objects.
     */
    protected $collSysPagess;
    protected $collSysPagessPartial;

    /**
     * @var        PropelObjectCollection|SysUsers[] Collection to store aggregation of SysUsers objects.
     */
    protected $collSysUserssRelatedByIdUser;
    protected $collSysUserssRelatedByIdUserPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $cmsArticlessRelatedByIdUserScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $cmsArticlessRelatedByUserModifiedScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $cmsPagessScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $cmsVideosXArticlesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $cmsVideosXPagesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $sysChatssRelatedByIdSenderScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $sysChatssRelatedByIdReceiverScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $sysEnterprisessRelatedByUserModifiedScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $sysNotificationssRelatedByIdSenderScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $sysNotificationssRelatedByIdReceiverScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $sysPagessScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $sysUserssRelatedByIdUserScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->state = 'CREATED';
        $this->lang_code = 'spanish';
        $this->tour = 'YES';
        $this->member = 'NO';
    }

    /**
     * Initializes internal state of BaseSysUsers object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id_user] column value.
     *
     * @return int
     */
    public function getIdUser()
    {

        return $this->id_user;
    }

    /**
     * Get the [username] column value.
     * Se puede usar CI como username
     * @return string
     */
    public function getUsername()
    {

        return $this->username;
    }

    /**
     * Get the [password] column value.
     *
     * @return string
     */
    public function getPassword()
    {

        return $this->password;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Get the [first_name] column value.
     *
     * @return string
     */
    public function getFirstName()
    {

        return $this->first_name;
    }

    /**
     * Get the [last_name] column value.
     *
     * @return string
     */
    public function getLastName()
    {

        return $this->last_name;
    }

    /**
     * Get the [state] column value.
     *
     * @return string
     */
    public function getState()
    {

        return $this->state;
    }

    /**
     * Get the [id_rol] column value.
     *
     * @return int
     */
    public function getIdRol()
    {

        return $this->id_rol;
    }

    /**
     * Get the [id_photo] column value.
     *
     * @return int
     */
    public function getIdPhoto()
    {

        return $this->id_photo;
    }

    /**
     * Get the [optionally formatted] temporal [created] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreated($format = 'Y-m-d H:i:s')
    {
        if ($this->created === null) {
            return null;
        }

        if ($this->created === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->created);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [modified] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getModified($format = 'Y-m-d H:i:s')
    {
        if ($this->modified === null) {
            return null;
        }

        if ($this->modified === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->modified);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->modified, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [user_modified] column value.
     *
     * @return int
     */
    public function getUserModified()
    {

        return $this->user_modified;
    }

    /**
     * Get the [lang_code] column value.
     *
     * @return string
     */
    public function getLangCode()
    {

        return $this->lang_code;
    }

    /**
     * Get the [phone] column value.
     *
     * @return string
     */
    public function getPhone()
    {

        return $this->phone;
    }

    /**
     * Get the [mobile] column value.
     *
     * @return string
     */
    public function getMobile()
    {

        return $this->mobile;
    }

    /**
     * Get the [id_time_zone] column value.
     *
     * @return int
     */
    public function getIdTimeZone()
    {

        return $this->id_time_zone;
    }

    /**
     * Get the [id_city_birthday] column value.
     *
     * @return int
     */
    public function getIdCityBirthday()
    {

        return $this->id_city_birthday;
    }

    /**
     * Get the [id_country_birthday] column value.
     *
     * @return int
     */
    public function getIdCountryBirthday()
    {

        return $this->id_country_birthday;
    }

    /**
     * Get the [id_city_address] column value.
     *
     * @return int
     */
    public function getIdCityAddress()
    {

        return $this->id_city_address;
    }

    /**
     * Get the [id_country_address] column value.
     *
     * @return int
     */
    public function getIdCountryAddress()
    {

        return $this->id_country_address;
    }

    /**
     * Get the [gender] column value.
     *
     * @return string
     */
    public function getGender()
    {

        return $this->gender;
    }

    /**
     * Get the [optionally formatted] temporal [birthday] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getBirthday($format = '%x')
    {
        if ($this->birthday === null) {
            return null;
        }

        if ($this->birthday === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->birthday);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->birthday, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [birthplace] column value.
     *
     * @return string
     */
    public function getBirthplace()
    {

        return $this->birthplace;
    }

    /**
     * Get the [hash] column value.
     *
     * @return string
     */
    public function getHash()
    {

        return $this->hash;
    }

    /**
     * Get the [tour] column value.
     *
     * @return string
     */
    public function getTour()
    {

        return $this->tour;
    }

    /**
     * Get the [marital_status] column value.
     *
     * @return string
     */
    public function getMaritalStatus()
    {

        return $this->marital_status;
    }

    /**
     * Get the [level_education] column value.
     *
     * @return string
     */
    public function getLevelEducation()
    {

        return $this->level_education;
    }

    /**
     * Get the [occupation] column value.
     *
     * @return string
     */
    public function getOccupation()
    {

        return $this->occupation;
    }

    /**
     * Get the [department_address] column value.
     *
     * @return string
     */
    public function getDepartmentAddress()
    {

        return $this->department_address;
    }

    /**
     * Get the [department_birthday] column value.
     *
     * @return string
     */
    public function getDepartmentBirthday()
    {

        return $this->department_birthday;
    }

    /**
     * Get the [address] column value.
     *
     * @return string
     */
    public function getAddress()
    {

        return $this->address;
    }

    /**
     * Get the [id_church] column value.
     *
     * @return int
     */
    public function getIdChurch()
    {

        return $this->id_church;
    }

    /**
     * Get the [member] column value.
     * Condición del asociado BAUTIZADO/SIMPATIZANTE
     * @return string
     */
    public function getMember()
    {

        return $this->member;
    }

    /**
     * Get the [pastor_baptism] column value.
     * Pastor por el que fué bautizado
     * @return string
     */
    public function getPastorBaptism()
    {

        return $this->pastor_baptism;
    }

    /**
     * Get the [optionally formatted] temporal [date_baptism] column value.
     * Fecha del bautizmo
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateBaptism($format = 'Y-m-d H:i:s')
    {
        if ($this->date_baptism === null) {
            return null;
        }

        if ($this->date_baptism === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_baptism);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_baptism, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [id_enterprise] column value.
     * ID de la empresa a la que pertenece el usuario
     * @return int
     */
    public function getIdEnterprise()
    {

        return $this->id_enterprise;
    }

    /**
     * Set the value of [id_user] column.
     *
     * @param  int $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setIdUser($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_user !== $v) {
            $this->id_user = $v;
            $this->modifiedColumns[] = SysUsersPeer::ID_USER;
        }


        return $this;
    } // setIdUser()

    /**
     * Set the value of [username] column.
     * Se puede usar CI como username
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setUsername($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->username !== $v) {
            $this->username = $v;
            $this->modifiedColumns[] = SysUsersPeer::USERNAME;
        }


        return $this;
    } // setUsername()

    /**
     * Set the value of [password] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[] = SysUsersPeer::PASSWORD;
        }


        return $this;
    } // setPassword()

    /**
     * Set the value of [email] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = SysUsersPeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [first_name] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setFirstName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->first_name !== $v) {
            $this->first_name = $v;
            $this->modifiedColumns[] = SysUsersPeer::FIRST_NAME;
        }


        return $this;
    } // setFirstName()

    /**
     * Set the value of [last_name] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setLastName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->last_name !== $v) {
            $this->last_name = $v;
            $this->modifiedColumns[] = SysUsersPeer::LAST_NAME;
        }


        return $this;
    } // setLastName()

    /**
     * Set the value of [state] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setState($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->state !== $v) {
            $this->state = $v;
            $this->modifiedColumns[] = SysUsersPeer::STATE;
        }


        return $this;
    } // setState()

    /**
     * Set the value of [id_rol] column.
     *
     * @param  int $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setIdRol($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_rol !== $v) {
            $this->id_rol = $v;
            $this->modifiedColumns[] = SysUsersPeer::ID_ROL;
        }

        if ($this->aSysRoles !== null && $this->aSysRoles->getIdRol() !== $v) {
            $this->aSysRoles = null;
        }


        return $this;
    } // setIdRol()

    /**
     * Set the value of [id_photo] column.
     *
     * @param  int $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setIdPhoto($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_photo !== $v) {
            $this->id_photo = $v;
            $this->modifiedColumns[] = SysUsersPeer::ID_PHOTO;
        }

        if ($this->aSysFiles !== null && $this->aSysFiles->getIdFile() !== $v) {
            $this->aSysFiles = null;
        }


        return $this;
    } // setIdPhoto()

    /**
     * Sets the value of [created] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SysUsers The current object (for fluent API support)
     */
    public function setCreated($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created !== null || $dt !== null) {
            $currentDateAsString = ($this->created !== null && $tmpDt = new DateTime($this->created)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->created = $newDateAsString;
                $this->modifiedColumns[] = SysUsersPeer::CREATED;
            }
        } // if either are not null


        return $this;
    } // setCreated()

    /**
     * Sets the value of [modified] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SysUsers The current object (for fluent API support)
     */
    public function setModified($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->modified !== null || $dt !== null) {
            $currentDateAsString = ($this->modified !== null && $tmpDt = new DateTime($this->modified)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->modified = $newDateAsString;
                $this->modifiedColumns[] = SysUsersPeer::MODIFIED;
            }
        } // if either are not null


        return $this;
    } // setModified()

    /**
     * Set the value of [user_modified] column.
     *
     * @param  int $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setUserModified($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->user_modified !== $v) {
            $this->user_modified = $v;
            $this->modifiedColumns[] = SysUsersPeer::USER_MODIFIED;
        }

        if ($this->aSysUsersRelatedByUserModified !== null && $this->aSysUsersRelatedByUserModified->getIdUser() !== $v) {
            $this->aSysUsersRelatedByUserModified = null;
        }


        return $this;
    } // setUserModified()

    /**
     * Set the value of [lang_code] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setLangCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->lang_code !== $v) {
            $this->lang_code = $v;
            $this->modifiedColumns[] = SysUsersPeer::LANG_CODE;
        }


        return $this;
    } // setLangCode()

    /**
     * Set the value of [phone] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phone !== $v) {
            $this->phone = $v;
            $this->modifiedColumns[] = SysUsersPeer::PHONE;
        }


        return $this;
    } // setPhone()

    /**
     * Set the value of [mobile] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setMobile($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mobile !== $v) {
            $this->mobile = $v;
            $this->modifiedColumns[] = SysUsersPeer::MOBILE;
        }


        return $this;
    } // setMobile()

    /**
     * Set the value of [id_time_zone] column.
     *
     * @param  int $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setIdTimeZone($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_time_zone !== $v) {
            $this->id_time_zone = $v;
            $this->modifiedColumns[] = SysUsersPeer::ID_TIME_ZONE;
        }


        return $this;
    } // setIdTimeZone()

    /**
     * Set the value of [id_city_birthday] column.
     *
     * @param  int $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setIdCityBirthday($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_city_birthday !== $v) {
            $this->id_city_birthday = $v;
            $this->modifiedColumns[] = SysUsersPeer::ID_CITY_BIRTHDAY;
        }


        return $this;
    } // setIdCityBirthday()

    /**
     * Set the value of [id_country_birthday] column.
     *
     * @param  int $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setIdCountryBirthday($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_country_birthday !== $v) {
            $this->id_country_birthday = $v;
            $this->modifiedColumns[] = SysUsersPeer::ID_COUNTRY_BIRTHDAY;
        }


        return $this;
    } // setIdCountryBirthday()

    /**
     * Set the value of [id_city_address] column.
     *
     * @param  int $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setIdCityAddress($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_city_address !== $v) {
            $this->id_city_address = $v;
            $this->modifiedColumns[] = SysUsersPeer::ID_CITY_ADDRESS;
        }


        return $this;
    } // setIdCityAddress()

    /**
     * Set the value of [id_country_address] column.
     *
     * @param  int $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setIdCountryAddress($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_country_address !== $v) {
            $this->id_country_address = $v;
            $this->modifiedColumns[] = SysUsersPeer::ID_COUNTRY_ADDRESS;
        }


        return $this;
    } // setIdCountryAddress()

    /**
     * Set the value of [gender] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setGender($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->gender !== $v) {
            $this->gender = $v;
            $this->modifiedColumns[] = SysUsersPeer::GENDER;
        }


        return $this;
    } // setGender()

    /**
     * Sets the value of [birthday] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SysUsers The current object (for fluent API support)
     */
    public function setBirthday($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->birthday !== null || $dt !== null) {
            $currentDateAsString = ($this->birthday !== null && $tmpDt = new DateTime($this->birthday)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->birthday = $newDateAsString;
                $this->modifiedColumns[] = SysUsersPeer::BIRTHDAY;
            }
        } // if either are not null


        return $this;
    } // setBirthday()

    /**
     * Set the value of [birthplace] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setBirthplace($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->birthplace !== $v) {
            $this->birthplace = $v;
            $this->modifiedColumns[] = SysUsersPeer::BIRTHPLACE;
        }


        return $this;
    } // setBirthplace()

    /**
     * Set the value of [hash] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setHash($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->hash !== $v) {
            $this->hash = $v;
            $this->modifiedColumns[] = SysUsersPeer::HASH;
        }


        return $this;
    } // setHash()

    /**
     * Set the value of [tour] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setTour($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tour !== $v) {
            $this->tour = $v;
            $this->modifiedColumns[] = SysUsersPeer::TOUR;
        }


        return $this;
    } // setTour()

    /**
     * Set the value of [marital_status] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setMaritalStatus($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->marital_status !== $v) {
            $this->marital_status = $v;
            $this->modifiedColumns[] = SysUsersPeer::MARITAL_STATUS;
        }


        return $this;
    } // setMaritalStatus()

    /**
     * Set the value of [level_education] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setLevelEducation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->level_education !== $v) {
            $this->level_education = $v;
            $this->modifiedColumns[] = SysUsersPeer::LEVEL_EDUCATION;
        }


        return $this;
    } // setLevelEducation()

    /**
     * Set the value of [occupation] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setOccupation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->occupation !== $v) {
            $this->occupation = $v;
            $this->modifiedColumns[] = SysUsersPeer::OCCUPATION;
        }


        return $this;
    } // setOccupation()

    /**
     * Set the value of [department_address] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setDepartmentAddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->department_address !== $v) {
            $this->department_address = $v;
            $this->modifiedColumns[] = SysUsersPeer::DEPARTMENT_ADDRESS;
        }


        return $this;
    } // setDepartmentAddress()

    /**
     * Set the value of [department_birthday] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setDepartmentBirthday($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->department_birthday !== $v) {
            $this->department_birthday = $v;
            $this->modifiedColumns[] = SysUsersPeer::DEPARTMENT_BIRTHDAY;
        }


        return $this;
    } // setDepartmentBirthday()

    /**
     * Set the value of [address] column.
     *
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setAddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->address !== $v) {
            $this->address = $v;
            $this->modifiedColumns[] = SysUsersPeer::ADDRESS;
        }


        return $this;
    } // setAddress()

    /**
     * Set the value of [id_church] column.
     *
     * @param  int $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setIdChurch($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_church !== $v) {
            $this->id_church = $v;
            $this->modifiedColumns[] = SysUsersPeer::ID_CHURCH;
        }


        return $this;
    } // setIdChurch()

    /**
     * Set the value of [member] column.
     * Condición del asociado BAUTIZADO/SIMPATIZANTE
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setMember($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->member !== $v) {
            $this->member = $v;
            $this->modifiedColumns[] = SysUsersPeer::MEMBER;
        }


        return $this;
    } // setMember()

    /**
     * Set the value of [pastor_baptism] column.
     * Pastor por el que fué bautizado
     * @param  string $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setPastorBaptism($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pastor_baptism !== $v) {
            $this->pastor_baptism = $v;
            $this->modifiedColumns[] = SysUsersPeer::PASTOR_BAPTISM;
        }


        return $this;
    } // setPastorBaptism()

    /**
     * Sets the value of [date_baptism] column to a normalized version of the date/time value specified.
     * Fecha del bautizmo
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SysUsers The current object (for fluent API support)
     */
    public function setDateBaptism($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_baptism !== null || $dt !== null) {
            $currentDateAsString = ($this->date_baptism !== null && $tmpDt = new DateTime($this->date_baptism)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_baptism = $newDateAsString;
                $this->modifiedColumns[] = SysUsersPeer::DATE_BAPTISM;
            }
        } // if either are not null


        return $this;
    } // setDateBaptism()

    /**
     * Set the value of [id_enterprise] column.
     * ID de la empresa a la que pertenece el usuario
     * @param  int $v new value
     * @return SysUsers The current object (for fluent API support)
     */
    public function setIdEnterprise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_enterprise !== $v) {
            $this->id_enterprise = $v;
            $this->modifiedColumns[] = SysUsersPeer::ID_ENTERPRISE;
        }

        if ($this->aSysEnterprisesRelatedByIdEnterprise !== null && $this->aSysEnterprisesRelatedByIdEnterprise->getIdEnterprise() !== $v) {
            $this->aSysEnterprisesRelatedByIdEnterprise = null;
        }


        return $this;
    } // setIdEnterprise()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->state !== 'CREATED') {
                return false;
            }

            if ($this->lang_code !== 'spanish') {
                return false;
            }

            if ($this->tour !== 'YES') {
                return false;
            }

            if ($this->member !== 'NO') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_user = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->username = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->password = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->email = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->first_name = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->last_name = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->state = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->id_rol = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->id_photo = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
            $this->created = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->modified = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->user_modified = ($row[$startcol + 11] !== null) ? (int) $row[$startcol + 11] : null;
            $this->lang_code = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->phone = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->mobile = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->id_time_zone = ($row[$startcol + 15] !== null) ? (int) $row[$startcol + 15] : null;
            $this->id_city_birthday = ($row[$startcol + 16] !== null) ? (int) $row[$startcol + 16] : null;
            $this->id_country_birthday = ($row[$startcol + 17] !== null) ? (int) $row[$startcol + 17] : null;
            $this->id_city_address = ($row[$startcol + 18] !== null) ? (int) $row[$startcol + 18] : null;
            $this->id_country_address = ($row[$startcol + 19] !== null) ? (int) $row[$startcol + 19] : null;
            $this->gender = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->birthday = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->birthplace = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->hash = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->tour = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
            $this->marital_status = ($row[$startcol + 25] !== null) ? (string) $row[$startcol + 25] : null;
            $this->level_education = ($row[$startcol + 26] !== null) ? (string) $row[$startcol + 26] : null;
            $this->occupation = ($row[$startcol + 27] !== null) ? (string) $row[$startcol + 27] : null;
            $this->department_address = ($row[$startcol + 28] !== null) ? (string) $row[$startcol + 28] : null;
            $this->department_birthday = ($row[$startcol + 29] !== null) ? (string) $row[$startcol + 29] : null;
            $this->address = ($row[$startcol + 30] !== null) ? (string) $row[$startcol + 30] : null;
            $this->id_church = ($row[$startcol + 31] !== null) ? (int) $row[$startcol + 31] : null;
            $this->member = ($row[$startcol + 32] !== null) ? (string) $row[$startcol + 32] : null;
            $this->pastor_baptism = ($row[$startcol + 33] !== null) ? (string) $row[$startcol + 33] : null;
            $this->date_baptism = ($row[$startcol + 34] !== null) ? (string) $row[$startcol + 34] : null;
            $this->id_enterprise = ($row[$startcol + 35] !== null) ? (int) $row[$startcol + 35] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 36; // 36 = SysUsersPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating SysUsers object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSysRoles !== null && $this->id_rol !== $this->aSysRoles->getIdRol()) {
            $this->aSysRoles = null;
        }
        if ($this->aSysFiles !== null && $this->id_photo !== $this->aSysFiles->getIdFile()) {
            $this->aSysFiles = null;
        }
        if ($this->aSysUsersRelatedByUserModified !== null && $this->user_modified !== $this->aSysUsersRelatedByUserModified->getIdUser()) {
            $this->aSysUsersRelatedByUserModified = null;
        }
        if ($this->aSysEnterprisesRelatedByIdEnterprise !== null && $this->id_enterprise !== $this->aSysEnterprisesRelatedByIdEnterprise->getIdEnterprise()) {
            $this->aSysEnterprisesRelatedByIdEnterprise = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SysUsersPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SysUsersPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSysEnterprisesRelatedByIdEnterprise = null;
            $this->aSysRoles = null;
            $this->aSysFiles = null;
            $this->aSysUsersRelatedByUserModified = null;
            $this->collCmsArticlessRelatedByIdUser = null;

            $this->collCmsArticlessRelatedByUserModified = null;

            $this->collCmsPagess = null;

            $this->collCmsVideosXArticles = null;

            $this->collCmsVideosXPages = null;

            $this->collSysChatssRelatedByIdSender = null;

            $this->collSysChatssRelatedByIdReceiver = null;

            $this->collSysEnterprisessRelatedByUserModified = null;

            $this->collSysNotificationssRelatedByIdSender = null;

            $this->collSysNotificationssRelatedByIdReceiver = null;

            $this->collSysPagess = null;

            $this->collSysUserssRelatedByIdUser = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SysUsersPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SysUsersQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SysUsersPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SysUsersPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSysEnterprisesRelatedByIdEnterprise !== null) {
                if ($this->aSysEnterprisesRelatedByIdEnterprise->isModified() || $this->aSysEnterprisesRelatedByIdEnterprise->isNew()) {
                    $affectedRows += $this->aSysEnterprisesRelatedByIdEnterprise->save($con);
                }
                $this->setSysEnterprisesRelatedByIdEnterprise($this->aSysEnterprisesRelatedByIdEnterprise);
            }

            if ($this->aSysRoles !== null) {
                if ($this->aSysRoles->isModified() || $this->aSysRoles->isNew()) {
                    $affectedRows += $this->aSysRoles->save($con);
                }
                $this->setSysRoles($this->aSysRoles);
            }

            if ($this->aSysFiles !== null) {
                if ($this->aSysFiles->isModified() || $this->aSysFiles->isNew()) {
                    $affectedRows += $this->aSysFiles->save($con);
                }
                $this->setSysFiles($this->aSysFiles);
            }

            if ($this->aSysUsersRelatedByUserModified !== null) {
                if ($this->aSysUsersRelatedByUserModified->isModified() || $this->aSysUsersRelatedByUserModified->isNew()) {
                    $affectedRows += $this->aSysUsersRelatedByUserModified->save($con);
                }
                $this->setSysUsersRelatedByUserModified($this->aSysUsersRelatedByUserModified);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->cmsArticlessRelatedByIdUserScheduledForDeletion !== null) {
                if (!$this->cmsArticlessRelatedByIdUserScheduledForDeletion->isEmpty()) {
                    foreach ($this->cmsArticlessRelatedByIdUserScheduledForDeletion as $cmsArticlesRelatedByIdUser) {
                        // need to save related object because we set the relation to null
                        $cmsArticlesRelatedByIdUser->save($con);
                    }
                    $this->cmsArticlessRelatedByIdUserScheduledForDeletion = null;
                }
            }

            if ($this->collCmsArticlessRelatedByIdUser !== null) {
                foreach ($this->collCmsArticlessRelatedByIdUser as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->cmsArticlessRelatedByUserModifiedScheduledForDeletion !== null) {
                if (!$this->cmsArticlessRelatedByUserModifiedScheduledForDeletion->isEmpty()) {
                    foreach ($this->cmsArticlessRelatedByUserModifiedScheduledForDeletion as $cmsArticlesRelatedByUserModified) {
                        // need to save related object because we set the relation to null
                        $cmsArticlesRelatedByUserModified->save($con);
                    }
                    $this->cmsArticlessRelatedByUserModifiedScheduledForDeletion = null;
                }
            }

            if ($this->collCmsArticlessRelatedByUserModified !== null) {
                foreach ($this->collCmsArticlessRelatedByUserModified as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->cmsPagessScheduledForDeletion !== null) {
                if (!$this->cmsPagessScheduledForDeletion->isEmpty()) {
                    foreach ($this->cmsPagessScheduledForDeletion as $cmsPages) {
                        // need to save related object because we set the relation to null
                        $cmsPages->save($con);
                    }
                    $this->cmsPagessScheduledForDeletion = null;
                }
            }

            if ($this->collCmsPagess !== null) {
                foreach ($this->collCmsPagess as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->cmsVideosXArticlesScheduledForDeletion !== null) {
                if (!$this->cmsVideosXArticlesScheduledForDeletion->isEmpty()) {
                    foreach ($this->cmsVideosXArticlesScheduledForDeletion as $cmsVideosXArticle) {
                        // need to save related object because we set the relation to null
                        $cmsVideosXArticle->save($con);
                    }
                    $this->cmsVideosXArticlesScheduledForDeletion = null;
                }
            }

            if ($this->collCmsVideosXArticles !== null) {
                foreach ($this->collCmsVideosXArticles as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->cmsVideosXPagesScheduledForDeletion !== null) {
                if (!$this->cmsVideosXPagesScheduledForDeletion->isEmpty()) {
                    foreach ($this->cmsVideosXPagesScheduledForDeletion as $cmsVideosXPage) {
                        // need to save related object because we set the relation to null
                        $cmsVideosXPage->save($con);
                    }
                    $this->cmsVideosXPagesScheduledForDeletion = null;
                }
            }

            if ($this->collCmsVideosXPages !== null) {
                foreach ($this->collCmsVideosXPages as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sysChatssRelatedByIdSenderScheduledForDeletion !== null) {
                if (!$this->sysChatssRelatedByIdSenderScheduledForDeletion->isEmpty()) {
                    foreach ($this->sysChatssRelatedByIdSenderScheduledForDeletion as $sysChatsRelatedByIdSender) {
                        // need to save related object because we set the relation to null
                        $sysChatsRelatedByIdSender->save($con);
                    }
                    $this->sysChatssRelatedByIdSenderScheduledForDeletion = null;
                }
            }

            if ($this->collSysChatssRelatedByIdSender !== null) {
                foreach ($this->collSysChatssRelatedByIdSender as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sysChatssRelatedByIdReceiverScheduledForDeletion !== null) {
                if (!$this->sysChatssRelatedByIdReceiverScheduledForDeletion->isEmpty()) {
                    foreach ($this->sysChatssRelatedByIdReceiverScheduledForDeletion as $sysChatsRelatedByIdReceiver) {
                        // need to save related object because we set the relation to null
                        $sysChatsRelatedByIdReceiver->save($con);
                    }
                    $this->sysChatssRelatedByIdReceiverScheduledForDeletion = null;
                }
            }

            if ($this->collSysChatssRelatedByIdReceiver !== null) {
                foreach ($this->collSysChatssRelatedByIdReceiver as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sysEnterprisessRelatedByUserModifiedScheduledForDeletion !== null) {
                if (!$this->sysEnterprisessRelatedByUserModifiedScheduledForDeletion->isEmpty()) {
                    foreach ($this->sysEnterprisessRelatedByUserModifiedScheduledForDeletion as $sysEnterprisesRelatedByUserModified) {
                        // need to save related object because we set the relation to null
                        $sysEnterprisesRelatedByUserModified->save($con);
                    }
                    $this->sysEnterprisessRelatedByUserModifiedScheduledForDeletion = null;
                }
            }

            if ($this->collSysEnterprisessRelatedByUserModified !== null) {
                foreach ($this->collSysEnterprisessRelatedByUserModified as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sysNotificationssRelatedByIdSenderScheduledForDeletion !== null) {
                if (!$this->sysNotificationssRelatedByIdSenderScheduledForDeletion->isEmpty()) {
                    foreach ($this->sysNotificationssRelatedByIdSenderScheduledForDeletion as $sysNotificationsRelatedByIdSender) {
                        // need to save related object because we set the relation to null
                        $sysNotificationsRelatedByIdSender->save($con);
                    }
                    $this->sysNotificationssRelatedByIdSenderScheduledForDeletion = null;
                }
            }

            if ($this->collSysNotificationssRelatedByIdSender !== null) {
                foreach ($this->collSysNotificationssRelatedByIdSender as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sysNotificationssRelatedByIdReceiverScheduledForDeletion !== null) {
                if (!$this->sysNotificationssRelatedByIdReceiverScheduledForDeletion->isEmpty()) {
                    foreach ($this->sysNotificationssRelatedByIdReceiverScheduledForDeletion as $sysNotificationsRelatedByIdReceiver) {
                        // need to save related object because we set the relation to null
                        $sysNotificationsRelatedByIdReceiver->save($con);
                    }
                    $this->sysNotificationssRelatedByIdReceiverScheduledForDeletion = null;
                }
            }

            if ($this->collSysNotificationssRelatedByIdReceiver !== null) {
                foreach ($this->collSysNotificationssRelatedByIdReceiver as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sysPagessScheduledForDeletion !== null) {
                if (!$this->sysPagessScheduledForDeletion->isEmpty()) {
                    foreach ($this->sysPagessScheduledForDeletion as $sysPages) {
                        // need to save related object because we set the relation to null
                        $sysPages->save($con);
                    }
                    $this->sysPagessScheduledForDeletion = null;
                }
            }

            if ($this->collSysPagess !== null) {
                foreach ($this->collSysPagess as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sysUserssRelatedByIdUserScheduledForDeletion !== null) {
                if (!$this->sysUserssRelatedByIdUserScheduledForDeletion->isEmpty()) {
                    foreach ($this->sysUserssRelatedByIdUserScheduledForDeletion as $sysUsersRelatedByIdUser) {
                        // need to save related object because we set the relation to null
                        $sysUsersRelatedByIdUser->save($con);
                    }
                    $this->sysUserssRelatedByIdUserScheduledForDeletion = null;
                }
            }

            if ($this->collSysUserssRelatedByIdUser !== null) {
                foreach ($this->collSysUserssRelatedByIdUser as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = SysUsersPeer::ID_USER;
        if (null !== $this->id_user) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SysUsersPeer::ID_USER . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SysUsersPeer::ID_USER)) {
            $modifiedColumns[':p' . $index++]  = '`id_user`';
        }
        if ($this->isColumnModified(SysUsersPeer::USERNAME)) {
            $modifiedColumns[':p' . $index++]  = '`username`';
        }
        if ($this->isColumnModified(SysUsersPeer::PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = '`password`';
        }
        if ($this->isColumnModified(SysUsersPeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }
        if ($this->isColumnModified(SysUsersPeer::FIRST_NAME)) {
            $modifiedColumns[':p' . $index++]  = '`first_name`';
        }
        if ($this->isColumnModified(SysUsersPeer::LAST_NAME)) {
            $modifiedColumns[':p' . $index++]  = '`last_name`';
        }
        if ($this->isColumnModified(SysUsersPeer::STATE)) {
            $modifiedColumns[':p' . $index++]  = '`state`';
        }
        if ($this->isColumnModified(SysUsersPeer::ID_ROL)) {
            $modifiedColumns[':p' . $index++]  = '`id_rol`';
        }
        if ($this->isColumnModified(SysUsersPeer::ID_PHOTO)) {
            $modifiedColumns[':p' . $index++]  = '`id_photo`';
        }
        if ($this->isColumnModified(SysUsersPeer::CREATED)) {
            $modifiedColumns[':p' . $index++]  = '`created`';
        }
        if ($this->isColumnModified(SysUsersPeer::MODIFIED)) {
            $modifiedColumns[':p' . $index++]  = '`modified`';
        }
        if ($this->isColumnModified(SysUsersPeer::USER_MODIFIED)) {
            $modifiedColumns[':p' . $index++]  = '`user_modified`';
        }
        if ($this->isColumnModified(SysUsersPeer::LANG_CODE)) {
            $modifiedColumns[':p' . $index++]  = '`lang_code`';
        }
        if ($this->isColumnModified(SysUsersPeer::PHONE)) {
            $modifiedColumns[':p' . $index++]  = '`phone`';
        }
        if ($this->isColumnModified(SysUsersPeer::MOBILE)) {
            $modifiedColumns[':p' . $index++]  = '`mobile`';
        }
        if ($this->isColumnModified(SysUsersPeer::ID_TIME_ZONE)) {
            $modifiedColumns[':p' . $index++]  = '`id_time_zone`';
        }
        if ($this->isColumnModified(SysUsersPeer::ID_CITY_BIRTHDAY)) {
            $modifiedColumns[':p' . $index++]  = '`id_city_birthday`';
        }
        if ($this->isColumnModified(SysUsersPeer::ID_COUNTRY_BIRTHDAY)) {
            $modifiedColumns[':p' . $index++]  = '`id_country_birthday`';
        }
        if ($this->isColumnModified(SysUsersPeer::ID_CITY_ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = '`id_city_address`';
        }
        if ($this->isColumnModified(SysUsersPeer::ID_COUNTRY_ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = '`id_country_address`';
        }
        if ($this->isColumnModified(SysUsersPeer::GENDER)) {
            $modifiedColumns[':p' . $index++]  = '`gender`';
        }
        if ($this->isColumnModified(SysUsersPeer::BIRTHDAY)) {
            $modifiedColumns[':p' . $index++]  = '`birthday`';
        }
        if ($this->isColumnModified(SysUsersPeer::BIRTHPLACE)) {
            $modifiedColumns[':p' . $index++]  = '`birthplace`';
        }
        if ($this->isColumnModified(SysUsersPeer::HASH)) {
            $modifiedColumns[':p' . $index++]  = '`hash`';
        }
        if ($this->isColumnModified(SysUsersPeer::TOUR)) {
            $modifiedColumns[':p' . $index++]  = '`tour`';
        }
        if ($this->isColumnModified(SysUsersPeer::MARITAL_STATUS)) {
            $modifiedColumns[':p' . $index++]  = '`marital_status`';
        }
        if ($this->isColumnModified(SysUsersPeer::LEVEL_EDUCATION)) {
            $modifiedColumns[':p' . $index++]  = '`level_education`';
        }
        if ($this->isColumnModified(SysUsersPeer::OCCUPATION)) {
            $modifiedColumns[':p' . $index++]  = '`occupation`';
        }
        if ($this->isColumnModified(SysUsersPeer::DEPARTMENT_ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = '`department_address`';
        }
        if ($this->isColumnModified(SysUsersPeer::DEPARTMENT_BIRTHDAY)) {
            $modifiedColumns[':p' . $index++]  = '`department_birthday`';
        }
        if ($this->isColumnModified(SysUsersPeer::ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = '`address`';
        }
        if ($this->isColumnModified(SysUsersPeer::ID_CHURCH)) {
            $modifiedColumns[':p' . $index++]  = '`id_church`';
        }
        if ($this->isColumnModified(SysUsersPeer::MEMBER)) {
            $modifiedColumns[':p' . $index++]  = '`member`';
        }
        if ($this->isColumnModified(SysUsersPeer::PASTOR_BAPTISM)) {
            $modifiedColumns[':p' . $index++]  = '`pastor_baptism`';
        }
        if ($this->isColumnModified(SysUsersPeer::DATE_BAPTISM)) {
            $modifiedColumns[':p' . $index++]  = '`date_baptism`';
        }
        if ($this->isColumnModified(SysUsersPeer::ID_ENTERPRISE)) {
            $modifiedColumns[':p' . $index++]  = '`id_enterprise`';
        }

        $sql = sprintf(
            'INSERT INTO `sys_users` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_user`':
                        $stmt->bindValue($identifier, $this->id_user, PDO::PARAM_INT);
                        break;
                    case '`username`':
                        $stmt->bindValue($identifier, $this->username, PDO::PARAM_STR);
                        break;
                    case '`password`':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`first_name`':
                        $stmt->bindValue($identifier, $this->first_name, PDO::PARAM_STR);
                        break;
                    case '`last_name`':
                        $stmt->bindValue($identifier, $this->last_name, PDO::PARAM_STR);
                        break;
                    case '`state`':
                        $stmt->bindValue($identifier, $this->state, PDO::PARAM_STR);
                        break;
                    case '`id_rol`':
                        $stmt->bindValue($identifier, $this->id_rol, PDO::PARAM_INT);
                        break;
                    case '`id_photo`':
                        $stmt->bindValue($identifier, $this->id_photo, PDO::PARAM_INT);
                        break;
                    case '`created`':
                        $stmt->bindValue($identifier, $this->created, PDO::PARAM_STR);
                        break;
                    case '`modified`':
                        $stmt->bindValue($identifier, $this->modified, PDO::PARAM_STR);
                        break;
                    case '`user_modified`':
                        $stmt->bindValue($identifier, $this->user_modified, PDO::PARAM_INT);
                        break;
                    case '`lang_code`':
                        $stmt->bindValue($identifier, $this->lang_code, PDO::PARAM_STR);
                        break;
                    case '`phone`':
                        $stmt->bindValue($identifier, $this->phone, PDO::PARAM_STR);
                        break;
                    case '`mobile`':
                        $stmt->bindValue($identifier, $this->mobile, PDO::PARAM_STR);
                        break;
                    case '`id_time_zone`':
                        $stmt->bindValue($identifier, $this->id_time_zone, PDO::PARAM_INT);
                        break;
                    case '`id_city_birthday`':
                        $stmt->bindValue($identifier, $this->id_city_birthday, PDO::PARAM_INT);
                        break;
                    case '`id_country_birthday`':
                        $stmt->bindValue($identifier, $this->id_country_birthday, PDO::PARAM_INT);
                        break;
                    case '`id_city_address`':
                        $stmt->bindValue($identifier, $this->id_city_address, PDO::PARAM_INT);
                        break;
                    case '`id_country_address`':
                        $stmt->bindValue($identifier, $this->id_country_address, PDO::PARAM_INT);
                        break;
                    case '`gender`':
                        $stmt->bindValue($identifier, $this->gender, PDO::PARAM_STR);
                        break;
                    case '`birthday`':
                        $stmt->bindValue($identifier, $this->birthday, PDO::PARAM_STR);
                        break;
                    case '`birthplace`':
                        $stmt->bindValue($identifier, $this->birthplace, PDO::PARAM_STR);
                        break;
                    case '`hash`':
                        $stmt->bindValue($identifier, $this->hash, PDO::PARAM_STR);
                        break;
                    case '`tour`':
                        $stmt->bindValue($identifier, $this->tour, PDO::PARAM_STR);
                        break;
                    case '`marital_status`':
                        $stmt->bindValue($identifier, $this->marital_status, PDO::PARAM_STR);
                        break;
                    case '`level_education`':
                        $stmt->bindValue($identifier, $this->level_education, PDO::PARAM_STR);
                        break;
                    case '`occupation`':
                        $stmt->bindValue($identifier, $this->occupation, PDO::PARAM_STR);
                        break;
                    case '`department_address`':
                        $stmt->bindValue($identifier, $this->department_address, PDO::PARAM_STR);
                        break;
                    case '`department_birthday`':
                        $stmt->bindValue($identifier, $this->department_birthday, PDO::PARAM_STR);
                        break;
                    case '`address`':
                        $stmt->bindValue($identifier, $this->address, PDO::PARAM_STR);
                        break;
                    case '`id_church`':
                        $stmt->bindValue($identifier, $this->id_church, PDO::PARAM_INT);
                        break;
                    case '`member`':
                        $stmt->bindValue($identifier, $this->member, PDO::PARAM_STR);
                        break;
                    case '`pastor_baptism`':
                        $stmt->bindValue($identifier, $this->pastor_baptism, PDO::PARAM_STR);
                        break;
                    case '`date_baptism`':
                        $stmt->bindValue($identifier, $this->date_baptism, PDO::PARAM_STR);
                        break;
                    case '`id_enterprise`':
                        $stmt->bindValue($identifier, $this->id_enterprise, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setIdUser($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSysEnterprisesRelatedByIdEnterprise !== null) {
                if (!$this->aSysEnterprisesRelatedByIdEnterprise->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSysEnterprisesRelatedByIdEnterprise->getValidationFailures());
                }
            }

            if ($this->aSysRoles !== null) {
                if (!$this->aSysRoles->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSysRoles->getValidationFailures());
                }
            }

            if ($this->aSysFiles !== null) {
                if (!$this->aSysFiles->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSysFiles->getValidationFailures());
                }
            }

            if ($this->aSysUsersRelatedByUserModified !== null) {
                if (!$this->aSysUsersRelatedByUserModified->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSysUsersRelatedByUserModified->getValidationFailures());
                }
            }


            if (($retval = SysUsersPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCmsArticlessRelatedByIdUser !== null) {
                    foreach ($this->collCmsArticlessRelatedByIdUser as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCmsArticlessRelatedByUserModified !== null) {
                    foreach ($this->collCmsArticlessRelatedByUserModified as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCmsPagess !== null) {
                    foreach ($this->collCmsPagess as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCmsVideosXArticles !== null) {
                    foreach ($this->collCmsVideosXArticles as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCmsVideosXPages !== null) {
                    foreach ($this->collCmsVideosXPages as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSysChatssRelatedByIdSender !== null) {
                    foreach ($this->collSysChatssRelatedByIdSender as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSysChatssRelatedByIdReceiver !== null) {
                    foreach ($this->collSysChatssRelatedByIdReceiver as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSysEnterprisessRelatedByUserModified !== null) {
                    foreach ($this->collSysEnterprisessRelatedByUserModified as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSysNotificationssRelatedByIdSender !== null) {
                    foreach ($this->collSysNotificationssRelatedByIdSender as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSysNotificationssRelatedByIdReceiver !== null) {
                    foreach ($this->collSysNotificationssRelatedByIdReceiver as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSysPagess !== null) {
                    foreach ($this->collSysPagess as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSysUserssRelatedByIdUser !== null) {
                    foreach ($this->collSysUserssRelatedByIdUser as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SysUsersPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdUser();
                break;
            case 1:
                return $this->getUsername();
                break;
            case 2:
                return $this->getPassword();
                break;
            case 3:
                return $this->getEmail();
                break;
            case 4:
                return $this->getFirstName();
                break;
            case 5:
                return $this->getLastName();
                break;
            case 6:
                return $this->getState();
                break;
            case 7:
                return $this->getIdRol();
                break;
            case 8:
                return $this->getIdPhoto();
                break;
            case 9:
                return $this->getCreated();
                break;
            case 10:
                return $this->getModified();
                break;
            case 11:
                return $this->getUserModified();
                break;
            case 12:
                return $this->getLangCode();
                break;
            case 13:
                return $this->getPhone();
                break;
            case 14:
                return $this->getMobile();
                break;
            case 15:
                return $this->getIdTimeZone();
                break;
            case 16:
                return $this->getIdCityBirthday();
                break;
            case 17:
                return $this->getIdCountryBirthday();
                break;
            case 18:
                return $this->getIdCityAddress();
                break;
            case 19:
                return $this->getIdCountryAddress();
                break;
            case 20:
                return $this->getGender();
                break;
            case 21:
                return $this->getBirthday();
                break;
            case 22:
                return $this->getBirthplace();
                break;
            case 23:
                return $this->getHash();
                break;
            case 24:
                return $this->getTour();
                break;
            case 25:
                return $this->getMaritalStatus();
                break;
            case 26:
                return $this->getLevelEducation();
                break;
            case 27:
                return $this->getOccupation();
                break;
            case 28:
                return $this->getDepartmentAddress();
                break;
            case 29:
                return $this->getDepartmentBirthday();
                break;
            case 30:
                return $this->getAddress();
                break;
            case 31:
                return $this->getIdChurch();
                break;
            case 32:
                return $this->getMember();
                break;
            case 33:
                return $this->getPastorBaptism();
                break;
            case 34:
                return $this->getDateBaptism();
                break;
            case 35:
                return $this->getIdEnterprise();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['SysUsers'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SysUsers'][$this->getPrimaryKey()] = true;
        $keys = SysUsersPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdUser(),
            $keys[1] => $this->getUsername(),
            $keys[2] => $this->getPassword(),
            $keys[3] => $this->getEmail(),
            $keys[4] => $this->getFirstName(),
            $keys[5] => $this->getLastName(),
            $keys[6] => $this->getState(),
            $keys[7] => $this->getIdRol(),
            $keys[8] => $this->getIdPhoto(),
            $keys[9] => $this->getCreated(),
            $keys[10] => $this->getModified(),
            $keys[11] => $this->getUserModified(),
            $keys[12] => $this->getLangCode(),
            $keys[13] => $this->getPhone(),
            $keys[14] => $this->getMobile(),
            $keys[15] => $this->getIdTimeZone(),
            $keys[16] => $this->getIdCityBirthday(),
            $keys[17] => $this->getIdCountryBirthday(),
            $keys[18] => $this->getIdCityAddress(),
            $keys[19] => $this->getIdCountryAddress(),
            $keys[20] => $this->getGender(),
            $keys[21] => $this->getBirthday(),
            $keys[22] => $this->getBirthplace(),
            $keys[23] => $this->getHash(),
            $keys[24] => $this->getTour(),
            $keys[25] => $this->getMaritalStatus(),
            $keys[26] => $this->getLevelEducation(),
            $keys[27] => $this->getOccupation(),
            $keys[28] => $this->getDepartmentAddress(),
            $keys[29] => $this->getDepartmentBirthday(),
            $keys[30] => $this->getAddress(),
            $keys[31] => $this->getIdChurch(),
            $keys[32] => $this->getMember(),
            $keys[33] => $this->getPastorBaptism(),
            $keys[34] => $this->getDateBaptism(),
            $keys[35] => $this->getIdEnterprise(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aSysEnterprisesRelatedByIdEnterprise) {
                $result['SysEnterprisesRelatedByIdEnterprise'] = $this->aSysEnterprisesRelatedByIdEnterprise->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSysRoles) {
                $result['SysRoles'] = $this->aSysRoles->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSysFiles) {
                $result['SysFiles'] = $this->aSysFiles->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSysUsersRelatedByUserModified) {
                $result['SysUsersRelatedByUserModified'] = $this->aSysUsersRelatedByUserModified->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCmsArticlessRelatedByIdUser) {
                $result['CmsArticlessRelatedByIdUser'] = $this->collCmsArticlessRelatedByIdUser->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCmsArticlessRelatedByUserModified) {
                $result['CmsArticlessRelatedByUserModified'] = $this->collCmsArticlessRelatedByUserModified->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCmsPagess) {
                $result['CmsPagess'] = $this->collCmsPagess->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCmsVideosXArticles) {
                $result['CmsVideosXArticles'] = $this->collCmsVideosXArticles->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCmsVideosXPages) {
                $result['CmsVideosXPages'] = $this->collCmsVideosXPages->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSysChatssRelatedByIdSender) {
                $result['SysChatssRelatedByIdSender'] = $this->collSysChatssRelatedByIdSender->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSysChatssRelatedByIdReceiver) {
                $result['SysChatssRelatedByIdReceiver'] = $this->collSysChatssRelatedByIdReceiver->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSysEnterprisessRelatedByUserModified) {
                $result['SysEnterprisessRelatedByUserModified'] = $this->collSysEnterprisessRelatedByUserModified->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSysNotificationssRelatedByIdSender) {
                $result['SysNotificationssRelatedByIdSender'] = $this->collSysNotificationssRelatedByIdSender->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSysNotificationssRelatedByIdReceiver) {
                $result['SysNotificationssRelatedByIdReceiver'] = $this->collSysNotificationssRelatedByIdReceiver->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSysPagess) {
                $result['SysPagess'] = $this->collSysPagess->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSysUserssRelatedByIdUser) {
                $result['SysUserssRelatedByIdUser'] = $this->collSysUserssRelatedByIdUser->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SysUsersPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdUser($value);
                break;
            case 1:
                $this->setUsername($value);
                break;
            case 2:
                $this->setPassword($value);
                break;
            case 3:
                $this->setEmail($value);
                break;
            case 4:
                $this->setFirstName($value);
                break;
            case 5:
                $this->setLastName($value);
                break;
            case 6:
                $this->setState($value);
                break;
            case 7:
                $this->setIdRol($value);
                break;
            case 8:
                $this->setIdPhoto($value);
                break;
            case 9:
                $this->setCreated($value);
                break;
            case 10:
                $this->setModified($value);
                break;
            case 11:
                $this->setUserModified($value);
                break;
            case 12:
                $this->setLangCode($value);
                break;
            case 13:
                $this->setPhone($value);
                break;
            case 14:
                $this->setMobile($value);
                break;
            case 15:
                $this->setIdTimeZone($value);
                break;
            case 16:
                $this->setIdCityBirthday($value);
                break;
            case 17:
                $this->setIdCountryBirthday($value);
                break;
            case 18:
                $this->setIdCityAddress($value);
                break;
            case 19:
                $this->setIdCountryAddress($value);
                break;
            case 20:
                $this->setGender($value);
                break;
            case 21:
                $this->setBirthday($value);
                break;
            case 22:
                $this->setBirthplace($value);
                break;
            case 23:
                $this->setHash($value);
                break;
            case 24:
                $this->setTour($value);
                break;
            case 25:
                $this->setMaritalStatus($value);
                break;
            case 26:
                $this->setLevelEducation($value);
                break;
            case 27:
                $this->setOccupation($value);
                break;
            case 28:
                $this->setDepartmentAddress($value);
                break;
            case 29:
                $this->setDepartmentBirthday($value);
                break;
            case 30:
                $this->setAddress($value);
                break;
            case 31:
                $this->setIdChurch($value);
                break;
            case 32:
                $this->setMember($value);
                break;
            case 33:
                $this->setPastorBaptism($value);
                break;
            case 34:
                $this->setDateBaptism($value);
                break;
            case 35:
                $this->setIdEnterprise($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SysUsersPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdUser($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setUsername($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setPassword($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setEmail($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setFirstName($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setLastName($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setState($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setIdRol($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setIdPhoto($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setCreated($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setModified($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setUserModified($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setLangCode($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setPhone($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setMobile($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setIdTimeZone($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setIdCityBirthday($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setIdCountryBirthday($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setIdCityAddress($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setIdCountryAddress($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setGender($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setBirthday($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setBirthplace($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setHash($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setTour($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setMaritalStatus($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setLevelEducation($arr[$keys[26]]);
        if (array_key_exists($keys[27], $arr)) $this->setOccupation($arr[$keys[27]]);
        if (array_key_exists($keys[28], $arr)) $this->setDepartmentAddress($arr[$keys[28]]);
        if (array_key_exists($keys[29], $arr)) $this->setDepartmentBirthday($arr[$keys[29]]);
        if (array_key_exists($keys[30], $arr)) $this->setAddress($arr[$keys[30]]);
        if (array_key_exists($keys[31], $arr)) $this->setIdChurch($arr[$keys[31]]);
        if (array_key_exists($keys[32], $arr)) $this->setMember($arr[$keys[32]]);
        if (array_key_exists($keys[33], $arr)) $this->setPastorBaptism($arr[$keys[33]]);
        if (array_key_exists($keys[34], $arr)) $this->setDateBaptism($arr[$keys[34]]);
        if (array_key_exists($keys[35], $arr)) $this->setIdEnterprise($arr[$keys[35]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SysUsersPeer::DATABASE_NAME);

        if ($this->isColumnModified(SysUsersPeer::ID_USER)) $criteria->add(SysUsersPeer::ID_USER, $this->id_user);
        if ($this->isColumnModified(SysUsersPeer::USERNAME)) $criteria->add(SysUsersPeer::USERNAME, $this->username);
        if ($this->isColumnModified(SysUsersPeer::PASSWORD)) $criteria->add(SysUsersPeer::PASSWORD, $this->password);
        if ($this->isColumnModified(SysUsersPeer::EMAIL)) $criteria->add(SysUsersPeer::EMAIL, $this->email);
        if ($this->isColumnModified(SysUsersPeer::FIRST_NAME)) $criteria->add(SysUsersPeer::FIRST_NAME, $this->first_name);
        if ($this->isColumnModified(SysUsersPeer::LAST_NAME)) $criteria->add(SysUsersPeer::LAST_NAME, $this->last_name);
        if ($this->isColumnModified(SysUsersPeer::STATE)) $criteria->add(SysUsersPeer::STATE, $this->state);
        if ($this->isColumnModified(SysUsersPeer::ID_ROL)) $criteria->add(SysUsersPeer::ID_ROL, $this->id_rol);
        if ($this->isColumnModified(SysUsersPeer::ID_PHOTO)) $criteria->add(SysUsersPeer::ID_PHOTO, $this->id_photo);
        if ($this->isColumnModified(SysUsersPeer::CREATED)) $criteria->add(SysUsersPeer::CREATED, $this->created);
        if ($this->isColumnModified(SysUsersPeer::MODIFIED)) $criteria->add(SysUsersPeer::MODIFIED, $this->modified);
        if ($this->isColumnModified(SysUsersPeer::USER_MODIFIED)) $criteria->add(SysUsersPeer::USER_MODIFIED, $this->user_modified);
        if ($this->isColumnModified(SysUsersPeer::LANG_CODE)) $criteria->add(SysUsersPeer::LANG_CODE, $this->lang_code);
        if ($this->isColumnModified(SysUsersPeer::PHONE)) $criteria->add(SysUsersPeer::PHONE, $this->phone);
        if ($this->isColumnModified(SysUsersPeer::MOBILE)) $criteria->add(SysUsersPeer::MOBILE, $this->mobile);
        if ($this->isColumnModified(SysUsersPeer::ID_TIME_ZONE)) $criteria->add(SysUsersPeer::ID_TIME_ZONE, $this->id_time_zone);
        if ($this->isColumnModified(SysUsersPeer::ID_CITY_BIRTHDAY)) $criteria->add(SysUsersPeer::ID_CITY_BIRTHDAY, $this->id_city_birthday);
        if ($this->isColumnModified(SysUsersPeer::ID_COUNTRY_BIRTHDAY)) $criteria->add(SysUsersPeer::ID_COUNTRY_BIRTHDAY, $this->id_country_birthday);
        if ($this->isColumnModified(SysUsersPeer::ID_CITY_ADDRESS)) $criteria->add(SysUsersPeer::ID_CITY_ADDRESS, $this->id_city_address);
        if ($this->isColumnModified(SysUsersPeer::ID_COUNTRY_ADDRESS)) $criteria->add(SysUsersPeer::ID_COUNTRY_ADDRESS, $this->id_country_address);
        if ($this->isColumnModified(SysUsersPeer::GENDER)) $criteria->add(SysUsersPeer::GENDER, $this->gender);
        if ($this->isColumnModified(SysUsersPeer::BIRTHDAY)) $criteria->add(SysUsersPeer::BIRTHDAY, $this->birthday);
        if ($this->isColumnModified(SysUsersPeer::BIRTHPLACE)) $criteria->add(SysUsersPeer::BIRTHPLACE, $this->birthplace);
        if ($this->isColumnModified(SysUsersPeer::HASH)) $criteria->add(SysUsersPeer::HASH, $this->hash);
        if ($this->isColumnModified(SysUsersPeer::TOUR)) $criteria->add(SysUsersPeer::TOUR, $this->tour);
        if ($this->isColumnModified(SysUsersPeer::MARITAL_STATUS)) $criteria->add(SysUsersPeer::MARITAL_STATUS, $this->marital_status);
        if ($this->isColumnModified(SysUsersPeer::LEVEL_EDUCATION)) $criteria->add(SysUsersPeer::LEVEL_EDUCATION, $this->level_education);
        if ($this->isColumnModified(SysUsersPeer::OCCUPATION)) $criteria->add(SysUsersPeer::OCCUPATION, $this->occupation);
        if ($this->isColumnModified(SysUsersPeer::DEPARTMENT_ADDRESS)) $criteria->add(SysUsersPeer::DEPARTMENT_ADDRESS, $this->department_address);
        if ($this->isColumnModified(SysUsersPeer::DEPARTMENT_BIRTHDAY)) $criteria->add(SysUsersPeer::DEPARTMENT_BIRTHDAY, $this->department_birthday);
        if ($this->isColumnModified(SysUsersPeer::ADDRESS)) $criteria->add(SysUsersPeer::ADDRESS, $this->address);
        if ($this->isColumnModified(SysUsersPeer::ID_CHURCH)) $criteria->add(SysUsersPeer::ID_CHURCH, $this->id_church);
        if ($this->isColumnModified(SysUsersPeer::MEMBER)) $criteria->add(SysUsersPeer::MEMBER, $this->member);
        if ($this->isColumnModified(SysUsersPeer::PASTOR_BAPTISM)) $criteria->add(SysUsersPeer::PASTOR_BAPTISM, $this->pastor_baptism);
        if ($this->isColumnModified(SysUsersPeer::DATE_BAPTISM)) $criteria->add(SysUsersPeer::DATE_BAPTISM, $this->date_baptism);
        if ($this->isColumnModified(SysUsersPeer::ID_ENTERPRISE)) $criteria->add(SysUsersPeer::ID_ENTERPRISE, $this->id_enterprise);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SysUsersPeer::DATABASE_NAME);
        $criteria->add(SysUsersPeer::ID_USER, $this->id_user);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdUser();
    }

    /**
     * Generic method to set the primary key (id_user column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdUser($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdUser();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of SysUsers (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUsername($this->getUsername());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setFirstName($this->getFirstName());
        $copyObj->setLastName($this->getLastName());
        $copyObj->setState($this->getState());
        $copyObj->setIdRol($this->getIdRol());
        $copyObj->setIdPhoto($this->getIdPhoto());
        $copyObj->setCreated($this->getCreated());
        $copyObj->setModified($this->getModified());
        $copyObj->setUserModified($this->getUserModified());
        $copyObj->setLangCode($this->getLangCode());
        $copyObj->setPhone($this->getPhone());
        $copyObj->setMobile($this->getMobile());
        $copyObj->setIdTimeZone($this->getIdTimeZone());
        $copyObj->setIdCityBirthday($this->getIdCityBirthday());
        $copyObj->setIdCountryBirthday($this->getIdCountryBirthday());
        $copyObj->setIdCityAddress($this->getIdCityAddress());
        $copyObj->setIdCountryAddress($this->getIdCountryAddress());
        $copyObj->setGender($this->getGender());
        $copyObj->setBirthday($this->getBirthday());
        $copyObj->setBirthplace($this->getBirthplace());
        $copyObj->setHash($this->getHash());
        $copyObj->setTour($this->getTour());
        $copyObj->setMaritalStatus($this->getMaritalStatus());
        $copyObj->setLevelEducation($this->getLevelEducation());
        $copyObj->setOccupation($this->getOccupation());
        $copyObj->setDepartmentAddress($this->getDepartmentAddress());
        $copyObj->setDepartmentBirthday($this->getDepartmentBirthday());
        $copyObj->setAddress($this->getAddress());
        $copyObj->setIdChurch($this->getIdChurch());
        $copyObj->setMember($this->getMember());
        $copyObj->setPastorBaptism($this->getPastorBaptism());
        $copyObj->setDateBaptism($this->getDateBaptism());
        $copyObj->setIdEnterprise($this->getIdEnterprise());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCmsArticlessRelatedByIdUser() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCmsArticlesRelatedByIdUser($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCmsArticlessRelatedByUserModified() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCmsArticlesRelatedByUserModified($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCmsPagess() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCmsPages($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCmsVideosXArticles() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCmsVideosXArticle($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCmsVideosXPages() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCmsVideosXPage($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSysChatssRelatedByIdSender() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSysChatsRelatedByIdSender($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSysChatssRelatedByIdReceiver() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSysChatsRelatedByIdReceiver($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSysEnterprisessRelatedByUserModified() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSysEnterprisesRelatedByUserModified($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSysNotificationssRelatedByIdSender() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSysNotificationsRelatedByIdSender($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSysNotificationssRelatedByIdReceiver() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSysNotificationsRelatedByIdReceiver($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSysPagess() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSysPages($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSysUserssRelatedByIdUser() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSysUsersRelatedByIdUser($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdUser(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return SysUsers Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SysUsersPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SysUsersPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a SysEnterprises object.
     *
     * @param                  SysEnterprises $v
     * @return SysUsers The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSysEnterprisesRelatedByIdEnterprise(SysEnterprises $v = null)
    {
        if ($v === null) {
            $this->setIdEnterprise(NULL);
        } else {
            $this->setIdEnterprise($v->getIdEnterprise());
        }

        $this->aSysEnterprisesRelatedByIdEnterprise = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SysEnterprises object, it will not be re-added.
        if ($v !== null) {
            $v->addSysUsersRelatedByIdEnterprise($this);
        }


        return $this;
    }


    /**
     * Get the associated SysEnterprises object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SysEnterprises The associated SysEnterprises object.
     * @throws PropelException
     */
    public function getSysEnterprisesRelatedByIdEnterprise(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSysEnterprisesRelatedByIdEnterprise === null && ($this->id_enterprise !== null) && $doQuery) {
            $this->aSysEnterprisesRelatedByIdEnterprise = SysEnterprisesQuery::create()->findPk($this->id_enterprise, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSysEnterprisesRelatedByIdEnterprise->addSysUserssRelatedByIdEnterprise($this);
             */
        }

        return $this->aSysEnterprisesRelatedByIdEnterprise;
    }

    /**
     * Declares an association between this object and a SysRoles object.
     *
     * @param                  SysRoles $v
     * @return SysUsers The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSysRoles(SysRoles $v = null)
    {
        if ($v === null) {
            $this->setIdRol(NULL);
        } else {
            $this->setIdRol($v->getIdRol());
        }

        $this->aSysRoles = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SysRoles object, it will not be re-added.
        if ($v !== null) {
            $v->addSysUsers($this);
        }


        return $this;
    }


    /**
     * Get the associated SysRoles object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SysRoles The associated SysRoles object.
     * @throws PropelException
     */
    public function getSysRoles(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSysRoles === null && ($this->id_rol !== null) && $doQuery) {
            $this->aSysRoles = SysRolesQuery::create()->findPk($this->id_rol, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSysRoles->addSysUserss($this);
             */
        }

        return $this->aSysRoles;
    }

    /**
     * Declares an association between this object and a SysFiles object.
     *
     * @param                  SysFiles $v
     * @return SysUsers The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSysFiles(SysFiles $v = null)
    {
        if ($v === null) {
            $this->setIdPhoto(NULL);
        } else {
            $this->setIdPhoto($v->getIdFile());
        }

        $this->aSysFiles = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SysFiles object, it will not be re-added.
        if ($v !== null) {
            $v->addSysUsers($this);
        }


        return $this;
    }


    /**
     * Get the associated SysFiles object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SysFiles The associated SysFiles object.
     * @throws PropelException
     */
    public function getSysFiles(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSysFiles === null && ($this->id_photo !== null) && $doQuery) {
            $this->aSysFiles = SysFilesQuery::create()->findPk($this->id_photo, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSysFiles->addSysUserss($this);
             */
        }

        return $this->aSysFiles;
    }

    /**
     * Declares an association between this object and a SysUsers object.
     *
     * @param                  SysUsers $v
     * @return SysUsers The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSysUsersRelatedByUserModified(SysUsers $v = null)
    {
        if ($v === null) {
            $this->setUserModified(NULL);
        } else {
            $this->setUserModified($v->getIdUser());
        }

        $this->aSysUsersRelatedByUserModified = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SysUsers object, it will not be re-added.
        if ($v !== null) {
            $v->addSysUsersRelatedByIdUser($this);
        }


        return $this;
    }


    /**
     * Get the associated SysUsers object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SysUsers The associated SysUsers object.
     * @throws PropelException
     */
    public function getSysUsersRelatedByUserModified(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSysUsersRelatedByUserModified === null && ($this->user_modified !== null) && $doQuery) {
            $this->aSysUsersRelatedByUserModified = SysUsersQuery::create()->findPk($this->user_modified, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSysUsersRelatedByUserModified->addSysUserssRelatedByIdUser($this);
             */
        }

        return $this->aSysUsersRelatedByUserModified;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CmsArticlesRelatedByIdUser' == $relationName) {
            $this->initCmsArticlessRelatedByIdUser();
        }
        if ('CmsArticlesRelatedByUserModified' == $relationName) {
            $this->initCmsArticlessRelatedByUserModified();
        }
        if ('CmsPages' == $relationName) {
            $this->initCmsPagess();
        }
        if ('CmsVideosXArticle' == $relationName) {
            $this->initCmsVideosXArticles();
        }
        if ('CmsVideosXPage' == $relationName) {
            $this->initCmsVideosXPages();
        }
        if ('SysChatsRelatedByIdSender' == $relationName) {
            $this->initSysChatssRelatedByIdSender();
        }
        if ('SysChatsRelatedByIdReceiver' == $relationName) {
            $this->initSysChatssRelatedByIdReceiver();
        }
        if ('SysEnterprisesRelatedByUserModified' == $relationName) {
            $this->initSysEnterprisessRelatedByUserModified();
        }
        if ('SysNotificationsRelatedByIdSender' == $relationName) {
            $this->initSysNotificationssRelatedByIdSender();
        }
        if ('SysNotificationsRelatedByIdReceiver' == $relationName) {
            $this->initSysNotificationssRelatedByIdReceiver();
        }
        if ('SysPages' == $relationName) {
            $this->initSysPagess();
        }
        if ('SysUsersRelatedByIdUser' == $relationName) {
            $this->initSysUserssRelatedByIdUser();
        }
    }

    /**
     * Clears out the collCmsArticlessRelatedByIdUser collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SysUsers The current object (for fluent API support)
     * @see        addCmsArticlessRelatedByIdUser()
     */
    public function clearCmsArticlessRelatedByIdUser()
    {
        $this->collCmsArticlessRelatedByIdUser = null; // important to set this to null since that means it is uninitialized
        $this->collCmsArticlessRelatedByIdUserPartial = null;

        return $this;
    }

    /**
     * reset is the collCmsArticlessRelatedByIdUser collection loaded partially
     *
     * @return void
     */
    public function resetPartialCmsArticlessRelatedByIdUser($v = true)
    {
        $this->collCmsArticlessRelatedByIdUserPartial = $v;
    }

    /**
     * Initializes the collCmsArticlessRelatedByIdUser collection.
     *
     * By default this just sets the collCmsArticlessRelatedByIdUser collection to an empty array (like clearcollCmsArticlessRelatedByIdUser());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCmsArticlessRelatedByIdUser($overrideExisting = true)
    {
        if (null !== $this->collCmsArticlessRelatedByIdUser && !$overrideExisting) {
            return;
        }
        $this->collCmsArticlessRelatedByIdUser = new PropelObjectCollection();
        $this->collCmsArticlessRelatedByIdUser->setModel('CmsArticles');
    }

    /**
     * Gets an array of CmsArticles objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SysUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CmsArticles[] List of CmsArticles objects
     * @throws PropelException
     */
    public function getCmsArticlessRelatedByIdUser($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCmsArticlessRelatedByIdUserPartial && !$this->isNew();
        if (null === $this->collCmsArticlessRelatedByIdUser || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCmsArticlessRelatedByIdUser) {
                // return empty collection
                $this->initCmsArticlessRelatedByIdUser();
            } else {
                $collCmsArticlessRelatedByIdUser = CmsArticlesQuery::create(null, $criteria)
                    ->filterBySysUsersRelatedByIdUser($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCmsArticlessRelatedByIdUserPartial && count($collCmsArticlessRelatedByIdUser)) {
                      $this->initCmsArticlessRelatedByIdUser(false);

                      foreach ($collCmsArticlessRelatedByIdUser as $obj) {
                        if (false == $this->collCmsArticlessRelatedByIdUser->contains($obj)) {
                          $this->collCmsArticlessRelatedByIdUser->append($obj);
                        }
                      }

                      $this->collCmsArticlessRelatedByIdUserPartial = true;
                    }

                    $collCmsArticlessRelatedByIdUser->getInternalIterator()->rewind();

                    return $collCmsArticlessRelatedByIdUser;
                }

                if ($partial && $this->collCmsArticlessRelatedByIdUser) {
                    foreach ($this->collCmsArticlessRelatedByIdUser as $obj) {
                        if ($obj->isNew()) {
                            $collCmsArticlessRelatedByIdUser[] = $obj;
                        }
                    }
                }

                $this->collCmsArticlessRelatedByIdUser = $collCmsArticlessRelatedByIdUser;
                $this->collCmsArticlessRelatedByIdUserPartial = false;
            }
        }

        return $this->collCmsArticlessRelatedByIdUser;
    }

    /**
     * Sets a collection of CmsArticlesRelatedByIdUser objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $cmsArticlessRelatedByIdUser A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SysUsers The current object (for fluent API support)
     */
    public function setCmsArticlessRelatedByIdUser(PropelCollection $cmsArticlessRelatedByIdUser, PropelPDO $con = null)
    {
        $cmsArticlessRelatedByIdUserToDelete = $this->getCmsArticlessRelatedByIdUser(new Criteria(), $con)->diff($cmsArticlessRelatedByIdUser);


        $this->cmsArticlessRelatedByIdUserScheduledForDeletion = $cmsArticlessRelatedByIdUserToDelete;

        foreach ($cmsArticlessRelatedByIdUserToDelete as $cmsArticlesRelatedByIdUserRemoved) {
            $cmsArticlesRelatedByIdUserRemoved->setSysUsersRelatedByIdUser(null);
        }

        $this->collCmsArticlessRelatedByIdUser = null;
        foreach ($cmsArticlessRelatedByIdUser as $cmsArticlesRelatedByIdUser) {
            $this->addCmsArticlesRelatedByIdUser($cmsArticlesRelatedByIdUser);
        }

        $this->collCmsArticlessRelatedByIdUser = $cmsArticlessRelatedByIdUser;
        $this->collCmsArticlessRelatedByIdUserPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CmsArticles objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CmsArticles objects.
     * @throws PropelException
     */
    public function countCmsArticlessRelatedByIdUser(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCmsArticlessRelatedByIdUserPartial && !$this->isNew();
        if (null === $this->collCmsArticlessRelatedByIdUser || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCmsArticlessRelatedByIdUser) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCmsArticlessRelatedByIdUser());
            }
            $query = CmsArticlesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySysUsersRelatedByIdUser($this)
                ->count($con);
        }

        return count($this->collCmsArticlessRelatedByIdUser);
    }

    /**
     * Method called to associate a CmsArticles object to this object
     * through the CmsArticles foreign key attribute.
     *
     * @param    CmsArticles $l CmsArticles
     * @return SysUsers The current object (for fluent API support)
     */
    public function addCmsArticlesRelatedByIdUser(CmsArticles $l)
    {
        if ($this->collCmsArticlessRelatedByIdUser === null) {
            $this->initCmsArticlessRelatedByIdUser();
            $this->collCmsArticlessRelatedByIdUserPartial = true;
        }

        if (!in_array($l, $this->collCmsArticlessRelatedByIdUser->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCmsArticlesRelatedByIdUser($l);

            if ($this->cmsArticlessRelatedByIdUserScheduledForDeletion and $this->cmsArticlessRelatedByIdUserScheduledForDeletion->contains($l)) {
                $this->cmsArticlessRelatedByIdUserScheduledForDeletion->remove($this->cmsArticlessRelatedByIdUserScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	CmsArticlesRelatedByIdUser $cmsArticlesRelatedByIdUser The cmsArticlesRelatedByIdUser object to add.
     */
    protected function doAddCmsArticlesRelatedByIdUser($cmsArticlesRelatedByIdUser)
    {
        $this->collCmsArticlessRelatedByIdUser[]= $cmsArticlesRelatedByIdUser;
        $cmsArticlesRelatedByIdUser->setSysUsersRelatedByIdUser($this);
    }

    /**
     * @param	CmsArticlesRelatedByIdUser $cmsArticlesRelatedByIdUser The cmsArticlesRelatedByIdUser object to remove.
     * @return SysUsers The current object (for fluent API support)
     */
    public function removeCmsArticlesRelatedByIdUser($cmsArticlesRelatedByIdUser)
    {
        if ($this->getCmsArticlessRelatedByIdUser()->contains($cmsArticlesRelatedByIdUser)) {
            $this->collCmsArticlessRelatedByIdUser->remove($this->collCmsArticlessRelatedByIdUser->search($cmsArticlesRelatedByIdUser));
            if (null === $this->cmsArticlessRelatedByIdUserScheduledForDeletion) {
                $this->cmsArticlessRelatedByIdUserScheduledForDeletion = clone $this->collCmsArticlessRelatedByIdUser;
                $this->cmsArticlessRelatedByIdUserScheduledForDeletion->clear();
            }
            $this->cmsArticlessRelatedByIdUserScheduledForDeletion[]= $cmsArticlesRelatedByIdUser;
            $cmsArticlesRelatedByIdUser->setSysUsersRelatedByIdUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysUsers is new, it will return
     * an empty collection; or if this SysUsers has previously
     * been saved, it will retrieve related CmsArticlessRelatedByIdUser from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysUsers.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsArticles[] List of CmsArticles objects
     */
    public function getCmsArticlessRelatedByIdUserJoinSysEnterprises($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsArticlesQuery::create(null, $criteria);
        $query->joinWith('SysEnterprises', $join_behavior);

        return $this->getCmsArticlessRelatedByIdUser($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysUsers is new, it will return
     * an empty collection; or if this SysUsers has previously
     * been saved, it will retrieve related CmsArticlessRelatedByIdUser from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysUsers.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsArticles[] List of CmsArticles objects
     */
    public function getCmsArticlessRelatedByIdUserJoinCmsPages($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsArticlesQuery::create(null, $criteria);
        $query->joinWith('CmsPages', $join_behavior);

        return $this->getCmsArticlessRelatedByIdUser($query, $con);
    }

    /**
     * Clears out the collCmsArticlessRelatedByUserModified collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SysUsers The current object (for fluent API support)
     * @see        addCmsArticlessRelatedByUserModified()
     */
    public function clearCmsArticlessRelatedByUserModified()
    {
        $this->collCmsArticlessRelatedByUserModified = null; // important to set this to null since that means it is uninitialized
        $this->collCmsArticlessRelatedByUserModifiedPartial = null;

        return $this;
    }

    /**
     * reset is the collCmsArticlessRelatedByUserModified collection loaded partially
     *
     * @return void
     */
    public function resetPartialCmsArticlessRelatedByUserModified($v = true)
    {
        $this->collCmsArticlessRelatedByUserModifiedPartial = $v;
    }

    /**
     * Initializes the collCmsArticlessRelatedByUserModified collection.
     *
     * By default this just sets the collCmsArticlessRelatedByUserModified collection to an empty array (like clearcollCmsArticlessRelatedByUserModified());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCmsArticlessRelatedByUserModified($overrideExisting = true)
    {
        if (null !== $this->collCmsArticlessRelatedByUserModified && !$overrideExisting) {
            return;
        }
        $this->collCmsArticlessRelatedByUserModified = new PropelObjectCollection();
        $this->collCmsArticlessRelatedByUserModified->setModel('CmsArticles');
    }

    /**
     * Gets an array of CmsArticles objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SysUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CmsArticles[] List of CmsArticles objects
     * @throws PropelException
     */
    public function getCmsArticlessRelatedByUserModified($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCmsArticlessRelatedByUserModifiedPartial && !$this->isNew();
        if (null === $this->collCmsArticlessRelatedByUserModified || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCmsArticlessRelatedByUserModified) {
                // return empty collection
                $this->initCmsArticlessRelatedByUserModified();
            } else {
                $collCmsArticlessRelatedByUserModified = CmsArticlesQuery::create(null, $criteria)
                    ->filterBySysUsersRelatedByUserModified($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCmsArticlessRelatedByUserModifiedPartial && count($collCmsArticlessRelatedByUserModified)) {
                      $this->initCmsArticlessRelatedByUserModified(false);

                      foreach ($collCmsArticlessRelatedByUserModified as $obj) {
                        if (false == $this->collCmsArticlessRelatedByUserModified->contains($obj)) {
                          $this->collCmsArticlessRelatedByUserModified->append($obj);
                        }
                      }

                      $this->collCmsArticlessRelatedByUserModifiedPartial = true;
                    }

                    $collCmsArticlessRelatedByUserModified->getInternalIterator()->rewind();

                    return $collCmsArticlessRelatedByUserModified;
                }

                if ($partial && $this->collCmsArticlessRelatedByUserModified) {
                    foreach ($this->collCmsArticlessRelatedByUserModified as $obj) {
                        if ($obj->isNew()) {
                            $collCmsArticlessRelatedByUserModified[] = $obj;
                        }
                    }
                }

                $this->collCmsArticlessRelatedByUserModified = $collCmsArticlessRelatedByUserModified;
                $this->collCmsArticlessRelatedByUserModifiedPartial = false;
            }
        }

        return $this->collCmsArticlessRelatedByUserModified;
    }

    /**
     * Sets a collection of CmsArticlesRelatedByUserModified objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $cmsArticlessRelatedByUserModified A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SysUsers The current object (for fluent API support)
     */
    public function setCmsArticlessRelatedByUserModified(PropelCollection $cmsArticlessRelatedByUserModified, PropelPDO $con = null)
    {
        $cmsArticlessRelatedByUserModifiedToDelete = $this->getCmsArticlessRelatedByUserModified(new Criteria(), $con)->diff($cmsArticlessRelatedByUserModified);


        $this->cmsArticlessRelatedByUserModifiedScheduledForDeletion = $cmsArticlessRelatedByUserModifiedToDelete;

        foreach ($cmsArticlessRelatedByUserModifiedToDelete as $cmsArticlesRelatedByUserModifiedRemoved) {
            $cmsArticlesRelatedByUserModifiedRemoved->setSysUsersRelatedByUserModified(null);
        }

        $this->collCmsArticlessRelatedByUserModified = null;
        foreach ($cmsArticlessRelatedByUserModified as $cmsArticlesRelatedByUserModified) {
            $this->addCmsArticlesRelatedByUserModified($cmsArticlesRelatedByUserModified);
        }

        $this->collCmsArticlessRelatedByUserModified = $cmsArticlessRelatedByUserModified;
        $this->collCmsArticlessRelatedByUserModifiedPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CmsArticles objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CmsArticles objects.
     * @throws PropelException
     */
    public function countCmsArticlessRelatedByUserModified(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCmsArticlessRelatedByUserModifiedPartial && !$this->isNew();
        if (null === $this->collCmsArticlessRelatedByUserModified || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCmsArticlessRelatedByUserModified) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCmsArticlessRelatedByUserModified());
            }
            $query = CmsArticlesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySysUsersRelatedByUserModified($this)
                ->count($con);
        }

        return count($this->collCmsArticlessRelatedByUserModified);
    }

    /**
     * Method called to associate a CmsArticles object to this object
     * through the CmsArticles foreign key attribute.
     *
     * @param    CmsArticles $l CmsArticles
     * @return SysUsers The current object (for fluent API support)
     */
    public function addCmsArticlesRelatedByUserModified(CmsArticles $l)
    {
        if ($this->collCmsArticlessRelatedByUserModified === null) {
            $this->initCmsArticlessRelatedByUserModified();
            $this->collCmsArticlessRelatedByUserModifiedPartial = true;
        }

        if (!in_array($l, $this->collCmsArticlessRelatedByUserModified->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCmsArticlesRelatedByUserModified($l);

            if ($this->cmsArticlessRelatedByUserModifiedScheduledForDeletion and $this->cmsArticlessRelatedByUserModifiedScheduledForDeletion->contains($l)) {
                $this->cmsArticlessRelatedByUserModifiedScheduledForDeletion->remove($this->cmsArticlessRelatedByUserModifiedScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	CmsArticlesRelatedByUserModified $cmsArticlesRelatedByUserModified The cmsArticlesRelatedByUserModified object to add.
     */
    protected function doAddCmsArticlesRelatedByUserModified($cmsArticlesRelatedByUserModified)
    {
        $this->collCmsArticlessRelatedByUserModified[]= $cmsArticlesRelatedByUserModified;
        $cmsArticlesRelatedByUserModified->setSysUsersRelatedByUserModified($this);
    }

    /**
     * @param	CmsArticlesRelatedByUserModified $cmsArticlesRelatedByUserModified The cmsArticlesRelatedByUserModified object to remove.
     * @return SysUsers The current object (for fluent API support)
     */
    public function removeCmsArticlesRelatedByUserModified($cmsArticlesRelatedByUserModified)
    {
        if ($this->getCmsArticlessRelatedByUserModified()->contains($cmsArticlesRelatedByUserModified)) {
            $this->collCmsArticlessRelatedByUserModified->remove($this->collCmsArticlessRelatedByUserModified->search($cmsArticlesRelatedByUserModified));
            if (null === $this->cmsArticlessRelatedByUserModifiedScheduledForDeletion) {
                $this->cmsArticlessRelatedByUserModifiedScheduledForDeletion = clone $this->collCmsArticlessRelatedByUserModified;
                $this->cmsArticlessRelatedByUserModifiedScheduledForDeletion->clear();
            }
            $this->cmsArticlessRelatedByUserModifiedScheduledForDeletion[]= $cmsArticlesRelatedByUserModified;
            $cmsArticlesRelatedByUserModified->setSysUsersRelatedByUserModified(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysUsers is new, it will return
     * an empty collection; or if this SysUsers has previously
     * been saved, it will retrieve related CmsArticlessRelatedByUserModified from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysUsers.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsArticles[] List of CmsArticles objects
     */
    public function getCmsArticlessRelatedByUserModifiedJoinSysEnterprises($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsArticlesQuery::create(null, $criteria);
        $query->joinWith('SysEnterprises', $join_behavior);

        return $this->getCmsArticlessRelatedByUserModified($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysUsers is new, it will return
     * an empty collection; or if this SysUsers has previously
     * been saved, it will retrieve related CmsArticlessRelatedByUserModified from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysUsers.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsArticles[] List of CmsArticles objects
     */
    public function getCmsArticlessRelatedByUserModifiedJoinCmsPages($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsArticlesQuery::create(null, $criteria);
        $query->joinWith('CmsPages', $join_behavior);

        return $this->getCmsArticlessRelatedByUserModified($query, $con);
    }

    /**
     * Clears out the collCmsPagess collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SysUsers The current object (for fluent API support)
     * @see        addCmsPagess()
     */
    public function clearCmsPagess()
    {
        $this->collCmsPagess = null; // important to set this to null since that means it is uninitialized
        $this->collCmsPagessPartial = null;

        return $this;
    }

    /**
     * reset is the collCmsPagess collection loaded partially
     *
     * @return void
     */
    public function resetPartialCmsPagess($v = true)
    {
        $this->collCmsPagessPartial = $v;
    }

    /**
     * Initializes the collCmsPagess collection.
     *
     * By default this just sets the collCmsPagess collection to an empty array (like clearcollCmsPagess());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCmsPagess($overrideExisting = true)
    {
        if (null !== $this->collCmsPagess && !$overrideExisting) {
            return;
        }
        $this->collCmsPagess = new PropelObjectCollection();
        $this->collCmsPagess->setModel('CmsPages');
    }

    /**
     * Gets an array of CmsPages objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SysUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CmsPages[] List of CmsPages objects
     * @throws PropelException
     */
    public function getCmsPagess($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCmsPagessPartial && !$this->isNew();
        if (null === $this->collCmsPagess || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCmsPagess) {
                // return empty collection
                $this->initCmsPagess();
            } else {
                $collCmsPagess = CmsPagesQuery::create(null, $criteria)
                    ->filterBySysUsers($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCmsPagessPartial && count($collCmsPagess)) {
                      $this->initCmsPagess(false);

                      foreach ($collCmsPagess as $obj) {
                        if (false == $this->collCmsPagess->contains($obj)) {
                          $this->collCmsPagess->append($obj);
                        }
                      }

                      $this->collCmsPagessPartial = true;
                    }

                    $collCmsPagess->getInternalIterator()->rewind();

                    return $collCmsPagess;
                }

                if ($partial && $this->collCmsPagess) {
                    foreach ($this->collCmsPagess as $obj) {
                        if ($obj->isNew()) {
                            $collCmsPagess[] = $obj;
                        }
                    }
                }

                $this->collCmsPagess = $collCmsPagess;
                $this->collCmsPagessPartial = false;
            }
        }

        return $this->collCmsPagess;
    }

    /**
     * Sets a collection of CmsPages objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $cmsPagess A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SysUsers The current object (for fluent API support)
     */
    public function setCmsPagess(PropelCollection $cmsPagess, PropelPDO $con = null)
    {
        $cmsPagessToDelete = $this->getCmsPagess(new Criteria(), $con)->diff($cmsPagess);


        $this->cmsPagessScheduledForDeletion = $cmsPagessToDelete;

        foreach ($cmsPagessToDelete as $cmsPagesRemoved) {
            $cmsPagesRemoved->setSysUsers(null);
        }

        $this->collCmsPagess = null;
        foreach ($cmsPagess as $cmsPages) {
            $this->addCmsPages($cmsPages);
        }

        $this->collCmsPagess = $cmsPagess;
        $this->collCmsPagessPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CmsPages objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CmsPages objects.
     * @throws PropelException
     */
    public function countCmsPagess(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCmsPagessPartial && !$this->isNew();
        if (null === $this->collCmsPagess || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCmsPagess) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCmsPagess());
            }
            $query = CmsPagesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySysUsers($this)
                ->count($con);
        }

        return count($this->collCmsPagess);
    }

    /**
     * Method called to associate a CmsPages object to this object
     * through the CmsPages foreign key attribute.
     *
     * @param    CmsPages $l CmsPages
     * @return SysUsers The current object (for fluent API support)
     */
    public function addCmsPages(CmsPages $l)
    {
        if ($this->collCmsPagess === null) {
            $this->initCmsPagess();
            $this->collCmsPagessPartial = true;
        }

        if (!in_array($l, $this->collCmsPagess->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCmsPages($l);

            if ($this->cmsPagessScheduledForDeletion and $this->cmsPagessScheduledForDeletion->contains($l)) {
                $this->cmsPagessScheduledForDeletion->remove($this->cmsPagessScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	CmsPages $cmsPages The cmsPages object to add.
     */
    protected function doAddCmsPages($cmsPages)
    {
        $this->collCmsPagess[]= $cmsPages;
        $cmsPages->setSysUsers($this);
    }

    /**
     * @param	CmsPages $cmsPages The cmsPages object to remove.
     * @return SysUsers The current object (for fluent API support)
     */
    public function removeCmsPages($cmsPages)
    {
        if ($this->getCmsPagess()->contains($cmsPages)) {
            $this->collCmsPagess->remove($this->collCmsPagess->search($cmsPages));
            if (null === $this->cmsPagessScheduledForDeletion) {
                $this->cmsPagessScheduledForDeletion = clone $this->collCmsPagess;
                $this->cmsPagessScheduledForDeletion->clear();
            }
            $this->cmsPagessScheduledForDeletion[]= $cmsPages;
            $cmsPages->setSysUsers(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysUsers is new, it will return
     * an empty collection; or if this SysUsers has previously
     * been saved, it will retrieve related CmsPagess from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysUsers.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsPages[] List of CmsPages objects
     */
    public function getCmsPagessJoinSysEnterprises($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsPagesQuery::create(null, $criteria);
        $query->joinWith('SysEnterprises', $join_behavior);

        return $this->getCmsPagess($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysUsers is new, it will return
     * an empty collection; or if this SysUsers has previously
     * been saved, it will retrieve related CmsPagess from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysUsers.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsPages[] List of CmsPages objects
     */
    public function getCmsPagessJoinCmsTemplates($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsPagesQuery::create(null, $criteria);
        $query->joinWith('CmsTemplates', $join_behavior);

        return $this->getCmsPagess($query, $con);
    }

    /**
     * Clears out the collCmsVideosXArticles collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SysUsers The current object (for fluent API support)
     * @see        addCmsVideosXArticles()
     */
    public function clearCmsVideosXArticles()
    {
        $this->collCmsVideosXArticles = null; // important to set this to null since that means it is uninitialized
        $this->collCmsVideosXArticlesPartial = null;

        return $this;
    }

    /**
     * reset is the collCmsVideosXArticles collection loaded partially
     *
     * @return void
     */
    public function resetPartialCmsVideosXArticles($v = true)
    {
        $this->collCmsVideosXArticlesPartial = $v;
    }

    /**
     * Initializes the collCmsVideosXArticles collection.
     *
     * By default this just sets the collCmsVideosXArticles collection to an empty array (like clearcollCmsVideosXArticles());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCmsVideosXArticles($overrideExisting = true)
    {
        if (null !== $this->collCmsVideosXArticles && !$overrideExisting) {
            return;
        }
        $this->collCmsVideosXArticles = new PropelObjectCollection();
        $this->collCmsVideosXArticles->setModel('CmsVideosXArticle');
    }

    /**
     * Gets an array of CmsVideosXArticle objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SysUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CmsVideosXArticle[] List of CmsVideosXArticle objects
     * @throws PropelException
     */
    public function getCmsVideosXArticles($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCmsVideosXArticlesPartial && !$this->isNew();
        if (null === $this->collCmsVideosXArticles || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCmsVideosXArticles) {
                // return empty collection
                $this->initCmsVideosXArticles();
            } else {
                $collCmsVideosXArticles = CmsVideosXArticleQuery::create(null, $criteria)
                    ->filterBySysUsers($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCmsVideosXArticlesPartial && count($collCmsVideosXArticles)) {
                      $this->initCmsVideosXArticles(false);

                      foreach ($collCmsVideosXArticles as $obj) {
                        if (false == $this->collCmsVideosXArticles->contains($obj)) {
                          $this->collCmsVideosXArticles->append($obj);
                        }
                      }

                      $this->collCmsVideosXArticlesPartial = true;
                    }

                    $collCmsVideosXArticles->getInternalIterator()->rewind();

                    return $collCmsVideosXArticles;
                }

                if ($partial && $this->collCmsVideosXArticles) {
                    foreach ($this->collCmsVideosXArticles as $obj) {
                        if ($obj->isNew()) {
                            $collCmsVideosXArticles[] = $obj;
                        }
                    }
                }

                $this->collCmsVideosXArticles = $collCmsVideosXArticles;
                $this->collCmsVideosXArticlesPartial = false;
            }
        }

        return $this->collCmsVideosXArticles;
    }

    /**
     * Sets a collection of CmsVideosXArticle objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $cmsVideosXArticles A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SysUsers The current object (for fluent API support)
     */
    public function setCmsVideosXArticles(PropelCollection $cmsVideosXArticles, PropelPDO $con = null)
    {
        $cmsVideosXArticlesToDelete = $this->getCmsVideosXArticles(new Criteria(), $con)->diff($cmsVideosXArticles);


        $this->cmsVideosXArticlesScheduledForDeletion = $cmsVideosXArticlesToDelete;

        foreach ($cmsVideosXArticlesToDelete as $cmsVideosXArticleRemoved) {
            $cmsVideosXArticleRemoved->setSysUsers(null);
        }

        $this->collCmsVideosXArticles = null;
        foreach ($cmsVideosXArticles as $cmsVideosXArticle) {
            $this->addCmsVideosXArticle($cmsVideosXArticle);
        }

        $this->collCmsVideosXArticles = $cmsVideosXArticles;
        $this->collCmsVideosXArticlesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CmsVideosXArticle objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CmsVideosXArticle objects.
     * @throws PropelException
     */
    public function countCmsVideosXArticles(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCmsVideosXArticlesPartial && !$this->isNew();
        if (null === $this->collCmsVideosXArticles || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCmsVideosXArticles) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCmsVideosXArticles());
            }
            $query = CmsVideosXArticleQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySysUsers($this)
                ->count($con);
        }

        return count($this->collCmsVideosXArticles);
    }

    /**
     * Method called to associate a CmsVideosXArticle object to this object
     * through the CmsVideosXArticle foreign key attribute.
     *
     * @param    CmsVideosXArticle $l CmsVideosXArticle
     * @return SysUsers The current object (for fluent API support)
     */
    public function addCmsVideosXArticle(CmsVideosXArticle $l)
    {
        if ($this->collCmsVideosXArticles === null) {
            $this->initCmsVideosXArticles();
            $this->collCmsVideosXArticlesPartial = true;
        }

        if (!in_array($l, $this->collCmsVideosXArticles->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCmsVideosXArticle($l);

            if ($this->cmsVideosXArticlesScheduledForDeletion and $this->cmsVideosXArticlesScheduledForDeletion->contains($l)) {
                $this->cmsVideosXArticlesScheduledForDeletion->remove($this->cmsVideosXArticlesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	CmsVideosXArticle $cmsVideosXArticle The cmsVideosXArticle object to add.
     */
    protected function doAddCmsVideosXArticle($cmsVideosXArticle)
    {
        $this->collCmsVideosXArticles[]= $cmsVideosXArticle;
        $cmsVideosXArticle->setSysUsers($this);
    }

    /**
     * @param	CmsVideosXArticle $cmsVideosXArticle The cmsVideosXArticle object to remove.
     * @return SysUsers The current object (for fluent API support)
     */
    public function removeCmsVideosXArticle($cmsVideosXArticle)
    {
        if ($this->getCmsVideosXArticles()->contains($cmsVideosXArticle)) {
            $this->collCmsVideosXArticles->remove($this->collCmsVideosXArticles->search($cmsVideosXArticle));
            if (null === $this->cmsVideosXArticlesScheduledForDeletion) {
                $this->cmsVideosXArticlesScheduledForDeletion = clone $this->collCmsVideosXArticles;
                $this->cmsVideosXArticlesScheduledForDeletion->clear();
            }
            $this->cmsVideosXArticlesScheduledForDeletion[]= $cmsVideosXArticle;
            $cmsVideosXArticle->setSysUsers(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysUsers is new, it will return
     * an empty collection; or if this SysUsers has previously
     * been saved, it will retrieve related CmsVideosXArticles from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysUsers.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsVideosXArticle[] List of CmsVideosXArticle objects
     */
    public function getCmsVideosXArticlesJoinCmsArticles($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsVideosXArticleQuery::create(null, $criteria);
        $query->joinWith('CmsArticles', $join_behavior);

        return $this->getCmsVideosXArticles($query, $con);
    }

    /**
     * Clears out the collCmsVideosXPages collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SysUsers The current object (for fluent API support)
     * @see        addCmsVideosXPages()
     */
    public function clearCmsVideosXPages()
    {
        $this->collCmsVideosXPages = null; // important to set this to null since that means it is uninitialized
        $this->collCmsVideosXPagesPartial = null;

        return $this;
    }

    /**
     * reset is the collCmsVideosXPages collection loaded partially
     *
     * @return void
     */
    public function resetPartialCmsVideosXPages($v = true)
    {
        $this->collCmsVideosXPagesPartial = $v;
    }

    /**
     * Initializes the collCmsVideosXPages collection.
     *
     * By default this just sets the collCmsVideosXPages collection to an empty array (like clearcollCmsVideosXPages());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCmsVideosXPages($overrideExisting = true)
    {
        if (null !== $this->collCmsVideosXPages && !$overrideExisting) {
            return;
        }
        $this->collCmsVideosXPages = new PropelObjectCollection();
        $this->collCmsVideosXPages->setModel('CmsVideosXPage');
    }

    /**
     * Gets an array of CmsVideosXPage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SysUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CmsVideosXPage[] List of CmsVideosXPage objects
     * @throws PropelException
     */
    public function getCmsVideosXPages($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCmsVideosXPagesPartial && !$this->isNew();
        if (null === $this->collCmsVideosXPages || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCmsVideosXPages) {
                // return empty collection
                $this->initCmsVideosXPages();
            } else {
                $collCmsVideosXPages = CmsVideosXPageQuery::create(null, $criteria)
                    ->filterBySysUsers($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCmsVideosXPagesPartial && count($collCmsVideosXPages)) {
                      $this->initCmsVideosXPages(false);

                      foreach ($collCmsVideosXPages as $obj) {
                        if (false == $this->collCmsVideosXPages->contains($obj)) {
                          $this->collCmsVideosXPages->append($obj);
                        }
                      }

                      $this->collCmsVideosXPagesPartial = true;
                    }

                    $collCmsVideosXPages->getInternalIterator()->rewind();

                    return $collCmsVideosXPages;
                }

                if ($partial && $this->collCmsVideosXPages) {
                    foreach ($this->collCmsVideosXPages as $obj) {
                        if ($obj->isNew()) {
                            $collCmsVideosXPages[] = $obj;
                        }
                    }
                }

                $this->collCmsVideosXPages = $collCmsVideosXPages;
                $this->collCmsVideosXPagesPartial = false;
            }
        }

        return $this->collCmsVideosXPages;
    }

    /**
     * Sets a collection of CmsVideosXPage objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $cmsVideosXPages A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SysUsers The current object (for fluent API support)
     */
    public function setCmsVideosXPages(PropelCollection $cmsVideosXPages, PropelPDO $con = null)
    {
        $cmsVideosXPagesToDelete = $this->getCmsVideosXPages(new Criteria(), $con)->diff($cmsVideosXPages);


        $this->cmsVideosXPagesScheduledForDeletion = $cmsVideosXPagesToDelete;

        foreach ($cmsVideosXPagesToDelete as $cmsVideosXPageRemoved) {
            $cmsVideosXPageRemoved->setSysUsers(null);
        }

        $this->collCmsVideosXPages = null;
        foreach ($cmsVideosXPages as $cmsVideosXPage) {
            $this->addCmsVideosXPage($cmsVideosXPage);
        }

        $this->collCmsVideosXPages = $cmsVideosXPages;
        $this->collCmsVideosXPagesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CmsVideosXPage objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CmsVideosXPage objects.
     * @throws PropelException
     */
    public function countCmsVideosXPages(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCmsVideosXPagesPartial && !$this->isNew();
        if (null === $this->collCmsVideosXPages || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCmsVideosXPages) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCmsVideosXPages());
            }
            $query = CmsVideosXPageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySysUsers($this)
                ->count($con);
        }

        return count($this->collCmsVideosXPages);
    }

    /**
     * Method called to associate a CmsVideosXPage object to this object
     * through the CmsVideosXPage foreign key attribute.
     *
     * @param    CmsVideosXPage $l CmsVideosXPage
     * @return SysUsers The current object (for fluent API support)
     */
    public function addCmsVideosXPage(CmsVideosXPage $l)
    {
        if ($this->collCmsVideosXPages === null) {
            $this->initCmsVideosXPages();
            $this->collCmsVideosXPagesPartial = true;
        }

        if (!in_array($l, $this->collCmsVideosXPages->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCmsVideosXPage($l);

            if ($this->cmsVideosXPagesScheduledForDeletion and $this->cmsVideosXPagesScheduledForDeletion->contains($l)) {
                $this->cmsVideosXPagesScheduledForDeletion->remove($this->cmsVideosXPagesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	CmsVideosXPage $cmsVideosXPage The cmsVideosXPage object to add.
     */
    protected function doAddCmsVideosXPage($cmsVideosXPage)
    {
        $this->collCmsVideosXPages[]= $cmsVideosXPage;
        $cmsVideosXPage->setSysUsers($this);
    }

    /**
     * @param	CmsVideosXPage $cmsVideosXPage The cmsVideosXPage object to remove.
     * @return SysUsers The current object (for fluent API support)
     */
    public function removeCmsVideosXPage($cmsVideosXPage)
    {
        if ($this->getCmsVideosXPages()->contains($cmsVideosXPage)) {
            $this->collCmsVideosXPages->remove($this->collCmsVideosXPages->search($cmsVideosXPage));
            if (null === $this->cmsVideosXPagesScheduledForDeletion) {
                $this->cmsVideosXPagesScheduledForDeletion = clone $this->collCmsVideosXPages;
                $this->cmsVideosXPagesScheduledForDeletion->clear();
            }
            $this->cmsVideosXPagesScheduledForDeletion[]= $cmsVideosXPage;
            $cmsVideosXPage->setSysUsers(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysUsers is new, it will return
     * an empty collection; or if this SysUsers has previously
     * been saved, it will retrieve related CmsVideosXPages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysUsers.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsVideosXPage[] List of CmsVideosXPage objects
     */
    public function getCmsVideosXPagesJoinCmsPages($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsVideosXPageQuery::create(null, $criteria);
        $query->joinWith('CmsPages', $join_behavior);

        return $this->getCmsVideosXPages($query, $con);
    }

    /**
     * Clears out the collSysChatssRelatedByIdSender collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SysUsers The current object (for fluent API support)
     * @see        addSysChatssRelatedByIdSender()
     */
    public function clearSysChatssRelatedByIdSender()
    {
        $this->collSysChatssRelatedByIdSender = null; // important to set this to null since that means it is uninitialized
        $this->collSysChatssRelatedByIdSenderPartial = null;

        return $this;
    }

    /**
     * reset is the collSysChatssRelatedByIdSender collection loaded partially
     *
     * @return void
     */
    public function resetPartialSysChatssRelatedByIdSender($v = true)
    {
        $this->collSysChatssRelatedByIdSenderPartial = $v;
    }

    /**
     * Initializes the collSysChatssRelatedByIdSender collection.
     *
     * By default this just sets the collSysChatssRelatedByIdSender collection to an empty array (like clearcollSysChatssRelatedByIdSender());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSysChatssRelatedByIdSender($overrideExisting = true)
    {
        if (null !== $this->collSysChatssRelatedByIdSender && !$overrideExisting) {
            return;
        }
        $this->collSysChatssRelatedByIdSender = new PropelObjectCollection();
        $this->collSysChatssRelatedByIdSender->setModel('SysChats');
    }

    /**
     * Gets an array of SysChats objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SysUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SysChats[] List of SysChats objects
     * @throws PropelException
     */
    public function getSysChatssRelatedByIdSender($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSysChatssRelatedByIdSenderPartial && !$this->isNew();
        if (null === $this->collSysChatssRelatedByIdSender || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSysChatssRelatedByIdSender) {
                // return empty collection
                $this->initSysChatssRelatedByIdSender();
            } else {
                $collSysChatssRelatedByIdSender = SysChatsQuery::create(null, $criteria)
                    ->filterBySysUsersRelatedByIdSender($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSysChatssRelatedByIdSenderPartial && count($collSysChatssRelatedByIdSender)) {
                      $this->initSysChatssRelatedByIdSender(false);

                      foreach ($collSysChatssRelatedByIdSender as $obj) {
                        if (false == $this->collSysChatssRelatedByIdSender->contains($obj)) {
                          $this->collSysChatssRelatedByIdSender->append($obj);
                        }
                      }

                      $this->collSysChatssRelatedByIdSenderPartial = true;
                    }

                    $collSysChatssRelatedByIdSender->getInternalIterator()->rewind();

                    return $collSysChatssRelatedByIdSender;
                }

                if ($partial && $this->collSysChatssRelatedByIdSender) {
                    foreach ($this->collSysChatssRelatedByIdSender as $obj) {
                        if ($obj->isNew()) {
                            $collSysChatssRelatedByIdSender[] = $obj;
                        }
                    }
                }

                $this->collSysChatssRelatedByIdSender = $collSysChatssRelatedByIdSender;
                $this->collSysChatssRelatedByIdSenderPartial = false;
            }
        }

        return $this->collSysChatssRelatedByIdSender;
    }

    /**
     * Sets a collection of SysChatsRelatedByIdSender objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $sysChatssRelatedByIdSender A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SysUsers The current object (for fluent API support)
     */
    public function setSysChatssRelatedByIdSender(PropelCollection $sysChatssRelatedByIdSender, PropelPDO $con = null)
    {
        $sysChatssRelatedByIdSenderToDelete = $this->getSysChatssRelatedByIdSender(new Criteria(), $con)->diff($sysChatssRelatedByIdSender);


        $this->sysChatssRelatedByIdSenderScheduledForDeletion = $sysChatssRelatedByIdSenderToDelete;

        foreach ($sysChatssRelatedByIdSenderToDelete as $sysChatsRelatedByIdSenderRemoved) {
            $sysChatsRelatedByIdSenderRemoved->setSysUsersRelatedByIdSender(null);
        }

        $this->collSysChatssRelatedByIdSender = null;
        foreach ($sysChatssRelatedByIdSender as $sysChatsRelatedByIdSender) {
            $this->addSysChatsRelatedByIdSender($sysChatsRelatedByIdSender);
        }

        $this->collSysChatssRelatedByIdSender = $sysChatssRelatedByIdSender;
        $this->collSysChatssRelatedByIdSenderPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SysChats objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SysChats objects.
     * @throws PropelException
     */
    public function countSysChatssRelatedByIdSender(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSysChatssRelatedByIdSenderPartial && !$this->isNew();
        if (null === $this->collSysChatssRelatedByIdSender || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSysChatssRelatedByIdSender) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSysChatssRelatedByIdSender());
            }
            $query = SysChatsQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySysUsersRelatedByIdSender($this)
                ->count($con);
        }

        return count($this->collSysChatssRelatedByIdSender);
    }

    /**
     * Method called to associate a SysChats object to this object
     * through the SysChats foreign key attribute.
     *
     * @param    SysChats $l SysChats
     * @return SysUsers The current object (for fluent API support)
     */
    public function addSysChatsRelatedByIdSender(SysChats $l)
    {
        if ($this->collSysChatssRelatedByIdSender === null) {
            $this->initSysChatssRelatedByIdSender();
            $this->collSysChatssRelatedByIdSenderPartial = true;
        }

        if (!in_array($l, $this->collSysChatssRelatedByIdSender->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSysChatsRelatedByIdSender($l);

            if ($this->sysChatssRelatedByIdSenderScheduledForDeletion and $this->sysChatssRelatedByIdSenderScheduledForDeletion->contains($l)) {
                $this->sysChatssRelatedByIdSenderScheduledForDeletion->remove($this->sysChatssRelatedByIdSenderScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	SysChatsRelatedByIdSender $sysChatsRelatedByIdSender The sysChatsRelatedByIdSender object to add.
     */
    protected function doAddSysChatsRelatedByIdSender($sysChatsRelatedByIdSender)
    {
        $this->collSysChatssRelatedByIdSender[]= $sysChatsRelatedByIdSender;
        $sysChatsRelatedByIdSender->setSysUsersRelatedByIdSender($this);
    }

    /**
     * @param	SysChatsRelatedByIdSender $sysChatsRelatedByIdSender The sysChatsRelatedByIdSender object to remove.
     * @return SysUsers The current object (for fluent API support)
     */
    public function removeSysChatsRelatedByIdSender($sysChatsRelatedByIdSender)
    {
        if ($this->getSysChatssRelatedByIdSender()->contains($sysChatsRelatedByIdSender)) {
            $this->collSysChatssRelatedByIdSender->remove($this->collSysChatssRelatedByIdSender->search($sysChatsRelatedByIdSender));
            if (null === $this->sysChatssRelatedByIdSenderScheduledForDeletion) {
                $this->sysChatssRelatedByIdSenderScheduledForDeletion = clone $this->collSysChatssRelatedByIdSender;
                $this->sysChatssRelatedByIdSenderScheduledForDeletion->clear();
            }
            $this->sysChatssRelatedByIdSenderScheduledForDeletion[]= $sysChatsRelatedByIdSender;
            $sysChatsRelatedByIdSender->setSysUsersRelatedByIdSender(null);
        }

        return $this;
    }

    /**
     * Clears out the collSysChatssRelatedByIdReceiver collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SysUsers The current object (for fluent API support)
     * @see        addSysChatssRelatedByIdReceiver()
     */
    public function clearSysChatssRelatedByIdReceiver()
    {
        $this->collSysChatssRelatedByIdReceiver = null; // important to set this to null since that means it is uninitialized
        $this->collSysChatssRelatedByIdReceiverPartial = null;

        return $this;
    }

    /**
     * reset is the collSysChatssRelatedByIdReceiver collection loaded partially
     *
     * @return void
     */
    public function resetPartialSysChatssRelatedByIdReceiver($v = true)
    {
        $this->collSysChatssRelatedByIdReceiverPartial = $v;
    }

    /**
     * Initializes the collSysChatssRelatedByIdReceiver collection.
     *
     * By default this just sets the collSysChatssRelatedByIdReceiver collection to an empty array (like clearcollSysChatssRelatedByIdReceiver());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSysChatssRelatedByIdReceiver($overrideExisting = true)
    {
        if (null !== $this->collSysChatssRelatedByIdReceiver && !$overrideExisting) {
            return;
        }
        $this->collSysChatssRelatedByIdReceiver = new PropelObjectCollection();
        $this->collSysChatssRelatedByIdReceiver->setModel('SysChats');
    }

    /**
     * Gets an array of SysChats objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SysUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SysChats[] List of SysChats objects
     * @throws PropelException
     */
    public function getSysChatssRelatedByIdReceiver($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSysChatssRelatedByIdReceiverPartial && !$this->isNew();
        if (null === $this->collSysChatssRelatedByIdReceiver || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSysChatssRelatedByIdReceiver) {
                // return empty collection
                $this->initSysChatssRelatedByIdReceiver();
            } else {
                $collSysChatssRelatedByIdReceiver = SysChatsQuery::create(null, $criteria)
                    ->filterBySysUsersRelatedByIdReceiver($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSysChatssRelatedByIdReceiverPartial && count($collSysChatssRelatedByIdReceiver)) {
                      $this->initSysChatssRelatedByIdReceiver(false);

                      foreach ($collSysChatssRelatedByIdReceiver as $obj) {
                        if (false == $this->collSysChatssRelatedByIdReceiver->contains($obj)) {
                          $this->collSysChatssRelatedByIdReceiver->append($obj);
                        }
                      }

                      $this->collSysChatssRelatedByIdReceiverPartial = true;
                    }

                    $collSysChatssRelatedByIdReceiver->getInternalIterator()->rewind();

                    return $collSysChatssRelatedByIdReceiver;
                }

                if ($partial && $this->collSysChatssRelatedByIdReceiver) {
                    foreach ($this->collSysChatssRelatedByIdReceiver as $obj) {
                        if ($obj->isNew()) {
                            $collSysChatssRelatedByIdReceiver[] = $obj;
                        }
                    }
                }

                $this->collSysChatssRelatedByIdReceiver = $collSysChatssRelatedByIdReceiver;
                $this->collSysChatssRelatedByIdReceiverPartial = false;
            }
        }

        return $this->collSysChatssRelatedByIdReceiver;
    }

    /**
     * Sets a collection of SysChatsRelatedByIdReceiver objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $sysChatssRelatedByIdReceiver A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SysUsers The current object (for fluent API support)
     */
    public function setSysChatssRelatedByIdReceiver(PropelCollection $sysChatssRelatedByIdReceiver, PropelPDO $con = null)
    {
        $sysChatssRelatedByIdReceiverToDelete = $this->getSysChatssRelatedByIdReceiver(new Criteria(), $con)->diff($sysChatssRelatedByIdReceiver);


        $this->sysChatssRelatedByIdReceiverScheduledForDeletion = $sysChatssRelatedByIdReceiverToDelete;

        foreach ($sysChatssRelatedByIdReceiverToDelete as $sysChatsRelatedByIdReceiverRemoved) {
            $sysChatsRelatedByIdReceiverRemoved->setSysUsersRelatedByIdReceiver(null);
        }

        $this->collSysChatssRelatedByIdReceiver = null;
        foreach ($sysChatssRelatedByIdReceiver as $sysChatsRelatedByIdReceiver) {
            $this->addSysChatsRelatedByIdReceiver($sysChatsRelatedByIdReceiver);
        }

        $this->collSysChatssRelatedByIdReceiver = $sysChatssRelatedByIdReceiver;
        $this->collSysChatssRelatedByIdReceiverPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SysChats objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SysChats objects.
     * @throws PropelException
     */
    public function countSysChatssRelatedByIdReceiver(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSysChatssRelatedByIdReceiverPartial && !$this->isNew();
        if (null === $this->collSysChatssRelatedByIdReceiver || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSysChatssRelatedByIdReceiver) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSysChatssRelatedByIdReceiver());
            }
            $query = SysChatsQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySysUsersRelatedByIdReceiver($this)
                ->count($con);
        }

        return count($this->collSysChatssRelatedByIdReceiver);
    }

    /**
     * Method called to associate a SysChats object to this object
     * through the SysChats foreign key attribute.
     *
     * @param    SysChats $l SysChats
     * @return SysUsers The current object (for fluent API support)
     */
    public function addSysChatsRelatedByIdReceiver(SysChats $l)
    {
        if ($this->collSysChatssRelatedByIdReceiver === null) {
            $this->initSysChatssRelatedByIdReceiver();
            $this->collSysChatssRelatedByIdReceiverPartial = true;
        }

        if (!in_array($l, $this->collSysChatssRelatedByIdReceiver->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSysChatsRelatedByIdReceiver($l);

            if ($this->sysChatssRelatedByIdReceiverScheduledForDeletion and $this->sysChatssRelatedByIdReceiverScheduledForDeletion->contains($l)) {
                $this->sysChatssRelatedByIdReceiverScheduledForDeletion->remove($this->sysChatssRelatedByIdReceiverScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	SysChatsRelatedByIdReceiver $sysChatsRelatedByIdReceiver The sysChatsRelatedByIdReceiver object to add.
     */
    protected function doAddSysChatsRelatedByIdReceiver($sysChatsRelatedByIdReceiver)
    {
        $this->collSysChatssRelatedByIdReceiver[]= $sysChatsRelatedByIdReceiver;
        $sysChatsRelatedByIdReceiver->setSysUsersRelatedByIdReceiver($this);
    }

    /**
     * @param	SysChatsRelatedByIdReceiver $sysChatsRelatedByIdReceiver The sysChatsRelatedByIdReceiver object to remove.
     * @return SysUsers The current object (for fluent API support)
     */
    public function removeSysChatsRelatedByIdReceiver($sysChatsRelatedByIdReceiver)
    {
        if ($this->getSysChatssRelatedByIdReceiver()->contains($sysChatsRelatedByIdReceiver)) {
            $this->collSysChatssRelatedByIdReceiver->remove($this->collSysChatssRelatedByIdReceiver->search($sysChatsRelatedByIdReceiver));
            if (null === $this->sysChatssRelatedByIdReceiverScheduledForDeletion) {
                $this->sysChatssRelatedByIdReceiverScheduledForDeletion = clone $this->collSysChatssRelatedByIdReceiver;
                $this->sysChatssRelatedByIdReceiverScheduledForDeletion->clear();
            }
            $this->sysChatssRelatedByIdReceiverScheduledForDeletion[]= $sysChatsRelatedByIdReceiver;
            $sysChatsRelatedByIdReceiver->setSysUsersRelatedByIdReceiver(null);
        }

        return $this;
    }

    /**
     * Clears out the collSysEnterprisessRelatedByUserModified collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SysUsers The current object (for fluent API support)
     * @see        addSysEnterprisessRelatedByUserModified()
     */
    public function clearSysEnterprisessRelatedByUserModified()
    {
        $this->collSysEnterprisessRelatedByUserModified = null; // important to set this to null since that means it is uninitialized
        $this->collSysEnterprisessRelatedByUserModifiedPartial = null;

        return $this;
    }

    /**
     * reset is the collSysEnterprisessRelatedByUserModified collection loaded partially
     *
     * @return void
     */
    public function resetPartialSysEnterprisessRelatedByUserModified($v = true)
    {
        $this->collSysEnterprisessRelatedByUserModifiedPartial = $v;
    }

    /**
     * Initializes the collSysEnterprisessRelatedByUserModified collection.
     *
     * By default this just sets the collSysEnterprisessRelatedByUserModified collection to an empty array (like clearcollSysEnterprisessRelatedByUserModified());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSysEnterprisessRelatedByUserModified($overrideExisting = true)
    {
        if (null !== $this->collSysEnterprisessRelatedByUserModified && !$overrideExisting) {
            return;
        }
        $this->collSysEnterprisessRelatedByUserModified = new PropelObjectCollection();
        $this->collSysEnterprisessRelatedByUserModified->setModel('SysEnterprises');
    }

    /**
     * Gets an array of SysEnterprises objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SysUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SysEnterprises[] List of SysEnterprises objects
     * @throws PropelException
     */
    public function getSysEnterprisessRelatedByUserModified($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSysEnterprisessRelatedByUserModifiedPartial && !$this->isNew();
        if (null === $this->collSysEnterprisessRelatedByUserModified || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSysEnterprisessRelatedByUserModified) {
                // return empty collection
                $this->initSysEnterprisessRelatedByUserModified();
            } else {
                $collSysEnterprisessRelatedByUserModified = SysEnterprisesQuery::create(null, $criteria)
                    ->filterBySysUsersRelatedByUserModified($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSysEnterprisessRelatedByUserModifiedPartial && count($collSysEnterprisessRelatedByUserModified)) {
                      $this->initSysEnterprisessRelatedByUserModified(false);

                      foreach ($collSysEnterprisessRelatedByUserModified as $obj) {
                        if (false == $this->collSysEnterprisessRelatedByUserModified->contains($obj)) {
                          $this->collSysEnterprisessRelatedByUserModified->append($obj);
                        }
                      }

                      $this->collSysEnterprisessRelatedByUserModifiedPartial = true;
                    }

                    $collSysEnterprisessRelatedByUserModified->getInternalIterator()->rewind();

                    return $collSysEnterprisessRelatedByUserModified;
                }

                if ($partial && $this->collSysEnterprisessRelatedByUserModified) {
                    foreach ($this->collSysEnterprisessRelatedByUserModified as $obj) {
                        if ($obj->isNew()) {
                            $collSysEnterprisessRelatedByUserModified[] = $obj;
                        }
                    }
                }

                $this->collSysEnterprisessRelatedByUserModified = $collSysEnterprisessRelatedByUserModified;
                $this->collSysEnterprisessRelatedByUserModifiedPartial = false;
            }
        }

        return $this->collSysEnterprisessRelatedByUserModified;
    }

    /**
     * Sets a collection of SysEnterprisesRelatedByUserModified objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $sysEnterprisessRelatedByUserModified A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SysUsers The current object (for fluent API support)
     */
    public function setSysEnterprisessRelatedByUserModified(PropelCollection $sysEnterprisessRelatedByUserModified, PropelPDO $con = null)
    {
        $sysEnterprisessRelatedByUserModifiedToDelete = $this->getSysEnterprisessRelatedByUserModified(new Criteria(), $con)->diff($sysEnterprisessRelatedByUserModified);


        $this->sysEnterprisessRelatedByUserModifiedScheduledForDeletion = $sysEnterprisessRelatedByUserModifiedToDelete;

        foreach ($sysEnterprisessRelatedByUserModifiedToDelete as $sysEnterprisesRelatedByUserModifiedRemoved) {
            $sysEnterprisesRelatedByUserModifiedRemoved->setSysUsersRelatedByUserModified(null);
        }

        $this->collSysEnterprisessRelatedByUserModified = null;
        foreach ($sysEnterprisessRelatedByUserModified as $sysEnterprisesRelatedByUserModified) {
            $this->addSysEnterprisesRelatedByUserModified($sysEnterprisesRelatedByUserModified);
        }

        $this->collSysEnterprisessRelatedByUserModified = $sysEnterprisessRelatedByUserModified;
        $this->collSysEnterprisessRelatedByUserModifiedPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SysEnterprises objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SysEnterprises objects.
     * @throws PropelException
     */
    public function countSysEnterprisessRelatedByUserModified(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSysEnterprisessRelatedByUserModifiedPartial && !$this->isNew();
        if (null === $this->collSysEnterprisessRelatedByUserModified || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSysEnterprisessRelatedByUserModified) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSysEnterprisessRelatedByUserModified());
            }
            $query = SysEnterprisesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySysUsersRelatedByUserModified($this)
                ->count($con);
        }

        return count($this->collSysEnterprisessRelatedByUserModified);
    }

    /**
     * Method called to associate a SysEnterprises object to this object
     * through the SysEnterprises foreign key attribute.
     *
     * @param    SysEnterprises $l SysEnterprises
     * @return SysUsers The current object (for fluent API support)
     */
    public function addSysEnterprisesRelatedByUserModified(SysEnterprises $l)
    {
        if ($this->collSysEnterprisessRelatedByUserModified === null) {
            $this->initSysEnterprisessRelatedByUserModified();
            $this->collSysEnterprisessRelatedByUserModifiedPartial = true;
        }

        if (!in_array($l, $this->collSysEnterprisessRelatedByUserModified->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSysEnterprisesRelatedByUserModified($l);

            if ($this->sysEnterprisessRelatedByUserModifiedScheduledForDeletion and $this->sysEnterprisessRelatedByUserModifiedScheduledForDeletion->contains($l)) {
                $this->sysEnterprisessRelatedByUserModifiedScheduledForDeletion->remove($this->sysEnterprisessRelatedByUserModifiedScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	SysEnterprisesRelatedByUserModified $sysEnterprisesRelatedByUserModified The sysEnterprisesRelatedByUserModified object to add.
     */
    protected function doAddSysEnterprisesRelatedByUserModified($sysEnterprisesRelatedByUserModified)
    {
        $this->collSysEnterprisessRelatedByUserModified[]= $sysEnterprisesRelatedByUserModified;
        $sysEnterprisesRelatedByUserModified->setSysUsersRelatedByUserModified($this);
    }

    /**
     * @param	SysEnterprisesRelatedByUserModified $sysEnterprisesRelatedByUserModified The sysEnterprisesRelatedByUserModified object to remove.
     * @return SysUsers The current object (for fluent API support)
     */
    public function removeSysEnterprisesRelatedByUserModified($sysEnterprisesRelatedByUserModified)
    {
        if ($this->getSysEnterprisessRelatedByUserModified()->contains($sysEnterprisesRelatedByUserModified)) {
            $this->collSysEnterprisessRelatedByUserModified->remove($this->collSysEnterprisessRelatedByUserModified->search($sysEnterprisesRelatedByUserModified));
            if (null === $this->sysEnterprisessRelatedByUserModifiedScheduledForDeletion) {
                $this->sysEnterprisessRelatedByUserModifiedScheduledForDeletion = clone $this->collSysEnterprisessRelatedByUserModified;
                $this->sysEnterprisessRelatedByUserModifiedScheduledForDeletion->clear();
            }
            $this->sysEnterprisessRelatedByUserModifiedScheduledForDeletion[]= $sysEnterprisesRelatedByUserModified;
            $sysEnterprisesRelatedByUserModified->setSysUsersRelatedByUserModified(null);
        }

        return $this;
    }

    /**
     * Clears out the collSysNotificationssRelatedByIdSender collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SysUsers The current object (for fluent API support)
     * @see        addSysNotificationssRelatedByIdSender()
     */
    public function clearSysNotificationssRelatedByIdSender()
    {
        $this->collSysNotificationssRelatedByIdSender = null; // important to set this to null since that means it is uninitialized
        $this->collSysNotificationssRelatedByIdSenderPartial = null;

        return $this;
    }

    /**
     * reset is the collSysNotificationssRelatedByIdSender collection loaded partially
     *
     * @return void
     */
    public function resetPartialSysNotificationssRelatedByIdSender($v = true)
    {
        $this->collSysNotificationssRelatedByIdSenderPartial = $v;
    }

    /**
     * Initializes the collSysNotificationssRelatedByIdSender collection.
     *
     * By default this just sets the collSysNotificationssRelatedByIdSender collection to an empty array (like clearcollSysNotificationssRelatedByIdSender());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSysNotificationssRelatedByIdSender($overrideExisting = true)
    {
        if (null !== $this->collSysNotificationssRelatedByIdSender && !$overrideExisting) {
            return;
        }
        $this->collSysNotificationssRelatedByIdSender = new PropelObjectCollection();
        $this->collSysNotificationssRelatedByIdSender->setModel('SysNotifications');
    }

    /**
     * Gets an array of SysNotifications objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SysUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SysNotifications[] List of SysNotifications objects
     * @throws PropelException
     */
    public function getSysNotificationssRelatedByIdSender($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSysNotificationssRelatedByIdSenderPartial && !$this->isNew();
        if (null === $this->collSysNotificationssRelatedByIdSender || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSysNotificationssRelatedByIdSender) {
                // return empty collection
                $this->initSysNotificationssRelatedByIdSender();
            } else {
                $collSysNotificationssRelatedByIdSender = SysNotificationsQuery::create(null, $criteria)
                    ->filterBySysUsersRelatedByIdSender($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSysNotificationssRelatedByIdSenderPartial && count($collSysNotificationssRelatedByIdSender)) {
                      $this->initSysNotificationssRelatedByIdSender(false);

                      foreach ($collSysNotificationssRelatedByIdSender as $obj) {
                        if (false == $this->collSysNotificationssRelatedByIdSender->contains($obj)) {
                          $this->collSysNotificationssRelatedByIdSender->append($obj);
                        }
                      }

                      $this->collSysNotificationssRelatedByIdSenderPartial = true;
                    }

                    $collSysNotificationssRelatedByIdSender->getInternalIterator()->rewind();

                    return $collSysNotificationssRelatedByIdSender;
                }

                if ($partial && $this->collSysNotificationssRelatedByIdSender) {
                    foreach ($this->collSysNotificationssRelatedByIdSender as $obj) {
                        if ($obj->isNew()) {
                            $collSysNotificationssRelatedByIdSender[] = $obj;
                        }
                    }
                }

                $this->collSysNotificationssRelatedByIdSender = $collSysNotificationssRelatedByIdSender;
                $this->collSysNotificationssRelatedByIdSenderPartial = false;
            }
        }

        return $this->collSysNotificationssRelatedByIdSender;
    }

    /**
     * Sets a collection of SysNotificationsRelatedByIdSender objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $sysNotificationssRelatedByIdSender A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SysUsers The current object (for fluent API support)
     */
    public function setSysNotificationssRelatedByIdSender(PropelCollection $sysNotificationssRelatedByIdSender, PropelPDO $con = null)
    {
        $sysNotificationssRelatedByIdSenderToDelete = $this->getSysNotificationssRelatedByIdSender(new Criteria(), $con)->diff($sysNotificationssRelatedByIdSender);


        $this->sysNotificationssRelatedByIdSenderScheduledForDeletion = $sysNotificationssRelatedByIdSenderToDelete;

        foreach ($sysNotificationssRelatedByIdSenderToDelete as $sysNotificationsRelatedByIdSenderRemoved) {
            $sysNotificationsRelatedByIdSenderRemoved->setSysUsersRelatedByIdSender(null);
        }

        $this->collSysNotificationssRelatedByIdSender = null;
        foreach ($sysNotificationssRelatedByIdSender as $sysNotificationsRelatedByIdSender) {
            $this->addSysNotificationsRelatedByIdSender($sysNotificationsRelatedByIdSender);
        }

        $this->collSysNotificationssRelatedByIdSender = $sysNotificationssRelatedByIdSender;
        $this->collSysNotificationssRelatedByIdSenderPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SysNotifications objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SysNotifications objects.
     * @throws PropelException
     */
    public function countSysNotificationssRelatedByIdSender(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSysNotificationssRelatedByIdSenderPartial && !$this->isNew();
        if (null === $this->collSysNotificationssRelatedByIdSender || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSysNotificationssRelatedByIdSender) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSysNotificationssRelatedByIdSender());
            }
            $query = SysNotificationsQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySysUsersRelatedByIdSender($this)
                ->count($con);
        }

        return count($this->collSysNotificationssRelatedByIdSender);
    }

    /**
     * Method called to associate a SysNotifications object to this object
     * through the SysNotifications foreign key attribute.
     *
     * @param    SysNotifications $l SysNotifications
     * @return SysUsers The current object (for fluent API support)
     */
    public function addSysNotificationsRelatedByIdSender(SysNotifications $l)
    {
        if ($this->collSysNotificationssRelatedByIdSender === null) {
            $this->initSysNotificationssRelatedByIdSender();
            $this->collSysNotificationssRelatedByIdSenderPartial = true;
        }

        if (!in_array($l, $this->collSysNotificationssRelatedByIdSender->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSysNotificationsRelatedByIdSender($l);

            if ($this->sysNotificationssRelatedByIdSenderScheduledForDeletion and $this->sysNotificationssRelatedByIdSenderScheduledForDeletion->contains($l)) {
                $this->sysNotificationssRelatedByIdSenderScheduledForDeletion->remove($this->sysNotificationssRelatedByIdSenderScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	SysNotificationsRelatedByIdSender $sysNotificationsRelatedByIdSender The sysNotificationsRelatedByIdSender object to add.
     */
    protected function doAddSysNotificationsRelatedByIdSender($sysNotificationsRelatedByIdSender)
    {
        $this->collSysNotificationssRelatedByIdSender[]= $sysNotificationsRelatedByIdSender;
        $sysNotificationsRelatedByIdSender->setSysUsersRelatedByIdSender($this);
    }

    /**
     * @param	SysNotificationsRelatedByIdSender $sysNotificationsRelatedByIdSender The sysNotificationsRelatedByIdSender object to remove.
     * @return SysUsers The current object (for fluent API support)
     */
    public function removeSysNotificationsRelatedByIdSender($sysNotificationsRelatedByIdSender)
    {
        if ($this->getSysNotificationssRelatedByIdSender()->contains($sysNotificationsRelatedByIdSender)) {
            $this->collSysNotificationssRelatedByIdSender->remove($this->collSysNotificationssRelatedByIdSender->search($sysNotificationsRelatedByIdSender));
            if (null === $this->sysNotificationssRelatedByIdSenderScheduledForDeletion) {
                $this->sysNotificationssRelatedByIdSenderScheduledForDeletion = clone $this->collSysNotificationssRelatedByIdSender;
                $this->sysNotificationssRelatedByIdSenderScheduledForDeletion->clear();
            }
            $this->sysNotificationssRelatedByIdSenderScheduledForDeletion[]= $sysNotificationsRelatedByIdSender;
            $sysNotificationsRelatedByIdSender->setSysUsersRelatedByIdSender(null);
        }

        return $this;
    }

    /**
     * Clears out the collSysNotificationssRelatedByIdReceiver collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SysUsers The current object (for fluent API support)
     * @see        addSysNotificationssRelatedByIdReceiver()
     */
    public function clearSysNotificationssRelatedByIdReceiver()
    {
        $this->collSysNotificationssRelatedByIdReceiver = null; // important to set this to null since that means it is uninitialized
        $this->collSysNotificationssRelatedByIdReceiverPartial = null;

        return $this;
    }

    /**
     * reset is the collSysNotificationssRelatedByIdReceiver collection loaded partially
     *
     * @return void
     */
    public function resetPartialSysNotificationssRelatedByIdReceiver($v = true)
    {
        $this->collSysNotificationssRelatedByIdReceiverPartial = $v;
    }

    /**
     * Initializes the collSysNotificationssRelatedByIdReceiver collection.
     *
     * By default this just sets the collSysNotificationssRelatedByIdReceiver collection to an empty array (like clearcollSysNotificationssRelatedByIdReceiver());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSysNotificationssRelatedByIdReceiver($overrideExisting = true)
    {
        if (null !== $this->collSysNotificationssRelatedByIdReceiver && !$overrideExisting) {
            return;
        }
        $this->collSysNotificationssRelatedByIdReceiver = new PropelObjectCollection();
        $this->collSysNotificationssRelatedByIdReceiver->setModel('SysNotifications');
    }

    /**
     * Gets an array of SysNotifications objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SysUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SysNotifications[] List of SysNotifications objects
     * @throws PropelException
     */
    public function getSysNotificationssRelatedByIdReceiver($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSysNotificationssRelatedByIdReceiverPartial && !$this->isNew();
        if (null === $this->collSysNotificationssRelatedByIdReceiver || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSysNotificationssRelatedByIdReceiver) {
                // return empty collection
                $this->initSysNotificationssRelatedByIdReceiver();
            } else {
                $collSysNotificationssRelatedByIdReceiver = SysNotificationsQuery::create(null, $criteria)
                    ->filterBySysUsersRelatedByIdReceiver($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSysNotificationssRelatedByIdReceiverPartial && count($collSysNotificationssRelatedByIdReceiver)) {
                      $this->initSysNotificationssRelatedByIdReceiver(false);

                      foreach ($collSysNotificationssRelatedByIdReceiver as $obj) {
                        if (false == $this->collSysNotificationssRelatedByIdReceiver->contains($obj)) {
                          $this->collSysNotificationssRelatedByIdReceiver->append($obj);
                        }
                      }

                      $this->collSysNotificationssRelatedByIdReceiverPartial = true;
                    }

                    $collSysNotificationssRelatedByIdReceiver->getInternalIterator()->rewind();

                    return $collSysNotificationssRelatedByIdReceiver;
                }

                if ($partial && $this->collSysNotificationssRelatedByIdReceiver) {
                    foreach ($this->collSysNotificationssRelatedByIdReceiver as $obj) {
                        if ($obj->isNew()) {
                            $collSysNotificationssRelatedByIdReceiver[] = $obj;
                        }
                    }
                }

                $this->collSysNotificationssRelatedByIdReceiver = $collSysNotificationssRelatedByIdReceiver;
                $this->collSysNotificationssRelatedByIdReceiverPartial = false;
            }
        }

        return $this->collSysNotificationssRelatedByIdReceiver;
    }

    /**
     * Sets a collection of SysNotificationsRelatedByIdReceiver objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $sysNotificationssRelatedByIdReceiver A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SysUsers The current object (for fluent API support)
     */
    public function setSysNotificationssRelatedByIdReceiver(PropelCollection $sysNotificationssRelatedByIdReceiver, PropelPDO $con = null)
    {
        $sysNotificationssRelatedByIdReceiverToDelete = $this->getSysNotificationssRelatedByIdReceiver(new Criteria(), $con)->diff($sysNotificationssRelatedByIdReceiver);


        $this->sysNotificationssRelatedByIdReceiverScheduledForDeletion = $sysNotificationssRelatedByIdReceiverToDelete;

        foreach ($sysNotificationssRelatedByIdReceiverToDelete as $sysNotificationsRelatedByIdReceiverRemoved) {
            $sysNotificationsRelatedByIdReceiverRemoved->setSysUsersRelatedByIdReceiver(null);
        }

        $this->collSysNotificationssRelatedByIdReceiver = null;
        foreach ($sysNotificationssRelatedByIdReceiver as $sysNotificationsRelatedByIdReceiver) {
            $this->addSysNotificationsRelatedByIdReceiver($sysNotificationsRelatedByIdReceiver);
        }

        $this->collSysNotificationssRelatedByIdReceiver = $sysNotificationssRelatedByIdReceiver;
        $this->collSysNotificationssRelatedByIdReceiverPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SysNotifications objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SysNotifications objects.
     * @throws PropelException
     */
    public function countSysNotificationssRelatedByIdReceiver(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSysNotificationssRelatedByIdReceiverPartial && !$this->isNew();
        if (null === $this->collSysNotificationssRelatedByIdReceiver || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSysNotificationssRelatedByIdReceiver) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSysNotificationssRelatedByIdReceiver());
            }
            $query = SysNotificationsQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySysUsersRelatedByIdReceiver($this)
                ->count($con);
        }

        return count($this->collSysNotificationssRelatedByIdReceiver);
    }

    /**
     * Method called to associate a SysNotifications object to this object
     * through the SysNotifications foreign key attribute.
     *
     * @param    SysNotifications $l SysNotifications
     * @return SysUsers The current object (for fluent API support)
     */
    public function addSysNotificationsRelatedByIdReceiver(SysNotifications $l)
    {
        if ($this->collSysNotificationssRelatedByIdReceiver === null) {
            $this->initSysNotificationssRelatedByIdReceiver();
            $this->collSysNotificationssRelatedByIdReceiverPartial = true;
        }

        if (!in_array($l, $this->collSysNotificationssRelatedByIdReceiver->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSysNotificationsRelatedByIdReceiver($l);

            if ($this->sysNotificationssRelatedByIdReceiverScheduledForDeletion and $this->sysNotificationssRelatedByIdReceiverScheduledForDeletion->contains($l)) {
                $this->sysNotificationssRelatedByIdReceiverScheduledForDeletion->remove($this->sysNotificationssRelatedByIdReceiverScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	SysNotificationsRelatedByIdReceiver $sysNotificationsRelatedByIdReceiver The sysNotificationsRelatedByIdReceiver object to add.
     */
    protected function doAddSysNotificationsRelatedByIdReceiver($sysNotificationsRelatedByIdReceiver)
    {
        $this->collSysNotificationssRelatedByIdReceiver[]= $sysNotificationsRelatedByIdReceiver;
        $sysNotificationsRelatedByIdReceiver->setSysUsersRelatedByIdReceiver($this);
    }

    /**
     * @param	SysNotificationsRelatedByIdReceiver $sysNotificationsRelatedByIdReceiver The sysNotificationsRelatedByIdReceiver object to remove.
     * @return SysUsers The current object (for fluent API support)
     */
    public function removeSysNotificationsRelatedByIdReceiver($sysNotificationsRelatedByIdReceiver)
    {
        if ($this->getSysNotificationssRelatedByIdReceiver()->contains($sysNotificationsRelatedByIdReceiver)) {
            $this->collSysNotificationssRelatedByIdReceiver->remove($this->collSysNotificationssRelatedByIdReceiver->search($sysNotificationsRelatedByIdReceiver));
            if (null === $this->sysNotificationssRelatedByIdReceiverScheduledForDeletion) {
                $this->sysNotificationssRelatedByIdReceiverScheduledForDeletion = clone $this->collSysNotificationssRelatedByIdReceiver;
                $this->sysNotificationssRelatedByIdReceiverScheduledForDeletion->clear();
            }
            $this->sysNotificationssRelatedByIdReceiverScheduledForDeletion[]= $sysNotificationsRelatedByIdReceiver;
            $sysNotificationsRelatedByIdReceiver->setSysUsersRelatedByIdReceiver(null);
        }

        return $this;
    }

    /**
     * Clears out the collSysPagess collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SysUsers The current object (for fluent API support)
     * @see        addSysPagess()
     */
    public function clearSysPagess()
    {
        $this->collSysPagess = null; // important to set this to null since that means it is uninitialized
        $this->collSysPagessPartial = null;

        return $this;
    }

    /**
     * reset is the collSysPagess collection loaded partially
     *
     * @return void
     */
    public function resetPartialSysPagess($v = true)
    {
        $this->collSysPagessPartial = $v;
    }

    /**
     * Initializes the collSysPagess collection.
     *
     * By default this just sets the collSysPagess collection to an empty array (like clearcollSysPagess());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSysPagess($overrideExisting = true)
    {
        if (null !== $this->collSysPagess && !$overrideExisting) {
            return;
        }
        $this->collSysPagess = new PropelObjectCollection();
        $this->collSysPagess->setModel('SysPages');
    }

    /**
     * Gets an array of SysPages objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SysUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SysPages[] List of SysPages objects
     * @throws PropelException
     */
    public function getSysPagess($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSysPagessPartial && !$this->isNew();
        if (null === $this->collSysPagess || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSysPagess) {
                // return empty collection
                $this->initSysPagess();
            } else {
                $collSysPagess = SysPagesQuery::create(null, $criteria)
                    ->filterBySysUsers($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSysPagessPartial && count($collSysPagess)) {
                      $this->initSysPagess(false);

                      foreach ($collSysPagess as $obj) {
                        if (false == $this->collSysPagess->contains($obj)) {
                          $this->collSysPagess->append($obj);
                        }
                      }

                      $this->collSysPagessPartial = true;
                    }

                    $collSysPagess->getInternalIterator()->rewind();

                    return $collSysPagess;
                }

                if ($partial && $this->collSysPagess) {
                    foreach ($this->collSysPagess as $obj) {
                        if ($obj->isNew()) {
                            $collSysPagess[] = $obj;
                        }
                    }
                }

                $this->collSysPagess = $collSysPagess;
                $this->collSysPagessPartial = false;
            }
        }

        return $this->collSysPagess;
    }

    /**
     * Sets a collection of SysPages objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $sysPagess A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SysUsers The current object (for fluent API support)
     */
    public function setSysPagess(PropelCollection $sysPagess, PropelPDO $con = null)
    {
        $sysPagessToDelete = $this->getSysPagess(new Criteria(), $con)->diff($sysPagess);


        $this->sysPagessScheduledForDeletion = $sysPagessToDelete;

        foreach ($sysPagessToDelete as $sysPagesRemoved) {
            $sysPagesRemoved->setSysUsers(null);
        }

        $this->collSysPagess = null;
        foreach ($sysPagess as $sysPages) {
            $this->addSysPages($sysPages);
        }

        $this->collSysPagess = $sysPagess;
        $this->collSysPagessPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SysPages objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SysPages objects.
     * @throws PropelException
     */
    public function countSysPagess(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSysPagessPartial && !$this->isNew();
        if (null === $this->collSysPagess || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSysPagess) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSysPagess());
            }
            $query = SysPagesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySysUsers($this)
                ->count($con);
        }

        return count($this->collSysPagess);
    }

    /**
     * Method called to associate a SysPages object to this object
     * through the SysPages foreign key attribute.
     *
     * @param    SysPages $l SysPages
     * @return SysUsers The current object (for fluent API support)
     */
    public function addSysPages(SysPages $l)
    {
        if ($this->collSysPagess === null) {
            $this->initSysPagess();
            $this->collSysPagessPartial = true;
        }

        if (!in_array($l, $this->collSysPagess->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSysPages($l);

            if ($this->sysPagessScheduledForDeletion and $this->sysPagessScheduledForDeletion->contains($l)) {
                $this->sysPagessScheduledForDeletion->remove($this->sysPagessScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	SysPages $sysPages The sysPages object to add.
     */
    protected function doAddSysPages($sysPages)
    {
        $this->collSysPagess[]= $sysPages;
        $sysPages->setSysUsers($this);
    }

    /**
     * @param	SysPages $sysPages The sysPages object to remove.
     * @return SysUsers The current object (for fluent API support)
     */
    public function removeSysPages($sysPages)
    {
        if ($this->getSysPagess()->contains($sysPages)) {
            $this->collSysPagess->remove($this->collSysPagess->search($sysPages));
            if (null === $this->sysPagessScheduledForDeletion) {
                $this->sysPagessScheduledForDeletion = clone $this->collSysPagess;
                $this->sysPagessScheduledForDeletion->clear();
            }
            $this->sysPagessScheduledForDeletion[]= $sysPages;
            $sysPages->setSysUsers(null);
        }

        return $this;
    }

    /**
     * Clears out the collSysUserssRelatedByIdUser collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SysUsers The current object (for fluent API support)
     * @see        addSysUserssRelatedByIdUser()
     */
    public function clearSysUserssRelatedByIdUser()
    {
        $this->collSysUserssRelatedByIdUser = null; // important to set this to null since that means it is uninitialized
        $this->collSysUserssRelatedByIdUserPartial = null;

        return $this;
    }

    /**
     * reset is the collSysUserssRelatedByIdUser collection loaded partially
     *
     * @return void
     */
    public function resetPartialSysUserssRelatedByIdUser($v = true)
    {
        $this->collSysUserssRelatedByIdUserPartial = $v;
    }

    /**
     * Initializes the collSysUserssRelatedByIdUser collection.
     *
     * By default this just sets the collSysUserssRelatedByIdUser collection to an empty array (like clearcollSysUserssRelatedByIdUser());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSysUserssRelatedByIdUser($overrideExisting = true)
    {
        if (null !== $this->collSysUserssRelatedByIdUser && !$overrideExisting) {
            return;
        }
        $this->collSysUserssRelatedByIdUser = new PropelObjectCollection();
        $this->collSysUserssRelatedByIdUser->setModel('SysUsers');
    }

    /**
     * Gets an array of SysUsers objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SysUsers is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SysUsers[] List of SysUsers objects
     * @throws PropelException
     */
    public function getSysUserssRelatedByIdUser($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSysUserssRelatedByIdUserPartial && !$this->isNew();
        if (null === $this->collSysUserssRelatedByIdUser || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSysUserssRelatedByIdUser) {
                // return empty collection
                $this->initSysUserssRelatedByIdUser();
            } else {
                $collSysUserssRelatedByIdUser = SysUsersQuery::create(null, $criteria)
                    ->filterBySysUsersRelatedByUserModified($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSysUserssRelatedByIdUserPartial && count($collSysUserssRelatedByIdUser)) {
                      $this->initSysUserssRelatedByIdUser(false);

                      foreach ($collSysUserssRelatedByIdUser as $obj) {
                        if (false == $this->collSysUserssRelatedByIdUser->contains($obj)) {
                          $this->collSysUserssRelatedByIdUser->append($obj);
                        }
                      }

                      $this->collSysUserssRelatedByIdUserPartial = true;
                    }

                    $collSysUserssRelatedByIdUser->getInternalIterator()->rewind();

                    return $collSysUserssRelatedByIdUser;
                }

                if ($partial && $this->collSysUserssRelatedByIdUser) {
                    foreach ($this->collSysUserssRelatedByIdUser as $obj) {
                        if ($obj->isNew()) {
                            $collSysUserssRelatedByIdUser[] = $obj;
                        }
                    }
                }

                $this->collSysUserssRelatedByIdUser = $collSysUserssRelatedByIdUser;
                $this->collSysUserssRelatedByIdUserPartial = false;
            }
        }

        return $this->collSysUserssRelatedByIdUser;
    }

    /**
     * Sets a collection of SysUsersRelatedByIdUser objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $sysUserssRelatedByIdUser A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SysUsers The current object (for fluent API support)
     */
    public function setSysUserssRelatedByIdUser(PropelCollection $sysUserssRelatedByIdUser, PropelPDO $con = null)
    {
        $sysUserssRelatedByIdUserToDelete = $this->getSysUserssRelatedByIdUser(new Criteria(), $con)->diff($sysUserssRelatedByIdUser);


        $this->sysUserssRelatedByIdUserScheduledForDeletion = $sysUserssRelatedByIdUserToDelete;

        foreach ($sysUserssRelatedByIdUserToDelete as $sysUsersRelatedByIdUserRemoved) {
            $sysUsersRelatedByIdUserRemoved->setSysUsersRelatedByUserModified(null);
        }

        $this->collSysUserssRelatedByIdUser = null;
        foreach ($sysUserssRelatedByIdUser as $sysUsersRelatedByIdUser) {
            $this->addSysUsersRelatedByIdUser($sysUsersRelatedByIdUser);
        }

        $this->collSysUserssRelatedByIdUser = $sysUserssRelatedByIdUser;
        $this->collSysUserssRelatedByIdUserPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SysUsers objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SysUsers objects.
     * @throws PropelException
     */
    public function countSysUserssRelatedByIdUser(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSysUserssRelatedByIdUserPartial && !$this->isNew();
        if (null === $this->collSysUserssRelatedByIdUser || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSysUserssRelatedByIdUser) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSysUserssRelatedByIdUser());
            }
            $query = SysUsersQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySysUsersRelatedByUserModified($this)
                ->count($con);
        }

        return count($this->collSysUserssRelatedByIdUser);
    }

    /**
     * Method called to associate a SysUsers object to this object
     * through the SysUsers foreign key attribute.
     *
     * @param    SysUsers $l SysUsers
     * @return SysUsers The current object (for fluent API support)
     */
    public function addSysUsersRelatedByIdUser(SysUsers $l)
    {
        if ($this->collSysUserssRelatedByIdUser === null) {
            $this->initSysUserssRelatedByIdUser();
            $this->collSysUserssRelatedByIdUserPartial = true;
        }

        if (!in_array($l, $this->collSysUserssRelatedByIdUser->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSysUsersRelatedByIdUser($l);

            if ($this->sysUserssRelatedByIdUserScheduledForDeletion and $this->sysUserssRelatedByIdUserScheduledForDeletion->contains($l)) {
                $this->sysUserssRelatedByIdUserScheduledForDeletion->remove($this->sysUserssRelatedByIdUserScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	SysUsersRelatedByIdUser $sysUsersRelatedByIdUser The sysUsersRelatedByIdUser object to add.
     */
    protected function doAddSysUsersRelatedByIdUser($sysUsersRelatedByIdUser)
    {
        $this->collSysUserssRelatedByIdUser[]= $sysUsersRelatedByIdUser;
        $sysUsersRelatedByIdUser->setSysUsersRelatedByUserModified($this);
    }

    /**
     * @param	SysUsersRelatedByIdUser $sysUsersRelatedByIdUser The sysUsersRelatedByIdUser object to remove.
     * @return SysUsers The current object (for fluent API support)
     */
    public function removeSysUsersRelatedByIdUser($sysUsersRelatedByIdUser)
    {
        if ($this->getSysUserssRelatedByIdUser()->contains($sysUsersRelatedByIdUser)) {
            $this->collSysUserssRelatedByIdUser->remove($this->collSysUserssRelatedByIdUser->search($sysUsersRelatedByIdUser));
            if (null === $this->sysUserssRelatedByIdUserScheduledForDeletion) {
                $this->sysUserssRelatedByIdUserScheduledForDeletion = clone $this->collSysUserssRelatedByIdUser;
                $this->sysUserssRelatedByIdUserScheduledForDeletion->clear();
            }
            $this->sysUserssRelatedByIdUserScheduledForDeletion[]= $sysUsersRelatedByIdUser;
            $sysUsersRelatedByIdUser->setSysUsersRelatedByUserModified(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysUsers is new, it will return
     * an empty collection; or if this SysUsers has previously
     * been saved, it will retrieve related SysUserssRelatedByIdUser from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysUsers.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SysUsers[] List of SysUsers objects
     */
    public function getSysUserssRelatedByIdUserJoinSysEnterprisesRelatedByIdEnterprise($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SysUsersQuery::create(null, $criteria);
        $query->joinWith('SysEnterprisesRelatedByIdEnterprise', $join_behavior);

        return $this->getSysUserssRelatedByIdUser($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysUsers is new, it will return
     * an empty collection; or if this SysUsers has previously
     * been saved, it will retrieve related SysUserssRelatedByIdUser from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysUsers.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SysUsers[] List of SysUsers objects
     */
    public function getSysUserssRelatedByIdUserJoinSysRoles($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SysUsersQuery::create(null, $criteria);
        $query->joinWith('SysRoles', $join_behavior);

        return $this->getSysUserssRelatedByIdUser($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysUsers is new, it will return
     * an empty collection; or if this SysUsers has previously
     * been saved, it will retrieve related SysUserssRelatedByIdUser from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysUsers.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SysUsers[] List of SysUsers objects
     */
    public function getSysUserssRelatedByIdUserJoinSysFiles($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SysUsersQuery::create(null, $criteria);
        $query->joinWith('SysFiles', $join_behavior);

        return $this->getSysUserssRelatedByIdUser($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_user = null;
        $this->username = null;
        $this->password = null;
        $this->email = null;
        $this->first_name = null;
        $this->last_name = null;
        $this->state = null;
        $this->id_rol = null;
        $this->id_photo = null;
        $this->created = null;
        $this->modified = null;
        $this->user_modified = null;
        $this->lang_code = null;
        $this->phone = null;
        $this->mobile = null;
        $this->id_time_zone = null;
        $this->id_city_birthday = null;
        $this->id_country_birthday = null;
        $this->id_city_address = null;
        $this->id_country_address = null;
        $this->gender = null;
        $this->birthday = null;
        $this->birthplace = null;
        $this->hash = null;
        $this->tour = null;
        $this->marital_status = null;
        $this->level_education = null;
        $this->occupation = null;
        $this->department_address = null;
        $this->department_birthday = null;
        $this->address = null;
        $this->id_church = null;
        $this->member = null;
        $this->pastor_baptism = null;
        $this->date_baptism = null;
        $this->id_enterprise = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCmsArticlessRelatedByIdUser) {
                foreach ($this->collCmsArticlessRelatedByIdUser as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCmsArticlessRelatedByUserModified) {
                foreach ($this->collCmsArticlessRelatedByUserModified as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCmsPagess) {
                foreach ($this->collCmsPagess as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCmsVideosXArticles) {
                foreach ($this->collCmsVideosXArticles as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCmsVideosXPages) {
                foreach ($this->collCmsVideosXPages as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSysChatssRelatedByIdSender) {
                foreach ($this->collSysChatssRelatedByIdSender as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSysChatssRelatedByIdReceiver) {
                foreach ($this->collSysChatssRelatedByIdReceiver as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSysEnterprisessRelatedByUserModified) {
                foreach ($this->collSysEnterprisessRelatedByUserModified as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSysNotificationssRelatedByIdSender) {
                foreach ($this->collSysNotificationssRelatedByIdSender as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSysNotificationssRelatedByIdReceiver) {
                foreach ($this->collSysNotificationssRelatedByIdReceiver as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSysPagess) {
                foreach ($this->collSysPagess as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSysUserssRelatedByIdUser) {
                foreach ($this->collSysUserssRelatedByIdUser as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aSysEnterprisesRelatedByIdEnterprise instanceof Persistent) {
              $this->aSysEnterprisesRelatedByIdEnterprise->clearAllReferences($deep);
            }
            if ($this->aSysRoles instanceof Persistent) {
              $this->aSysRoles->clearAllReferences($deep);
            }
            if ($this->aSysFiles instanceof Persistent) {
              $this->aSysFiles->clearAllReferences($deep);
            }
            if ($this->aSysUsersRelatedByUserModified instanceof Persistent) {
              $this->aSysUsersRelatedByUserModified->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCmsArticlessRelatedByIdUser instanceof PropelCollection) {
            $this->collCmsArticlessRelatedByIdUser->clearIterator();
        }
        $this->collCmsArticlessRelatedByIdUser = null;
        if ($this->collCmsArticlessRelatedByUserModified instanceof PropelCollection) {
            $this->collCmsArticlessRelatedByUserModified->clearIterator();
        }
        $this->collCmsArticlessRelatedByUserModified = null;
        if ($this->collCmsPagess instanceof PropelCollection) {
            $this->collCmsPagess->clearIterator();
        }
        $this->collCmsPagess = null;
        if ($this->collCmsVideosXArticles instanceof PropelCollection) {
            $this->collCmsVideosXArticles->clearIterator();
        }
        $this->collCmsVideosXArticles = null;
        if ($this->collCmsVideosXPages instanceof PropelCollection) {
            $this->collCmsVideosXPages->clearIterator();
        }
        $this->collCmsVideosXPages = null;
        if ($this->collSysChatssRelatedByIdSender instanceof PropelCollection) {
            $this->collSysChatssRelatedByIdSender->clearIterator();
        }
        $this->collSysChatssRelatedByIdSender = null;
        if ($this->collSysChatssRelatedByIdReceiver instanceof PropelCollection) {
            $this->collSysChatssRelatedByIdReceiver->clearIterator();
        }
        $this->collSysChatssRelatedByIdReceiver = null;
        if ($this->collSysEnterprisessRelatedByUserModified instanceof PropelCollection) {
            $this->collSysEnterprisessRelatedByUserModified->clearIterator();
        }
        $this->collSysEnterprisessRelatedByUserModified = null;
        if ($this->collSysNotificationssRelatedByIdSender instanceof PropelCollection) {
            $this->collSysNotificationssRelatedByIdSender->clearIterator();
        }
        $this->collSysNotificationssRelatedByIdSender = null;
        if ($this->collSysNotificationssRelatedByIdReceiver instanceof PropelCollection) {
            $this->collSysNotificationssRelatedByIdReceiver->clearIterator();
        }
        $this->collSysNotificationssRelatedByIdReceiver = null;
        if ($this->collSysPagess instanceof PropelCollection) {
            $this->collSysPagess->clearIterator();
        }
        $this->collSysPagess = null;
        if ($this->collSysUserssRelatedByIdUser instanceof PropelCollection) {
            $this->collSysUserssRelatedByIdUser->clearIterator();
        }
        $this->collSysUserssRelatedByIdUser = null;
        $this->aSysEnterprisesRelatedByIdEnterprise = null;
        $this->aSysRoles = null;
        $this->aSysFiles = null;
        $this->aSysUsersRelatedByUserModified = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SysUsersPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}

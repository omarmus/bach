<?php


/**
 * Base class that represents a query for the 'cms_files_x_page' table.
 *
 *
 *
 * @method CmsFilesXPageQuery orderByIdFile($order = Criteria::ASC) Order by the id_file column
 * @method CmsFilesXPageQuery orderByIdPage($order = Criteria::ASC) Order by the id_page column
 * @method CmsFilesXPageQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method CmsFilesXPageQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method CmsFilesXPageQuery orderByPrimary($order = Criteria::ASC) Order by the primary column
 * @method CmsFilesXPageQuery orderByState($order = Criteria::ASC) Order by the state column
 *
 * @method CmsFilesXPageQuery groupByIdFile() Group by the id_file column
 * @method CmsFilesXPageQuery groupByIdPage() Group by the id_page column
 * @method CmsFilesXPageQuery groupByType() Group by the type column
 * @method CmsFilesXPageQuery groupByDescription() Group by the description column
 * @method CmsFilesXPageQuery groupByPrimary() Group by the primary column
 * @method CmsFilesXPageQuery groupByState() Group by the state column
 *
 * @method CmsFilesXPageQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CmsFilesXPageQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CmsFilesXPageQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CmsFilesXPageQuery leftJoinSysFiles($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysFiles relation
 * @method CmsFilesXPageQuery rightJoinSysFiles($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysFiles relation
 * @method CmsFilesXPageQuery innerJoinSysFiles($relationAlias = null) Adds a INNER JOIN clause to the query using the SysFiles relation
 *
 * @method CmsFilesXPageQuery leftJoinCmsPages($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsPages relation
 * @method CmsFilesXPageQuery rightJoinCmsPages($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsPages relation
 * @method CmsFilesXPageQuery innerJoinCmsPages($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsPages relation
 *
 * @method CmsFilesXPage findOne(PropelPDO $con = null) Return the first CmsFilesXPage matching the query
 * @method CmsFilesXPage findOneOrCreate(PropelPDO $con = null) Return the first CmsFilesXPage matching the query, or a new CmsFilesXPage object populated from the query conditions when no match is found
 *
 * @method CmsFilesXPage findOneByIdPage(int $id_page) Return the first CmsFilesXPage filtered by the id_page column
 * @method CmsFilesXPage findOneByType(string $type) Return the first CmsFilesXPage filtered by the type column
 * @method CmsFilesXPage findOneByDescription(string $description) Return the first CmsFilesXPage filtered by the description column
 * @method CmsFilesXPage findOneByPrimary(string $primary) Return the first CmsFilesXPage filtered by the primary column
 * @method CmsFilesXPage findOneByState(string $state) Return the first CmsFilesXPage filtered by the state column
 *
 * @method array findByIdFile(int $id_file) Return CmsFilesXPage objects filtered by the id_file column
 * @method array findByIdPage(int $id_page) Return CmsFilesXPage objects filtered by the id_page column
 * @method array findByType(string $type) Return CmsFilesXPage objects filtered by the type column
 * @method array findByDescription(string $description) Return CmsFilesXPage objects filtered by the description column
 * @method array findByPrimary(string $primary) Return CmsFilesXPage objects filtered by the primary column
 * @method array findByState(string $state) Return CmsFilesXPage objects filtered by the state column
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseCmsFilesXPageQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCmsFilesXPageQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bach';
        }
        if (null === $modelName) {
            $modelName = 'CmsFilesXPage';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CmsFilesXPageQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CmsFilesXPageQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CmsFilesXPageQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CmsFilesXPageQuery) {
            return $criteria;
        }
        $query = new CmsFilesXPageQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CmsFilesXPage|CmsFilesXPage[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CmsFilesXPagePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CmsFilesXPagePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CmsFilesXPage A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdFile($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CmsFilesXPage A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_file`, `id_page`, `type`, `description`, `primary`, `state` FROM `cms_files_x_page` WHERE `id_file` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CmsFilesXPage();
            $obj->hydrate($row);
            CmsFilesXPagePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CmsFilesXPage|CmsFilesXPage[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CmsFilesXPage[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CmsFilesXPageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CmsFilesXPagePeer::ID_FILE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CmsFilesXPageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CmsFilesXPagePeer::ID_FILE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_file column
     *
     * Example usage:
     * <code>
     * $query->filterByIdFile(1234); // WHERE id_file = 1234
     * $query->filterByIdFile(array(12, 34)); // WHERE id_file IN (12, 34)
     * $query->filterByIdFile(array('min' => 12)); // WHERE id_file >= 12
     * $query->filterByIdFile(array('max' => 12)); // WHERE id_file <= 12
     * </code>
     *
     * @see       filterBySysFiles()
     *
     * @param     mixed $idFile The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsFilesXPageQuery The current query, for fluid interface
     */
    public function filterByIdFile($idFile = null, $comparison = null)
    {
        if (is_array($idFile)) {
            $useMinMax = false;
            if (isset($idFile['min'])) {
                $this->addUsingAlias(CmsFilesXPagePeer::ID_FILE, $idFile['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idFile['max'])) {
                $this->addUsingAlias(CmsFilesXPagePeer::ID_FILE, $idFile['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsFilesXPagePeer::ID_FILE, $idFile, $comparison);
    }

    /**
     * Filter the query on the id_page column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPage(1234); // WHERE id_page = 1234
     * $query->filterByIdPage(array(12, 34)); // WHERE id_page IN (12, 34)
     * $query->filterByIdPage(array('min' => 12)); // WHERE id_page >= 12
     * $query->filterByIdPage(array('max' => 12)); // WHERE id_page <= 12
     * </code>
     *
     * @see       filterByCmsPages()
     *
     * @param     mixed $idPage The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsFilesXPageQuery The current query, for fluid interface
     */
    public function filterByIdPage($idPage = null, $comparison = null)
    {
        if (is_array($idPage)) {
            $useMinMax = false;
            if (isset($idPage['min'])) {
                $this->addUsingAlias(CmsFilesXPagePeer::ID_PAGE, $idPage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPage['max'])) {
                $this->addUsingAlias(CmsFilesXPagePeer::ID_PAGE, $idPage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsFilesXPagePeer::ID_PAGE, $idPage, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%'); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsFilesXPageQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $type)) {
                $type = str_replace('*', '%', $type);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsFilesXPagePeer::TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsFilesXPageQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsFilesXPagePeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the primary column
     *
     * Example usage:
     * <code>
     * $query->filterByPrimary('fooValue');   // WHERE primary = 'fooValue'
     * $query->filterByPrimary('%fooValue%'); // WHERE primary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $primary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsFilesXPageQuery The current query, for fluid interface
     */
    public function filterByPrimary($primary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($primary)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $primary)) {
                $primary = str_replace('*', '%', $primary);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsFilesXPagePeer::PRIMARY, $primary, $comparison);
    }

    /**
     * Filter the query on the state column
     *
     * Example usage:
     * <code>
     * $query->filterByState('fooValue');   // WHERE state = 'fooValue'
     * $query->filterByState('%fooValue%'); // WHERE state LIKE '%fooValue%'
     * </code>
     *
     * @param     string $state The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsFilesXPageQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($state)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $state)) {
                $state = str_replace('*', '%', $state);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsFilesXPagePeer::STATE, $state, $comparison);
    }

    /**
     * Filter the query by a related SysFiles object
     *
     * @param   SysFiles|PropelObjectCollection $sysFiles The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsFilesXPageQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysFiles($sysFiles, $comparison = null)
    {
        if ($sysFiles instanceof SysFiles) {
            return $this
                ->addUsingAlias(CmsFilesXPagePeer::ID_FILE, $sysFiles->getIdFile(), $comparison);
        } elseif ($sysFiles instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CmsFilesXPagePeer::ID_FILE, $sysFiles->toKeyValue('PrimaryKey', 'IdFile'), $comparison);
        } else {
            throw new PropelException('filterBySysFiles() only accepts arguments of type SysFiles or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysFiles relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsFilesXPageQuery The current query, for fluid interface
     */
    public function joinSysFiles($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysFiles');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysFiles');
        }

        return $this;
    }

    /**
     * Use the SysFiles relation SysFiles object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysFilesQuery A secondary query class using the current class as primary query
     */
    public function useSysFilesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSysFiles($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysFiles', 'SysFilesQuery');
    }

    /**
     * Filter the query by a related CmsPages object
     *
     * @param   CmsPages|PropelObjectCollection $cmsPages The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsFilesXPageQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsPages($cmsPages, $comparison = null)
    {
        if ($cmsPages instanceof CmsPages) {
            return $this
                ->addUsingAlias(CmsFilesXPagePeer::ID_PAGE, $cmsPages->getIdPage(), $comparison);
        } elseif ($cmsPages instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CmsFilesXPagePeer::ID_PAGE, $cmsPages->toKeyValue('PrimaryKey', 'IdPage'), $comparison);
        } else {
            throw new PropelException('filterByCmsPages() only accepts arguments of type CmsPages or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsPages relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsFilesXPageQuery The current query, for fluid interface
     */
    public function joinCmsPages($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsPages');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsPages');
        }

        return $this;
    }

    /**
     * Use the CmsPages relation CmsPages object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsPagesQuery A secondary query class using the current class as primary query
     */
    public function useCmsPagesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCmsPages($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsPages', 'CmsPagesQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CmsFilesXPage $cmsFilesXPage Object to remove from the list of results
     *
     * @return CmsFilesXPageQuery The current query, for fluid interface
     */
    public function prune($cmsFilesXPage = null)
    {
        if ($cmsFilesXPage) {
            $this->addUsingAlias(CmsFilesXPagePeer::ID_FILE, $cmsFilesXPage->getIdFile(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

<?php


/**
 * Base class that represents a query for the 'cms_tags' table.
 *
 *
 *
 * @method CmsTagsQuery orderByIdTag($order = Criteria::ASC) Order by the id_tag column
 * @method CmsTagsQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method CmsTagsQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method CmsTagsQuery orderByIdFile($order = Criteria::ASC) Order by the id_file column
 * @method CmsTagsQuery orderByState($order = Criteria::ASC) Order by the state column
 * @method CmsTagsQuery orderByIdEnterprise($order = Criteria::ASC) Order by the id_enterprise column
 *
 * @method CmsTagsQuery groupByIdTag() Group by the id_tag column
 * @method CmsTagsQuery groupByName() Group by the name column
 * @method CmsTagsQuery groupByDescription() Group by the description column
 * @method CmsTagsQuery groupByIdFile() Group by the id_file column
 * @method CmsTagsQuery groupByState() Group by the state column
 * @method CmsTagsQuery groupByIdEnterprise() Group by the id_enterprise column
 *
 * @method CmsTagsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CmsTagsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CmsTagsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CmsTagsQuery leftJoinSysEnterprises($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysEnterprises relation
 * @method CmsTagsQuery rightJoinSysEnterprises($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysEnterprises relation
 * @method CmsTagsQuery innerJoinSysEnterprises($relationAlias = null) Adds a INNER JOIN clause to the query using the SysEnterprises relation
 *
 * @method CmsTagsQuery leftJoinSysFiles($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysFiles relation
 * @method CmsTagsQuery rightJoinSysFiles($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysFiles relation
 * @method CmsTagsQuery innerJoinSysFiles($relationAlias = null) Adds a INNER JOIN clause to the query using the SysFiles relation
 *
 * @method CmsTagsQuery leftJoinCmsTagsXArticle($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsTagsXArticle relation
 * @method CmsTagsQuery rightJoinCmsTagsXArticle($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsTagsXArticle relation
 * @method CmsTagsQuery innerJoinCmsTagsXArticle($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsTagsXArticle relation
 *
 * @method CmsTags findOne(PropelPDO $con = null) Return the first CmsTags matching the query
 * @method CmsTags findOneOrCreate(PropelPDO $con = null) Return the first CmsTags matching the query, or a new CmsTags object populated from the query conditions when no match is found
 *
 * @method CmsTags findOneByName(string $name) Return the first CmsTags filtered by the name column
 * @method CmsTags findOneByDescription(string $description) Return the first CmsTags filtered by the description column
 * @method CmsTags findOneByIdFile(int $id_file) Return the first CmsTags filtered by the id_file column
 * @method CmsTags findOneByState(string $state) Return the first CmsTags filtered by the state column
 * @method CmsTags findOneByIdEnterprise(int $id_enterprise) Return the first CmsTags filtered by the id_enterprise column
 *
 * @method array findByIdTag(int $id_tag) Return CmsTags objects filtered by the id_tag column
 * @method array findByName(string $name) Return CmsTags objects filtered by the name column
 * @method array findByDescription(string $description) Return CmsTags objects filtered by the description column
 * @method array findByIdFile(int $id_file) Return CmsTags objects filtered by the id_file column
 * @method array findByState(string $state) Return CmsTags objects filtered by the state column
 * @method array findByIdEnterprise(int $id_enterprise) Return CmsTags objects filtered by the id_enterprise column
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseCmsTagsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCmsTagsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bach';
        }
        if (null === $modelName) {
            $modelName = 'CmsTags';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CmsTagsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CmsTagsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CmsTagsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CmsTagsQuery) {
            return $criteria;
        }
        $query = new CmsTagsQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CmsTags|CmsTags[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CmsTagsPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CmsTagsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CmsTags A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdTag($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CmsTags A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_tag`, `name`, `description`, `id_file`, `state`, `id_enterprise` FROM `cms_tags` WHERE `id_tag` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CmsTags();
            $obj->hydrate($row);
            CmsTagsPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CmsTags|CmsTags[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CmsTags[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CmsTagsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CmsTagsPeer::ID_TAG, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CmsTagsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CmsTagsPeer::ID_TAG, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_tag column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTag(1234); // WHERE id_tag = 1234
     * $query->filterByIdTag(array(12, 34)); // WHERE id_tag IN (12, 34)
     * $query->filterByIdTag(array('min' => 12)); // WHERE id_tag >= 12
     * $query->filterByIdTag(array('max' => 12)); // WHERE id_tag <= 12
     * </code>
     *
     * @param     mixed $idTag The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsTagsQuery The current query, for fluid interface
     */
    public function filterByIdTag($idTag = null, $comparison = null)
    {
        if (is_array($idTag)) {
            $useMinMax = false;
            if (isset($idTag['min'])) {
                $this->addUsingAlias(CmsTagsPeer::ID_TAG, $idTag['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTag['max'])) {
                $this->addUsingAlias(CmsTagsPeer::ID_TAG, $idTag['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsTagsPeer::ID_TAG, $idTag, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsTagsQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsTagsPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsTagsQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsTagsPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the id_file column
     *
     * Example usage:
     * <code>
     * $query->filterByIdFile(1234); // WHERE id_file = 1234
     * $query->filterByIdFile(array(12, 34)); // WHERE id_file IN (12, 34)
     * $query->filterByIdFile(array('min' => 12)); // WHERE id_file >= 12
     * $query->filterByIdFile(array('max' => 12)); // WHERE id_file <= 12
     * </code>
     *
     * @see       filterBySysFiles()
     *
     * @param     mixed $idFile The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsTagsQuery The current query, for fluid interface
     */
    public function filterByIdFile($idFile = null, $comparison = null)
    {
        if (is_array($idFile)) {
            $useMinMax = false;
            if (isset($idFile['min'])) {
                $this->addUsingAlias(CmsTagsPeer::ID_FILE, $idFile['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idFile['max'])) {
                $this->addUsingAlias(CmsTagsPeer::ID_FILE, $idFile['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsTagsPeer::ID_FILE, $idFile, $comparison);
    }

    /**
     * Filter the query on the state column
     *
     * Example usage:
     * <code>
     * $query->filterByState('fooValue');   // WHERE state = 'fooValue'
     * $query->filterByState('%fooValue%'); // WHERE state LIKE '%fooValue%'
     * </code>
     *
     * @param     string $state The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsTagsQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($state)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $state)) {
                $state = str_replace('*', '%', $state);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsTagsPeer::STATE, $state, $comparison);
    }

    /**
     * Filter the query on the id_enterprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEnterprise(1234); // WHERE id_enterprise = 1234
     * $query->filterByIdEnterprise(array(12, 34)); // WHERE id_enterprise IN (12, 34)
     * $query->filterByIdEnterprise(array('min' => 12)); // WHERE id_enterprise >= 12
     * $query->filterByIdEnterprise(array('max' => 12)); // WHERE id_enterprise <= 12
     * </code>
     *
     * @see       filterBySysEnterprises()
     *
     * @param     mixed $idEnterprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsTagsQuery The current query, for fluid interface
     */
    public function filterByIdEnterprise($idEnterprise = null, $comparison = null)
    {
        if (is_array($idEnterprise)) {
            $useMinMax = false;
            if (isset($idEnterprise['min'])) {
                $this->addUsingAlias(CmsTagsPeer::ID_ENTERPRISE, $idEnterprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEnterprise['max'])) {
                $this->addUsingAlias(CmsTagsPeer::ID_ENTERPRISE, $idEnterprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsTagsPeer::ID_ENTERPRISE, $idEnterprise, $comparison);
    }

    /**
     * Filter the query by a related SysEnterprises object
     *
     * @param   SysEnterprises|PropelObjectCollection $sysEnterprises The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsTagsQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysEnterprises($sysEnterprises, $comparison = null)
    {
        if ($sysEnterprises instanceof SysEnterprises) {
            return $this
                ->addUsingAlias(CmsTagsPeer::ID_ENTERPRISE, $sysEnterprises->getIdEnterprise(), $comparison);
        } elseif ($sysEnterprises instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CmsTagsPeer::ID_ENTERPRISE, $sysEnterprises->toKeyValue('PrimaryKey', 'IdEnterprise'), $comparison);
        } else {
            throw new PropelException('filterBySysEnterprises() only accepts arguments of type SysEnterprises or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysEnterprises relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsTagsQuery The current query, for fluid interface
     */
    public function joinSysEnterprises($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysEnterprises');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysEnterprises');
        }

        return $this;
    }

    /**
     * Use the SysEnterprises relation SysEnterprises object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysEnterprisesQuery A secondary query class using the current class as primary query
     */
    public function useSysEnterprisesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysEnterprises($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysEnterprises', 'SysEnterprisesQuery');
    }

    /**
     * Filter the query by a related SysFiles object
     *
     * @param   SysFiles|PropelObjectCollection $sysFiles The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsTagsQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysFiles($sysFiles, $comparison = null)
    {
        if ($sysFiles instanceof SysFiles) {
            return $this
                ->addUsingAlias(CmsTagsPeer::ID_FILE, $sysFiles->getIdFile(), $comparison);
        } elseif ($sysFiles instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CmsTagsPeer::ID_FILE, $sysFiles->toKeyValue('PrimaryKey', 'IdFile'), $comparison);
        } else {
            throw new PropelException('filterBySysFiles() only accepts arguments of type SysFiles or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysFiles relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsTagsQuery The current query, for fluid interface
     */
    public function joinSysFiles($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysFiles');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysFiles');
        }

        return $this;
    }

    /**
     * Use the SysFiles relation SysFiles object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysFilesQuery A secondary query class using the current class as primary query
     */
    public function useSysFilesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysFiles($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysFiles', 'SysFilesQuery');
    }

    /**
     * Filter the query by a related CmsTagsXArticle object
     *
     * @param   CmsTagsXArticle|PropelObjectCollection $cmsTagsXArticle  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsTagsQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsTagsXArticle($cmsTagsXArticle, $comparison = null)
    {
        if ($cmsTagsXArticle instanceof CmsTagsXArticle) {
            return $this
                ->addUsingAlias(CmsTagsPeer::ID_TAG, $cmsTagsXArticle->getIdTag(), $comparison);
        } elseif ($cmsTagsXArticle instanceof PropelObjectCollection) {
            return $this
                ->useCmsTagsXArticleQuery()
                ->filterByPrimaryKeys($cmsTagsXArticle->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCmsTagsXArticle() only accepts arguments of type CmsTagsXArticle or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsTagsXArticle relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsTagsQuery The current query, for fluid interface
     */
    public function joinCmsTagsXArticle($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsTagsXArticle');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsTagsXArticle');
        }

        return $this;
    }

    /**
     * Use the CmsTagsXArticle relation CmsTagsXArticle object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsTagsXArticleQuery A secondary query class using the current class as primary query
     */
    public function useCmsTagsXArticleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCmsTagsXArticle($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsTagsXArticle', 'CmsTagsXArticleQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CmsTags $cmsTags Object to remove from the list of results
     *
     * @return CmsTagsQuery The current query, for fluid interface
     */
    public function prune($cmsTags = null)
    {
        if ($cmsTags) {
            $this->addUsingAlias(CmsTagsPeer::ID_TAG, $cmsTags->getIdTag(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

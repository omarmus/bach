<?php


/**
 * Base class that represents a query for the 'sys_time_zone' table.
 *
 *
 *
 * @method SysTimeZoneQuery orderByIdTimeZone($order = Criteria::ASC) Order by the id_time_zone column
 * @method SysTimeZoneQuery orderByRegion($order = Criteria::ASC) Order by the region column
 * @method SysTimeZoneQuery orderByCountry($order = Criteria::ASC) Order by the country column
 * @method SysTimeZoneQuery orderByCity($order = Criteria::ASC) Order by the city column
 * @method SysTimeZoneQuery orderByState($order = Criteria::ASC) Order by the state column
 *
 * @method SysTimeZoneQuery groupByIdTimeZone() Group by the id_time_zone column
 * @method SysTimeZoneQuery groupByRegion() Group by the region column
 * @method SysTimeZoneQuery groupByCountry() Group by the country column
 * @method SysTimeZoneQuery groupByCity() Group by the city column
 * @method SysTimeZoneQuery groupByState() Group by the state column
 *
 * @method SysTimeZoneQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SysTimeZoneQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SysTimeZoneQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SysTimeZone findOne(PropelPDO $con = null) Return the first SysTimeZone matching the query
 * @method SysTimeZone findOneOrCreate(PropelPDO $con = null) Return the first SysTimeZone matching the query, or a new SysTimeZone object populated from the query conditions when no match is found
 *
 * @method SysTimeZone findOneByRegion(string $region) Return the first SysTimeZone filtered by the region column
 * @method SysTimeZone findOneByCountry(string $country) Return the first SysTimeZone filtered by the country column
 * @method SysTimeZone findOneByCity(string $city) Return the first SysTimeZone filtered by the city column
 * @method SysTimeZone findOneByState(string $state) Return the first SysTimeZone filtered by the state column
 *
 * @method array findByIdTimeZone(int $id_time_zone) Return SysTimeZone objects filtered by the id_time_zone column
 * @method array findByRegion(string $region) Return SysTimeZone objects filtered by the region column
 * @method array findByCountry(string $country) Return SysTimeZone objects filtered by the country column
 * @method array findByCity(string $city) Return SysTimeZone objects filtered by the city column
 * @method array findByState(string $state) Return SysTimeZone objects filtered by the state column
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseSysTimeZoneQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSysTimeZoneQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bach';
        }
        if (null === $modelName) {
            $modelName = 'SysTimeZone';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SysTimeZoneQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SysTimeZoneQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SysTimeZoneQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SysTimeZoneQuery) {
            return $criteria;
        }
        $query = new SysTimeZoneQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SysTimeZone|SysTimeZone[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SysTimeZonePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SysTimeZonePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SysTimeZone A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdTimeZone($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SysTimeZone A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_time_zone`, `region`, `country`, `city`, `state` FROM `sys_time_zone` WHERE `id_time_zone` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SysTimeZone();
            $obj->hydrate($row);
            SysTimeZonePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SysTimeZone|SysTimeZone[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SysTimeZone[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SysTimeZoneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SysTimeZonePeer::ID_TIME_ZONE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SysTimeZoneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SysTimeZonePeer::ID_TIME_ZONE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_time_zone column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTimeZone(1234); // WHERE id_time_zone = 1234
     * $query->filterByIdTimeZone(array(12, 34)); // WHERE id_time_zone IN (12, 34)
     * $query->filterByIdTimeZone(array('min' => 12)); // WHERE id_time_zone >= 12
     * $query->filterByIdTimeZone(array('max' => 12)); // WHERE id_time_zone <= 12
     * </code>
     *
     * @param     mixed $idTimeZone The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysTimeZoneQuery The current query, for fluid interface
     */
    public function filterByIdTimeZone($idTimeZone = null, $comparison = null)
    {
        if (is_array($idTimeZone)) {
            $useMinMax = false;
            if (isset($idTimeZone['min'])) {
                $this->addUsingAlias(SysTimeZonePeer::ID_TIME_ZONE, $idTimeZone['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTimeZone['max'])) {
                $this->addUsingAlias(SysTimeZonePeer::ID_TIME_ZONE, $idTimeZone['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysTimeZonePeer::ID_TIME_ZONE, $idTimeZone, $comparison);
    }

    /**
     * Filter the query on the region column
     *
     * Example usage:
     * <code>
     * $query->filterByRegion('fooValue');   // WHERE region = 'fooValue'
     * $query->filterByRegion('%fooValue%'); // WHERE region LIKE '%fooValue%'
     * </code>
     *
     * @param     string $region The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysTimeZoneQuery The current query, for fluid interface
     */
    public function filterByRegion($region = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($region)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $region)) {
                $region = str_replace('*', '%', $region);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysTimeZonePeer::REGION, $region, $comparison);
    }

    /**
     * Filter the query on the country column
     *
     * Example usage:
     * <code>
     * $query->filterByCountry('fooValue');   // WHERE country = 'fooValue'
     * $query->filterByCountry('%fooValue%'); // WHERE country LIKE '%fooValue%'
     * </code>
     *
     * @param     string $country The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysTimeZoneQuery The current query, for fluid interface
     */
    public function filterByCountry($country = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($country)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $country)) {
                $country = str_replace('*', '%', $country);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysTimeZonePeer::COUNTRY, $country, $comparison);
    }

    /**
     * Filter the query on the city column
     *
     * Example usage:
     * <code>
     * $query->filterByCity('fooValue');   // WHERE city = 'fooValue'
     * $query->filterByCity('%fooValue%'); // WHERE city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $city The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysTimeZoneQuery The current query, for fluid interface
     */
    public function filterByCity($city = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($city)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $city)) {
                $city = str_replace('*', '%', $city);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysTimeZonePeer::CITY, $city, $comparison);
    }

    /**
     * Filter the query on the state column
     *
     * Example usage:
     * <code>
     * $query->filterByState('fooValue');   // WHERE state = 'fooValue'
     * $query->filterByState('%fooValue%'); // WHERE state LIKE '%fooValue%'
     * </code>
     *
     * @param     string $state The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysTimeZoneQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($state)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $state)) {
                $state = str_replace('*', '%', $state);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysTimeZonePeer::STATE, $state, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   SysTimeZone $sysTimeZone Object to remove from the list of results
     *
     * @return SysTimeZoneQuery The current query, for fluid interface
     */
    public function prune($sysTimeZone = null)
    {
        if ($sysTimeZone) {
            $this->addUsingAlias(SysTimeZonePeer::ID_TIME_ZONE, $sysTimeZone->getIdTimeZone(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

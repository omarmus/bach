<?php


/**
 * Base class that represents a row from the 'sys_enterprises' table.
 *
 *
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseSysEnterprises extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'SysEnterprisesPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        SysEnterprisesPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_enterprise field.
     * @var        int
     */
    protected $id_enterprise;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the number_register field.
     * @var        string
     */
    protected $number_register;

    /**
     * The value for the address field.
     * @var        string
     */
    protected $address;

    /**
     * The value for the phone field.
     * @var        string
     */
    protected $phone;

    /**
     * The value for the fax field.
     * @var        string
     */
    protected $fax;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * The value for the web_page field.
     * @var        string
     */
    protected $web_page;

    /**
     * The value for the state field.
     * Note: this column has a database default value of: 'ACTIVE'
     * @var        string
     */
    protected $state;

    /**
     * The value for the created field.
     * @var        string
     */
    protected $created;

    /**
     * The value for the modified field.
     * @var        string
     */
    protected $modified;

    /**
     * The value for the user_modified field.
     * @var        int
     */
    protected $user_modified;

    /**
     * @var        SysUsers
     */
    protected $aSysUsersRelatedByUserModified;

    /**
     * @var        PropelObjectCollection|CmsArticles[] Collection to store aggregation of CmsArticles objects.
     */
    protected $collCmsArticless;
    protected $collCmsArticlessPartial;

    /**
     * @var        PropelObjectCollection|CmsPages[] Collection to store aggregation of CmsPages objects.
     */
    protected $collCmsPagess;
    protected $collCmsPagessPartial;

    /**
     * @var        PropelObjectCollection|CmsTags[] Collection to store aggregation of CmsTags objects.
     */
    protected $collCmsTagss;
    protected $collCmsTagssPartial;

    /**
     * @var        PropelObjectCollection|SmiChurches[] Collection to store aggregation of SmiChurches objects.
     */
    protected $collSmiChurchess;
    protected $collSmiChurchessPartial;

    /**
     * @var        PropelObjectCollection|SysUsers[] Collection to store aggregation of SysUsers objects.
     */
    protected $collSysUserssRelatedByIdEnterprise;
    protected $collSysUserssRelatedByIdEnterprisePartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $cmsArticlessScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $cmsPagessScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $cmsTagssScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $smiChurchessScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $sysUserssRelatedByIdEnterpriseScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->state = 'ACTIVE';
    }

    /**
     * Initializes internal state of BaseSysEnterprises object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id_enterprise] column value.
     *
     * @return int
     */
    public function getIdEnterprise()
    {

        return $this->id_enterprise;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * Get the [number_register] column value.
     *
     * @return string
     */
    public function getNumberRegister()
    {

        return $this->number_register;
    }

    /**
     * Get the [address] column value.
     *
     * @return string
     */
    public function getAddress()
    {

        return $this->address;
    }

    /**
     * Get the [phone] column value.
     *
     * @return string
     */
    public function getPhone()
    {

        return $this->phone;
    }

    /**
     * Get the [fax] column value.
     *
     * @return string
     */
    public function getFax()
    {

        return $this->fax;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Get the [web_page] column value.
     *
     * @return string
     */
    public function getWebPage()
    {

        return $this->web_page;
    }

    /**
     * Get the [state] column value.
     *
     * @return string
     */
    public function getState()
    {

        return $this->state;
    }

    /**
     * Get the [optionally formatted] temporal [created] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreated($format = 'Y-m-d H:i:s')
    {
        if ($this->created === null) {
            return null;
        }

        if ($this->created === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->created);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [modified] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getModified($format = 'Y-m-d H:i:s')
    {
        if ($this->modified === null) {
            return null;
        }

        if ($this->modified === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->modified);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->modified, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [user_modified] column value.
     *
     * @return int
     */
    public function getUserModified()
    {

        return $this->user_modified;
    }

    /**
     * Set the value of [id_enterprise] column.
     *
     * @param  int $v new value
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function setIdEnterprise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_enterprise !== $v) {
            $this->id_enterprise = $v;
            $this->modifiedColumns[] = SysEnterprisesPeer::ID_ENTERPRISE;
        }


        return $this;
    } // setIdEnterprise()

    /**
     * Set the value of [name] column.
     *
     * @param  string $v new value
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = SysEnterprisesPeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [number_register] column.
     *
     * @param  string $v new value
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function setNumberRegister($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->number_register !== $v) {
            $this->number_register = $v;
            $this->modifiedColumns[] = SysEnterprisesPeer::NUMBER_REGISTER;
        }


        return $this;
    } // setNumberRegister()

    /**
     * Set the value of [address] column.
     *
     * @param  string $v new value
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function setAddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->address !== $v) {
            $this->address = $v;
            $this->modifiedColumns[] = SysEnterprisesPeer::ADDRESS;
        }


        return $this;
    } // setAddress()

    /**
     * Set the value of [phone] column.
     *
     * @param  string $v new value
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phone !== $v) {
            $this->phone = $v;
            $this->modifiedColumns[] = SysEnterprisesPeer::PHONE;
        }


        return $this;
    } // setPhone()

    /**
     * Set the value of [fax] column.
     *
     * @param  string $v new value
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function setFax($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->fax !== $v) {
            $this->fax = $v;
            $this->modifiedColumns[] = SysEnterprisesPeer::FAX;
        }


        return $this;
    } // setFax()

    /**
     * Set the value of [email] column.
     *
     * @param  string $v new value
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = SysEnterprisesPeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [web_page] column.
     *
     * @param  string $v new value
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function setWebPage($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->web_page !== $v) {
            $this->web_page = $v;
            $this->modifiedColumns[] = SysEnterprisesPeer::WEB_PAGE;
        }


        return $this;
    } // setWebPage()

    /**
     * Set the value of [state] column.
     *
     * @param  string $v new value
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function setState($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->state !== $v) {
            $this->state = $v;
            $this->modifiedColumns[] = SysEnterprisesPeer::STATE;
        }


        return $this;
    } // setState()

    /**
     * Sets the value of [created] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function setCreated($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created !== null || $dt !== null) {
            $currentDateAsString = ($this->created !== null && $tmpDt = new DateTime($this->created)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->created = $newDateAsString;
                $this->modifiedColumns[] = SysEnterprisesPeer::CREATED;
            }
        } // if either are not null


        return $this;
    } // setCreated()

    /**
     * Sets the value of [modified] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function setModified($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->modified !== null || $dt !== null) {
            $currentDateAsString = ($this->modified !== null && $tmpDt = new DateTime($this->modified)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->modified = $newDateAsString;
                $this->modifiedColumns[] = SysEnterprisesPeer::MODIFIED;
            }
        } // if either are not null


        return $this;
    } // setModified()

    /**
     * Set the value of [user_modified] column.
     *
     * @param  int $v new value
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function setUserModified($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->user_modified !== $v) {
            $this->user_modified = $v;
            $this->modifiedColumns[] = SysEnterprisesPeer::USER_MODIFIED;
        }

        if ($this->aSysUsersRelatedByUserModified !== null && $this->aSysUsersRelatedByUserModified->getIdUser() !== $v) {
            $this->aSysUsersRelatedByUserModified = null;
        }


        return $this;
    } // setUserModified()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->state !== 'ACTIVE') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_enterprise = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->name = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->number_register = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->address = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->phone = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->fax = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->email = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->web_page = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->state = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->created = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->modified = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->user_modified = ($row[$startcol + 11] !== null) ? (int) $row[$startcol + 11] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 12; // 12 = SysEnterprisesPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating SysEnterprises object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSysUsersRelatedByUserModified !== null && $this->user_modified !== $this->aSysUsersRelatedByUserModified->getIdUser()) {
            $this->aSysUsersRelatedByUserModified = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SysEnterprisesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = SysEnterprisesPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSysUsersRelatedByUserModified = null;
            $this->collCmsArticless = null;

            $this->collCmsPagess = null;

            $this->collCmsTagss = null;

            $this->collSmiChurchess = null;

            $this->collSysUserssRelatedByIdEnterprise = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SysEnterprisesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = SysEnterprisesQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(SysEnterprisesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SysEnterprisesPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSysUsersRelatedByUserModified !== null) {
                if ($this->aSysUsersRelatedByUserModified->isModified() || $this->aSysUsersRelatedByUserModified->isNew()) {
                    $affectedRows += $this->aSysUsersRelatedByUserModified->save($con);
                }
                $this->setSysUsersRelatedByUserModified($this->aSysUsersRelatedByUserModified);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->cmsArticlessScheduledForDeletion !== null) {
                if (!$this->cmsArticlessScheduledForDeletion->isEmpty()) {
                    foreach ($this->cmsArticlessScheduledForDeletion as $cmsArticles) {
                        // need to save related object because we set the relation to null
                        $cmsArticles->save($con);
                    }
                    $this->cmsArticlessScheduledForDeletion = null;
                }
            }

            if ($this->collCmsArticless !== null) {
                foreach ($this->collCmsArticless as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->cmsPagessScheduledForDeletion !== null) {
                if (!$this->cmsPagessScheduledForDeletion->isEmpty()) {
                    foreach ($this->cmsPagessScheduledForDeletion as $cmsPages) {
                        // need to save related object because we set the relation to null
                        $cmsPages->save($con);
                    }
                    $this->cmsPagessScheduledForDeletion = null;
                }
            }

            if ($this->collCmsPagess !== null) {
                foreach ($this->collCmsPagess as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->cmsTagssScheduledForDeletion !== null) {
                if (!$this->cmsTagssScheduledForDeletion->isEmpty()) {
                    foreach ($this->cmsTagssScheduledForDeletion as $cmsTags) {
                        // need to save related object because we set the relation to null
                        $cmsTags->save($con);
                    }
                    $this->cmsTagssScheduledForDeletion = null;
                }
            }

            if ($this->collCmsTagss !== null) {
                foreach ($this->collCmsTagss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->smiChurchessScheduledForDeletion !== null) {
                if (!$this->smiChurchessScheduledForDeletion->isEmpty()) {
                    foreach ($this->smiChurchessScheduledForDeletion as $smiChurches) {
                        // need to save related object because we set the relation to null
                        $smiChurches->save($con);
                    }
                    $this->smiChurchessScheduledForDeletion = null;
                }
            }

            if ($this->collSmiChurchess !== null) {
                foreach ($this->collSmiChurchess as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sysUserssRelatedByIdEnterpriseScheduledForDeletion !== null) {
                if (!$this->sysUserssRelatedByIdEnterpriseScheduledForDeletion->isEmpty()) {
                    foreach ($this->sysUserssRelatedByIdEnterpriseScheduledForDeletion as $sysUsersRelatedByIdEnterprise) {
                        // need to save related object because we set the relation to null
                        $sysUsersRelatedByIdEnterprise->save($con);
                    }
                    $this->sysUserssRelatedByIdEnterpriseScheduledForDeletion = null;
                }
            }

            if ($this->collSysUserssRelatedByIdEnterprise !== null) {
                foreach ($this->collSysUserssRelatedByIdEnterprise as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = SysEnterprisesPeer::ID_ENTERPRISE;
        if (null !== $this->id_enterprise) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SysEnterprisesPeer::ID_ENTERPRISE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SysEnterprisesPeer::ID_ENTERPRISE)) {
            $modifiedColumns[':p' . $index++]  = '`id_enterprise`';
        }
        if ($this->isColumnModified(SysEnterprisesPeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '`name`';
        }
        if ($this->isColumnModified(SysEnterprisesPeer::NUMBER_REGISTER)) {
            $modifiedColumns[':p' . $index++]  = '`number_register`';
        }
        if ($this->isColumnModified(SysEnterprisesPeer::ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = '`address`';
        }
        if ($this->isColumnModified(SysEnterprisesPeer::PHONE)) {
            $modifiedColumns[':p' . $index++]  = '`phone`';
        }
        if ($this->isColumnModified(SysEnterprisesPeer::FAX)) {
            $modifiedColumns[':p' . $index++]  = '`fax`';
        }
        if ($this->isColumnModified(SysEnterprisesPeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }
        if ($this->isColumnModified(SysEnterprisesPeer::WEB_PAGE)) {
            $modifiedColumns[':p' . $index++]  = '`web_page`';
        }
        if ($this->isColumnModified(SysEnterprisesPeer::STATE)) {
            $modifiedColumns[':p' . $index++]  = '`state`';
        }
        if ($this->isColumnModified(SysEnterprisesPeer::CREATED)) {
            $modifiedColumns[':p' . $index++]  = '`created`';
        }
        if ($this->isColumnModified(SysEnterprisesPeer::MODIFIED)) {
            $modifiedColumns[':p' . $index++]  = '`modified`';
        }
        if ($this->isColumnModified(SysEnterprisesPeer::USER_MODIFIED)) {
            $modifiedColumns[':p' . $index++]  = '`user_modified`';
        }

        $sql = sprintf(
            'INSERT INTO `sys_enterprises` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_enterprise`':
                        $stmt->bindValue($identifier, $this->id_enterprise, PDO::PARAM_INT);
                        break;
                    case '`name`':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '`number_register`':
                        $stmt->bindValue($identifier, $this->number_register, PDO::PARAM_STR);
                        break;
                    case '`address`':
                        $stmt->bindValue($identifier, $this->address, PDO::PARAM_STR);
                        break;
                    case '`phone`':
                        $stmt->bindValue($identifier, $this->phone, PDO::PARAM_STR);
                        break;
                    case '`fax`':
                        $stmt->bindValue($identifier, $this->fax, PDO::PARAM_STR);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`web_page`':
                        $stmt->bindValue($identifier, $this->web_page, PDO::PARAM_STR);
                        break;
                    case '`state`':
                        $stmt->bindValue($identifier, $this->state, PDO::PARAM_STR);
                        break;
                    case '`created`':
                        $stmt->bindValue($identifier, $this->created, PDO::PARAM_STR);
                        break;
                    case '`modified`':
                        $stmt->bindValue($identifier, $this->modified, PDO::PARAM_STR);
                        break;
                    case '`user_modified`':
                        $stmt->bindValue($identifier, $this->user_modified, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setIdEnterprise($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSysUsersRelatedByUserModified !== null) {
                if (!$this->aSysUsersRelatedByUserModified->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSysUsersRelatedByUserModified->getValidationFailures());
                }
            }


            if (($retval = SysEnterprisesPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCmsArticless !== null) {
                    foreach ($this->collCmsArticless as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCmsPagess !== null) {
                    foreach ($this->collCmsPagess as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCmsTagss !== null) {
                    foreach ($this->collCmsTagss as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSmiChurchess !== null) {
                    foreach ($this->collSmiChurchess as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSysUserssRelatedByIdEnterprise !== null) {
                    foreach ($this->collSysUserssRelatedByIdEnterprise as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SysEnterprisesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdEnterprise();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getNumberRegister();
                break;
            case 3:
                return $this->getAddress();
                break;
            case 4:
                return $this->getPhone();
                break;
            case 5:
                return $this->getFax();
                break;
            case 6:
                return $this->getEmail();
                break;
            case 7:
                return $this->getWebPage();
                break;
            case 8:
                return $this->getState();
                break;
            case 9:
                return $this->getCreated();
                break;
            case 10:
                return $this->getModified();
                break;
            case 11:
                return $this->getUserModified();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['SysEnterprises'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SysEnterprises'][$this->getPrimaryKey()] = true;
        $keys = SysEnterprisesPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdEnterprise(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getNumberRegister(),
            $keys[3] => $this->getAddress(),
            $keys[4] => $this->getPhone(),
            $keys[5] => $this->getFax(),
            $keys[6] => $this->getEmail(),
            $keys[7] => $this->getWebPage(),
            $keys[8] => $this->getState(),
            $keys[9] => $this->getCreated(),
            $keys[10] => $this->getModified(),
            $keys[11] => $this->getUserModified(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aSysUsersRelatedByUserModified) {
                $result['SysUsersRelatedByUserModified'] = $this->aSysUsersRelatedByUserModified->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCmsArticless) {
                $result['CmsArticless'] = $this->collCmsArticless->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCmsPagess) {
                $result['CmsPagess'] = $this->collCmsPagess->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCmsTagss) {
                $result['CmsTagss'] = $this->collCmsTagss->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSmiChurchess) {
                $result['SmiChurchess'] = $this->collSmiChurchess->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSysUserssRelatedByIdEnterprise) {
                $result['SysUserssRelatedByIdEnterprise'] = $this->collSysUserssRelatedByIdEnterprise->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = SysEnterprisesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdEnterprise($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setNumberRegister($value);
                break;
            case 3:
                $this->setAddress($value);
                break;
            case 4:
                $this->setPhone($value);
                break;
            case 5:
                $this->setFax($value);
                break;
            case 6:
                $this->setEmail($value);
                break;
            case 7:
                $this->setWebPage($value);
                break;
            case 8:
                $this->setState($value);
                break;
            case 9:
                $this->setCreated($value);
                break;
            case 10:
                $this->setModified($value);
                break;
            case 11:
                $this->setUserModified($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = SysEnterprisesPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdEnterprise($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setName($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNumberRegister($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setAddress($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setPhone($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setFax($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setEmail($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setWebPage($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setState($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setCreated($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setModified($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setUserModified($arr[$keys[11]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SysEnterprisesPeer::DATABASE_NAME);

        if ($this->isColumnModified(SysEnterprisesPeer::ID_ENTERPRISE)) $criteria->add(SysEnterprisesPeer::ID_ENTERPRISE, $this->id_enterprise);
        if ($this->isColumnModified(SysEnterprisesPeer::NAME)) $criteria->add(SysEnterprisesPeer::NAME, $this->name);
        if ($this->isColumnModified(SysEnterprisesPeer::NUMBER_REGISTER)) $criteria->add(SysEnterprisesPeer::NUMBER_REGISTER, $this->number_register);
        if ($this->isColumnModified(SysEnterprisesPeer::ADDRESS)) $criteria->add(SysEnterprisesPeer::ADDRESS, $this->address);
        if ($this->isColumnModified(SysEnterprisesPeer::PHONE)) $criteria->add(SysEnterprisesPeer::PHONE, $this->phone);
        if ($this->isColumnModified(SysEnterprisesPeer::FAX)) $criteria->add(SysEnterprisesPeer::FAX, $this->fax);
        if ($this->isColumnModified(SysEnterprisesPeer::EMAIL)) $criteria->add(SysEnterprisesPeer::EMAIL, $this->email);
        if ($this->isColumnModified(SysEnterprisesPeer::WEB_PAGE)) $criteria->add(SysEnterprisesPeer::WEB_PAGE, $this->web_page);
        if ($this->isColumnModified(SysEnterprisesPeer::STATE)) $criteria->add(SysEnterprisesPeer::STATE, $this->state);
        if ($this->isColumnModified(SysEnterprisesPeer::CREATED)) $criteria->add(SysEnterprisesPeer::CREATED, $this->created);
        if ($this->isColumnModified(SysEnterprisesPeer::MODIFIED)) $criteria->add(SysEnterprisesPeer::MODIFIED, $this->modified);
        if ($this->isColumnModified(SysEnterprisesPeer::USER_MODIFIED)) $criteria->add(SysEnterprisesPeer::USER_MODIFIED, $this->user_modified);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(SysEnterprisesPeer::DATABASE_NAME);
        $criteria->add(SysEnterprisesPeer::ID_ENTERPRISE, $this->id_enterprise);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdEnterprise();
    }

    /**
     * Generic method to set the primary key (id_enterprise column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdEnterprise($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdEnterprise();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of SysEnterprises (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setName($this->getName());
        $copyObj->setNumberRegister($this->getNumberRegister());
        $copyObj->setAddress($this->getAddress());
        $copyObj->setPhone($this->getPhone());
        $copyObj->setFax($this->getFax());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setWebPage($this->getWebPage());
        $copyObj->setState($this->getState());
        $copyObj->setCreated($this->getCreated());
        $copyObj->setModified($this->getModified());
        $copyObj->setUserModified($this->getUserModified());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCmsArticless() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCmsArticles($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCmsPagess() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCmsPages($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCmsTagss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCmsTags($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSmiChurchess() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSmiChurches($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSysUserssRelatedByIdEnterprise() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSysUsersRelatedByIdEnterprise($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdEnterprise(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return SysEnterprises Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return SysEnterprisesPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new SysEnterprisesPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a SysUsers object.
     *
     * @param                  SysUsers $v
     * @return SysEnterprises The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSysUsersRelatedByUserModified(SysUsers $v = null)
    {
        if ($v === null) {
            $this->setUserModified(NULL);
        } else {
            $this->setUserModified($v->getIdUser());
        }

        $this->aSysUsersRelatedByUserModified = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SysUsers object, it will not be re-added.
        if ($v !== null) {
            $v->addSysEnterprisesRelatedByUserModified($this);
        }


        return $this;
    }


    /**
     * Get the associated SysUsers object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SysUsers The associated SysUsers object.
     * @throws PropelException
     */
    public function getSysUsersRelatedByUserModified(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSysUsersRelatedByUserModified === null && ($this->user_modified !== null) && $doQuery) {
            $this->aSysUsersRelatedByUserModified = SysUsersQuery::create()->findPk($this->user_modified, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSysUsersRelatedByUserModified->addSysEnterprisessRelatedByUserModified($this);
             */
        }

        return $this->aSysUsersRelatedByUserModified;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CmsArticles' == $relationName) {
            $this->initCmsArticless();
        }
        if ('CmsPages' == $relationName) {
            $this->initCmsPagess();
        }
        if ('CmsTags' == $relationName) {
            $this->initCmsTagss();
        }
        if ('SmiChurches' == $relationName) {
            $this->initSmiChurchess();
        }
        if ('SysUsersRelatedByIdEnterprise' == $relationName) {
            $this->initSysUserssRelatedByIdEnterprise();
        }
    }

    /**
     * Clears out the collCmsArticless collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SysEnterprises The current object (for fluent API support)
     * @see        addCmsArticless()
     */
    public function clearCmsArticless()
    {
        $this->collCmsArticless = null; // important to set this to null since that means it is uninitialized
        $this->collCmsArticlessPartial = null;

        return $this;
    }

    /**
     * reset is the collCmsArticless collection loaded partially
     *
     * @return void
     */
    public function resetPartialCmsArticless($v = true)
    {
        $this->collCmsArticlessPartial = $v;
    }

    /**
     * Initializes the collCmsArticless collection.
     *
     * By default this just sets the collCmsArticless collection to an empty array (like clearcollCmsArticless());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCmsArticless($overrideExisting = true)
    {
        if (null !== $this->collCmsArticless && !$overrideExisting) {
            return;
        }
        $this->collCmsArticless = new PropelObjectCollection();
        $this->collCmsArticless->setModel('CmsArticles');
    }

    /**
     * Gets an array of CmsArticles objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SysEnterprises is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CmsArticles[] List of CmsArticles objects
     * @throws PropelException
     */
    public function getCmsArticless($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCmsArticlessPartial && !$this->isNew();
        if (null === $this->collCmsArticless || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCmsArticless) {
                // return empty collection
                $this->initCmsArticless();
            } else {
                $collCmsArticless = CmsArticlesQuery::create(null, $criteria)
                    ->filterBySysEnterprises($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCmsArticlessPartial && count($collCmsArticless)) {
                      $this->initCmsArticless(false);

                      foreach ($collCmsArticless as $obj) {
                        if (false == $this->collCmsArticless->contains($obj)) {
                          $this->collCmsArticless->append($obj);
                        }
                      }

                      $this->collCmsArticlessPartial = true;
                    }

                    $collCmsArticless->getInternalIterator()->rewind();

                    return $collCmsArticless;
                }

                if ($partial && $this->collCmsArticless) {
                    foreach ($this->collCmsArticless as $obj) {
                        if ($obj->isNew()) {
                            $collCmsArticless[] = $obj;
                        }
                    }
                }

                $this->collCmsArticless = $collCmsArticless;
                $this->collCmsArticlessPartial = false;
            }
        }

        return $this->collCmsArticless;
    }

    /**
     * Sets a collection of CmsArticles objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $cmsArticless A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function setCmsArticless(PropelCollection $cmsArticless, PropelPDO $con = null)
    {
        $cmsArticlessToDelete = $this->getCmsArticless(new Criteria(), $con)->diff($cmsArticless);


        $this->cmsArticlessScheduledForDeletion = $cmsArticlessToDelete;

        foreach ($cmsArticlessToDelete as $cmsArticlesRemoved) {
            $cmsArticlesRemoved->setSysEnterprises(null);
        }

        $this->collCmsArticless = null;
        foreach ($cmsArticless as $cmsArticles) {
            $this->addCmsArticles($cmsArticles);
        }

        $this->collCmsArticless = $cmsArticless;
        $this->collCmsArticlessPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CmsArticles objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CmsArticles objects.
     * @throws PropelException
     */
    public function countCmsArticless(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCmsArticlessPartial && !$this->isNew();
        if (null === $this->collCmsArticless || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCmsArticless) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCmsArticless());
            }
            $query = CmsArticlesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySysEnterprises($this)
                ->count($con);
        }

        return count($this->collCmsArticless);
    }

    /**
     * Method called to associate a CmsArticles object to this object
     * through the CmsArticles foreign key attribute.
     *
     * @param    CmsArticles $l CmsArticles
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function addCmsArticles(CmsArticles $l)
    {
        if ($this->collCmsArticless === null) {
            $this->initCmsArticless();
            $this->collCmsArticlessPartial = true;
        }

        if (!in_array($l, $this->collCmsArticless->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCmsArticles($l);

            if ($this->cmsArticlessScheduledForDeletion and $this->cmsArticlessScheduledForDeletion->contains($l)) {
                $this->cmsArticlessScheduledForDeletion->remove($this->cmsArticlessScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	CmsArticles $cmsArticles The cmsArticles object to add.
     */
    protected function doAddCmsArticles($cmsArticles)
    {
        $this->collCmsArticless[]= $cmsArticles;
        $cmsArticles->setSysEnterprises($this);
    }

    /**
     * @param	CmsArticles $cmsArticles The cmsArticles object to remove.
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function removeCmsArticles($cmsArticles)
    {
        if ($this->getCmsArticless()->contains($cmsArticles)) {
            $this->collCmsArticless->remove($this->collCmsArticless->search($cmsArticles));
            if (null === $this->cmsArticlessScheduledForDeletion) {
                $this->cmsArticlessScheduledForDeletion = clone $this->collCmsArticless;
                $this->cmsArticlessScheduledForDeletion->clear();
            }
            $this->cmsArticlessScheduledForDeletion[]= $cmsArticles;
            $cmsArticles->setSysEnterprises(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysEnterprises is new, it will return
     * an empty collection; or if this SysEnterprises has previously
     * been saved, it will retrieve related CmsArticless from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysEnterprises.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsArticles[] List of CmsArticles objects
     */
    public function getCmsArticlessJoinCmsPages($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsArticlesQuery::create(null, $criteria);
        $query->joinWith('CmsPages', $join_behavior);

        return $this->getCmsArticless($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysEnterprises is new, it will return
     * an empty collection; or if this SysEnterprises has previously
     * been saved, it will retrieve related CmsArticless from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysEnterprises.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsArticles[] List of CmsArticles objects
     */
    public function getCmsArticlessJoinSysUsersRelatedByIdUser($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsArticlesQuery::create(null, $criteria);
        $query->joinWith('SysUsersRelatedByIdUser', $join_behavior);

        return $this->getCmsArticless($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysEnterprises is new, it will return
     * an empty collection; or if this SysEnterprises has previously
     * been saved, it will retrieve related CmsArticless from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysEnterprises.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsArticles[] List of CmsArticles objects
     */
    public function getCmsArticlessJoinSysUsersRelatedByUserModified($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsArticlesQuery::create(null, $criteria);
        $query->joinWith('SysUsersRelatedByUserModified', $join_behavior);

        return $this->getCmsArticless($query, $con);
    }

    /**
     * Clears out the collCmsPagess collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SysEnterprises The current object (for fluent API support)
     * @see        addCmsPagess()
     */
    public function clearCmsPagess()
    {
        $this->collCmsPagess = null; // important to set this to null since that means it is uninitialized
        $this->collCmsPagessPartial = null;

        return $this;
    }

    /**
     * reset is the collCmsPagess collection loaded partially
     *
     * @return void
     */
    public function resetPartialCmsPagess($v = true)
    {
        $this->collCmsPagessPartial = $v;
    }

    /**
     * Initializes the collCmsPagess collection.
     *
     * By default this just sets the collCmsPagess collection to an empty array (like clearcollCmsPagess());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCmsPagess($overrideExisting = true)
    {
        if (null !== $this->collCmsPagess && !$overrideExisting) {
            return;
        }
        $this->collCmsPagess = new PropelObjectCollection();
        $this->collCmsPagess->setModel('CmsPages');
    }

    /**
     * Gets an array of CmsPages objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SysEnterprises is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CmsPages[] List of CmsPages objects
     * @throws PropelException
     */
    public function getCmsPagess($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCmsPagessPartial && !$this->isNew();
        if (null === $this->collCmsPagess || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCmsPagess) {
                // return empty collection
                $this->initCmsPagess();
            } else {
                $collCmsPagess = CmsPagesQuery::create(null, $criteria)
                    ->filterBySysEnterprises($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCmsPagessPartial && count($collCmsPagess)) {
                      $this->initCmsPagess(false);

                      foreach ($collCmsPagess as $obj) {
                        if (false == $this->collCmsPagess->contains($obj)) {
                          $this->collCmsPagess->append($obj);
                        }
                      }

                      $this->collCmsPagessPartial = true;
                    }

                    $collCmsPagess->getInternalIterator()->rewind();

                    return $collCmsPagess;
                }

                if ($partial && $this->collCmsPagess) {
                    foreach ($this->collCmsPagess as $obj) {
                        if ($obj->isNew()) {
                            $collCmsPagess[] = $obj;
                        }
                    }
                }

                $this->collCmsPagess = $collCmsPagess;
                $this->collCmsPagessPartial = false;
            }
        }

        return $this->collCmsPagess;
    }

    /**
     * Sets a collection of CmsPages objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $cmsPagess A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function setCmsPagess(PropelCollection $cmsPagess, PropelPDO $con = null)
    {
        $cmsPagessToDelete = $this->getCmsPagess(new Criteria(), $con)->diff($cmsPagess);


        $this->cmsPagessScheduledForDeletion = $cmsPagessToDelete;

        foreach ($cmsPagessToDelete as $cmsPagesRemoved) {
            $cmsPagesRemoved->setSysEnterprises(null);
        }

        $this->collCmsPagess = null;
        foreach ($cmsPagess as $cmsPages) {
            $this->addCmsPages($cmsPages);
        }

        $this->collCmsPagess = $cmsPagess;
        $this->collCmsPagessPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CmsPages objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CmsPages objects.
     * @throws PropelException
     */
    public function countCmsPagess(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCmsPagessPartial && !$this->isNew();
        if (null === $this->collCmsPagess || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCmsPagess) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCmsPagess());
            }
            $query = CmsPagesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySysEnterprises($this)
                ->count($con);
        }

        return count($this->collCmsPagess);
    }

    /**
     * Method called to associate a CmsPages object to this object
     * through the CmsPages foreign key attribute.
     *
     * @param    CmsPages $l CmsPages
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function addCmsPages(CmsPages $l)
    {
        if ($this->collCmsPagess === null) {
            $this->initCmsPagess();
            $this->collCmsPagessPartial = true;
        }

        if (!in_array($l, $this->collCmsPagess->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCmsPages($l);

            if ($this->cmsPagessScheduledForDeletion and $this->cmsPagessScheduledForDeletion->contains($l)) {
                $this->cmsPagessScheduledForDeletion->remove($this->cmsPagessScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	CmsPages $cmsPages The cmsPages object to add.
     */
    protected function doAddCmsPages($cmsPages)
    {
        $this->collCmsPagess[]= $cmsPages;
        $cmsPages->setSysEnterprises($this);
    }

    /**
     * @param	CmsPages $cmsPages The cmsPages object to remove.
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function removeCmsPages($cmsPages)
    {
        if ($this->getCmsPagess()->contains($cmsPages)) {
            $this->collCmsPagess->remove($this->collCmsPagess->search($cmsPages));
            if (null === $this->cmsPagessScheduledForDeletion) {
                $this->cmsPagessScheduledForDeletion = clone $this->collCmsPagess;
                $this->cmsPagessScheduledForDeletion->clear();
            }
            $this->cmsPagessScheduledForDeletion[]= $cmsPages;
            $cmsPages->setSysEnterprises(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysEnterprises is new, it will return
     * an empty collection; or if this SysEnterprises has previously
     * been saved, it will retrieve related CmsPagess from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysEnterprises.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsPages[] List of CmsPages objects
     */
    public function getCmsPagessJoinCmsTemplates($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsPagesQuery::create(null, $criteria);
        $query->joinWith('CmsTemplates', $join_behavior);

        return $this->getCmsPagess($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysEnterprises is new, it will return
     * an empty collection; or if this SysEnterprises has previously
     * been saved, it will retrieve related CmsPagess from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysEnterprises.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsPages[] List of CmsPages objects
     */
    public function getCmsPagessJoinSysUsers($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsPagesQuery::create(null, $criteria);
        $query->joinWith('SysUsers', $join_behavior);

        return $this->getCmsPagess($query, $con);
    }

    /**
     * Clears out the collCmsTagss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SysEnterprises The current object (for fluent API support)
     * @see        addCmsTagss()
     */
    public function clearCmsTagss()
    {
        $this->collCmsTagss = null; // important to set this to null since that means it is uninitialized
        $this->collCmsTagssPartial = null;

        return $this;
    }

    /**
     * reset is the collCmsTagss collection loaded partially
     *
     * @return void
     */
    public function resetPartialCmsTagss($v = true)
    {
        $this->collCmsTagssPartial = $v;
    }

    /**
     * Initializes the collCmsTagss collection.
     *
     * By default this just sets the collCmsTagss collection to an empty array (like clearcollCmsTagss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCmsTagss($overrideExisting = true)
    {
        if (null !== $this->collCmsTagss && !$overrideExisting) {
            return;
        }
        $this->collCmsTagss = new PropelObjectCollection();
        $this->collCmsTagss->setModel('CmsTags');
    }

    /**
     * Gets an array of CmsTags objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SysEnterprises is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CmsTags[] List of CmsTags objects
     * @throws PropelException
     */
    public function getCmsTagss($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCmsTagssPartial && !$this->isNew();
        if (null === $this->collCmsTagss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCmsTagss) {
                // return empty collection
                $this->initCmsTagss();
            } else {
                $collCmsTagss = CmsTagsQuery::create(null, $criteria)
                    ->filterBySysEnterprises($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCmsTagssPartial && count($collCmsTagss)) {
                      $this->initCmsTagss(false);

                      foreach ($collCmsTagss as $obj) {
                        if (false == $this->collCmsTagss->contains($obj)) {
                          $this->collCmsTagss->append($obj);
                        }
                      }

                      $this->collCmsTagssPartial = true;
                    }

                    $collCmsTagss->getInternalIterator()->rewind();

                    return $collCmsTagss;
                }

                if ($partial && $this->collCmsTagss) {
                    foreach ($this->collCmsTagss as $obj) {
                        if ($obj->isNew()) {
                            $collCmsTagss[] = $obj;
                        }
                    }
                }

                $this->collCmsTagss = $collCmsTagss;
                $this->collCmsTagssPartial = false;
            }
        }

        return $this->collCmsTagss;
    }

    /**
     * Sets a collection of CmsTags objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $cmsTagss A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function setCmsTagss(PropelCollection $cmsTagss, PropelPDO $con = null)
    {
        $cmsTagssToDelete = $this->getCmsTagss(new Criteria(), $con)->diff($cmsTagss);


        $this->cmsTagssScheduledForDeletion = $cmsTagssToDelete;

        foreach ($cmsTagssToDelete as $cmsTagsRemoved) {
            $cmsTagsRemoved->setSysEnterprises(null);
        }

        $this->collCmsTagss = null;
        foreach ($cmsTagss as $cmsTags) {
            $this->addCmsTags($cmsTags);
        }

        $this->collCmsTagss = $cmsTagss;
        $this->collCmsTagssPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CmsTags objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CmsTags objects.
     * @throws PropelException
     */
    public function countCmsTagss(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCmsTagssPartial && !$this->isNew();
        if (null === $this->collCmsTagss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCmsTagss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCmsTagss());
            }
            $query = CmsTagsQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySysEnterprises($this)
                ->count($con);
        }

        return count($this->collCmsTagss);
    }

    /**
     * Method called to associate a CmsTags object to this object
     * through the CmsTags foreign key attribute.
     *
     * @param    CmsTags $l CmsTags
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function addCmsTags(CmsTags $l)
    {
        if ($this->collCmsTagss === null) {
            $this->initCmsTagss();
            $this->collCmsTagssPartial = true;
        }

        if (!in_array($l, $this->collCmsTagss->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCmsTags($l);

            if ($this->cmsTagssScheduledForDeletion and $this->cmsTagssScheduledForDeletion->contains($l)) {
                $this->cmsTagssScheduledForDeletion->remove($this->cmsTagssScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	CmsTags $cmsTags The cmsTags object to add.
     */
    protected function doAddCmsTags($cmsTags)
    {
        $this->collCmsTagss[]= $cmsTags;
        $cmsTags->setSysEnterprises($this);
    }

    /**
     * @param	CmsTags $cmsTags The cmsTags object to remove.
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function removeCmsTags($cmsTags)
    {
        if ($this->getCmsTagss()->contains($cmsTags)) {
            $this->collCmsTagss->remove($this->collCmsTagss->search($cmsTags));
            if (null === $this->cmsTagssScheduledForDeletion) {
                $this->cmsTagssScheduledForDeletion = clone $this->collCmsTagss;
                $this->cmsTagssScheduledForDeletion->clear();
            }
            $this->cmsTagssScheduledForDeletion[]= $cmsTags;
            $cmsTags->setSysEnterprises(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysEnterprises is new, it will return
     * an empty collection; or if this SysEnterprises has previously
     * been saved, it will retrieve related CmsTagss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysEnterprises.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsTags[] List of CmsTags objects
     */
    public function getCmsTagssJoinSysFiles($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsTagsQuery::create(null, $criteria);
        $query->joinWith('SysFiles', $join_behavior);

        return $this->getCmsTagss($query, $con);
    }

    /**
     * Clears out the collSmiChurchess collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SysEnterprises The current object (for fluent API support)
     * @see        addSmiChurchess()
     */
    public function clearSmiChurchess()
    {
        $this->collSmiChurchess = null; // important to set this to null since that means it is uninitialized
        $this->collSmiChurchessPartial = null;

        return $this;
    }

    /**
     * reset is the collSmiChurchess collection loaded partially
     *
     * @return void
     */
    public function resetPartialSmiChurchess($v = true)
    {
        $this->collSmiChurchessPartial = $v;
    }

    /**
     * Initializes the collSmiChurchess collection.
     *
     * By default this just sets the collSmiChurchess collection to an empty array (like clearcollSmiChurchess());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSmiChurchess($overrideExisting = true)
    {
        if (null !== $this->collSmiChurchess && !$overrideExisting) {
            return;
        }
        $this->collSmiChurchess = new PropelObjectCollection();
        $this->collSmiChurchess->setModel('SmiChurches');
    }

    /**
     * Gets an array of SmiChurches objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SysEnterprises is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SmiChurches[] List of SmiChurches objects
     * @throws PropelException
     */
    public function getSmiChurchess($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSmiChurchessPartial && !$this->isNew();
        if (null === $this->collSmiChurchess || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSmiChurchess) {
                // return empty collection
                $this->initSmiChurchess();
            } else {
                $collSmiChurchess = SmiChurchesQuery::create(null, $criteria)
                    ->filterBySysEnterprises($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSmiChurchessPartial && count($collSmiChurchess)) {
                      $this->initSmiChurchess(false);

                      foreach ($collSmiChurchess as $obj) {
                        if (false == $this->collSmiChurchess->contains($obj)) {
                          $this->collSmiChurchess->append($obj);
                        }
                      }

                      $this->collSmiChurchessPartial = true;
                    }

                    $collSmiChurchess->getInternalIterator()->rewind();

                    return $collSmiChurchess;
                }

                if ($partial && $this->collSmiChurchess) {
                    foreach ($this->collSmiChurchess as $obj) {
                        if ($obj->isNew()) {
                            $collSmiChurchess[] = $obj;
                        }
                    }
                }

                $this->collSmiChurchess = $collSmiChurchess;
                $this->collSmiChurchessPartial = false;
            }
        }

        return $this->collSmiChurchess;
    }

    /**
     * Sets a collection of SmiChurches objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $smiChurchess A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function setSmiChurchess(PropelCollection $smiChurchess, PropelPDO $con = null)
    {
        $smiChurchessToDelete = $this->getSmiChurchess(new Criteria(), $con)->diff($smiChurchess);


        $this->smiChurchessScheduledForDeletion = $smiChurchessToDelete;

        foreach ($smiChurchessToDelete as $smiChurchesRemoved) {
            $smiChurchesRemoved->setSysEnterprises(null);
        }

        $this->collSmiChurchess = null;
        foreach ($smiChurchess as $smiChurches) {
            $this->addSmiChurches($smiChurches);
        }

        $this->collSmiChurchess = $smiChurchess;
        $this->collSmiChurchessPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SmiChurches objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SmiChurches objects.
     * @throws PropelException
     */
    public function countSmiChurchess(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSmiChurchessPartial && !$this->isNew();
        if (null === $this->collSmiChurchess || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSmiChurchess) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSmiChurchess());
            }
            $query = SmiChurchesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySysEnterprises($this)
                ->count($con);
        }

        return count($this->collSmiChurchess);
    }

    /**
     * Method called to associate a SmiChurches object to this object
     * through the SmiChurches foreign key attribute.
     *
     * @param    SmiChurches $l SmiChurches
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function addSmiChurches(SmiChurches $l)
    {
        if ($this->collSmiChurchess === null) {
            $this->initSmiChurchess();
            $this->collSmiChurchessPartial = true;
        }

        if (!in_array($l, $this->collSmiChurchess->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSmiChurches($l);

            if ($this->smiChurchessScheduledForDeletion and $this->smiChurchessScheduledForDeletion->contains($l)) {
                $this->smiChurchessScheduledForDeletion->remove($this->smiChurchessScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	SmiChurches $smiChurches The smiChurches object to add.
     */
    protected function doAddSmiChurches($smiChurches)
    {
        $this->collSmiChurchess[]= $smiChurches;
        $smiChurches->setSysEnterprises($this);
    }

    /**
     * @param	SmiChurches $smiChurches The smiChurches object to remove.
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function removeSmiChurches($smiChurches)
    {
        if ($this->getSmiChurchess()->contains($smiChurches)) {
            $this->collSmiChurchess->remove($this->collSmiChurchess->search($smiChurches));
            if (null === $this->smiChurchessScheduledForDeletion) {
                $this->smiChurchessScheduledForDeletion = clone $this->collSmiChurchess;
                $this->smiChurchessScheduledForDeletion->clear();
            }
            $this->smiChurchessScheduledForDeletion[]= $smiChurches;
            $smiChurches->setSysEnterprises(null);
        }

        return $this;
    }

    /**
     * Clears out the collSysUserssRelatedByIdEnterprise collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return SysEnterprises The current object (for fluent API support)
     * @see        addSysUserssRelatedByIdEnterprise()
     */
    public function clearSysUserssRelatedByIdEnterprise()
    {
        $this->collSysUserssRelatedByIdEnterprise = null; // important to set this to null since that means it is uninitialized
        $this->collSysUserssRelatedByIdEnterprisePartial = null;

        return $this;
    }

    /**
     * reset is the collSysUserssRelatedByIdEnterprise collection loaded partially
     *
     * @return void
     */
    public function resetPartialSysUserssRelatedByIdEnterprise($v = true)
    {
        $this->collSysUserssRelatedByIdEnterprisePartial = $v;
    }

    /**
     * Initializes the collSysUserssRelatedByIdEnterprise collection.
     *
     * By default this just sets the collSysUserssRelatedByIdEnterprise collection to an empty array (like clearcollSysUserssRelatedByIdEnterprise());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSysUserssRelatedByIdEnterprise($overrideExisting = true)
    {
        if (null !== $this->collSysUserssRelatedByIdEnterprise && !$overrideExisting) {
            return;
        }
        $this->collSysUserssRelatedByIdEnterprise = new PropelObjectCollection();
        $this->collSysUserssRelatedByIdEnterprise->setModel('SysUsers');
    }

    /**
     * Gets an array of SysUsers objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this SysEnterprises is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SysUsers[] List of SysUsers objects
     * @throws PropelException
     */
    public function getSysUserssRelatedByIdEnterprise($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSysUserssRelatedByIdEnterprisePartial && !$this->isNew();
        if (null === $this->collSysUserssRelatedByIdEnterprise || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSysUserssRelatedByIdEnterprise) {
                // return empty collection
                $this->initSysUserssRelatedByIdEnterprise();
            } else {
                $collSysUserssRelatedByIdEnterprise = SysUsersQuery::create(null, $criteria)
                    ->filterBySysEnterprisesRelatedByIdEnterprise($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSysUserssRelatedByIdEnterprisePartial && count($collSysUserssRelatedByIdEnterprise)) {
                      $this->initSysUserssRelatedByIdEnterprise(false);

                      foreach ($collSysUserssRelatedByIdEnterprise as $obj) {
                        if (false == $this->collSysUserssRelatedByIdEnterprise->contains($obj)) {
                          $this->collSysUserssRelatedByIdEnterprise->append($obj);
                        }
                      }

                      $this->collSysUserssRelatedByIdEnterprisePartial = true;
                    }

                    $collSysUserssRelatedByIdEnterprise->getInternalIterator()->rewind();

                    return $collSysUserssRelatedByIdEnterprise;
                }

                if ($partial && $this->collSysUserssRelatedByIdEnterprise) {
                    foreach ($this->collSysUserssRelatedByIdEnterprise as $obj) {
                        if ($obj->isNew()) {
                            $collSysUserssRelatedByIdEnterprise[] = $obj;
                        }
                    }
                }

                $this->collSysUserssRelatedByIdEnterprise = $collSysUserssRelatedByIdEnterprise;
                $this->collSysUserssRelatedByIdEnterprisePartial = false;
            }
        }

        return $this->collSysUserssRelatedByIdEnterprise;
    }

    /**
     * Sets a collection of SysUsersRelatedByIdEnterprise objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $sysUserssRelatedByIdEnterprise A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function setSysUserssRelatedByIdEnterprise(PropelCollection $sysUserssRelatedByIdEnterprise, PropelPDO $con = null)
    {
        $sysUserssRelatedByIdEnterpriseToDelete = $this->getSysUserssRelatedByIdEnterprise(new Criteria(), $con)->diff($sysUserssRelatedByIdEnterprise);


        $this->sysUserssRelatedByIdEnterpriseScheduledForDeletion = $sysUserssRelatedByIdEnterpriseToDelete;

        foreach ($sysUserssRelatedByIdEnterpriseToDelete as $sysUsersRelatedByIdEnterpriseRemoved) {
            $sysUsersRelatedByIdEnterpriseRemoved->setSysEnterprisesRelatedByIdEnterprise(null);
        }

        $this->collSysUserssRelatedByIdEnterprise = null;
        foreach ($sysUserssRelatedByIdEnterprise as $sysUsersRelatedByIdEnterprise) {
            $this->addSysUsersRelatedByIdEnterprise($sysUsersRelatedByIdEnterprise);
        }

        $this->collSysUserssRelatedByIdEnterprise = $sysUserssRelatedByIdEnterprise;
        $this->collSysUserssRelatedByIdEnterprisePartial = false;

        return $this;
    }

    /**
     * Returns the number of related SysUsers objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SysUsers objects.
     * @throws PropelException
     */
    public function countSysUserssRelatedByIdEnterprise(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSysUserssRelatedByIdEnterprisePartial && !$this->isNew();
        if (null === $this->collSysUserssRelatedByIdEnterprise || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSysUserssRelatedByIdEnterprise) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSysUserssRelatedByIdEnterprise());
            }
            $query = SysUsersQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySysEnterprisesRelatedByIdEnterprise($this)
                ->count($con);
        }

        return count($this->collSysUserssRelatedByIdEnterprise);
    }

    /**
     * Method called to associate a SysUsers object to this object
     * through the SysUsers foreign key attribute.
     *
     * @param    SysUsers $l SysUsers
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function addSysUsersRelatedByIdEnterprise(SysUsers $l)
    {
        if ($this->collSysUserssRelatedByIdEnterprise === null) {
            $this->initSysUserssRelatedByIdEnterprise();
            $this->collSysUserssRelatedByIdEnterprisePartial = true;
        }

        if (!in_array($l, $this->collSysUserssRelatedByIdEnterprise->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSysUsersRelatedByIdEnterprise($l);

            if ($this->sysUserssRelatedByIdEnterpriseScheduledForDeletion and $this->sysUserssRelatedByIdEnterpriseScheduledForDeletion->contains($l)) {
                $this->sysUserssRelatedByIdEnterpriseScheduledForDeletion->remove($this->sysUserssRelatedByIdEnterpriseScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	SysUsersRelatedByIdEnterprise $sysUsersRelatedByIdEnterprise The sysUsersRelatedByIdEnterprise object to add.
     */
    protected function doAddSysUsersRelatedByIdEnterprise($sysUsersRelatedByIdEnterprise)
    {
        $this->collSysUserssRelatedByIdEnterprise[]= $sysUsersRelatedByIdEnterprise;
        $sysUsersRelatedByIdEnterprise->setSysEnterprisesRelatedByIdEnterprise($this);
    }

    /**
     * @param	SysUsersRelatedByIdEnterprise $sysUsersRelatedByIdEnterprise The sysUsersRelatedByIdEnterprise object to remove.
     * @return SysEnterprises The current object (for fluent API support)
     */
    public function removeSysUsersRelatedByIdEnterprise($sysUsersRelatedByIdEnterprise)
    {
        if ($this->getSysUserssRelatedByIdEnterprise()->contains($sysUsersRelatedByIdEnterprise)) {
            $this->collSysUserssRelatedByIdEnterprise->remove($this->collSysUserssRelatedByIdEnterprise->search($sysUsersRelatedByIdEnterprise));
            if (null === $this->sysUserssRelatedByIdEnterpriseScheduledForDeletion) {
                $this->sysUserssRelatedByIdEnterpriseScheduledForDeletion = clone $this->collSysUserssRelatedByIdEnterprise;
                $this->sysUserssRelatedByIdEnterpriseScheduledForDeletion->clear();
            }
            $this->sysUserssRelatedByIdEnterpriseScheduledForDeletion[]= $sysUsersRelatedByIdEnterprise;
            $sysUsersRelatedByIdEnterprise->setSysEnterprisesRelatedByIdEnterprise(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysEnterprises is new, it will return
     * an empty collection; or if this SysEnterprises has previously
     * been saved, it will retrieve related SysUserssRelatedByIdEnterprise from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysEnterprises.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SysUsers[] List of SysUsers objects
     */
    public function getSysUserssRelatedByIdEnterpriseJoinSysRoles($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SysUsersQuery::create(null, $criteria);
        $query->joinWith('SysRoles', $join_behavior);

        return $this->getSysUserssRelatedByIdEnterprise($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysEnterprises is new, it will return
     * an empty collection; or if this SysEnterprises has previously
     * been saved, it will retrieve related SysUserssRelatedByIdEnterprise from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysEnterprises.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SysUsers[] List of SysUsers objects
     */
    public function getSysUserssRelatedByIdEnterpriseJoinSysFiles($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SysUsersQuery::create(null, $criteria);
        $query->joinWith('SysFiles', $join_behavior);

        return $this->getSysUserssRelatedByIdEnterprise($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SysEnterprises is new, it will return
     * an empty collection; or if this SysEnterprises has previously
     * been saved, it will retrieve related SysUserssRelatedByIdEnterprise from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SysEnterprises.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SysUsers[] List of SysUsers objects
     */
    public function getSysUserssRelatedByIdEnterpriseJoinSysUsersRelatedByUserModified($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SysUsersQuery::create(null, $criteria);
        $query->joinWith('SysUsersRelatedByUserModified', $join_behavior);

        return $this->getSysUserssRelatedByIdEnterprise($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_enterprise = null;
        $this->name = null;
        $this->number_register = null;
        $this->address = null;
        $this->phone = null;
        $this->fax = null;
        $this->email = null;
        $this->web_page = null;
        $this->state = null;
        $this->created = null;
        $this->modified = null;
        $this->user_modified = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCmsArticless) {
                foreach ($this->collCmsArticless as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCmsPagess) {
                foreach ($this->collCmsPagess as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCmsTagss) {
                foreach ($this->collCmsTagss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSmiChurchess) {
                foreach ($this->collSmiChurchess as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSysUserssRelatedByIdEnterprise) {
                foreach ($this->collSysUserssRelatedByIdEnterprise as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aSysUsersRelatedByUserModified instanceof Persistent) {
              $this->aSysUsersRelatedByUserModified->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCmsArticless instanceof PropelCollection) {
            $this->collCmsArticless->clearIterator();
        }
        $this->collCmsArticless = null;
        if ($this->collCmsPagess instanceof PropelCollection) {
            $this->collCmsPagess->clearIterator();
        }
        $this->collCmsPagess = null;
        if ($this->collCmsTagss instanceof PropelCollection) {
            $this->collCmsTagss->clearIterator();
        }
        $this->collCmsTagss = null;
        if ($this->collSmiChurchess instanceof PropelCollection) {
            $this->collSmiChurchess->clearIterator();
        }
        $this->collSmiChurchess = null;
        if ($this->collSysUserssRelatedByIdEnterprise instanceof PropelCollection) {
            $this->collSysUserssRelatedByIdEnterprise->clearIterator();
        }
        $this->collSysUserssRelatedByIdEnterprise = null;
        $this->aSysUsersRelatedByUserModified = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SysEnterprisesPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}

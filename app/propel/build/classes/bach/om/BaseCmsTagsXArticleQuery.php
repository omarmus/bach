<?php


/**
 * Base class that represents a query for the 'cms_tags_x_article' table.
 *
 *
 *
 * @method CmsTagsXArticleQuery orderByIdTag($order = Criteria::ASC) Order by the id_tag column
 * @method CmsTagsXArticleQuery orderByIdArticle($order = Criteria::ASC) Order by the id_article column
 *
 * @method CmsTagsXArticleQuery groupByIdTag() Group by the id_tag column
 * @method CmsTagsXArticleQuery groupByIdArticle() Group by the id_article column
 *
 * @method CmsTagsXArticleQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CmsTagsXArticleQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CmsTagsXArticleQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CmsTagsXArticleQuery leftJoinCmsTags($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsTags relation
 * @method CmsTagsXArticleQuery rightJoinCmsTags($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsTags relation
 * @method CmsTagsXArticleQuery innerJoinCmsTags($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsTags relation
 *
 * @method CmsTagsXArticleQuery leftJoinCmsArticles($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsArticles relation
 * @method CmsTagsXArticleQuery rightJoinCmsArticles($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsArticles relation
 * @method CmsTagsXArticleQuery innerJoinCmsArticles($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsArticles relation
 *
 * @method CmsTagsXArticle findOne(PropelPDO $con = null) Return the first CmsTagsXArticle matching the query
 * @method CmsTagsXArticle findOneOrCreate(PropelPDO $con = null) Return the first CmsTagsXArticle matching the query, or a new CmsTagsXArticle object populated from the query conditions when no match is found
 *
 * @method CmsTagsXArticle findOneByIdTag(int $id_tag) Return the first CmsTagsXArticle filtered by the id_tag column
 * @method CmsTagsXArticle findOneByIdArticle(int $id_article) Return the first CmsTagsXArticle filtered by the id_article column
 *
 * @method array findByIdTag(int $id_tag) Return CmsTagsXArticle objects filtered by the id_tag column
 * @method array findByIdArticle(int $id_article) Return CmsTagsXArticle objects filtered by the id_article column
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseCmsTagsXArticleQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCmsTagsXArticleQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bach';
        }
        if (null === $modelName) {
            $modelName = 'CmsTagsXArticle';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CmsTagsXArticleQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CmsTagsXArticleQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CmsTagsXArticleQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CmsTagsXArticleQuery) {
            return $criteria;
        }
        $query = new CmsTagsXArticleQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query
                         A Primary key composition: [$id_tag, $id_article]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CmsTagsXArticle|CmsTagsXArticle[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CmsTagsXArticlePeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CmsTagsXArticlePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CmsTagsXArticle A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_tag`, `id_article` FROM `cms_tags_x_article` WHERE `id_tag` = :p0 AND `id_article` = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CmsTagsXArticle();
            $obj->hydrate($row);
            CmsTagsXArticlePeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CmsTagsXArticle|CmsTagsXArticle[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CmsTagsXArticle[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CmsTagsXArticleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(CmsTagsXArticlePeer::ID_TAG, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(CmsTagsXArticlePeer::ID_ARTICLE, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CmsTagsXArticleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(CmsTagsXArticlePeer::ID_TAG, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(CmsTagsXArticlePeer::ID_ARTICLE, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the id_tag column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTag(1234); // WHERE id_tag = 1234
     * $query->filterByIdTag(array(12, 34)); // WHERE id_tag IN (12, 34)
     * $query->filterByIdTag(array('min' => 12)); // WHERE id_tag >= 12
     * $query->filterByIdTag(array('max' => 12)); // WHERE id_tag <= 12
     * </code>
     *
     * @see       filterByCmsTags()
     *
     * @param     mixed $idTag The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsTagsXArticleQuery The current query, for fluid interface
     */
    public function filterByIdTag($idTag = null, $comparison = null)
    {
        if (is_array($idTag)) {
            $useMinMax = false;
            if (isset($idTag['min'])) {
                $this->addUsingAlias(CmsTagsXArticlePeer::ID_TAG, $idTag['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTag['max'])) {
                $this->addUsingAlias(CmsTagsXArticlePeer::ID_TAG, $idTag['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsTagsXArticlePeer::ID_TAG, $idTag, $comparison);
    }

    /**
     * Filter the query on the id_article column
     *
     * Example usage:
     * <code>
     * $query->filterByIdArticle(1234); // WHERE id_article = 1234
     * $query->filterByIdArticle(array(12, 34)); // WHERE id_article IN (12, 34)
     * $query->filterByIdArticle(array('min' => 12)); // WHERE id_article >= 12
     * $query->filterByIdArticle(array('max' => 12)); // WHERE id_article <= 12
     * </code>
     *
     * @see       filterByCmsArticles()
     *
     * @param     mixed $idArticle The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsTagsXArticleQuery The current query, for fluid interface
     */
    public function filterByIdArticle($idArticle = null, $comparison = null)
    {
        if (is_array($idArticle)) {
            $useMinMax = false;
            if (isset($idArticle['min'])) {
                $this->addUsingAlias(CmsTagsXArticlePeer::ID_ARTICLE, $idArticle['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idArticle['max'])) {
                $this->addUsingAlias(CmsTagsXArticlePeer::ID_ARTICLE, $idArticle['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CmsTagsXArticlePeer::ID_ARTICLE, $idArticle, $comparison);
    }

    /**
     * Filter the query by a related CmsTags object
     *
     * @param   CmsTags|PropelObjectCollection $cmsTags The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsTagsXArticleQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsTags($cmsTags, $comparison = null)
    {
        if ($cmsTags instanceof CmsTags) {
            return $this
                ->addUsingAlias(CmsTagsXArticlePeer::ID_TAG, $cmsTags->getIdTag(), $comparison);
        } elseif ($cmsTags instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CmsTagsXArticlePeer::ID_TAG, $cmsTags->toKeyValue('PrimaryKey', 'IdTag'), $comparison);
        } else {
            throw new PropelException('filterByCmsTags() only accepts arguments of type CmsTags or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsTags relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsTagsXArticleQuery The current query, for fluid interface
     */
    public function joinCmsTags($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsTags');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsTags');
        }

        return $this;
    }

    /**
     * Use the CmsTags relation CmsTags object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsTagsQuery A secondary query class using the current class as primary query
     */
    public function useCmsTagsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCmsTags($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsTags', 'CmsTagsQuery');
    }

    /**
     * Filter the query by a related CmsArticles object
     *
     * @param   CmsArticles|PropelObjectCollection $cmsArticles The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsTagsXArticleQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsArticles($cmsArticles, $comparison = null)
    {
        if ($cmsArticles instanceof CmsArticles) {
            return $this
                ->addUsingAlias(CmsTagsXArticlePeer::ID_ARTICLE, $cmsArticles->getIdArticle(), $comparison);
        } elseif ($cmsArticles instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CmsTagsXArticlePeer::ID_ARTICLE, $cmsArticles->toKeyValue('PrimaryKey', 'IdArticle'), $comparison);
        } else {
            throw new PropelException('filterByCmsArticles() only accepts arguments of type CmsArticles or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsArticles relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsTagsXArticleQuery The current query, for fluid interface
     */
    public function joinCmsArticles($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsArticles');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsArticles');
        }

        return $this;
    }

    /**
     * Use the CmsArticles relation CmsArticles object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsArticlesQuery A secondary query class using the current class as primary query
     */
    public function useCmsArticlesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCmsArticles($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsArticles', 'CmsArticlesQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CmsTagsXArticle $cmsTagsXArticle Object to remove from the list of results
     *
     * @return CmsTagsXArticleQuery The current query, for fluid interface
     */
    public function prune($cmsTagsXArticle = null)
    {
        if ($cmsTagsXArticle) {
            $this->addCond('pruneCond0', $this->getAliasedColName(CmsTagsXArticlePeer::ID_TAG), $cmsTagsXArticle->getIdTag(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(CmsTagsXArticlePeer::ID_ARTICLE), $cmsTagsXArticle->getIdArticle(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}

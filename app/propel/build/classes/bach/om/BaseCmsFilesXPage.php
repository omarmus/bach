<?php


/**
 * Base class that represents a row from the 'cms_files_x_page' table.
 *
 *
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseCmsFilesXPage extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'CmsFilesXPagePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CmsFilesXPagePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_file field.
     * @var        int
     */
    protected $id_file;

    /**
     * The value for the id_page field.
     * @var        int
     */
    protected $id_page;

    /**
     * The value for the type field.
     * @var        string
     */
    protected $type;

    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;

    /**
     * The value for the primary field.
     * Note: this column has a database default value of: 'NO'
     * @var        string
     */
    protected $primary;

    /**
     * The value for the state field.
     * Note: this column has a database default value of: 'ACTIVE'
     * @var        string
     */
    protected $state;

    /**
     * @var        SysFiles
     */
    protected $aSysFiles;

    /**
     * @var        CmsPages
     */
    protected $aCmsPages;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->primary = 'NO';
        $this->state = 'ACTIVE';
    }

    /**
     * Initializes internal state of BaseCmsFilesXPage object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id_file] column value.
     *
     * @return int
     */
    public function getIdFile()
    {

        return $this->id_file;
    }

    /**
     * Get the [id_page] column value.
     *
     * @return int
     */
    public function getIdPage()
    {

        return $this->id_page;
    }

    /**
     * Get the [type] column value.
     *
     * @return string
     */
    public function getType()
    {

        return $this->type;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {

        return $this->description;
    }

    /**
     * Get the [primary] column value.
     *
     * @return string
     */
    public function getPrimary()
    {

        return $this->primary;
    }

    /**
     * Get the [state] column value.
     *
     * @return string
     */
    public function getState()
    {

        return $this->state;
    }

    /**
     * Set the value of [id_file] column.
     *
     * @param  int $v new value
     * @return CmsFilesXPage The current object (for fluent API support)
     */
    public function setIdFile($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_file !== $v) {
            $this->id_file = $v;
            $this->modifiedColumns[] = CmsFilesXPagePeer::ID_FILE;
        }

        if ($this->aSysFiles !== null && $this->aSysFiles->getIdFile() !== $v) {
            $this->aSysFiles = null;
        }


        return $this;
    } // setIdFile()

    /**
     * Set the value of [id_page] column.
     *
     * @param  int $v new value
     * @return CmsFilesXPage The current object (for fluent API support)
     */
    public function setIdPage($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_page !== $v) {
            $this->id_page = $v;
            $this->modifiedColumns[] = CmsFilesXPagePeer::ID_PAGE;
        }

        if ($this->aCmsPages !== null && $this->aCmsPages->getIdPage() !== $v) {
            $this->aCmsPages = null;
        }


        return $this;
    } // setIdPage()

    /**
     * Set the value of [type] column.
     *
     * @param  string $v new value
     * @return CmsFilesXPage The current object (for fluent API support)
     */
    public function setType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->type !== $v) {
            $this->type = $v;
            $this->modifiedColumns[] = CmsFilesXPagePeer::TYPE;
        }


        return $this;
    } // setType()

    /**
     * Set the value of [description] column.
     *
     * @param  string $v new value
     * @return CmsFilesXPage The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = CmsFilesXPagePeer::DESCRIPTION;
        }


        return $this;
    } // setDescription()

    /**
     * Set the value of [primary] column.
     *
     * @param  string $v new value
     * @return CmsFilesXPage The current object (for fluent API support)
     */
    public function setPrimary($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->primary !== $v) {
            $this->primary = $v;
            $this->modifiedColumns[] = CmsFilesXPagePeer::PRIMARY;
        }


        return $this;
    } // setPrimary()

    /**
     * Set the value of [state] column.
     *
     * @param  string $v new value
     * @return CmsFilesXPage The current object (for fluent API support)
     */
    public function setState($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->state !== $v) {
            $this->state = $v;
            $this->modifiedColumns[] = CmsFilesXPagePeer::STATE;
        }


        return $this;
    } // setState()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->primary !== 'NO') {
                return false;
            }

            if ($this->state !== 'ACTIVE') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_file = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->id_page = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->type = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->description = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->primary = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->state = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 6; // 6 = CmsFilesXPagePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CmsFilesXPage object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSysFiles !== null && $this->id_file !== $this->aSysFiles->getIdFile()) {
            $this->aSysFiles = null;
        }
        if ($this->aCmsPages !== null && $this->id_page !== $this->aCmsPages->getIdPage()) {
            $this->aCmsPages = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CmsFilesXPagePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CmsFilesXPagePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSysFiles = null;
            $this->aCmsPages = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CmsFilesXPagePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CmsFilesXPageQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CmsFilesXPagePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CmsFilesXPagePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSysFiles !== null) {
                if ($this->aSysFiles->isModified() || $this->aSysFiles->isNew()) {
                    $affectedRows += $this->aSysFiles->save($con);
                }
                $this->setSysFiles($this->aSysFiles);
            }

            if ($this->aCmsPages !== null) {
                if ($this->aCmsPages->isModified() || $this->aCmsPages->isNew()) {
                    $affectedRows += $this->aCmsPages->save($con);
                }
                $this->setCmsPages($this->aCmsPages);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CmsFilesXPagePeer::ID_FILE)) {
            $modifiedColumns[':p' . $index++]  = '`id_file`';
        }
        if ($this->isColumnModified(CmsFilesXPagePeer::ID_PAGE)) {
            $modifiedColumns[':p' . $index++]  = '`id_page`';
        }
        if ($this->isColumnModified(CmsFilesXPagePeer::TYPE)) {
            $modifiedColumns[':p' . $index++]  = '`type`';
        }
        if ($this->isColumnModified(CmsFilesXPagePeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = '`description`';
        }
        if ($this->isColumnModified(CmsFilesXPagePeer::PRIMARY)) {
            $modifiedColumns[':p' . $index++]  = '`primary`';
        }
        if ($this->isColumnModified(CmsFilesXPagePeer::STATE)) {
            $modifiedColumns[':p' . $index++]  = '`state`';
        }

        $sql = sprintf(
            'INSERT INTO `cms_files_x_page` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_file`':
                        $stmt->bindValue($identifier, $this->id_file, PDO::PARAM_INT);
                        break;
                    case '`id_page`':
                        $stmt->bindValue($identifier, $this->id_page, PDO::PARAM_INT);
                        break;
                    case '`type`':
                        $stmt->bindValue($identifier, $this->type, PDO::PARAM_STR);
                        break;
                    case '`description`':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case '`primary`':
                        $stmt->bindValue($identifier, $this->primary, PDO::PARAM_STR);
                        break;
                    case '`state`':
                        $stmt->bindValue($identifier, $this->state, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSysFiles !== null) {
                if (!$this->aSysFiles->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSysFiles->getValidationFailures());
                }
            }

            if ($this->aCmsPages !== null) {
                if (!$this->aCmsPages->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCmsPages->getValidationFailures());
                }
            }


            if (($retval = CmsFilesXPagePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CmsFilesXPagePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdFile();
                break;
            case 1:
                return $this->getIdPage();
                break;
            case 2:
                return $this->getType();
                break;
            case 3:
                return $this->getDescription();
                break;
            case 4:
                return $this->getPrimary();
                break;
            case 5:
                return $this->getState();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CmsFilesXPage'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CmsFilesXPage'][$this->getPrimaryKey()] = true;
        $keys = CmsFilesXPagePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdFile(),
            $keys[1] => $this->getIdPage(),
            $keys[2] => $this->getType(),
            $keys[3] => $this->getDescription(),
            $keys[4] => $this->getPrimary(),
            $keys[5] => $this->getState(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aSysFiles) {
                $result['SysFiles'] = $this->aSysFiles->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCmsPages) {
                $result['CmsPages'] = $this->aCmsPages->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CmsFilesXPagePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdFile($value);
                break;
            case 1:
                $this->setIdPage($value);
                break;
            case 2:
                $this->setType($value);
                break;
            case 3:
                $this->setDescription($value);
                break;
            case 4:
                $this->setPrimary($value);
                break;
            case 5:
                $this->setState($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CmsFilesXPagePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdFile($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setIdPage($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setType($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setDescription($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setPrimary($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setState($arr[$keys[5]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CmsFilesXPagePeer::DATABASE_NAME);

        if ($this->isColumnModified(CmsFilesXPagePeer::ID_FILE)) $criteria->add(CmsFilesXPagePeer::ID_FILE, $this->id_file);
        if ($this->isColumnModified(CmsFilesXPagePeer::ID_PAGE)) $criteria->add(CmsFilesXPagePeer::ID_PAGE, $this->id_page);
        if ($this->isColumnModified(CmsFilesXPagePeer::TYPE)) $criteria->add(CmsFilesXPagePeer::TYPE, $this->type);
        if ($this->isColumnModified(CmsFilesXPagePeer::DESCRIPTION)) $criteria->add(CmsFilesXPagePeer::DESCRIPTION, $this->description);
        if ($this->isColumnModified(CmsFilesXPagePeer::PRIMARY)) $criteria->add(CmsFilesXPagePeer::PRIMARY, $this->primary);
        if ($this->isColumnModified(CmsFilesXPagePeer::STATE)) $criteria->add(CmsFilesXPagePeer::STATE, $this->state);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CmsFilesXPagePeer::DATABASE_NAME);
        $criteria->add(CmsFilesXPagePeer::ID_FILE, $this->id_file);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdFile();
    }

    /**
     * Generic method to set the primary key (id_file column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdFile($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdFile();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CmsFilesXPage (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdPage($this->getIdPage());
        $copyObj->setType($this->getType());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setPrimary($this->getPrimary());
        $copyObj->setState($this->getState());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            $relObj = $this->getSysFiles();
            if ($relObj) {
                $copyObj->setSysFiles($relObj->copy($deepCopy));
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdFile(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CmsFilesXPage Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CmsFilesXPagePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CmsFilesXPagePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a SysFiles object.
     *
     * @param                  SysFiles $v
     * @return CmsFilesXPage The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSysFiles(SysFiles $v = null)
    {
        if ($v === null) {
            $this->setIdFile(NULL);
        } else {
            $this->setIdFile($v->getIdFile());
        }

        $this->aSysFiles = $v;

        // Add binding for other direction of this 1:1 relationship.
        if ($v !== null) {
            $v->setCmsFilesXPage($this);
        }


        return $this;
    }


    /**
     * Get the associated SysFiles object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SysFiles The associated SysFiles object.
     * @throws PropelException
     */
    public function getSysFiles(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSysFiles === null && ($this->id_file !== null) && $doQuery) {
            $this->aSysFiles = SysFilesQuery::create()->findPk($this->id_file, $con);
            // Because this foreign key represents a one-to-one relationship, we will create a bi-directional association.
            $this->aSysFiles->setCmsFilesXPage($this);
        }

        return $this->aSysFiles;
    }

    /**
     * Declares an association between this object and a CmsPages object.
     *
     * @param                  CmsPages $v
     * @return CmsFilesXPage The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCmsPages(CmsPages $v = null)
    {
        if ($v === null) {
            $this->setIdPage(NULL);
        } else {
            $this->setIdPage($v->getIdPage());
        }

        $this->aCmsPages = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CmsPages object, it will not be re-added.
        if ($v !== null) {
            $v->addCmsFilesXPage($this);
        }


        return $this;
    }


    /**
     * Get the associated CmsPages object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CmsPages The associated CmsPages object.
     * @throws PropelException
     */
    public function getCmsPages(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCmsPages === null && ($this->id_page !== null) && $doQuery) {
            $this->aCmsPages = CmsPagesQuery::create()->findPk($this->id_page, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCmsPages->addCmsFilesXPages($this);
             */
        }

        return $this->aCmsPages;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_file = null;
        $this->id_page = null;
        $this->type = null;
        $this->description = null;
        $this->primary = null;
        $this->state = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aSysFiles instanceof Persistent) {
              $this->aSysFiles->clearAllReferences($deep);
            }
            if ($this->aCmsPages instanceof Persistent) {
              $this->aCmsPages->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aSysFiles = null;
        $this->aCmsPages = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CmsFilesXPagePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}

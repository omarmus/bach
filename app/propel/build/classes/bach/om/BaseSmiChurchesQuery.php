<?php


/**
 * Base class that represents a query for the 'smi_churches' table.
 *
 *
 *
 * @method SmiChurchesQuery orderByIdChurch($order = Criteria::ASC) Order by the id_church column
 * @method SmiChurchesQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method SmiChurchesQuery orderByAddress($order = Criteria::ASC) Order by the address column
 * @method SmiChurchesQuery orderByConstancyType($order = Criteria::ASC) Order by the constancy_type column
 * @method SmiChurchesQuery orderByArea($order = Criteria::ASC) Order by the area column
 * @method SmiChurchesQuery orderByDepartment($order = Criteria::ASC) Order by the department column
 * @method SmiChurchesQuery orderByIdCity($order = Criteria::ASC) Order by the id_city column
 * @method SmiChurchesQuery orderByLatitude($order = Criteria::ASC) Order by the latitude column
 * @method SmiChurchesQuery orderByLength($order = Criteria::ASC) Order by the length column
 * @method SmiChurchesQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method SmiChurchesQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method SmiChurchesQuery orderByState($order = Criteria::ASC) Order by the state column
 * @method SmiChurchesQuery orderByIdCountry($order = Criteria::ASC) Order by the id_country column
 * @method SmiChurchesQuery orderByIdEnterprise($order = Criteria::ASC) Order by the id_enterprise column
 *
 * @method SmiChurchesQuery groupByIdChurch() Group by the id_church column
 * @method SmiChurchesQuery groupByName() Group by the name column
 * @method SmiChurchesQuery groupByAddress() Group by the address column
 * @method SmiChurchesQuery groupByConstancyType() Group by the constancy_type column
 * @method SmiChurchesQuery groupByArea() Group by the area column
 * @method SmiChurchesQuery groupByDepartment() Group by the department column
 * @method SmiChurchesQuery groupByIdCity() Group by the id_city column
 * @method SmiChurchesQuery groupByLatitude() Group by the latitude column
 * @method SmiChurchesQuery groupByLength() Group by the length column
 * @method SmiChurchesQuery groupByPhone() Group by the phone column
 * @method SmiChurchesQuery groupByEmail() Group by the email column
 * @method SmiChurchesQuery groupByState() Group by the state column
 * @method SmiChurchesQuery groupByIdCountry() Group by the id_country column
 * @method SmiChurchesQuery groupByIdEnterprise() Group by the id_enterprise column
 *
 * @method SmiChurchesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SmiChurchesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SmiChurchesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SmiChurchesQuery leftJoinSysEnterprises($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysEnterprises relation
 * @method SmiChurchesQuery rightJoinSysEnterprises($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysEnterprises relation
 * @method SmiChurchesQuery innerJoinSysEnterprises($relationAlias = null) Adds a INNER JOIN clause to the query using the SysEnterprises relation
 *
 * @method SmiChurches findOne(PropelPDO $con = null) Return the first SmiChurches matching the query
 * @method SmiChurches findOneOrCreate(PropelPDO $con = null) Return the first SmiChurches matching the query, or a new SmiChurches object populated from the query conditions when no match is found
 *
 * @method SmiChurches findOneByName(string $name) Return the first SmiChurches filtered by the name column
 * @method SmiChurches findOneByAddress(string $address) Return the first SmiChurches filtered by the address column
 * @method SmiChurches findOneByConstancyType(string $constancy_type) Return the first SmiChurches filtered by the constancy_type column
 * @method SmiChurches findOneByArea(int $area) Return the first SmiChurches filtered by the area column
 * @method SmiChurches findOneByDepartment(string $department) Return the first SmiChurches filtered by the department column
 * @method SmiChurches findOneByIdCity(int $id_city) Return the first SmiChurches filtered by the id_city column
 * @method SmiChurches findOneByLatitude(string $latitude) Return the first SmiChurches filtered by the latitude column
 * @method SmiChurches findOneByLength(string $length) Return the first SmiChurches filtered by the length column
 * @method SmiChurches findOneByPhone(string $phone) Return the first SmiChurches filtered by the phone column
 * @method SmiChurches findOneByEmail(string $email) Return the first SmiChurches filtered by the email column
 * @method SmiChurches findOneByState(string $state) Return the first SmiChurches filtered by the state column
 * @method SmiChurches findOneByIdCountry(int $id_country) Return the first SmiChurches filtered by the id_country column
 * @method SmiChurches findOneByIdEnterprise(int $id_enterprise) Return the first SmiChurches filtered by the id_enterprise column
 *
 * @method array findByIdChurch(int $id_church) Return SmiChurches objects filtered by the id_church column
 * @method array findByName(string $name) Return SmiChurches objects filtered by the name column
 * @method array findByAddress(string $address) Return SmiChurches objects filtered by the address column
 * @method array findByConstancyType(string $constancy_type) Return SmiChurches objects filtered by the constancy_type column
 * @method array findByArea(int $area) Return SmiChurches objects filtered by the area column
 * @method array findByDepartment(string $department) Return SmiChurches objects filtered by the department column
 * @method array findByIdCity(int $id_city) Return SmiChurches objects filtered by the id_city column
 * @method array findByLatitude(string $latitude) Return SmiChurches objects filtered by the latitude column
 * @method array findByLength(string $length) Return SmiChurches objects filtered by the length column
 * @method array findByPhone(string $phone) Return SmiChurches objects filtered by the phone column
 * @method array findByEmail(string $email) Return SmiChurches objects filtered by the email column
 * @method array findByState(string $state) Return SmiChurches objects filtered by the state column
 * @method array findByIdCountry(int $id_country) Return SmiChurches objects filtered by the id_country column
 * @method array findByIdEnterprise(int $id_enterprise) Return SmiChurches objects filtered by the id_enterprise column
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseSmiChurchesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSmiChurchesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bach';
        }
        if (null === $modelName) {
            $modelName = 'SmiChurches';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SmiChurchesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SmiChurchesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SmiChurchesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SmiChurchesQuery) {
            return $criteria;
        }
        $query = new SmiChurchesQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SmiChurches|SmiChurches[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SmiChurchesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SmiChurchesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SmiChurches A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdChurch($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SmiChurches A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_church`, `name`, `address`, `constancy_type`, `area`, `department`, `id_city`, `latitude`, `length`, `phone`, `email`, `state`, `id_country`, `id_enterprise` FROM `smi_churches` WHERE `id_church` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SmiChurches();
            $obj->hydrate($row);
            SmiChurchesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SmiChurches|SmiChurches[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SmiChurches[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SmiChurchesPeer::ID_CHURCH, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SmiChurchesPeer::ID_CHURCH, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_church column
     *
     * Example usage:
     * <code>
     * $query->filterByIdChurch(1234); // WHERE id_church = 1234
     * $query->filterByIdChurch(array(12, 34)); // WHERE id_church IN (12, 34)
     * $query->filterByIdChurch(array('min' => 12)); // WHERE id_church >= 12
     * $query->filterByIdChurch(array('max' => 12)); // WHERE id_church <= 12
     * </code>
     *
     * @param     mixed $idChurch The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function filterByIdChurch($idChurch = null, $comparison = null)
    {
        if (is_array($idChurch)) {
            $useMinMax = false;
            if (isset($idChurch['min'])) {
                $this->addUsingAlias(SmiChurchesPeer::ID_CHURCH, $idChurch['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idChurch['max'])) {
                $this->addUsingAlias(SmiChurchesPeer::ID_CHURCH, $idChurch['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SmiChurchesPeer::ID_CHURCH, $idChurch, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SmiChurchesPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the address column
     *
     * Example usage:
     * <code>
     * $query->filterByAddress('fooValue');   // WHERE address = 'fooValue'
     * $query->filterByAddress('%fooValue%'); // WHERE address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $address The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function filterByAddress($address = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($address)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $address)) {
                $address = str_replace('*', '%', $address);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SmiChurchesPeer::ADDRESS, $address, $comparison);
    }

    /**
     * Filter the query on the constancy_type column
     *
     * Example usage:
     * <code>
     * $query->filterByConstancyType('fooValue');   // WHERE constancy_type = 'fooValue'
     * $query->filterByConstancyType('%fooValue%'); // WHERE constancy_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $constancyType The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function filterByConstancyType($constancyType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($constancyType)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $constancyType)) {
                $constancyType = str_replace('*', '%', $constancyType);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SmiChurchesPeer::CONSTANCY_TYPE, $constancyType, $comparison);
    }

    /**
     * Filter the query on the area column
     *
     * Example usage:
     * <code>
     * $query->filterByArea(1234); // WHERE area = 1234
     * $query->filterByArea(array(12, 34)); // WHERE area IN (12, 34)
     * $query->filterByArea(array('min' => 12)); // WHERE area >= 12
     * $query->filterByArea(array('max' => 12)); // WHERE area <= 12
     * </code>
     *
     * @param     mixed $area The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function filterByArea($area = null, $comparison = null)
    {
        if (is_array($area)) {
            $useMinMax = false;
            if (isset($area['min'])) {
                $this->addUsingAlias(SmiChurchesPeer::AREA, $area['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($area['max'])) {
                $this->addUsingAlias(SmiChurchesPeer::AREA, $area['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SmiChurchesPeer::AREA, $area, $comparison);
    }

    /**
     * Filter the query on the department column
     *
     * Example usage:
     * <code>
     * $query->filterByDepartment('fooValue');   // WHERE department = 'fooValue'
     * $query->filterByDepartment('%fooValue%'); // WHERE department LIKE '%fooValue%'
     * </code>
     *
     * @param     string $department The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function filterByDepartment($department = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($department)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $department)) {
                $department = str_replace('*', '%', $department);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SmiChurchesPeer::DEPARTMENT, $department, $comparison);
    }

    /**
     * Filter the query on the id_city column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCity(1234); // WHERE id_city = 1234
     * $query->filterByIdCity(array(12, 34)); // WHERE id_city IN (12, 34)
     * $query->filterByIdCity(array('min' => 12)); // WHERE id_city >= 12
     * $query->filterByIdCity(array('max' => 12)); // WHERE id_city <= 12
     * </code>
     *
     * @param     mixed $idCity The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function filterByIdCity($idCity = null, $comparison = null)
    {
        if (is_array($idCity)) {
            $useMinMax = false;
            if (isset($idCity['min'])) {
                $this->addUsingAlias(SmiChurchesPeer::ID_CITY, $idCity['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCity['max'])) {
                $this->addUsingAlias(SmiChurchesPeer::ID_CITY, $idCity['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SmiChurchesPeer::ID_CITY, $idCity, $comparison);
    }

    /**
     * Filter the query on the latitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLatitude(1234); // WHERE latitude = 1234
     * $query->filterByLatitude(array(12, 34)); // WHERE latitude IN (12, 34)
     * $query->filterByLatitude(array('min' => 12)); // WHERE latitude >= 12
     * $query->filterByLatitude(array('max' => 12)); // WHERE latitude <= 12
     * </code>
     *
     * @param     mixed $latitude The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function filterByLatitude($latitude = null, $comparison = null)
    {
        if (is_array($latitude)) {
            $useMinMax = false;
            if (isset($latitude['min'])) {
                $this->addUsingAlias(SmiChurchesPeer::LATITUDE, $latitude['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($latitude['max'])) {
                $this->addUsingAlias(SmiChurchesPeer::LATITUDE, $latitude['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SmiChurchesPeer::LATITUDE, $latitude, $comparison);
    }

    /**
     * Filter the query on the length column
     *
     * Example usage:
     * <code>
     * $query->filterByLength(1234); // WHERE length = 1234
     * $query->filterByLength(array(12, 34)); // WHERE length IN (12, 34)
     * $query->filterByLength(array('min' => 12)); // WHERE length >= 12
     * $query->filterByLength(array('max' => 12)); // WHERE length <= 12
     * </code>
     *
     * @param     mixed $length The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function filterByLength($length = null, $comparison = null)
    {
        if (is_array($length)) {
            $useMinMax = false;
            if (isset($length['min'])) {
                $this->addUsingAlias(SmiChurchesPeer::LENGTH, $length['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($length['max'])) {
                $this->addUsingAlias(SmiChurchesPeer::LENGTH, $length['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SmiChurchesPeer::LENGTH, $length, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE phone = 'fooValue'
     * $query->filterByPhone('%fooValue%'); // WHERE phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phone)) {
                $phone = str_replace('*', '%', $phone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SmiChurchesPeer::PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SmiChurchesPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the state column
     *
     * Example usage:
     * <code>
     * $query->filterByState('fooValue');   // WHERE state = 'fooValue'
     * $query->filterByState('%fooValue%'); // WHERE state LIKE '%fooValue%'
     * </code>
     *
     * @param     string $state The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($state)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $state)) {
                $state = str_replace('*', '%', $state);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SmiChurchesPeer::STATE, $state, $comparison);
    }

    /**
     * Filter the query on the id_country column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCountry(1234); // WHERE id_country = 1234
     * $query->filterByIdCountry(array(12, 34)); // WHERE id_country IN (12, 34)
     * $query->filterByIdCountry(array('min' => 12)); // WHERE id_country >= 12
     * $query->filterByIdCountry(array('max' => 12)); // WHERE id_country <= 12
     * </code>
     *
     * @param     mixed $idCountry The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function filterByIdCountry($idCountry = null, $comparison = null)
    {
        if (is_array($idCountry)) {
            $useMinMax = false;
            if (isset($idCountry['min'])) {
                $this->addUsingAlias(SmiChurchesPeer::ID_COUNTRY, $idCountry['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCountry['max'])) {
                $this->addUsingAlias(SmiChurchesPeer::ID_COUNTRY, $idCountry['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SmiChurchesPeer::ID_COUNTRY, $idCountry, $comparison);
    }

    /**
     * Filter the query on the id_enterprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEnterprise(1234); // WHERE id_enterprise = 1234
     * $query->filterByIdEnterprise(array(12, 34)); // WHERE id_enterprise IN (12, 34)
     * $query->filterByIdEnterprise(array('min' => 12)); // WHERE id_enterprise >= 12
     * $query->filterByIdEnterprise(array('max' => 12)); // WHERE id_enterprise <= 12
     * </code>
     *
     * @see       filterBySysEnterprises()
     *
     * @param     mixed $idEnterprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function filterByIdEnterprise($idEnterprise = null, $comparison = null)
    {
        if (is_array($idEnterprise)) {
            $useMinMax = false;
            if (isset($idEnterprise['min'])) {
                $this->addUsingAlias(SmiChurchesPeer::ID_ENTERPRISE, $idEnterprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEnterprise['max'])) {
                $this->addUsingAlias(SmiChurchesPeer::ID_ENTERPRISE, $idEnterprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SmiChurchesPeer::ID_ENTERPRISE, $idEnterprise, $comparison);
    }

    /**
     * Filter the query by a related SysEnterprises object
     *
     * @param   SysEnterprises|PropelObjectCollection $sysEnterprises The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SmiChurchesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysEnterprises($sysEnterprises, $comparison = null)
    {
        if ($sysEnterprises instanceof SysEnterprises) {
            return $this
                ->addUsingAlias(SmiChurchesPeer::ID_ENTERPRISE, $sysEnterprises->getIdEnterprise(), $comparison);
        } elseif ($sysEnterprises instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SmiChurchesPeer::ID_ENTERPRISE, $sysEnterprises->toKeyValue('PrimaryKey', 'IdEnterprise'), $comparison);
        } else {
            throw new PropelException('filterBySysEnterprises() only accepts arguments of type SysEnterprises or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysEnterprises relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function joinSysEnterprises($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysEnterprises');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysEnterprises');
        }

        return $this;
    }

    /**
     * Use the SysEnterprises relation SysEnterprises object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysEnterprisesQuery A secondary query class using the current class as primary query
     */
    public function useSysEnterprisesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysEnterprises($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysEnterprises', 'SysEnterprisesQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SmiChurches $smiChurches Object to remove from the list of results
     *
     * @return SmiChurchesQuery The current query, for fluid interface
     */
    public function prune($smiChurches = null)
    {
        if ($smiChurches) {
            $this->addUsingAlias(SmiChurchesPeer::ID_CHURCH, $smiChurches->getIdChurch(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

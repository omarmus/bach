<?php


/**
 * Base class that represents a query for the 'sys_users' table.
 *
 *
 *
 * @method SysUsersQuery orderByIdUser($order = Criteria::ASC) Order by the id_user column
 * @method SysUsersQuery orderByUsername($order = Criteria::ASC) Order by the username column
 * @method SysUsersQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method SysUsersQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method SysUsersQuery orderByFirstName($order = Criteria::ASC) Order by the first_name column
 * @method SysUsersQuery orderByLastName($order = Criteria::ASC) Order by the last_name column
 * @method SysUsersQuery orderByState($order = Criteria::ASC) Order by the state column
 * @method SysUsersQuery orderByIdRol($order = Criteria::ASC) Order by the id_rol column
 * @method SysUsersQuery orderByIdPhoto($order = Criteria::ASC) Order by the id_photo column
 * @method SysUsersQuery orderByCreated($order = Criteria::ASC) Order by the created column
 * @method SysUsersQuery orderByModified($order = Criteria::ASC) Order by the modified column
 * @method SysUsersQuery orderByUserModified($order = Criteria::ASC) Order by the user_modified column
 * @method SysUsersQuery orderByLangCode($order = Criteria::ASC) Order by the lang_code column
 * @method SysUsersQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method SysUsersQuery orderByMobile($order = Criteria::ASC) Order by the mobile column
 * @method SysUsersQuery orderByIdTimeZone($order = Criteria::ASC) Order by the id_time_zone column
 * @method SysUsersQuery orderByIdCityBirthday($order = Criteria::ASC) Order by the id_city_birthday column
 * @method SysUsersQuery orderByIdCountryBirthday($order = Criteria::ASC) Order by the id_country_birthday column
 * @method SysUsersQuery orderByIdCityAddress($order = Criteria::ASC) Order by the id_city_address column
 * @method SysUsersQuery orderByIdCountryAddress($order = Criteria::ASC) Order by the id_country_address column
 * @method SysUsersQuery orderByGender($order = Criteria::ASC) Order by the gender column
 * @method SysUsersQuery orderByBirthday($order = Criteria::ASC) Order by the birthday column
 * @method SysUsersQuery orderByBirthplace($order = Criteria::ASC) Order by the birthplace column
 * @method SysUsersQuery orderByHash($order = Criteria::ASC) Order by the hash column
 * @method SysUsersQuery orderByTour($order = Criteria::ASC) Order by the tour column
 * @method SysUsersQuery orderByMaritalStatus($order = Criteria::ASC) Order by the marital_status column
 * @method SysUsersQuery orderByLevelEducation($order = Criteria::ASC) Order by the level_education column
 * @method SysUsersQuery orderByOccupation($order = Criteria::ASC) Order by the occupation column
 * @method SysUsersQuery orderByDepartmentAddress($order = Criteria::ASC) Order by the department_address column
 * @method SysUsersQuery orderByDepartmentBirthday($order = Criteria::ASC) Order by the department_birthday column
 * @method SysUsersQuery orderByAddress($order = Criteria::ASC) Order by the address column
 * @method SysUsersQuery orderByIdChurch($order = Criteria::ASC) Order by the id_church column
 * @method SysUsersQuery orderByMember($order = Criteria::ASC) Order by the member column
 * @method SysUsersQuery orderByPastorBaptism($order = Criteria::ASC) Order by the pastor_baptism column
 * @method SysUsersQuery orderByDateBaptism($order = Criteria::ASC) Order by the date_baptism column
 * @method SysUsersQuery orderByIdEnterprise($order = Criteria::ASC) Order by the id_enterprise column
 *
 * @method SysUsersQuery groupByIdUser() Group by the id_user column
 * @method SysUsersQuery groupByUsername() Group by the username column
 * @method SysUsersQuery groupByPassword() Group by the password column
 * @method SysUsersQuery groupByEmail() Group by the email column
 * @method SysUsersQuery groupByFirstName() Group by the first_name column
 * @method SysUsersQuery groupByLastName() Group by the last_name column
 * @method SysUsersQuery groupByState() Group by the state column
 * @method SysUsersQuery groupByIdRol() Group by the id_rol column
 * @method SysUsersQuery groupByIdPhoto() Group by the id_photo column
 * @method SysUsersQuery groupByCreated() Group by the created column
 * @method SysUsersQuery groupByModified() Group by the modified column
 * @method SysUsersQuery groupByUserModified() Group by the user_modified column
 * @method SysUsersQuery groupByLangCode() Group by the lang_code column
 * @method SysUsersQuery groupByPhone() Group by the phone column
 * @method SysUsersQuery groupByMobile() Group by the mobile column
 * @method SysUsersQuery groupByIdTimeZone() Group by the id_time_zone column
 * @method SysUsersQuery groupByIdCityBirthday() Group by the id_city_birthday column
 * @method SysUsersQuery groupByIdCountryBirthday() Group by the id_country_birthday column
 * @method SysUsersQuery groupByIdCityAddress() Group by the id_city_address column
 * @method SysUsersQuery groupByIdCountryAddress() Group by the id_country_address column
 * @method SysUsersQuery groupByGender() Group by the gender column
 * @method SysUsersQuery groupByBirthday() Group by the birthday column
 * @method SysUsersQuery groupByBirthplace() Group by the birthplace column
 * @method SysUsersQuery groupByHash() Group by the hash column
 * @method SysUsersQuery groupByTour() Group by the tour column
 * @method SysUsersQuery groupByMaritalStatus() Group by the marital_status column
 * @method SysUsersQuery groupByLevelEducation() Group by the level_education column
 * @method SysUsersQuery groupByOccupation() Group by the occupation column
 * @method SysUsersQuery groupByDepartmentAddress() Group by the department_address column
 * @method SysUsersQuery groupByDepartmentBirthday() Group by the department_birthday column
 * @method SysUsersQuery groupByAddress() Group by the address column
 * @method SysUsersQuery groupByIdChurch() Group by the id_church column
 * @method SysUsersQuery groupByMember() Group by the member column
 * @method SysUsersQuery groupByPastorBaptism() Group by the pastor_baptism column
 * @method SysUsersQuery groupByDateBaptism() Group by the date_baptism column
 * @method SysUsersQuery groupByIdEnterprise() Group by the id_enterprise column
 *
 * @method SysUsersQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SysUsersQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SysUsersQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SysUsersQuery leftJoinSysEnterprisesRelatedByIdEnterprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysEnterprisesRelatedByIdEnterprise relation
 * @method SysUsersQuery rightJoinSysEnterprisesRelatedByIdEnterprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysEnterprisesRelatedByIdEnterprise relation
 * @method SysUsersQuery innerJoinSysEnterprisesRelatedByIdEnterprise($relationAlias = null) Adds a INNER JOIN clause to the query using the SysEnterprisesRelatedByIdEnterprise relation
 *
 * @method SysUsersQuery leftJoinSysRoles($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysRoles relation
 * @method SysUsersQuery rightJoinSysRoles($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysRoles relation
 * @method SysUsersQuery innerJoinSysRoles($relationAlias = null) Adds a INNER JOIN clause to the query using the SysRoles relation
 *
 * @method SysUsersQuery leftJoinSysFiles($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysFiles relation
 * @method SysUsersQuery rightJoinSysFiles($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysFiles relation
 * @method SysUsersQuery innerJoinSysFiles($relationAlias = null) Adds a INNER JOIN clause to the query using the SysFiles relation
 *
 * @method SysUsersQuery leftJoinSysUsersRelatedByUserModified($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysUsersRelatedByUserModified relation
 * @method SysUsersQuery rightJoinSysUsersRelatedByUserModified($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysUsersRelatedByUserModified relation
 * @method SysUsersQuery innerJoinSysUsersRelatedByUserModified($relationAlias = null) Adds a INNER JOIN clause to the query using the SysUsersRelatedByUserModified relation
 *
 * @method SysUsersQuery leftJoinCmsArticlesRelatedByIdUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsArticlesRelatedByIdUser relation
 * @method SysUsersQuery rightJoinCmsArticlesRelatedByIdUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsArticlesRelatedByIdUser relation
 * @method SysUsersQuery innerJoinCmsArticlesRelatedByIdUser($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsArticlesRelatedByIdUser relation
 *
 * @method SysUsersQuery leftJoinCmsArticlesRelatedByUserModified($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsArticlesRelatedByUserModified relation
 * @method SysUsersQuery rightJoinCmsArticlesRelatedByUserModified($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsArticlesRelatedByUserModified relation
 * @method SysUsersQuery innerJoinCmsArticlesRelatedByUserModified($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsArticlesRelatedByUserModified relation
 *
 * @method SysUsersQuery leftJoinCmsPages($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsPages relation
 * @method SysUsersQuery rightJoinCmsPages($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsPages relation
 * @method SysUsersQuery innerJoinCmsPages($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsPages relation
 *
 * @method SysUsersQuery leftJoinCmsVideosXArticle($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsVideosXArticle relation
 * @method SysUsersQuery rightJoinCmsVideosXArticle($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsVideosXArticle relation
 * @method SysUsersQuery innerJoinCmsVideosXArticle($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsVideosXArticle relation
 *
 * @method SysUsersQuery leftJoinCmsVideosXPage($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsVideosXPage relation
 * @method SysUsersQuery rightJoinCmsVideosXPage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsVideosXPage relation
 * @method SysUsersQuery innerJoinCmsVideosXPage($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsVideosXPage relation
 *
 * @method SysUsersQuery leftJoinSysChatsRelatedByIdSender($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysChatsRelatedByIdSender relation
 * @method SysUsersQuery rightJoinSysChatsRelatedByIdSender($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysChatsRelatedByIdSender relation
 * @method SysUsersQuery innerJoinSysChatsRelatedByIdSender($relationAlias = null) Adds a INNER JOIN clause to the query using the SysChatsRelatedByIdSender relation
 *
 * @method SysUsersQuery leftJoinSysChatsRelatedByIdReceiver($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysChatsRelatedByIdReceiver relation
 * @method SysUsersQuery rightJoinSysChatsRelatedByIdReceiver($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysChatsRelatedByIdReceiver relation
 * @method SysUsersQuery innerJoinSysChatsRelatedByIdReceiver($relationAlias = null) Adds a INNER JOIN clause to the query using the SysChatsRelatedByIdReceiver relation
 *
 * @method SysUsersQuery leftJoinSysEnterprisesRelatedByUserModified($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysEnterprisesRelatedByUserModified relation
 * @method SysUsersQuery rightJoinSysEnterprisesRelatedByUserModified($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysEnterprisesRelatedByUserModified relation
 * @method SysUsersQuery innerJoinSysEnterprisesRelatedByUserModified($relationAlias = null) Adds a INNER JOIN clause to the query using the SysEnterprisesRelatedByUserModified relation
 *
 * @method SysUsersQuery leftJoinSysNotificationsRelatedByIdSender($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysNotificationsRelatedByIdSender relation
 * @method SysUsersQuery rightJoinSysNotificationsRelatedByIdSender($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysNotificationsRelatedByIdSender relation
 * @method SysUsersQuery innerJoinSysNotificationsRelatedByIdSender($relationAlias = null) Adds a INNER JOIN clause to the query using the SysNotificationsRelatedByIdSender relation
 *
 * @method SysUsersQuery leftJoinSysNotificationsRelatedByIdReceiver($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysNotificationsRelatedByIdReceiver relation
 * @method SysUsersQuery rightJoinSysNotificationsRelatedByIdReceiver($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysNotificationsRelatedByIdReceiver relation
 * @method SysUsersQuery innerJoinSysNotificationsRelatedByIdReceiver($relationAlias = null) Adds a INNER JOIN clause to the query using the SysNotificationsRelatedByIdReceiver relation
 *
 * @method SysUsersQuery leftJoinSysPages($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysPages relation
 * @method SysUsersQuery rightJoinSysPages($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysPages relation
 * @method SysUsersQuery innerJoinSysPages($relationAlias = null) Adds a INNER JOIN clause to the query using the SysPages relation
 *
 * @method SysUsersQuery leftJoinSysUsersRelatedByIdUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysUsersRelatedByIdUser relation
 * @method SysUsersQuery rightJoinSysUsersRelatedByIdUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysUsersRelatedByIdUser relation
 * @method SysUsersQuery innerJoinSysUsersRelatedByIdUser($relationAlias = null) Adds a INNER JOIN clause to the query using the SysUsersRelatedByIdUser relation
 *
 * @method SysUsers findOne(PropelPDO $con = null) Return the first SysUsers matching the query
 * @method SysUsers findOneOrCreate(PropelPDO $con = null) Return the first SysUsers matching the query, or a new SysUsers object populated from the query conditions when no match is found
 *
 * @method SysUsers findOneByUsername(string $username) Return the first SysUsers filtered by the username column
 * @method SysUsers findOneByPassword(string $password) Return the first SysUsers filtered by the password column
 * @method SysUsers findOneByEmail(string $email) Return the first SysUsers filtered by the email column
 * @method SysUsers findOneByFirstName(string $first_name) Return the first SysUsers filtered by the first_name column
 * @method SysUsers findOneByLastName(string $last_name) Return the first SysUsers filtered by the last_name column
 * @method SysUsers findOneByState(string $state) Return the first SysUsers filtered by the state column
 * @method SysUsers findOneByIdRol(int $id_rol) Return the first SysUsers filtered by the id_rol column
 * @method SysUsers findOneByIdPhoto(int $id_photo) Return the first SysUsers filtered by the id_photo column
 * @method SysUsers findOneByCreated(string $created) Return the first SysUsers filtered by the created column
 * @method SysUsers findOneByModified(string $modified) Return the first SysUsers filtered by the modified column
 * @method SysUsers findOneByUserModified(int $user_modified) Return the first SysUsers filtered by the user_modified column
 * @method SysUsers findOneByLangCode(string $lang_code) Return the first SysUsers filtered by the lang_code column
 * @method SysUsers findOneByPhone(string $phone) Return the first SysUsers filtered by the phone column
 * @method SysUsers findOneByMobile(string $mobile) Return the first SysUsers filtered by the mobile column
 * @method SysUsers findOneByIdTimeZone(int $id_time_zone) Return the first SysUsers filtered by the id_time_zone column
 * @method SysUsers findOneByIdCityBirthday(int $id_city_birthday) Return the first SysUsers filtered by the id_city_birthday column
 * @method SysUsers findOneByIdCountryBirthday(int $id_country_birthday) Return the first SysUsers filtered by the id_country_birthday column
 * @method SysUsers findOneByIdCityAddress(int $id_city_address) Return the first SysUsers filtered by the id_city_address column
 * @method SysUsers findOneByIdCountryAddress(int $id_country_address) Return the first SysUsers filtered by the id_country_address column
 * @method SysUsers findOneByGender(string $gender) Return the first SysUsers filtered by the gender column
 * @method SysUsers findOneByBirthday(string $birthday) Return the first SysUsers filtered by the birthday column
 * @method SysUsers findOneByBirthplace(string $birthplace) Return the first SysUsers filtered by the birthplace column
 * @method SysUsers findOneByHash(string $hash) Return the first SysUsers filtered by the hash column
 * @method SysUsers findOneByTour(string $tour) Return the first SysUsers filtered by the tour column
 * @method SysUsers findOneByMaritalStatus(string $marital_status) Return the first SysUsers filtered by the marital_status column
 * @method SysUsers findOneByLevelEducation(string $level_education) Return the first SysUsers filtered by the level_education column
 * @method SysUsers findOneByOccupation(string $occupation) Return the first SysUsers filtered by the occupation column
 * @method SysUsers findOneByDepartmentAddress(string $department_address) Return the first SysUsers filtered by the department_address column
 * @method SysUsers findOneByDepartmentBirthday(string $department_birthday) Return the first SysUsers filtered by the department_birthday column
 * @method SysUsers findOneByAddress(string $address) Return the first SysUsers filtered by the address column
 * @method SysUsers findOneByIdChurch(int $id_church) Return the first SysUsers filtered by the id_church column
 * @method SysUsers findOneByMember(string $member) Return the first SysUsers filtered by the member column
 * @method SysUsers findOneByPastorBaptism(string $pastor_baptism) Return the first SysUsers filtered by the pastor_baptism column
 * @method SysUsers findOneByDateBaptism(string $date_baptism) Return the first SysUsers filtered by the date_baptism column
 * @method SysUsers findOneByIdEnterprise(int $id_enterprise) Return the first SysUsers filtered by the id_enterprise column
 *
 * @method array findByIdUser(int $id_user) Return SysUsers objects filtered by the id_user column
 * @method array findByUsername(string $username) Return SysUsers objects filtered by the username column
 * @method array findByPassword(string $password) Return SysUsers objects filtered by the password column
 * @method array findByEmail(string $email) Return SysUsers objects filtered by the email column
 * @method array findByFirstName(string $first_name) Return SysUsers objects filtered by the first_name column
 * @method array findByLastName(string $last_name) Return SysUsers objects filtered by the last_name column
 * @method array findByState(string $state) Return SysUsers objects filtered by the state column
 * @method array findByIdRol(int $id_rol) Return SysUsers objects filtered by the id_rol column
 * @method array findByIdPhoto(int $id_photo) Return SysUsers objects filtered by the id_photo column
 * @method array findByCreated(string $created) Return SysUsers objects filtered by the created column
 * @method array findByModified(string $modified) Return SysUsers objects filtered by the modified column
 * @method array findByUserModified(int $user_modified) Return SysUsers objects filtered by the user_modified column
 * @method array findByLangCode(string $lang_code) Return SysUsers objects filtered by the lang_code column
 * @method array findByPhone(string $phone) Return SysUsers objects filtered by the phone column
 * @method array findByMobile(string $mobile) Return SysUsers objects filtered by the mobile column
 * @method array findByIdTimeZone(int $id_time_zone) Return SysUsers objects filtered by the id_time_zone column
 * @method array findByIdCityBirthday(int $id_city_birthday) Return SysUsers objects filtered by the id_city_birthday column
 * @method array findByIdCountryBirthday(int $id_country_birthday) Return SysUsers objects filtered by the id_country_birthday column
 * @method array findByIdCityAddress(int $id_city_address) Return SysUsers objects filtered by the id_city_address column
 * @method array findByIdCountryAddress(int $id_country_address) Return SysUsers objects filtered by the id_country_address column
 * @method array findByGender(string $gender) Return SysUsers objects filtered by the gender column
 * @method array findByBirthday(string $birthday) Return SysUsers objects filtered by the birthday column
 * @method array findByBirthplace(string $birthplace) Return SysUsers objects filtered by the birthplace column
 * @method array findByHash(string $hash) Return SysUsers objects filtered by the hash column
 * @method array findByTour(string $tour) Return SysUsers objects filtered by the tour column
 * @method array findByMaritalStatus(string $marital_status) Return SysUsers objects filtered by the marital_status column
 * @method array findByLevelEducation(string $level_education) Return SysUsers objects filtered by the level_education column
 * @method array findByOccupation(string $occupation) Return SysUsers objects filtered by the occupation column
 * @method array findByDepartmentAddress(string $department_address) Return SysUsers objects filtered by the department_address column
 * @method array findByDepartmentBirthday(string $department_birthday) Return SysUsers objects filtered by the department_birthday column
 * @method array findByAddress(string $address) Return SysUsers objects filtered by the address column
 * @method array findByIdChurch(int $id_church) Return SysUsers objects filtered by the id_church column
 * @method array findByMember(string $member) Return SysUsers objects filtered by the member column
 * @method array findByPastorBaptism(string $pastor_baptism) Return SysUsers objects filtered by the pastor_baptism column
 * @method array findByDateBaptism(string $date_baptism) Return SysUsers objects filtered by the date_baptism column
 * @method array findByIdEnterprise(int $id_enterprise) Return SysUsers objects filtered by the id_enterprise column
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseSysUsersQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSysUsersQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bach';
        }
        if (null === $modelName) {
            $modelName = 'SysUsers';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SysUsersQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SysUsersQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SysUsersQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SysUsersQuery) {
            return $criteria;
        }
        $query = new SysUsersQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SysUsers|SysUsers[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SysUsersPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SysUsersPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SysUsers A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdUser($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SysUsers A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_user`, `username`, `password`, `email`, `first_name`, `last_name`, `state`, `id_rol`, `id_photo`, `created`, `modified`, `user_modified`, `lang_code`, `phone`, `mobile`, `id_time_zone`, `id_city_birthday`, `id_country_birthday`, `id_city_address`, `id_country_address`, `gender`, `birthday`, `birthplace`, `hash`, `tour`, `marital_status`, `level_education`, `occupation`, `department_address`, `department_birthday`, `address`, `id_church`, `member`, `pastor_baptism`, `date_baptism`, `id_enterprise` FROM `sys_users` WHERE `id_user` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SysUsers();
            $obj->hydrate($row);
            SysUsersPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SysUsers|SysUsers[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SysUsers[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SysUsersPeer::ID_USER, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SysUsersPeer::ID_USER, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_user column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUser(1234); // WHERE id_user = 1234
     * $query->filterByIdUser(array(12, 34)); // WHERE id_user IN (12, 34)
     * $query->filterByIdUser(array('min' => 12)); // WHERE id_user >= 12
     * $query->filterByIdUser(array('max' => 12)); // WHERE id_user <= 12
     * </code>
     *
     * @param     mixed $idUser The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByIdUser($idUser = null, $comparison = null)
    {
        if (is_array($idUser)) {
            $useMinMax = false;
            if (isset($idUser['min'])) {
                $this->addUsingAlias(SysUsersPeer::ID_USER, $idUser['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUser['max'])) {
                $this->addUsingAlias(SysUsersPeer::ID_USER, $idUser['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::ID_USER, $idUser, $comparison);
    }

    /**
     * Filter the query on the username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE username = 'fooValue'
     * $query->filterByUsername('%fooValue%'); // WHERE username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $username)) {
                $username = str_replace('*', '%', $username);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%'); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $password)) {
                $password = str_replace('*', '%', $password);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the first_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstName('fooValue');   // WHERE first_name = 'fooValue'
     * $query->filterByFirstName('%fooValue%'); // WHERE first_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByFirstName($firstName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $firstName)) {
                $firstName = str_replace('*', '%', $firstName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::FIRST_NAME, $firstName, $comparison);
    }

    /**
     * Filter the query on the last_name column
     *
     * Example usage:
     * <code>
     * $query->filterByLastName('fooValue');   // WHERE last_name = 'fooValue'
     * $query->filterByLastName('%fooValue%'); // WHERE last_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByLastName($lastName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lastName)) {
                $lastName = str_replace('*', '%', $lastName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::LAST_NAME, $lastName, $comparison);
    }

    /**
     * Filter the query on the state column
     *
     * Example usage:
     * <code>
     * $query->filterByState('fooValue');   // WHERE state = 'fooValue'
     * $query->filterByState('%fooValue%'); // WHERE state LIKE '%fooValue%'
     * </code>
     *
     * @param     string $state The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($state)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $state)) {
                $state = str_replace('*', '%', $state);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::STATE, $state, $comparison);
    }

    /**
     * Filter the query on the id_rol column
     *
     * Example usage:
     * <code>
     * $query->filterByIdRol(1234); // WHERE id_rol = 1234
     * $query->filterByIdRol(array(12, 34)); // WHERE id_rol IN (12, 34)
     * $query->filterByIdRol(array('min' => 12)); // WHERE id_rol >= 12
     * $query->filterByIdRol(array('max' => 12)); // WHERE id_rol <= 12
     * </code>
     *
     * @see       filterBySysRoles()
     *
     * @param     mixed $idRol The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByIdRol($idRol = null, $comparison = null)
    {
        if (is_array($idRol)) {
            $useMinMax = false;
            if (isset($idRol['min'])) {
                $this->addUsingAlias(SysUsersPeer::ID_ROL, $idRol['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idRol['max'])) {
                $this->addUsingAlias(SysUsersPeer::ID_ROL, $idRol['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::ID_ROL, $idRol, $comparison);
    }

    /**
     * Filter the query on the id_photo column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPhoto(1234); // WHERE id_photo = 1234
     * $query->filterByIdPhoto(array(12, 34)); // WHERE id_photo IN (12, 34)
     * $query->filterByIdPhoto(array('min' => 12)); // WHERE id_photo >= 12
     * $query->filterByIdPhoto(array('max' => 12)); // WHERE id_photo <= 12
     * </code>
     *
     * @see       filterBySysFiles()
     *
     * @param     mixed $idPhoto The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByIdPhoto($idPhoto = null, $comparison = null)
    {
        if (is_array($idPhoto)) {
            $useMinMax = false;
            if (isset($idPhoto['min'])) {
                $this->addUsingAlias(SysUsersPeer::ID_PHOTO, $idPhoto['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPhoto['max'])) {
                $this->addUsingAlias(SysUsersPeer::ID_PHOTO, $idPhoto['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::ID_PHOTO, $idPhoto, $comparison);
    }

    /**
     * Filter the query on the created column
     *
     * Example usage:
     * <code>
     * $query->filterByCreated('2011-03-14'); // WHERE created = '2011-03-14'
     * $query->filterByCreated('now'); // WHERE created = '2011-03-14'
     * $query->filterByCreated(array('max' => 'yesterday')); // WHERE created < '2011-03-13'
     * </code>
     *
     * @param     mixed $created The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByCreated($created = null, $comparison = null)
    {
        if (is_array($created)) {
            $useMinMax = false;
            if (isset($created['min'])) {
                $this->addUsingAlias(SysUsersPeer::CREATED, $created['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($created['max'])) {
                $this->addUsingAlias(SysUsersPeer::CREATED, $created['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::CREATED, $created, $comparison);
    }

    /**
     * Filter the query on the modified column
     *
     * Example usage:
     * <code>
     * $query->filterByModified('2011-03-14'); // WHERE modified = '2011-03-14'
     * $query->filterByModified('now'); // WHERE modified = '2011-03-14'
     * $query->filterByModified(array('max' => 'yesterday')); // WHERE modified < '2011-03-13'
     * </code>
     *
     * @param     mixed $modified The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByModified($modified = null, $comparison = null)
    {
        if (is_array($modified)) {
            $useMinMax = false;
            if (isset($modified['min'])) {
                $this->addUsingAlias(SysUsersPeer::MODIFIED, $modified['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modified['max'])) {
                $this->addUsingAlias(SysUsersPeer::MODIFIED, $modified['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::MODIFIED, $modified, $comparison);
    }

    /**
     * Filter the query on the user_modified column
     *
     * Example usage:
     * <code>
     * $query->filterByUserModified(1234); // WHERE user_modified = 1234
     * $query->filterByUserModified(array(12, 34)); // WHERE user_modified IN (12, 34)
     * $query->filterByUserModified(array('min' => 12)); // WHERE user_modified >= 12
     * $query->filterByUserModified(array('max' => 12)); // WHERE user_modified <= 12
     * </code>
     *
     * @see       filterBySysUsersRelatedByUserModified()
     *
     * @param     mixed $userModified The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByUserModified($userModified = null, $comparison = null)
    {
        if (is_array($userModified)) {
            $useMinMax = false;
            if (isset($userModified['min'])) {
                $this->addUsingAlias(SysUsersPeer::USER_MODIFIED, $userModified['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userModified['max'])) {
                $this->addUsingAlias(SysUsersPeer::USER_MODIFIED, $userModified['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::USER_MODIFIED, $userModified, $comparison);
    }

    /**
     * Filter the query on the lang_code column
     *
     * Example usage:
     * <code>
     * $query->filterByLangCode('fooValue');   // WHERE lang_code = 'fooValue'
     * $query->filterByLangCode('%fooValue%'); // WHERE lang_code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $langCode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByLangCode($langCode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($langCode)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $langCode)) {
                $langCode = str_replace('*', '%', $langCode);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::LANG_CODE, $langCode, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE phone = 'fooValue'
     * $query->filterByPhone('%fooValue%'); // WHERE phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phone)) {
                $phone = str_replace('*', '%', $phone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the mobile column
     *
     * Example usage:
     * <code>
     * $query->filterByMobile('fooValue');   // WHERE mobile = 'fooValue'
     * $query->filterByMobile('%fooValue%'); // WHERE mobile LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mobile The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByMobile($mobile = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mobile)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mobile)) {
                $mobile = str_replace('*', '%', $mobile);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::MOBILE, $mobile, $comparison);
    }

    /**
     * Filter the query on the id_time_zone column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTimeZone(1234); // WHERE id_time_zone = 1234
     * $query->filterByIdTimeZone(array(12, 34)); // WHERE id_time_zone IN (12, 34)
     * $query->filterByIdTimeZone(array('min' => 12)); // WHERE id_time_zone >= 12
     * $query->filterByIdTimeZone(array('max' => 12)); // WHERE id_time_zone <= 12
     * </code>
     *
     * @param     mixed $idTimeZone The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByIdTimeZone($idTimeZone = null, $comparison = null)
    {
        if (is_array($idTimeZone)) {
            $useMinMax = false;
            if (isset($idTimeZone['min'])) {
                $this->addUsingAlias(SysUsersPeer::ID_TIME_ZONE, $idTimeZone['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTimeZone['max'])) {
                $this->addUsingAlias(SysUsersPeer::ID_TIME_ZONE, $idTimeZone['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::ID_TIME_ZONE, $idTimeZone, $comparison);
    }

    /**
     * Filter the query on the id_city_birthday column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCityBirthday(1234); // WHERE id_city_birthday = 1234
     * $query->filterByIdCityBirthday(array(12, 34)); // WHERE id_city_birthday IN (12, 34)
     * $query->filterByIdCityBirthday(array('min' => 12)); // WHERE id_city_birthday >= 12
     * $query->filterByIdCityBirthday(array('max' => 12)); // WHERE id_city_birthday <= 12
     * </code>
     *
     * @param     mixed $idCityBirthday The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByIdCityBirthday($idCityBirthday = null, $comparison = null)
    {
        if (is_array($idCityBirthday)) {
            $useMinMax = false;
            if (isset($idCityBirthday['min'])) {
                $this->addUsingAlias(SysUsersPeer::ID_CITY_BIRTHDAY, $idCityBirthday['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCityBirthday['max'])) {
                $this->addUsingAlias(SysUsersPeer::ID_CITY_BIRTHDAY, $idCityBirthday['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::ID_CITY_BIRTHDAY, $idCityBirthday, $comparison);
    }

    /**
     * Filter the query on the id_country_birthday column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCountryBirthday(1234); // WHERE id_country_birthday = 1234
     * $query->filterByIdCountryBirthday(array(12, 34)); // WHERE id_country_birthday IN (12, 34)
     * $query->filterByIdCountryBirthday(array('min' => 12)); // WHERE id_country_birthday >= 12
     * $query->filterByIdCountryBirthday(array('max' => 12)); // WHERE id_country_birthday <= 12
     * </code>
     *
     * @param     mixed $idCountryBirthday The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByIdCountryBirthday($idCountryBirthday = null, $comparison = null)
    {
        if (is_array($idCountryBirthday)) {
            $useMinMax = false;
            if (isset($idCountryBirthday['min'])) {
                $this->addUsingAlias(SysUsersPeer::ID_COUNTRY_BIRTHDAY, $idCountryBirthday['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCountryBirthday['max'])) {
                $this->addUsingAlias(SysUsersPeer::ID_COUNTRY_BIRTHDAY, $idCountryBirthday['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::ID_COUNTRY_BIRTHDAY, $idCountryBirthday, $comparison);
    }

    /**
     * Filter the query on the id_city_address column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCityAddress(1234); // WHERE id_city_address = 1234
     * $query->filterByIdCityAddress(array(12, 34)); // WHERE id_city_address IN (12, 34)
     * $query->filterByIdCityAddress(array('min' => 12)); // WHERE id_city_address >= 12
     * $query->filterByIdCityAddress(array('max' => 12)); // WHERE id_city_address <= 12
     * </code>
     *
     * @param     mixed $idCityAddress The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByIdCityAddress($idCityAddress = null, $comparison = null)
    {
        if (is_array($idCityAddress)) {
            $useMinMax = false;
            if (isset($idCityAddress['min'])) {
                $this->addUsingAlias(SysUsersPeer::ID_CITY_ADDRESS, $idCityAddress['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCityAddress['max'])) {
                $this->addUsingAlias(SysUsersPeer::ID_CITY_ADDRESS, $idCityAddress['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::ID_CITY_ADDRESS, $idCityAddress, $comparison);
    }

    /**
     * Filter the query on the id_country_address column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCountryAddress(1234); // WHERE id_country_address = 1234
     * $query->filterByIdCountryAddress(array(12, 34)); // WHERE id_country_address IN (12, 34)
     * $query->filterByIdCountryAddress(array('min' => 12)); // WHERE id_country_address >= 12
     * $query->filterByIdCountryAddress(array('max' => 12)); // WHERE id_country_address <= 12
     * </code>
     *
     * @param     mixed $idCountryAddress The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByIdCountryAddress($idCountryAddress = null, $comparison = null)
    {
        if (is_array($idCountryAddress)) {
            $useMinMax = false;
            if (isset($idCountryAddress['min'])) {
                $this->addUsingAlias(SysUsersPeer::ID_COUNTRY_ADDRESS, $idCountryAddress['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCountryAddress['max'])) {
                $this->addUsingAlias(SysUsersPeer::ID_COUNTRY_ADDRESS, $idCountryAddress['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::ID_COUNTRY_ADDRESS, $idCountryAddress, $comparison);
    }

    /**
     * Filter the query on the gender column
     *
     * Example usage:
     * <code>
     * $query->filterByGender('fooValue');   // WHERE gender = 'fooValue'
     * $query->filterByGender('%fooValue%'); // WHERE gender LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gender The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByGender($gender = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gender)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $gender)) {
                $gender = str_replace('*', '%', $gender);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::GENDER, $gender, $comparison);
    }

    /**
     * Filter the query on the birthday column
     *
     * Example usage:
     * <code>
     * $query->filterByBirthday('2011-03-14'); // WHERE birthday = '2011-03-14'
     * $query->filterByBirthday('now'); // WHERE birthday = '2011-03-14'
     * $query->filterByBirthday(array('max' => 'yesterday')); // WHERE birthday < '2011-03-13'
     * </code>
     *
     * @param     mixed $birthday The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByBirthday($birthday = null, $comparison = null)
    {
        if (is_array($birthday)) {
            $useMinMax = false;
            if (isset($birthday['min'])) {
                $this->addUsingAlias(SysUsersPeer::BIRTHDAY, $birthday['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($birthday['max'])) {
                $this->addUsingAlias(SysUsersPeer::BIRTHDAY, $birthday['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::BIRTHDAY, $birthday, $comparison);
    }

    /**
     * Filter the query on the birthplace column
     *
     * Example usage:
     * <code>
     * $query->filterByBirthplace('fooValue');   // WHERE birthplace = 'fooValue'
     * $query->filterByBirthplace('%fooValue%'); // WHERE birthplace LIKE '%fooValue%'
     * </code>
     *
     * @param     string $birthplace The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByBirthplace($birthplace = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($birthplace)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $birthplace)) {
                $birthplace = str_replace('*', '%', $birthplace);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::BIRTHPLACE, $birthplace, $comparison);
    }

    /**
     * Filter the query on the hash column
     *
     * Example usage:
     * <code>
     * $query->filterByHash('fooValue');   // WHERE hash = 'fooValue'
     * $query->filterByHash('%fooValue%'); // WHERE hash LIKE '%fooValue%'
     * </code>
     *
     * @param     string $hash The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByHash($hash = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($hash)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $hash)) {
                $hash = str_replace('*', '%', $hash);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::HASH, $hash, $comparison);
    }

    /**
     * Filter the query on the tour column
     *
     * Example usage:
     * <code>
     * $query->filterByTour('fooValue');   // WHERE tour = 'fooValue'
     * $query->filterByTour('%fooValue%'); // WHERE tour LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tour The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByTour($tour = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tour)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tour)) {
                $tour = str_replace('*', '%', $tour);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::TOUR, $tour, $comparison);
    }

    /**
     * Filter the query on the marital_status column
     *
     * Example usage:
     * <code>
     * $query->filterByMaritalStatus('fooValue');   // WHERE marital_status = 'fooValue'
     * $query->filterByMaritalStatus('%fooValue%'); // WHERE marital_status LIKE '%fooValue%'
     * </code>
     *
     * @param     string $maritalStatus The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByMaritalStatus($maritalStatus = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($maritalStatus)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $maritalStatus)) {
                $maritalStatus = str_replace('*', '%', $maritalStatus);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::MARITAL_STATUS, $maritalStatus, $comparison);
    }

    /**
     * Filter the query on the level_education column
     *
     * Example usage:
     * <code>
     * $query->filterByLevelEducation('fooValue');   // WHERE level_education = 'fooValue'
     * $query->filterByLevelEducation('%fooValue%'); // WHERE level_education LIKE '%fooValue%'
     * </code>
     *
     * @param     string $levelEducation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByLevelEducation($levelEducation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($levelEducation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $levelEducation)) {
                $levelEducation = str_replace('*', '%', $levelEducation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::LEVEL_EDUCATION, $levelEducation, $comparison);
    }

    /**
     * Filter the query on the occupation column
     *
     * Example usage:
     * <code>
     * $query->filterByOccupation('fooValue');   // WHERE occupation = 'fooValue'
     * $query->filterByOccupation('%fooValue%'); // WHERE occupation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $occupation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByOccupation($occupation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($occupation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $occupation)) {
                $occupation = str_replace('*', '%', $occupation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::OCCUPATION, $occupation, $comparison);
    }

    /**
     * Filter the query on the department_address column
     *
     * Example usage:
     * <code>
     * $query->filterByDepartmentAddress('fooValue');   // WHERE department_address = 'fooValue'
     * $query->filterByDepartmentAddress('%fooValue%'); // WHERE department_address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $departmentAddress The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByDepartmentAddress($departmentAddress = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($departmentAddress)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $departmentAddress)) {
                $departmentAddress = str_replace('*', '%', $departmentAddress);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::DEPARTMENT_ADDRESS, $departmentAddress, $comparison);
    }

    /**
     * Filter the query on the department_birthday column
     *
     * Example usage:
     * <code>
     * $query->filterByDepartmentBirthday('fooValue');   // WHERE department_birthday = 'fooValue'
     * $query->filterByDepartmentBirthday('%fooValue%'); // WHERE department_birthday LIKE '%fooValue%'
     * </code>
     *
     * @param     string $departmentBirthday The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByDepartmentBirthday($departmentBirthday = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($departmentBirthday)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $departmentBirthday)) {
                $departmentBirthday = str_replace('*', '%', $departmentBirthday);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::DEPARTMENT_BIRTHDAY, $departmentBirthday, $comparison);
    }

    /**
     * Filter the query on the address column
     *
     * Example usage:
     * <code>
     * $query->filterByAddress('fooValue');   // WHERE address = 'fooValue'
     * $query->filterByAddress('%fooValue%'); // WHERE address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $address The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByAddress($address = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($address)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $address)) {
                $address = str_replace('*', '%', $address);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::ADDRESS, $address, $comparison);
    }

    /**
     * Filter the query on the id_church column
     *
     * Example usage:
     * <code>
     * $query->filterByIdChurch(1234); // WHERE id_church = 1234
     * $query->filterByIdChurch(array(12, 34)); // WHERE id_church IN (12, 34)
     * $query->filterByIdChurch(array('min' => 12)); // WHERE id_church >= 12
     * $query->filterByIdChurch(array('max' => 12)); // WHERE id_church <= 12
     * </code>
     *
     * @param     mixed $idChurch The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByIdChurch($idChurch = null, $comparison = null)
    {
        if (is_array($idChurch)) {
            $useMinMax = false;
            if (isset($idChurch['min'])) {
                $this->addUsingAlias(SysUsersPeer::ID_CHURCH, $idChurch['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idChurch['max'])) {
                $this->addUsingAlias(SysUsersPeer::ID_CHURCH, $idChurch['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::ID_CHURCH, $idChurch, $comparison);
    }

    /**
     * Filter the query on the member column
     *
     * Example usage:
     * <code>
     * $query->filterByMember('fooValue');   // WHERE member = 'fooValue'
     * $query->filterByMember('%fooValue%'); // WHERE member LIKE '%fooValue%'
     * </code>
     *
     * @param     string $member The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByMember($member = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($member)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $member)) {
                $member = str_replace('*', '%', $member);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::MEMBER, $member, $comparison);
    }

    /**
     * Filter the query on the pastor_baptism column
     *
     * Example usage:
     * <code>
     * $query->filterByPastorBaptism('fooValue');   // WHERE pastor_baptism = 'fooValue'
     * $query->filterByPastorBaptism('%fooValue%'); // WHERE pastor_baptism LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pastorBaptism The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByPastorBaptism($pastorBaptism = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pastorBaptism)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $pastorBaptism)) {
                $pastorBaptism = str_replace('*', '%', $pastorBaptism);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::PASTOR_BAPTISM, $pastorBaptism, $comparison);
    }

    /**
     * Filter the query on the date_baptism column
     *
     * Example usage:
     * <code>
     * $query->filterByDateBaptism('2011-03-14'); // WHERE date_baptism = '2011-03-14'
     * $query->filterByDateBaptism('now'); // WHERE date_baptism = '2011-03-14'
     * $query->filterByDateBaptism(array('max' => 'yesterday')); // WHERE date_baptism < '2011-03-13'
     * </code>
     *
     * @param     mixed $dateBaptism The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByDateBaptism($dateBaptism = null, $comparison = null)
    {
        if (is_array($dateBaptism)) {
            $useMinMax = false;
            if (isset($dateBaptism['min'])) {
                $this->addUsingAlias(SysUsersPeer::DATE_BAPTISM, $dateBaptism['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateBaptism['max'])) {
                $this->addUsingAlias(SysUsersPeer::DATE_BAPTISM, $dateBaptism['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::DATE_BAPTISM, $dateBaptism, $comparison);
    }

    /**
     * Filter the query on the id_enterprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEnterprise(1234); // WHERE id_enterprise = 1234
     * $query->filterByIdEnterprise(array(12, 34)); // WHERE id_enterprise IN (12, 34)
     * $query->filterByIdEnterprise(array('min' => 12)); // WHERE id_enterprise >= 12
     * $query->filterByIdEnterprise(array('max' => 12)); // WHERE id_enterprise <= 12
     * </code>
     *
     * @see       filterBySysEnterprisesRelatedByIdEnterprise()
     *
     * @param     mixed $idEnterprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function filterByIdEnterprise($idEnterprise = null, $comparison = null)
    {
        if (is_array($idEnterprise)) {
            $useMinMax = false;
            if (isset($idEnterprise['min'])) {
                $this->addUsingAlias(SysUsersPeer::ID_ENTERPRISE, $idEnterprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEnterprise['max'])) {
                $this->addUsingAlias(SysUsersPeer::ID_ENTERPRISE, $idEnterprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysUsersPeer::ID_ENTERPRISE, $idEnterprise, $comparison);
    }

    /**
     * Filter the query by a related SysEnterprises object
     *
     * @param   SysEnterprises|PropelObjectCollection $sysEnterprises The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysUsersQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysEnterprisesRelatedByIdEnterprise($sysEnterprises, $comparison = null)
    {
        if ($sysEnterprises instanceof SysEnterprises) {
            return $this
                ->addUsingAlias(SysUsersPeer::ID_ENTERPRISE, $sysEnterprises->getIdEnterprise(), $comparison);
        } elseif ($sysEnterprises instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SysUsersPeer::ID_ENTERPRISE, $sysEnterprises->toKeyValue('PrimaryKey', 'IdEnterprise'), $comparison);
        } else {
            throw new PropelException('filterBySysEnterprisesRelatedByIdEnterprise() only accepts arguments of type SysEnterprises or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysEnterprisesRelatedByIdEnterprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function joinSysEnterprisesRelatedByIdEnterprise($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysEnterprisesRelatedByIdEnterprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysEnterprisesRelatedByIdEnterprise');
        }

        return $this;
    }

    /**
     * Use the SysEnterprisesRelatedByIdEnterprise relation SysEnterprises object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysEnterprisesQuery A secondary query class using the current class as primary query
     */
    public function useSysEnterprisesRelatedByIdEnterpriseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysEnterprisesRelatedByIdEnterprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysEnterprisesRelatedByIdEnterprise', 'SysEnterprisesQuery');
    }

    /**
     * Filter the query by a related SysRoles object
     *
     * @param   SysRoles|PropelObjectCollection $sysRoles The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysUsersQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysRoles($sysRoles, $comparison = null)
    {
        if ($sysRoles instanceof SysRoles) {
            return $this
                ->addUsingAlias(SysUsersPeer::ID_ROL, $sysRoles->getIdRol(), $comparison);
        } elseif ($sysRoles instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SysUsersPeer::ID_ROL, $sysRoles->toKeyValue('PrimaryKey', 'IdRol'), $comparison);
        } else {
            throw new PropelException('filterBySysRoles() only accepts arguments of type SysRoles or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysRoles relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function joinSysRoles($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysRoles');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysRoles');
        }

        return $this;
    }

    /**
     * Use the SysRoles relation SysRoles object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysRolesQuery A secondary query class using the current class as primary query
     */
    public function useSysRolesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysRoles($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysRoles', 'SysRolesQuery');
    }

    /**
     * Filter the query by a related SysFiles object
     *
     * @param   SysFiles|PropelObjectCollection $sysFiles The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysUsersQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysFiles($sysFiles, $comparison = null)
    {
        if ($sysFiles instanceof SysFiles) {
            return $this
                ->addUsingAlias(SysUsersPeer::ID_PHOTO, $sysFiles->getIdFile(), $comparison);
        } elseif ($sysFiles instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SysUsersPeer::ID_PHOTO, $sysFiles->toKeyValue('PrimaryKey', 'IdFile'), $comparison);
        } else {
            throw new PropelException('filterBySysFiles() only accepts arguments of type SysFiles or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysFiles relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function joinSysFiles($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysFiles');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysFiles');
        }

        return $this;
    }

    /**
     * Use the SysFiles relation SysFiles object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysFilesQuery A secondary query class using the current class as primary query
     */
    public function useSysFilesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysFiles($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysFiles', 'SysFilesQuery');
    }

    /**
     * Filter the query by a related SysUsers object
     *
     * @param   SysUsers|PropelObjectCollection $sysUsers The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysUsersQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysUsersRelatedByUserModified($sysUsers, $comparison = null)
    {
        if ($sysUsers instanceof SysUsers) {
            return $this
                ->addUsingAlias(SysUsersPeer::USER_MODIFIED, $sysUsers->getIdUser(), $comparison);
        } elseif ($sysUsers instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SysUsersPeer::USER_MODIFIED, $sysUsers->toKeyValue('PrimaryKey', 'IdUser'), $comparison);
        } else {
            throw new PropelException('filterBySysUsersRelatedByUserModified() only accepts arguments of type SysUsers or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysUsersRelatedByUserModified relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function joinSysUsersRelatedByUserModified($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysUsersRelatedByUserModified');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysUsersRelatedByUserModified');
        }

        return $this;
    }

    /**
     * Use the SysUsersRelatedByUserModified relation SysUsers object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysUsersQuery A secondary query class using the current class as primary query
     */
    public function useSysUsersRelatedByUserModifiedQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysUsersRelatedByUserModified($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysUsersRelatedByUserModified', 'SysUsersQuery');
    }

    /**
     * Filter the query by a related CmsArticles object
     *
     * @param   CmsArticles|PropelObjectCollection $cmsArticles  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysUsersQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsArticlesRelatedByIdUser($cmsArticles, $comparison = null)
    {
        if ($cmsArticles instanceof CmsArticles) {
            return $this
                ->addUsingAlias(SysUsersPeer::ID_USER, $cmsArticles->getIdUser(), $comparison);
        } elseif ($cmsArticles instanceof PropelObjectCollection) {
            return $this
                ->useCmsArticlesRelatedByIdUserQuery()
                ->filterByPrimaryKeys($cmsArticles->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCmsArticlesRelatedByIdUser() only accepts arguments of type CmsArticles or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsArticlesRelatedByIdUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function joinCmsArticlesRelatedByIdUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsArticlesRelatedByIdUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsArticlesRelatedByIdUser');
        }

        return $this;
    }

    /**
     * Use the CmsArticlesRelatedByIdUser relation CmsArticles object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsArticlesQuery A secondary query class using the current class as primary query
     */
    public function useCmsArticlesRelatedByIdUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCmsArticlesRelatedByIdUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsArticlesRelatedByIdUser', 'CmsArticlesQuery');
    }

    /**
     * Filter the query by a related CmsArticles object
     *
     * @param   CmsArticles|PropelObjectCollection $cmsArticles  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysUsersQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsArticlesRelatedByUserModified($cmsArticles, $comparison = null)
    {
        if ($cmsArticles instanceof CmsArticles) {
            return $this
                ->addUsingAlias(SysUsersPeer::ID_USER, $cmsArticles->getUserModified(), $comparison);
        } elseif ($cmsArticles instanceof PropelObjectCollection) {
            return $this
                ->useCmsArticlesRelatedByUserModifiedQuery()
                ->filterByPrimaryKeys($cmsArticles->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCmsArticlesRelatedByUserModified() only accepts arguments of type CmsArticles or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsArticlesRelatedByUserModified relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function joinCmsArticlesRelatedByUserModified($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsArticlesRelatedByUserModified');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsArticlesRelatedByUserModified');
        }

        return $this;
    }

    /**
     * Use the CmsArticlesRelatedByUserModified relation CmsArticles object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsArticlesQuery A secondary query class using the current class as primary query
     */
    public function useCmsArticlesRelatedByUserModifiedQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCmsArticlesRelatedByUserModified($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsArticlesRelatedByUserModified', 'CmsArticlesQuery');
    }

    /**
     * Filter the query by a related CmsPages object
     *
     * @param   CmsPages|PropelObjectCollection $cmsPages  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysUsersQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsPages($cmsPages, $comparison = null)
    {
        if ($cmsPages instanceof CmsPages) {
            return $this
                ->addUsingAlias(SysUsersPeer::ID_USER, $cmsPages->getUserModified(), $comparison);
        } elseif ($cmsPages instanceof PropelObjectCollection) {
            return $this
                ->useCmsPagesQuery()
                ->filterByPrimaryKeys($cmsPages->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCmsPages() only accepts arguments of type CmsPages or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsPages relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function joinCmsPages($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsPages');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsPages');
        }

        return $this;
    }

    /**
     * Use the CmsPages relation CmsPages object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsPagesQuery A secondary query class using the current class as primary query
     */
    public function useCmsPagesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCmsPages($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsPages', 'CmsPagesQuery');
    }

    /**
     * Filter the query by a related CmsVideosXArticle object
     *
     * @param   CmsVideosXArticle|PropelObjectCollection $cmsVideosXArticle  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysUsersQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsVideosXArticle($cmsVideosXArticle, $comparison = null)
    {
        if ($cmsVideosXArticle instanceof CmsVideosXArticle) {
            return $this
                ->addUsingAlias(SysUsersPeer::ID_USER, $cmsVideosXArticle->getUserModified(), $comparison);
        } elseif ($cmsVideosXArticle instanceof PropelObjectCollection) {
            return $this
                ->useCmsVideosXArticleQuery()
                ->filterByPrimaryKeys($cmsVideosXArticle->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCmsVideosXArticle() only accepts arguments of type CmsVideosXArticle or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsVideosXArticle relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function joinCmsVideosXArticle($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsVideosXArticle');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsVideosXArticle');
        }

        return $this;
    }

    /**
     * Use the CmsVideosXArticle relation CmsVideosXArticle object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsVideosXArticleQuery A secondary query class using the current class as primary query
     */
    public function useCmsVideosXArticleQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCmsVideosXArticle($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsVideosXArticle', 'CmsVideosXArticleQuery');
    }

    /**
     * Filter the query by a related CmsVideosXPage object
     *
     * @param   CmsVideosXPage|PropelObjectCollection $cmsVideosXPage  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysUsersQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsVideosXPage($cmsVideosXPage, $comparison = null)
    {
        if ($cmsVideosXPage instanceof CmsVideosXPage) {
            return $this
                ->addUsingAlias(SysUsersPeer::ID_USER, $cmsVideosXPage->getUserModified(), $comparison);
        } elseif ($cmsVideosXPage instanceof PropelObjectCollection) {
            return $this
                ->useCmsVideosXPageQuery()
                ->filterByPrimaryKeys($cmsVideosXPage->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCmsVideosXPage() only accepts arguments of type CmsVideosXPage or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsVideosXPage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function joinCmsVideosXPage($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsVideosXPage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsVideosXPage');
        }

        return $this;
    }

    /**
     * Use the CmsVideosXPage relation CmsVideosXPage object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsVideosXPageQuery A secondary query class using the current class as primary query
     */
    public function useCmsVideosXPageQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCmsVideosXPage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsVideosXPage', 'CmsVideosXPageQuery');
    }

    /**
     * Filter the query by a related SysChats object
     *
     * @param   SysChats|PropelObjectCollection $sysChats  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysUsersQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysChatsRelatedByIdSender($sysChats, $comparison = null)
    {
        if ($sysChats instanceof SysChats) {
            return $this
                ->addUsingAlias(SysUsersPeer::ID_USER, $sysChats->getIdSender(), $comparison);
        } elseif ($sysChats instanceof PropelObjectCollection) {
            return $this
                ->useSysChatsRelatedByIdSenderQuery()
                ->filterByPrimaryKeys($sysChats->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySysChatsRelatedByIdSender() only accepts arguments of type SysChats or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysChatsRelatedByIdSender relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function joinSysChatsRelatedByIdSender($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysChatsRelatedByIdSender');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysChatsRelatedByIdSender');
        }

        return $this;
    }

    /**
     * Use the SysChatsRelatedByIdSender relation SysChats object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysChatsQuery A secondary query class using the current class as primary query
     */
    public function useSysChatsRelatedByIdSenderQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysChatsRelatedByIdSender($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysChatsRelatedByIdSender', 'SysChatsQuery');
    }

    /**
     * Filter the query by a related SysChats object
     *
     * @param   SysChats|PropelObjectCollection $sysChats  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysUsersQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysChatsRelatedByIdReceiver($sysChats, $comparison = null)
    {
        if ($sysChats instanceof SysChats) {
            return $this
                ->addUsingAlias(SysUsersPeer::ID_USER, $sysChats->getIdReceiver(), $comparison);
        } elseif ($sysChats instanceof PropelObjectCollection) {
            return $this
                ->useSysChatsRelatedByIdReceiverQuery()
                ->filterByPrimaryKeys($sysChats->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySysChatsRelatedByIdReceiver() only accepts arguments of type SysChats or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysChatsRelatedByIdReceiver relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function joinSysChatsRelatedByIdReceiver($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysChatsRelatedByIdReceiver');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysChatsRelatedByIdReceiver');
        }

        return $this;
    }

    /**
     * Use the SysChatsRelatedByIdReceiver relation SysChats object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysChatsQuery A secondary query class using the current class as primary query
     */
    public function useSysChatsRelatedByIdReceiverQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysChatsRelatedByIdReceiver($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysChatsRelatedByIdReceiver', 'SysChatsQuery');
    }

    /**
     * Filter the query by a related SysEnterprises object
     *
     * @param   SysEnterprises|PropelObjectCollection $sysEnterprises  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysUsersQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysEnterprisesRelatedByUserModified($sysEnterprises, $comparison = null)
    {
        if ($sysEnterprises instanceof SysEnterprises) {
            return $this
                ->addUsingAlias(SysUsersPeer::ID_USER, $sysEnterprises->getUserModified(), $comparison);
        } elseif ($sysEnterprises instanceof PropelObjectCollection) {
            return $this
                ->useSysEnterprisesRelatedByUserModifiedQuery()
                ->filterByPrimaryKeys($sysEnterprises->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySysEnterprisesRelatedByUserModified() only accepts arguments of type SysEnterprises or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysEnterprisesRelatedByUserModified relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function joinSysEnterprisesRelatedByUserModified($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysEnterprisesRelatedByUserModified');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysEnterprisesRelatedByUserModified');
        }

        return $this;
    }

    /**
     * Use the SysEnterprisesRelatedByUserModified relation SysEnterprises object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysEnterprisesQuery A secondary query class using the current class as primary query
     */
    public function useSysEnterprisesRelatedByUserModifiedQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysEnterprisesRelatedByUserModified($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysEnterprisesRelatedByUserModified', 'SysEnterprisesQuery');
    }

    /**
     * Filter the query by a related SysNotifications object
     *
     * @param   SysNotifications|PropelObjectCollection $sysNotifications  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysUsersQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysNotificationsRelatedByIdSender($sysNotifications, $comparison = null)
    {
        if ($sysNotifications instanceof SysNotifications) {
            return $this
                ->addUsingAlias(SysUsersPeer::ID_USER, $sysNotifications->getIdSender(), $comparison);
        } elseif ($sysNotifications instanceof PropelObjectCollection) {
            return $this
                ->useSysNotificationsRelatedByIdSenderQuery()
                ->filterByPrimaryKeys($sysNotifications->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySysNotificationsRelatedByIdSender() only accepts arguments of type SysNotifications or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysNotificationsRelatedByIdSender relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function joinSysNotificationsRelatedByIdSender($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysNotificationsRelatedByIdSender');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysNotificationsRelatedByIdSender');
        }

        return $this;
    }

    /**
     * Use the SysNotificationsRelatedByIdSender relation SysNotifications object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysNotificationsQuery A secondary query class using the current class as primary query
     */
    public function useSysNotificationsRelatedByIdSenderQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysNotificationsRelatedByIdSender($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysNotificationsRelatedByIdSender', 'SysNotificationsQuery');
    }

    /**
     * Filter the query by a related SysNotifications object
     *
     * @param   SysNotifications|PropelObjectCollection $sysNotifications  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysUsersQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysNotificationsRelatedByIdReceiver($sysNotifications, $comparison = null)
    {
        if ($sysNotifications instanceof SysNotifications) {
            return $this
                ->addUsingAlias(SysUsersPeer::ID_USER, $sysNotifications->getIdReceiver(), $comparison);
        } elseif ($sysNotifications instanceof PropelObjectCollection) {
            return $this
                ->useSysNotificationsRelatedByIdReceiverQuery()
                ->filterByPrimaryKeys($sysNotifications->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySysNotificationsRelatedByIdReceiver() only accepts arguments of type SysNotifications or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysNotificationsRelatedByIdReceiver relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function joinSysNotificationsRelatedByIdReceiver($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysNotificationsRelatedByIdReceiver');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysNotificationsRelatedByIdReceiver');
        }

        return $this;
    }

    /**
     * Use the SysNotificationsRelatedByIdReceiver relation SysNotifications object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysNotificationsQuery A secondary query class using the current class as primary query
     */
    public function useSysNotificationsRelatedByIdReceiverQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysNotificationsRelatedByIdReceiver($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysNotificationsRelatedByIdReceiver', 'SysNotificationsQuery');
    }

    /**
     * Filter the query by a related SysPages object
     *
     * @param   SysPages|PropelObjectCollection $sysPages  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysUsersQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysPages($sysPages, $comparison = null)
    {
        if ($sysPages instanceof SysPages) {
            return $this
                ->addUsingAlias(SysUsersPeer::ID_USER, $sysPages->getUserModified(), $comparison);
        } elseif ($sysPages instanceof PropelObjectCollection) {
            return $this
                ->useSysPagesQuery()
                ->filterByPrimaryKeys($sysPages->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySysPages() only accepts arguments of type SysPages or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysPages relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function joinSysPages($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysPages');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysPages');
        }

        return $this;
    }

    /**
     * Use the SysPages relation SysPages object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysPagesQuery A secondary query class using the current class as primary query
     */
    public function useSysPagesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysPages($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysPages', 'SysPagesQuery');
    }

    /**
     * Filter the query by a related SysUsers object
     *
     * @param   SysUsers|PropelObjectCollection $sysUsers  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 SysUsersQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySysUsersRelatedByIdUser($sysUsers, $comparison = null)
    {
        if ($sysUsers instanceof SysUsers) {
            return $this
                ->addUsingAlias(SysUsersPeer::ID_USER, $sysUsers->getUserModified(), $comparison);
        } elseif ($sysUsers instanceof PropelObjectCollection) {
            return $this
                ->useSysUsersRelatedByIdUserQuery()
                ->filterByPrimaryKeys($sysUsers->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySysUsersRelatedByIdUser() only accepts arguments of type SysUsers or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysUsersRelatedByIdUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function joinSysUsersRelatedByIdUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysUsersRelatedByIdUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysUsersRelatedByIdUser');
        }

        return $this;
    }

    /**
     * Use the SysUsersRelatedByIdUser relation SysUsers object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   SysUsersQuery A secondary query class using the current class as primary query
     */
    public function useSysUsersRelatedByIdUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSysUsersRelatedByIdUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysUsersRelatedByIdUser', 'SysUsersQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   SysUsers $sysUsers Object to remove from the list of results
     *
     * @return SysUsersQuery The current query, for fluid interface
     */
    public function prune($sysUsers = null)
    {
        if ($sysUsers) {
            $this->addUsingAlias(SysUsersPeer::ID_USER, $sysUsers->getIdUser(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

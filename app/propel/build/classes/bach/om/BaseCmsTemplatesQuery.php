<?php


/**
 * Base class that represents a query for the 'cms_templates' table.
 *
 *
 *
 * @method CmsTemplatesQuery orderByTemplate($order = Criteria::ASC) Order by the template column
 * @method CmsTemplatesQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method CmsTemplatesQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method CmsTemplatesQuery orderByType($order = Criteria::ASC) Order by the type column
 *
 * @method CmsTemplatesQuery groupByTemplate() Group by the template column
 * @method CmsTemplatesQuery groupByName() Group by the name column
 * @method CmsTemplatesQuery groupByDescription() Group by the description column
 * @method CmsTemplatesQuery groupByType() Group by the type column
 *
 * @method CmsTemplatesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CmsTemplatesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CmsTemplatesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CmsTemplatesQuery leftJoinCmsPages($relationAlias = null) Adds a LEFT JOIN clause to the query using the CmsPages relation
 * @method CmsTemplatesQuery rightJoinCmsPages($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CmsPages relation
 * @method CmsTemplatesQuery innerJoinCmsPages($relationAlias = null) Adds a INNER JOIN clause to the query using the CmsPages relation
 *
 * @method CmsTemplates findOne(PropelPDO $con = null) Return the first CmsTemplates matching the query
 * @method CmsTemplates findOneOrCreate(PropelPDO $con = null) Return the first CmsTemplates matching the query, or a new CmsTemplates object populated from the query conditions when no match is found
 *
 * @method CmsTemplates findOneByName(string $name) Return the first CmsTemplates filtered by the name column
 * @method CmsTemplates findOneByDescription(string $description) Return the first CmsTemplates filtered by the description column
 * @method CmsTemplates findOneByType(string $type) Return the first CmsTemplates filtered by the type column
 *
 * @method array findByTemplate(string $template) Return CmsTemplates objects filtered by the template column
 * @method array findByName(string $name) Return CmsTemplates objects filtered by the name column
 * @method array findByDescription(string $description) Return CmsTemplates objects filtered by the description column
 * @method array findByType(string $type) Return CmsTemplates objects filtered by the type column
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseCmsTemplatesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCmsTemplatesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bach';
        }
        if (null === $modelName) {
            $modelName = 'CmsTemplates';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CmsTemplatesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CmsTemplatesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CmsTemplatesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CmsTemplatesQuery) {
            return $criteria;
        }
        $query = new CmsTemplatesQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CmsTemplates|CmsTemplates[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CmsTemplatesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CmsTemplatesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CmsTemplates A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByTemplate($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CmsTemplates A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `template`, `name`, `description`, `type` FROM `cms_templates` WHERE `template` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CmsTemplates();
            $obj->hydrate($row);
            CmsTemplatesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CmsTemplates|CmsTemplates[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CmsTemplates[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CmsTemplatesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CmsTemplatesPeer::TEMPLATE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CmsTemplatesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CmsTemplatesPeer::TEMPLATE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the template column
     *
     * Example usage:
     * <code>
     * $query->filterByTemplate('fooValue');   // WHERE template = 'fooValue'
     * $query->filterByTemplate('%fooValue%'); // WHERE template LIKE '%fooValue%'
     * </code>
     *
     * @param     string $template The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsTemplatesQuery The current query, for fluid interface
     */
    public function filterByTemplate($template = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($template)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $template)) {
                $template = str_replace('*', '%', $template);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsTemplatesPeer::TEMPLATE, $template, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsTemplatesQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsTemplatesPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsTemplatesQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsTemplatesPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%'); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CmsTemplatesQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $type)) {
                $type = str_replace('*', '%', $type);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CmsTemplatesPeer::TYPE, $type, $comparison);
    }

    /**
     * Filter the query by a related CmsPages object
     *
     * @param   CmsPages|PropelObjectCollection $cmsPages  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CmsTemplatesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCmsPages($cmsPages, $comparison = null)
    {
        if ($cmsPages instanceof CmsPages) {
            return $this
                ->addUsingAlias(CmsTemplatesPeer::TEMPLATE, $cmsPages->getTemplate(), $comparison);
        } elseif ($cmsPages instanceof PropelObjectCollection) {
            return $this
                ->useCmsPagesQuery()
                ->filterByPrimaryKeys($cmsPages->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCmsPages() only accepts arguments of type CmsPages or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CmsPages relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CmsTemplatesQuery The current query, for fluid interface
     */
    public function joinCmsPages($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CmsPages');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CmsPages');
        }

        return $this;
    }

    /**
     * Use the CmsPages relation CmsPages object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CmsPagesQuery A secondary query class using the current class as primary query
     */
    public function useCmsPagesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCmsPages($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CmsPages', 'CmsPagesQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CmsTemplates $cmsTemplates Object to remove from the list of results
     *
     * @return CmsTemplatesQuery The current query, for fluid interface
     */
    public function prune($cmsTemplates = null)
    {
        if ($cmsTemplates) {
            $this->addUsingAlias(CmsTemplatesPeer::TEMPLATE, $cmsTemplates->getTemplate(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

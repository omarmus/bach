<?php


/**
 * Base class that represents a row from the 'cms_articles' table.
 *
 *
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseCmsArticles extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'CmsArticlesPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CmsArticlesPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_article field.
     * @var        int
     */
    protected $id_article;

    /**
     * The value for the title field.
     * @var        string
     */
    protected $title;

    /**
     * The value for the slug field.
     * @var        string
     */
    protected $slug;

    /**
     * The value for the pubdate field.
     * @var        string
     */
    protected $pubdate;

    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;

    /**
     * The value for the body field.
     * @var        string
     */
    protected $body;

    /**
     * The value for the created field.
     * @var        string
     */
    protected $created;

    /**
     * The value for the modified field.
     * @var        string
     */
    protected $modified;

    /**
     * The value for the user_modified field.
     * @var        int
     */
    protected $user_modified;

    /**
     * The value for the state field.
     * Note: this column has a database default value of: 'ACTIVE'
     * @var        string
     */
    protected $state;

    /**
     * The value for the id_page field.
     * @var        int
     */
    protected $id_page;

    /**
     * The value for the id_user field.
     * @var        int
     */
    protected $id_user;

    /**
     * The value for the number_visits field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $number_visits;

    /**
     * The value for the year field.
     * @var        int
     */
    protected $year;

    /**
     * The value for the others field.
     * @var        string
     */
    protected $others;

    /**
     * The value for the id_enterprise field.
     * @var        int
     */
    protected $id_enterprise;

    /**
     * @var        SysEnterprises
     */
    protected $aSysEnterprises;

    /**
     * @var        CmsPages
     */
    protected $aCmsPages;

    /**
     * @var        SysUsers
     */
    protected $aSysUsersRelatedByIdUser;

    /**
     * @var        SysUsers
     */
    protected $aSysUsersRelatedByUserModified;

    /**
     * @var        PropelObjectCollection|CmsFilesXArticle[] Collection to store aggregation of CmsFilesXArticle objects.
     */
    protected $collCmsFilesXArticles;
    protected $collCmsFilesXArticlesPartial;

    /**
     * @var        PropelObjectCollection|CmsTagsXArticle[] Collection to store aggregation of CmsTagsXArticle objects.
     */
    protected $collCmsTagsXArticles;
    protected $collCmsTagsXArticlesPartial;

    /**
     * @var        PropelObjectCollection|CmsVideosXArticle[] Collection to store aggregation of CmsVideosXArticle objects.
     */
    protected $collCmsVideosXArticles;
    protected $collCmsVideosXArticlesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $cmsFilesXArticlesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $cmsTagsXArticlesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $cmsVideosXArticlesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->state = 'ACTIVE';
        $this->number_visits = 0;
    }

    /**
     * Initializes internal state of BaseCmsArticles object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id_article] column value.
     *
     * @return int
     */
    public function getIdArticle()
    {

        return $this->id_article;
    }

    /**
     * Get the [title] column value.
     *
     * @return string
     */
    public function getTitle()
    {

        return $this->title;
    }

    /**
     * Get the [slug] column value.
     *
     * @return string
     */
    public function getSlug()
    {

        return $this->slug;
    }

    /**
     * Get the [optionally formatted] temporal [pubdate] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getPubdate($format = '%x')
    {
        if ($this->pubdate === null) {
            return null;
        }

        if ($this->pubdate === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->pubdate);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->pubdate, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {

        return $this->description;
    }

    /**
     * Get the [body] column value.
     *
     * @return string
     */
    public function getBody()
    {

        return $this->body;
    }

    /**
     * Get the [optionally formatted] temporal [created] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreated($format = 'Y-m-d H:i:s')
    {
        if ($this->created === null) {
            return null;
        }

        if ($this->created === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->created);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [modified] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getModified($format = 'Y-m-d H:i:s')
    {
        if ($this->modified === null) {
            return null;
        }

        if ($this->modified === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->modified);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->modified, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [user_modified] column value.
     *
     * @return int
     */
    public function getUserModified()
    {

        return $this->user_modified;
    }

    /**
     * Get the [state] column value.
     *
     * @return string
     */
    public function getState()
    {

        return $this->state;
    }

    /**
     * Get the [id_page] column value.
     *
     * @return int
     */
    public function getIdPage()
    {

        return $this->id_page;
    }

    /**
     * Get the [id_user] column value.
     * Usuario que creo el artículo
     * @return int
     */
    public function getIdUser()
    {

        return $this->id_user;
    }

    /**
     * Get the [number_visits] column value.
     *
     * @return int
     */
    public function getNumberVisits()
    {

        return $this->number_visits;
    }

    /**
     * Get the [year] column value.
     *
     * @return int
     */
    public function getYear()
    {

        return $this->year;
    }

    /**
     * Get the [others] column value.
     * Campo donde se guardará información extra del artículo
     * @return string
     */
    public function getOthers()
    {

        return $this->others;
    }

    /**
     * Get the [id_enterprise] column value.
     *
     * @return int
     */
    public function getIdEnterprise()
    {

        return $this->id_enterprise;
    }

    /**
     * Set the value of [id_article] column.
     *
     * @param  int $v new value
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setIdArticle($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_article !== $v) {
            $this->id_article = $v;
            $this->modifiedColumns[] = CmsArticlesPeer::ID_ARTICLE;
        }


        return $this;
    } // setIdArticle()

    /**
     * Set the value of [title] column.
     *
     * @param  string $v new value
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[] = CmsArticlesPeer::TITLE;
        }


        return $this;
    } // setTitle()

    /**
     * Set the value of [slug] column.
     *
     * @param  string $v new value
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setSlug($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->slug !== $v) {
            $this->slug = $v;
            $this->modifiedColumns[] = CmsArticlesPeer::SLUG;
        }


        return $this;
    } // setSlug()

    /**
     * Sets the value of [pubdate] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setPubdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->pubdate !== null || $dt !== null) {
            $currentDateAsString = ($this->pubdate !== null && $tmpDt = new DateTime($this->pubdate)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->pubdate = $newDateAsString;
                $this->modifiedColumns[] = CmsArticlesPeer::PUBDATE;
            }
        } // if either are not null


        return $this;
    } // setPubdate()

    /**
     * Set the value of [description] column.
     *
     * @param  string $v new value
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = CmsArticlesPeer::DESCRIPTION;
        }


        return $this;
    } // setDescription()

    /**
     * Set the value of [body] column.
     *
     * @param  string $v new value
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setBody($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->body !== $v) {
            $this->body = $v;
            $this->modifiedColumns[] = CmsArticlesPeer::BODY;
        }


        return $this;
    } // setBody()

    /**
     * Sets the value of [created] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setCreated($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created !== null || $dt !== null) {
            $currentDateAsString = ($this->created !== null && $tmpDt = new DateTime($this->created)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->created = $newDateAsString;
                $this->modifiedColumns[] = CmsArticlesPeer::CREATED;
            }
        } // if either are not null


        return $this;
    } // setCreated()

    /**
     * Sets the value of [modified] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setModified($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->modified !== null || $dt !== null) {
            $currentDateAsString = ($this->modified !== null && $tmpDt = new DateTime($this->modified)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->modified = $newDateAsString;
                $this->modifiedColumns[] = CmsArticlesPeer::MODIFIED;
            }
        } // if either are not null


        return $this;
    } // setModified()

    /**
     * Set the value of [user_modified] column.
     *
     * @param  int $v new value
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setUserModified($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->user_modified !== $v) {
            $this->user_modified = $v;
            $this->modifiedColumns[] = CmsArticlesPeer::USER_MODIFIED;
        }

        if ($this->aSysUsersRelatedByUserModified !== null && $this->aSysUsersRelatedByUserModified->getIdUser() !== $v) {
            $this->aSysUsersRelatedByUserModified = null;
        }


        return $this;
    } // setUserModified()

    /**
     * Set the value of [state] column.
     *
     * @param  string $v new value
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setState($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->state !== $v) {
            $this->state = $v;
            $this->modifiedColumns[] = CmsArticlesPeer::STATE;
        }


        return $this;
    } // setState()

    /**
     * Set the value of [id_page] column.
     *
     * @param  int $v new value
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setIdPage($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_page !== $v) {
            $this->id_page = $v;
            $this->modifiedColumns[] = CmsArticlesPeer::ID_PAGE;
        }

        if ($this->aCmsPages !== null && $this->aCmsPages->getIdPage() !== $v) {
            $this->aCmsPages = null;
        }


        return $this;
    } // setIdPage()

    /**
     * Set the value of [id_user] column.
     * Usuario que creo el artículo
     * @param  int $v new value
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setIdUser($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_user !== $v) {
            $this->id_user = $v;
            $this->modifiedColumns[] = CmsArticlesPeer::ID_USER;
        }

        if ($this->aSysUsersRelatedByIdUser !== null && $this->aSysUsersRelatedByIdUser->getIdUser() !== $v) {
            $this->aSysUsersRelatedByIdUser = null;
        }


        return $this;
    } // setIdUser()

    /**
     * Set the value of [number_visits] column.
     *
     * @param  int $v new value
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setNumberVisits($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->number_visits !== $v) {
            $this->number_visits = $v;
            $this->modifiedColumns[] = CmsArticlesPeer::NUMBER_VISITS;
        }


        return $this;
    } // setNumberVisits()

    /**
     * Set the value of [year] column.
     *
     * @param  int $v new value
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setYear($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->year !== $v) {
            $this->year = $v;
            $this->modifiedColumns[] = CmsArticlesPeer::YEAR;
        }


        return $this;
    } // setYear()

    /**
     * Set the value of [others] column.
     * Campo donde se guardará información extra del artículo
     * @param  string $v new value
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setOthers($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->others !== $v) {
            $this->others = $v;
            $this->modifiedColumns[] = CmsArticlesPeer::OTHERS;
        }


        return $this;
    } // setOthers()

    /**
     * Set the value of [id_enterprise] column.
     *
     * @param  int $v new value
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setIdEnterprise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_enterprise !== $v) {
            $this->id_enterprise = $v;
            $this->modifiedColumns[] = CmsArticlesPeer::ID_ENTERPRISE;
        }

        if ($this->aSysEnterprises !== null && $this->aSysEnterprises->getIdEnterprise() !== $v) {
            $this->aSysEnterprises = null;
        }


        return $this;
    } // setIdEnterprise()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->state !== 'ACTIVE') {
                return false;
            }

            if ($this->number_visits !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_article = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->title = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->slug = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->pubdate = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->description = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->body = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->created = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->modified = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->user_modified = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
            $this->state = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->id_page = ($row[$startcol + 10] !== null) ? (int) $row[$startcol + 10] : null;
            $this->id_user = ($row[$startcol + 11] !== null) ? (int) $row[$startcol + 11] : null;
            $this->number_visits = ($row[$startcol + 12] !== null) ? (int) $row[$startcol + 12] : null;
            $this->year = ($row[$startcol + 13] !== null) ? (int) $row[$startcol + 13] : null;
            $this->others = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->id_enterprise = ($row[$startcol + 15] !== null) ? (int) $row[$startcol + 15] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 16; // 16 = CmsArticlesPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CmsArticles object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSysUsersRelatedByUserModified !== null && $this->user_modified !== $this->aSysUsersRelatedByUserModified->getIdUser()) {
            $this->aSysUsersRelatedByUserModified = null;
        }
        if ($this->aCmsPages !== null && $this->id_page !== $this->aCmsPages->getIdPage()) {
            $this->aCmsPages = null;
        }
        if ($this->aSysUsersRelatedByIdUser !== null && $this->id_user !== $this->aSysUsersRelatedByIdUser->getIdUser()) {
            $this->aSysUsersRelatedByIdUser = null;
        }
        if ($this->aSysEnterprises !== null && $this->id_enterprise !== $this->aSysEnterprises->getIdEnterprise()) {
            $this->aSysEnterprises = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CmsArticlesPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSysEnterprises = null;
            $this->aCmsPages = null;
            $this->aSysUsersRelatedByIdUser = null;
            $this->aSysUsersRelatedByUserModified = null;
            $this->collCmsFilesXArticles = null;

            $this->collCmsTagsXArticles = null;

            $this->collCmsVideosXArticles = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CmsArticlesQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CmsArticlesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CmsArticlesPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSysEnterprises !== null) {
                if ($this->aSysEnterprises->isModified() || $this->aSysEnterprises->isNew()) {
                    $affectedRows += $this->aSysEnterprises->save($con);
                }
                $this->setSysEnterprises($this->aSysEnterprises);
            }

            if ($this->aCmsPages !== null) {
                if ($this->aCmsPages->isModified() || $this->aCmsPages->isNew()) {
                    $affectedRows += $this->aCmsPages->save($con);
                }
                $this->setCmsPages($this->aCmsPages);
            }

            if ($this->aSysUsersRelatedByIdUser !== null) {
                if ($this->aSysUsersRelatedByIdUser->isModified() || $this->aSysUsersRelatedByIdUser->isNew()) {
                    $affectedRows += $this->aSysUsersRelatedByIdUser->save($con);
                }
                $this->setSysUsersRelatedByIdUser($this->aSysUsersRelatedByIdUser);
            }

            if ($this->aSysUsersRelatedByUserModified !== null) {
                if ($this->aSysUsersRelatedByUserModified->isModified() || $this->aSysUsersRelatedByUserModified->isNew()) {
                    $affectedRows += $this->aSysUsersRelatedByUserModified->save($con);
                }
                $this->setSysUsersRelatedByUserModified($this->aSysUsersRelatedByUserModified);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->cmsFilesXArticlesScheduledForDeletion !== null) {
                if (!$this->cmsFilesXArticlesScheduledForDeletion->isEmpty()) {
                    CmsFilesXArticleQuery::create()
                        ->filterByPrimaryKeys($this->cmsFilesXArticlesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->cmsFilesXArticlesScheduledForDeletion = null;
                }
            }

            if ($this->collCmsFilesXArticles !== null) {
                foreach ($this->collCmsFilesXArticles as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->cmsTagsXArticlesScheduledForDeletion !== null) {
                if (!$this->cmsTagsXArticlesScheduledForDeletion->isEmpty()) {
                    CmsTagsXArticleQuery::create()
                        ->filterByPrimaryKeys($this->cmsTagsXArticlesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->cmsTagsXArticlesScheduledForDeletion = null;
                }
            }

            if ($this->collCmsTagsXArticles !== null) {
                foreach ($this->collCmsTagsXArticles as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->cmsVideosXArticlesScheduledForDeletion !== null) {
                if (!$this->cmsVideosXArticlesScheduledForDeletion->isEmpty()) {
                    CmsVideosXArticleQuery::create()
                        ->filterByPrimaryKeys($this->cmsVideosXArticlesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->cmsVideosXArticlesScheduledForDeletion = null;
                }
            }

            if ($this->collCmsVideosXArticles !== null) {
                foreach ($this->collCmsVideosXArticles as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CmsArticlesPeer::ID_ARTICLE;
        if (null !== $this->id_article) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CmsArticlesPeer::ID_ARTICLE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CmsArticlesPeer::ID_ARTICLE)) {
            $modifiedColumns[':p' . $index++]  = '`id_article`';
        }
        if ($this->isColumnModified(CmsArticlesPeer::TITLE)) {
            $modifiedColumns[':p' . $index++]  = '`title`';
        }
        if ($this->isColumnModified(CmsArticlesPeer::SLUG)) {
            $modifiedColumns[':p' . $index++]  = '`slug`';
        }
        if ($this->isColumnModified(CmsArticlesPeer::PUBDATE)) {
            $modifiedColumns[':p' . $index++]  = '`pubdate`';
        }
        if ($this->isColumnModified(CmsArticlesPeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = '`description`';
        }
        if ($this->isColumnModified(CmsArticlesPeer::BODY)) {
            $modifiedColumns[':p' . $index++]  = '`body`';
        }
        if ($this->isColumnModified(CmsArticlesPeer::CREATED)) {
            $modifiedColumns[':p' . $index++]  = '`created`';
        }
        if ($this->isColumnModified(CmsArticlesPeer::MODIFIED)) {
            $modifiedColumns[':p' . $index++]  = '`modified`';
        }
        if ($this->isColumnModified(CmsArticlesPeer::USER_MODIFIED)) {
            $modifiedColumns[':p' . $index++]  = '`user_modified`';
        }
        if ($this->isColumnModified(CmsArticlesPeer::STATE)) {
            $modifiedColumns[':p' . $index++]  = '`state`';
        }
        if ($this->isColumnModified(CmsArticlesPeer::ID_PAGE)) {
            $modifiedColumns[':p' . $index++]  = '`id_page`';
        }
        if ($this->isColumnModified(CmsArticlesPeer::ID_USER)) {
            $modifiedColumns[':p' . $index++]  = '`id_user`';
        }
        if ($this->isColumnModified(CmsArticlesPeer::NUMBER_VISITS)) {
            $modifiedColumns[':p' . $index++]  = '`number_visits`';
        }
        if ($this->isColumnModified(CmsArticlesPeer::YEAR)) {
            $modifiedColumns[':p' . $index++]  = '`year`';
        }
        if ($this->isColumnModified(CmsArticlesPeer::OTHERS)) {
            $modifiedColumns[':p' . $index++]  = '`others`';
        }
        if ($this->isColumnModified(CmsArticlesPeer::ID_ENTERPRISE)) {
            $modifiedColumns[':p' . $index++]  = '`id_enterprise`';
        }

        $sql = sprintf(
            'INSERT INTO `cms_articles` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_article`':
                        $stmt->bindValue($identifier, $this->id_article, PDO::PARAM_INT);
                        break;
                    case '`title`':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case '`slug`':
                        $stmt->bindValue($identifier, $this->slug, PDO::PARAM_STR);
                        break;
                    case '`pubdate`':
                        $stmt->bindValue($identifier, $this->pubdate, PDO::PARAM_STR);
                        break;
                    case '`description`':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case '`body`':
                        $stmt->bindValue($identifier, $this->body, PDO::PARAM_STR);
                        break;
                    case '`created`':
                        $stmt->bindValue($identifier, $this->created, PDO::PARAM_STR);
                        break;
                    case '`modified`':
                        $stmt->bindValue($identifier, $this->modified, PDO::PARAM_STR);
                        break;
                    case '`user_modified`':
                        $stmt->bindValue($identifier, $this->user_modified, PDO::PARAM_INT);
                        break;
                    case '`state`':
                        $stmt->bindValue($identifier, $this->state, PDO::PARAM_STR);
                        break;
                    case '`id_page`':
                        $stmt->bindValue($identifier, $this->id_page, PDO::PARAM_INT);
                        break;
                    case '`id_user`':
                        $stmt->bindValue($identifier, $this->id_user, PDO::PARAM_INT);
                        break;
                    case '`number_visits`':
                        $stmt->bindValue($identifier, $this->number_visits, PDO::PARAM_INT);
                        break;
                    case '`year`':
                        $stmt->bindValue($identifier, $this->year, PDO::PARAM_INT);
                        break;
                    case '`others`':
                        $stmt->bindValue($identifier, $this->others, PDO::PARAM_STR);
                        break;
                    case '`id_enterprise`':
                        $stmt->bindValue($identifier, $this->id_enterprise, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setIdArticle($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSysEnterprises !== null) {
                if (!$this->aSysEnterprises->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSysEnterprises->getValidationFailures());
                }
            }

            if ($this->aCmsPages !== null) {
                if (!$this->aCmsPages->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCmsPages->getValidationFailures());
                }
            }

            if ($this->aSysUsersRelatedByIdUser !== null) {
                if (!$this->aSysUsersRelatedByIdUser->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSysUsersRelatedByIdUser->getValidationFailures());
                }
            }

            if ($this->aSysUsersRelatedByUserModified !== null) {
                if (!$this->aSysUsersRelatedByUserModified->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSysUsersRelatedByUserModified->getValidationFailures());
                }
            }


            if (($retval = CmsArticlesPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCmsFilesXArticles !== null) {
                    foreach ($this->collCmsFilesXArticles as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCmsTagsXArticles !== null) {
                    foreach ($this->collCmsTagsXArticles as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCmsVideosXArticles !== null) {
                    foreach ($this->collCmsVideosXArticles as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CmsArticlesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdArticle();
                break;
            case 1:
                return $this->getTitle();
                break;
            case 2:
                return $this->getSlug();
                break;
            case 3:
                return $this->getPubdate();
                break;
            case 4:
                return $this->getDescription();
                break;
            case 5:
                return $this->getBody();
                break;
            case 6:
                return $this->getCreated();
                break;
            case 7:
                return $this->getModified();
                break;
            case 8:
                return $this->getUserModified();
                break;
            case 9:
                return $this->getState();
                break;
            case 10:
                return $this->getIdPage();
                break;
            case 11:
                return $this->getIdUser();
                break;
            case 12:
                return $this->getNumberVisits();
                break;
            case 13:
                return $this->getYear();
                break;
            case 14:
                return $this->getOthers();
                break;
            case 15:
                return $this->getIdEnterprise();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CmsArticles'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CmsArticles'][$this->getPrimaryKey()] = true;
        $keys = CmsArticlesPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdArticle(),
            $keys[1] => $this->getTitle(),
            $keys[2] => $this->getSlug(),
            $keys[3] => $this->getPubdate(),
            $keys[4] => $this->getDescription(),
            $keys[5] => $this->getBody(),
            $keys[6] => $this->getCreated(),
            $keys[7] => $this->getModified(),
            $keys[8] => $this->getUserModified(),
            $keys[9] => $this->getState(),
            $keys[10] => $this->getIdPage(),
            $keys[11] => $this->getIdUser(),
            $keys[12] => $this->getNumberVisits(),
            $keys[13] => $this->getYear(),
            $keys[14] => $this->getOthers(),
            $keys[15] => $this->getIdEnterprise(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aSysEnterprises) {
                $result['SysEnterprises'] = $this->aSysEnterprises->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCmsPages) {
                $result['CmsPages'] = $this->aCmsPages->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSysUsersRelatedByIdUser) {
                $result['SysUsersRelatedByIdUser'] = $this->aSysUsersRelatedByIdUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSysUsersRelatedByUserModified) {
                $result['SysUsersRelatedByUserModified'] = $this->aSysUsersRelatedByUserModified->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCmsFilesXArticles) {
                $result['CmsFilesXArticles'] = $this->collCmsFilesXArticles->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCmsTagsXArticles) {
                $result['CmsTagsXArticles'] = $this->collCmsTagsXArticles->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCmsVideosXArticles) {
                $result['CmsVideosXArticles'] = $this->collCmsVideosXArticles->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CmsArticlesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdArticle($value);
                break;
            case 1:
                $this->setTitle($value);
                break;
            case 2:
                $this->setSlug($value);
                break;
            case 3:
                $this->setPubdate($value);
                break;
            case 4:
                $this->setDescription($value);
                break;
            case 5:
                $this->setBody($value);
                break;
            case 6:
                $this->setCreated($value);
                break;
            case 7:
                $this->setModified($value);
                break;
            case 8:
                $this->setUserModified($value);
                break;
            case 9:
                $this->setState($value);
                break;
            case 10:
                $this->setIdPage($value);
                break;
            case 11:
                $this->setIdUser($value);
                break;
            case 12:
                $this->setNumberVisits($value);
                break;
            case 13:
                $this->setYear($value);
                break;
            case 14:
                $this->setOthers($value);
                break;
            case 15:
                $this->setIdEnterprise($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CmsArticlesPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdArticle($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setTitle($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setSlug($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setPubdate($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setDescription($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setBody($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setCreated($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setModified($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setUserModified($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setState($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setIdPage($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setIdUser($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setNumberVisits($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setYear($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setOthers($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setIdEnterprise($arr[$keys[15]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CmsArticlesPeer::DATABASE_NAME);

        if ($this->isColumnModified(CmsArticlesPeer::ID_ARTICLE)) $criteria->add(CmsArticlesPeer::ID_ARTICLE, $this->id_article);
        if ($this->isColumnModified(CmsArticlesPeer::TITLE)) $criteria->add(CmsArticlesPeer::TITLE, $this->title);
        if ($this->isColumnModified(CmsArticlesPeer::SLUG)) $criteria->add(CmsArticlesPeer::SLUG, $this->slug);
        if ($this->isColumnModified(CmsArticlesPeer::PUBDATE)) $criteria->add(CmsArticlesPeer::PUBDATE, $this->pubdate);
        if ($this->isColumnModified(CmsArticlesPeer::DESCRIPTION)) $criteria->add(CmsArticlesPeer::DESCRIPTION, $this->description);
        if ($this->isColumnModified(CmsArticlesPeer::BODY)) $criteria->add(CmsArticlesPeer::BODY, $this->body);
        if ($this->isColumnModified(CmsArticlesPeer::CREATED)) $criteria->add(CmsArticlesPeer::CREATED, $this->created);
        if ($this->isColumnModified(CmsArticlesPeer::MODIFIED)) $criteria->add(CmsArticlesPeer::MODIFIED, $this->modified);
        if ($this->isColumnModified(CmsArticlesPeer::USER_MODIFIED)) $criteria->add(CmsArticlesPeer::USER_MODIFIED, $this->user_modified);
        if ($this->isColumnModified(CmsArticlesPeer::STATE)) $criteria->add(CmsArticlesPeer::STATE, $this->state);
        if ($this->isColumnModified(CmsArticlesPeer::ID_PAGE)) $criteria->add(CmsArticlesPeer::ID_PAGE, $this->id_page);
        if ($this->isColumnModified(CmsArticlesPeer::ID_USER)) $criteria->add(CmsArticlesPeer::ID_USER, $this->id_user);
        if ($this->isColumnModified(CmsArticlesPeer::NUMBER_VISITS)) $criteria->add(CmsArticlesPeer::NUMBER_VISITS, $this->number_visits);
        if ($this->isColumnModified(CmsArticlesPeer::YEAR)) $criteria->add(CmsArticlesPeer::YEAR, $this->year);
        if ($this->isColumnModified(CmsArticlesPeer::OTHERS)) $criteria->add(CmsArticlesPeer::OTHERS, $this->others);
        if ($this->isColumnModified(CmsArticlesPeer::ID_ENTERPRISE)) $criteria->add(CmsArticlesPeer::ID_ENTERPRISE, $this->id_enterprise);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CmsArticlesPeer::DATABASE_NAME);
        $criteria->add(CmsArticlesPeer::ID_ARTICLE, $this->id_article);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdArticle();
    }

    /**
     * Generic method to set the primary key (id_article column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdArticle($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdArticle();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CmsArticles (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTitle($this->getTitle());
        $copyObj->setSlug($this->getSlug());
        $copyObj->setPubdate($this->getPubdate());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setBody($this->getBody());
        $copyObj->setCreated($this->getCreated());
        $copyObj->setModified($this->getModified());
        $copyObj->setUserModified($this->getUserModified());
        $copyObj->setState($this->getState());
        $copyObj->setIdPage($this->getIdPage());
        $copyObj->setIdUser($this->getIdUser());
        $copyObj->setNumberVisits($this->getNumberVisits());
        $copyObj->setYear($this->getYear());
        $copyObj->setOthers($this->getOthers());
        $copyObj->setIdEnterprise($this->getIdEnterprise());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCmsFilesXArticles() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCmsFilesXArticle($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCmsTagsXArticles() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCmsTagsXArticle($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCmsVideosXArticles() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCmsVideosXArticle($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdArticle(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CmsArticles Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CmsArticlesPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CmsArticlesPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a SysEnterprises object.
     *
     * @param                  SysEnterprises $v
     * @return CmsArticles The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSysEnterprises(SysEnterprises $v = null)
    {
        if ($v === null) {
            $this->setIdEnterprise(NULL);
        } else {
            $this->setIdEnterprise($v->getIdEnterprise());
        }

        $this->aSysEnterprises = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SysEnterprises object, it will not be re-added.
        if ($v !== null) {
            $v->addCmsArticles($this);
        }


        return $this;
    }


    /**
     * Get the associated SysEnterprises object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SysEnterprises The associated SysEnterprises object.
     * @throws PropelException
     */
    public function getSysEnterprises(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSysEnterprises === null && ($this->id_enterprise !== null) && $doQuery) {
            $this->aSysEnterprises = SysEnterprisesQuery::create()->findPk($this->id_enterprise, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSysEnterprises->addCmsArticless($this);
             */
        }

        return $this->aSysEnterprises;
    }

    /**
     * Declares an association between this object and a CmsPages object.
     *
     * @param                  CmsPages $v
     * @return CmsArticles The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCmsPages(CmsPages $v = null)
    {
        if ($v === null) {
            $this->setIdPage(NULL);
        } else {
            $this->setIdPage($v->getIdPage());
        }

        $this->aCmsPages = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CmsPages object, it will not be re-added.
        if ($v !== null) {
            $v->addCmsArticles($this);
        }


        return $this;
    }


    /**
     * Get the associated CmsPages object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CmsPages The associated CmsPages object.
     * @throws PropelException
     */
    public function getCmsPages(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCmsPages === null && ($this->id_page !== null) && $doQuery) {
            $this->aCmsPages = CmsPagesQuery::create()->findPk($this->id_page, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCmsPages->addCmsArticless($this);
             */
        }

        return $this->aCmsPages;
    }

    /**
     * Declares an association between this object and a SysUsers object.
     *
     * @param                  SysUsers $v
     * @return CmsArticles The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSysUsersRelatedByIdUser(SysUsers $v = null)
    {
        if ($v === null) {
            $this->setIdUser(NULL);
        } else {
            $this->setIdUser($v->getIdUser());
        }

        $this->aSysUsersRelatedByIdUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SysUsers object, it will not be re-added.
        if ($v !== null) {
            $v->addCmsArticlesRelatedByIdUser($this);
        }


        return $this;
    }


    /**
     * Get the associated SysUsers object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SysUsers The associated SysUsers object.
     * @throws PropelException
     */
    public function getSysUsersRelatedByIdUser(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSysUsersRelatedByIdUser === null && ($this->id_user !== null) && $doQuery) {
            $this->aSysUsersRelatedByIdUser = SysUsersQuery::create()->findPk($this->id_user, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSysUsersRelatedByIdUser->addCmsArticlessRelatedByIdUser($this);
             */
        }

        return $this->aSysUsersRelatedByIdUser;
    }

    /**
     * Declares an association between this object and a SysUsers object.
     *
     * @param                  SysUsers $v
     * @return CmsArticles The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSysUsersRelatedByUserModified(SysUsers $v = null)
    {
        if ($v === null) {
            $this->setUserModified(NULL);
        } else {
            $this->setUserModified($v->getIdUser());
        }

        $this->aSysUsersRelatedByUserModified = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SysUsers object, it will not be re-added.
        if ($v !== null) {
            $v->addCmsArticlesRelatedByUserModified($this);
        }


        return $this;
    }


    /**
     * Get the associated SysUsers object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SysUsers The associated SysUsers object.
     * @throws PropelException
     */
    public function getSysUsersRelatedByUserModified(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSysUsersRelatedByUserModified === null && ($this->user_modified !== null) && $doQuery) {
            $this->aSysUsersRelatedByUserModified = SysUsersQuery::create()->findPk($this->user_modified, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSysUsersRelatedByUserModified->addCmsArticlessRelatedByUserModified($this);
             */
        }

        return $this->aSysUsersRelatedByUserModified;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CmsFilesXArticle' == $relationName) {
            $this->initCmsFilesXArticles();
        }
        if ('CmsTagsXArticle' == $relationName) {
            $this->initCmsTagsXArticles();
        }
        if ('CmsVideosXArticle' == $relationName) {
            $this->initCmsVideosXArticles();
        }
    }

    /**
     * Clears out the collCmsFilesXArticles collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CmsArticles The current object (for fluent API support)
     * @see        addCmsFilesXArticles()
     */
    public function clearCmsFilesXArticles()
    {
        $this->collCmsFilesXArticles = null; // important to set this to null since that means it is uninitialized
        $this->collCmsFilesXArticlesPartial = null;

        return $this;
    }

    /**
     * reset is the collCmsFilesXArticles collection loaded partially
     *
     * @return void
     */
    public function resetPartialCmsFilesXArticles($v = true)
    {
        $this->collCmsFilesXArticlesPartial = $v;
    }

    /**
     * Initializes the collCmsFilesXArticles collection.
     *
     * By default this just sets the collCmsFilesXArticles collection to an empty array (like clearcollCmsFilesXArticles());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCmsFilesXArticles($overrideExisting = true)
    {
        if (null !== $this->collCmsFilesXArticles && !$overrideExisting) {
            return;
        }
        $this->collCmsFilesXArticles = new PropelObjectCollection();
        $this->collCmsFilesXArticles->setModel('CmsFilesXArticle');
    }

    /**
     * Gets an array of CmsFilesXArticle objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CmsArticles is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CmsFilesXArticle[] List of CmsFilesXArticle objects
     * @throws PropelException
     */
    public function getCmsFilesXArticles($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCmsFilesXArticlesPartial && !$this->isNew();
        if (null === $this->collCmsFilesXArticles || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCmsFilesXArticles) {
                // return empty collection
                $this->initCmsFilesXArticles();
            } else {
                $collCmsFilesXArticles = CmsFilesXArticleQuery::create(null, $criteria)
                    ->filterByCmsArticles($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCmsFilesXArticlesPartial && count($collCmsFilesXArticles)) {
                      $this->initCmsFilesXArticles(false);

                      foreach ($collCmsFilesXArticles as $obj) {
                        if (false == $this->collCmsFilesXArticles->contains($obj)) {
                          $this->collCmsFilesXArticles->append($obj);
                        }
                      }

                      $this->collCmsFilesXArticlesPartial = true;
                    }

                    $collCmsFilesXArticles->getInternalIterator()->rewind();

                    return $collCmsFilesXArticles;
                }

                if ($partial && $this->collCmsFilesXArticles) {
                    foreach ($this->collCmsFilesXArticles as $obj) {
                        if ($obj->isNew()) {
                            $collCmsFilesXArticles[] = $obj;
                        }
                    }
                }

                $this->collCmsFilesXArticles = $collCmsFilesXArticles;
                $this->collCmsFilesXArticlesPartial = false;
            }
        }

        return $this->collCmsFilesXArticles;
    }

    /**
     * Sets a collection of CmsFilesXArticle objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $cmsFilesXArticles A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setCmsFilesXArticles(PropelCollection $cmsFilesXArticles, PropelPDO $con = null)
    {
        $cmsFilesXArticlesToDelete = $this->getCmsFilesXArticles(new Criteria(), $con)->diff($cmsFilesXArticles);


        $this->cmsFilesXArticlesScheduledForDeletion = $cmsFilesXArticlesToDelete;

        foreach ($cmsFilesXArticlesToDelete as $cmsFilesXArticleRemoved) {
            $cmsFilesXArticleRemoved->setCmsArticles(null);
        }

        $this->collCmsFilesXArticles = null;
        foreach ($cmsFilesXArticles as $cmsFilesXArticle) {
            $this->addCmsFilesXArticle($cmsFilesXArticle);
        }

        $this->collCmsFilesXArticles = $cmsFilesXArticles;
        $this->collCmsFilesXArticlesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CmsFilesXArticle objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CmsFilesXArticle objects.
     * @throws PropelException
     */
    public function countCmsFilesXArticles(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCmsFilesXArticlesPartial && !$this->isNew();
        if (null === $this->collCmsFilesXArticles || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCmsFilesXArticles) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCmsFilesXArticles());
            }
            $query = CmsFilesXArticleQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCmsArticles($this)
                ->count($con);
        }

        return count($this->collCmsFilesXArticles);
    }

    /**
     * Method called to associate a CmsFilesXArticle object to this object
     * through the CmsFilesXArticle foreign key attribute.
     *
     * @param    CmsFilesXArticle $l CmsFilesXArticle
     * @return CmsArticles The current object (for fluent API support)
     */
    public function addCmsFilesXArticle(CmsFilesXArticle $l)
    {
        if ($this->collCmsFilesXArticles === null) {
            $this->initCmsFilesXArticles();
            $this->collCmsFilesXArticlesPartial = true;
        }

        if (!in_array($l, $this->collCmsFilesXArticles->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCmsFilesXArticle($l);

            if ($this->cmsFilesXArticlesScheduledForDeletion and $this->cmsFilesXArticlesScheduledForDeletion->contains($l)) {
                $this->cmsFilesXArticlesScheduledForDeletion->remove($this->cmsFilesXArticlesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	CmsFilesXArticle $cmsFilesXArticle The cmsFilesXArticle object to add.
     */
    protected function doAddCmsFilesXArticle($cmsFilesXArticle)
    {
        $this->collCmsFilesXArticles[]= $cmsFilesXArticle;
        $cmsFilesXArticle->setCmsArticles($this);
    }

    /**
     * @param	CmsFilesXArticle $cmsFilesXArticle The cmsFilesXArticle object to remove.
     * @return CmsArticles The current object (for fluent API support)
     */
    public function removeCmsFilesXArticle($cmsFilesXArticle)
    {
        if ($this->getCmsFilesXArticles()->contains($cmsFilesXArticle)) {
            $this->collCmsFilesXArticles->remove($this->collCmsFilesXArticles->search($cmsFilesXArticle));
            if (null === $this->cmsFilesXArticlesScheduledForDeletion) {
                $this->cmsFilesXArticlesScheduledForDeletion = clone $this->collCmsFilesXArticles;
                $this->cmsFilesXArticlesScheduledForDeletion->clear();
            }
            $this->cmsFilesXArticlesScheduledForDeletion[]= clone $cmsFilesXArticle;
            $cmsFilesXArticle->setCmsArticles(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CmsArticles is new, it will return
     * an empty collection; or if this CmsArticles has previously
     * been saved, it will retrieve related CmsFilesXArticles from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CmsArticles.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsFilesXArticle[] List of CmsFilesXArticle objects
     */
    public function getCmsFilesXArticlesJoinSysFiles($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsFilesXArticleQuery::create(null, $criteria);
        $query->joinWith('SysFiles', $join_behavior);

        return $this->getCmsFilesXArticles($query, $con);
    }

    /**
     * Clears out the collCmsTagsXArticles collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CmsArticles The current object (for fluent API support)
     * @see        addCmsTagsXArticles()
     */
    public function clearCmsTagsXArticles()
    {
        $this->collCmsTagsXArticles = null; // important to set this to null since that means it is uninitialized
        $this->collCmsTagsXArticlesPartial = null;

        return $this;
    }

    /**
     * reset is the collCmsTagsXArticles collection loaded partially
     *
     * @return void
     */
    public function resetPartialCmsTagsXArticles($v = true)
    {
        $this->collCmsTagsXArticlesPartial = $v;
    }

    /**
     * Initializes the collCmsTagsXArticles collection.
     *
     * By default this just sets the collCmsTagsXArticles collection to an empty array (like clearcollCmsTagsXArticles());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCmsTagsXArticles($overrideExisting = true)
    {
        if (null !== $this->collCmsTagsXArticles && !$overrideExisting) {
            return;
        }
        $this->collCmsTagsXArticles = new PropelObjectCollection();
        $this->collCmsTagsXArticles->setModel('CmsTagsXArticle');
    }

    /**
     * Gets an array of CmsTagsXArticle objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CmsArticles is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CmsTagsXArticle[] List of CmsTagsXArticle objects
     * @throws PropelException
     */
    public function getCmsTagsXArticles($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCmsTagsXArticlesPartial && !$this->isNew();
        if (null === $this->collCmsTagsXArticles || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCmsTagsXArticles) {
                // return empty collection
                $this->initCmsTagsXArticles();
            } else {
                $collCmsTagsXArticles = CmsTagsXArticleQuery::create(null, $criteria)
                    ->filterByCmsArticles($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCmsTagsXArticlesPartial && count($collCmsTagsXArticles)) {
                      $this->initCmsTagsXArticles(false);

                      foreach ($collCmsTagsXArticles as $obj) {
                        if (false == $this->collCmsTagsXArticles->contains($obj)) {
                          $this->collCmsTagsXArticles->append($obj);
                        }
                      }

                      $this->collCmsTagsXArticlesPartial = true;
                    }

                    $collCmsTagsXArticles->getInternalIterator()->rewind();

                    return $collCmsTagsXArticles;
                }

                if ($partial && $this->collCmsTagsXArticles) {
                    foreach ($this->collCmsTagsXArticles as $obj) {
                        if ($obj->isNew()) {
                            $collCmsTagsXArticles[] = $obj;
                        }
                    }
                }

                $this->collCmsTagsXArticles = $collCmsTagsXArticles;
                $this->collCmsTagsXArticlesPartial = false;
            }
        }

        return $this->collCmsTagsXArticles;
    }

    /**
     * Sets a collection of CmsTagsXArticle objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $cmsTagsXArticles A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setCmsTagsXArticles(PropelCollection $cmsTagsXArticles, PropelPDO $con = null)
    {
        $cmsTagsXArticlesToDelete = $this->getCmsTagsXArticles(new Criteria(), $con)->diff($cmsTagsXArticles);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->cmsTagsXArticlesScheduledForDeletion = clone $cmsTagsXArticlesToDelete;

        foreach ($cmsTagsXArticlesToDelete as $cmsTagsXArticleRemoved) {
            $cmsTagsXArticleRemoved->setCmsArticles(null);
        }

        $this->collCmsTagsXArticles = null;
        foreach ($cmsTagsXArticles as $cmsTagsXArticle) {
            $this->addCmsTagsXArticle($cmsTagsXArticle);
        }

        $this->collCmsTagsXArticles = $cmsTagsXArticles;
        $this->collCmsTagsXArticlesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CmsTagsXArticle objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CmsTagsXArticle objects.
     * @throws PropelException
     */
    public function countCmsTagsXArticles(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCmsTagsXArticlesPartial && !$this->isNew();
        if (null === $this->collCmsTagsXArticles || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCmsTagsXArticles) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCmsTagsXArticles());
            }
            $query = CmsTagsXArticleQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCmsArticles($this)
                ->count($con);
        }

        return count($this->collCmsTagsXArticles);
    }

    /**
     * Method called to associate a CmsTagsXArticle object to this object
     * through the CmsTagsXArticle foreign key attribute.
     *
     * @param    CmsTagsXArticle $l CmsTagsXArticle
     * @return CmsArticles The current object (for fluent API support)
     */
    public function addCmsTagsXArticle(CmsTagsXArticle $l)
    {
        if ($this->collCmsTagsXArticles === null) {
            $this->initCmsTagsXArticles();
            $this->collCmsTagsXArticlesPartial = true;
        }

        if (!in_array($l, $this->collCmsTagsXArticles->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCmsTagsXArticle($l);

            if ($this->cmsTagsXArticlesScheduledForDeletion and $this->cmsTagsXArticlesScheduledForDeletion->contains($l)) {
                $this->cmsTagsXArticlesScheduledForDeletion->remove($this->cmsTagsXArticlesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	CmsTagsXArticle $cmsTagsXArticle The cmsTagsXArticle object to add.
     */
    protected function doAddCmsTagsXArticle($cmsTagsXArticle)
    {
        $this->collCmsTagsXArticles[]= $cmsTagsXArticle;
        $cmsTagsXArticle->setCmsArticles($this);
    }

    /**
     * @param	CmsTagsXArticle $cmsTagsXArticle The cmsTagsXArticle object to remove.
     * @return CmsArticles The current object (for fluent API support)
     */
    public function removeCmsTagsXArticle($cmsTagsXArticle)
    {
        if ($this->getCmsTagsXArticles()->contains($cmsTagsXArticle)) {
            $this->collCmsTagsXArticles->remove($this->collCmsTagsXArticles->search($cmsTagsXArticle));
            if (null === $this->cmsTagsXArticlesScheduledForDeletion) {
                $this->cmsTagsXArticlesScheduledForDeletion = clone $this->collCmsTagsXArticles;
                $this->cmsTagsXArticlesScheduledForDeletion->clear();
            }
            $this->cmsTagsXArticlesScheduledForDeletion[]= clone $cmsTagsXArticle;
            $cmsTagsXArticle->setCmsArticles(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CmsArticles is new, it will return
     * an empty collection; or if this CmsArticles has previously
     * been saved, it will retrieve related CmsTagsXArticles from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CmsArticles.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsTagsXArticle[] List of CmsTagsXArticle objects
     */
    public function getCmsTagsXArticlesJoinCmsTags($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsTagsXArticleQuery::create(null, $criteria);
        $query->joinWith('CmsTags', $join_behavior);

        return $this->getCmsTagsXArticles($query, $con);
    }

    /**
     * Clears out the collCmsVideosXArticles collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CmsArticles The current object (for fluent API support)
     * @see        addCmsVideosXArticles()
     */
    public function clearCmsVideosXArticles()
    {
        $this->collCmsVideosXArticles = null; // important to set this to null since that means it is uninitialized
        $this->collCmsVideosXArticlesPartial = null;

        return $this;
    }

    /**
     * reset is the collCmsVideosXArticles collection loaded partially
     *
     * @return void
     */
    public function resetPartialCmsVideosXArticles($v = true)
    {
        $this->collCmsVideosXArticlesPartial = $v;
    }

    /**
     * Initializes the collCmsVideosXArticles collection.
     *
     * By default this just sets the collCmsVideosXArticles collection to an empty array (like clearcollCmsVideosXArticles());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCmsVideosXArticles($overrideExisting = true)
    {
        if (null !== $this->collCmsVideosXArticles && !$overrideExisting) {
            return;
        }
        $this->collCmsVideosXArticles = new PropelObjectCollection();
        $this->collCmsVideosXArticles->setModel('CmsVideosXArticle');
    }

    /**
     * Gets an array of CmsVideosXArticle objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CmsArticles is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CmsVideosXArticle[] List of CmsVideosXArticle objects
     * @throws PropelException
     */
    public function getCmsVideosXArticles($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCmsVideosXArticlesPartial && !$this->isNew();
        if (null === $this->collCmsVideosXArticles || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCmsVideosXArticles) {
                // return empty collection
                $this->initCmsVideosXArticles();
            } else {
                $collCmsVideosXArticles = CmsVideosXArticleQuery::create(null, $criteria)
                    ->filterByCmsArticles($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCmsVideosXArticlesPartial && count($collCmsVideosXArticles)) {
                      $this->initCmsVideosXArticles(false);

                      foreach ($collCmsVideosXArticles as $obj) {
                        if (false == $this->collCmsVideosXArticles->contains($obj)) {
                          $this->collCmsVideosXArticles->append($obj);
                        }
                      }

                      $this->collCmsVideosXArticlesPartial = true;
                    }

                    $collCmsVideosXArticles->getInternalIterator()->rewind();

                    return $collCmsVideosXArticles;
                }

                if ($partial && $this->collCmsVideosXArticles) {
                    foreach ($this->collCmsVideosXArticles as $obj) {
                        if ($obj->isNew()) {
                            $collCmsVideosXArticles[] = $obj;
                        }
                    }
                }

                $this->collCmsVideosXArticles = $collCmsVideosXArticles;
                $this->collCmsVideosXArticlesPartial = false;
            }
        }

        return $this->collCmsVideosXArticles;
    }

    /**
     * Sets a collection of CmsVideosXArticle objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $cmsVideosXArticles A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CmsArticles The current object (for fluent API support)
     */
    public function setCmsVideosXArticles(PropelCollection $cmsVideosXArticles, PropelPDO $con = null)
    {
        $cmsVideosXArticlesToDelete = $this->getCmsVideosXArticles(new Criteria(), $con)->diff($cmsVideosXArticles);


        $this->cmsVideosXArticlesScheduledForDeletion = $cmsVideosXArticlesToDelete;

        foreach ($cmsVideosXArticlesToDelete as $cmsVideosXArticleRemoved) {
            $cmsVideosXArticleRemoved->setCmsArticles(null);
        }

        $this->collCmsVideosXArticles = null;
        foreach ($cmsVideosXArticles as $cmsVideosXArticle) {
            $this->addCmsVideosXArticle($cmsVideosXArticle);
        }

        $this->collCmsVideosXArticles = $cmsVideosXArticles;
        $this->collCmsVideosXArticlesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CmsVideosXArticle objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CmsVideosXArticle objects.
     * @throws PropelException
     */
    public function countCmsVideosXArticles(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCmsVideosXArticlesPartial && !$this->isNew();
        if (null === $this->collCmsVideosXArticles || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCmsVideosXArticles) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCmsVideosXArticles());
            }
            $query = CmsVideosXArticleQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCmsArticles($this)
                ->count($con);
        }

        return count($this->collCmsVideosXArticles);
    }

    /**
     * Method called to associate a CmsVideosXArticle object to this object
     * through the CmsVideosXArticle foreign key attribute.
     *
     * @param    CmsVideosXArticle $l CmsVideosXArticle
     * @return CmsArticles The current object (for fluent API support)
     */
    public function addCmsVideosXArticle(CmsVideosXArticle $l)
    {
        if ($this->collCmsVideosXArticles === null) {
            $this->initCmsVideosXArticles();
            $this->collCmsVideosXArticlesPartial = true;
        }

        if (!in_array($l, $this->collCmsVideosXArticles->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCmsVideosXArticle($l);

            if ($this->cmsVideosXArticlesScheduledForDeletion and $this->cmsVideosXArticlesScheduledForDeletion->contains($l)) {
                $this->cmsVideosXArticlesScheduledForDeletion->remove($this->cmsVideosXArticlesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	CmsVideosXArticle $cmsVideosXArticle The cmsVideosXArticle object to add.
     */
    protected function doAddCmsVideosXArticle($cmsVideosXArticle)
    {
        $this->collCmsVideosXArticles[]= $cmsVideosXArticle;
        $cmsVideosXArticle->setCmsArticles($this);
    }

    /**
     * @param	CmsVideosXArticle $cmsVideosXArticle The cmsVideosXArticle object to remove.
     * @return CmsArticles The current object (for fluent API support)
     */
    public function removeCmsVideosXArticle($cmsVideosXArticle)
    {
        if ($this->getCmsVideosXArticles()->contains($cmsVideosXArticle)) {
            $this->collCmsVideosXArticles->remove($this->collCmsVideosXArticles->search($cmsVideosXArticle));
            if (null === $this->cmsVideosXArticlesScheduledForDeletion) {
                $this->cmsVideosXArticlesScheduledForDeletion = clone $this->collCmsVideosXArticles;
                $this->cmsVideosXArticlesScheduledForDeletion->clear();
            }
            $this->cmsVideosXArticlesScheduledForDeletion[]= clone $cmsVideosXArticle;
            $cmsVideosXArticle->setCmsArticles(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CmsArticles is new, it will return
     * an empty collection; or if this CmsArticles has previously
     * been saved, it will retrieve related CmsVideosXArticles from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CmsArticles.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsVideosXArticle[] List of CmsVideosXArticle objects
     */
    public function getCmsVideosXArticlesJoinSysUsers($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsVideosXArticleQuery::create(null, $criteria);
        $query->joinWith('SysUsers', $join_behavior);

        return $this->getCmsVideosXArticles($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_article = null;
        $this->title = null;
        $this->slug = null;
        $this->pubdate = null;
        $this->description = null;
        $this->body = null;
        $this->created = null;
        $this->modified = null;
        $this->user_modified = null;
        $this->state = null;
        $this->id_page = null;
        $this->id_user = null;
        $this->number_visits = null;
        $this->year = null;
        $this->others = null;
        $this->id_enterprise = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCmsFilesXArticles) {
                foreach ($this->collCmsFilesXArticles as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCmsTagsXArticles) {
                foreach ($this->collCmsTagsXArticles as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCmsVideosXArticles) {
                foreach ($this->collCmsVideosXArticles as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aSysEnterprises instanceof Persistent) {
              $this->aSysEnterprises->clearAllReferences($deep);
            }
            if ($this->aCmsPages instanceof Persistent) {
              $this->aCmsPages->clearAllReferences($deep);
            }
            if ($this->aSysUsersRelatedByIdUser instanceof Persistent) {
              $this->aSysUsersRelatedByIdUser->clearAllReferences($deep);
            }
            if ($this->aSysUsersRelatedByUserModified instanceof Persistent) {
              $this->aSysUsersRelatedByUserModified->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCmsFilesXArticles instanceof PropelCollection) {
            $this->collCmsFilesXArticles->clearIterator();
        }
        $this->collCmsFilesXArticles = null;
        if ($this->collCmsTagsXArticles instanceof PropelCollection) {
            $this->collCmsTagsXArticles->clearIterator();
        }
        $this->collCmsTagsXArticles = null;
        if ($this->collCmsVideosXArticles instanceof PropelCollection) {
            $this->collCmsVideosXArticles->clearIterator();
        }
        $this->collCmsVideosXArticles = null;
        $this->aSysEnterprises = null;
        $this->aCmsPages = null;
        $this->aSysUsersRelatedByIdUser = null;
        $this->aSysUsersRelatedByUserModified = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CmsArticlesPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}

<?php


/**
 * Base class that represents a row from the 'cms_pages' table.
 *
 *
 *
 * @package    propel.generator.bach.om
 */
abstract class BaseCmsPages extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'CmsPagesPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CmsPagesPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_page field.
     * @var        int
     */
    protected $id_page;

    /**
     * The value for the title field.
     * @var        string
     */
    protected $title;

    /**
     * The value for the slug field.
     * @var        string
     */
    protected $slug;

    /**
     * The value for the order field.
     * @var        int
     */
    protected $order;

    /**
     * The value for the id_parent field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $id_parent;

    /**
     * The value for the state field.
     * Note: this column has a database default value of: 'ACTIVE'
     * @var        string
     */
    protected $state;

    /**
     * The value for the template field.
     * @var        string
     */
    protected $template;

    /**
     * The value for the body field.
     * @var        string
     */
    protected $body;

    /**
     * The value for the visible field.
     * Note: this column has a database default value of: 'YES'
     * @var        string
     */
    protected $visible;

    /**
     * The value for the type field.
     * Note: this column has a database default value of: 'CMS'
     * @var        string
     */
    protected $type;

    /**
     * The value for the created field.
     * @var        string
     */
    protected $created;

    /**
     * The value for the modified field.
     * @var        string
     */
    protected $modified;

    /**
     * The value for the user_modified field.
     * @var        int
     */
    protected $user_modified;

    /**
     * The value for the id_enterprise field.
     * @var        int
     */
    protected $id_enterprise;

    /**
     * @var        SysEnterprises
     */
    protected $aSysEnterprises;

    /**
     * @var        CmsTemplates
     */
    protected $aCmsTemplates;

    /**
     * @var        SysUsers
     */
    protected $aSysUsers;

    /**
     * @var        PropelObjectCollection|CmsArticles[] Collection to store aggregation of CmsArticles objects.
     */
    protected $collCmsArticless;
    protected $collCmsArticlessPartial;

    /**
     * @var        PropelObjectCollection|CmsFilesXPage[] Collection to store aggregation of CmsFilesXPage objects.
     */
    protected $collCmsFilesXPages;
    protected $collCmsFilesXPagesPartial;

    /**
     * @var        PropelObjectCollection|CmsVideosXPage[] Collection to store aggregation of CmsVideosXPage objects.
     */
    protected $collCmsVideosXPages;
    protected $collCmsVideosXPagesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $cmsArticlessScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $cmsFilesXPagesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $cmsVideosXPagesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->id_parent = 0;
        $this->state = 'ACTIVE';
        $this->visible = 'YES';
        $this->type = 'CMS';
    }

    /**
     * Initializes internal state of BaseCmsPages object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id_page] column value.
     *
     * @return int
     */
    public function getIdPage()
    {

        return $this->id_page;
    }

    /**
     * Get the [title] column value.
     *
     * @return string
     */
    public function getTitle()
    {

        return $this->title;
    }

    /**
     * Get the [slug] column value.
     *
     * @return string
     */
    public function getSlug()
    {

        return $this->slug;
    }

    /**
     * Get the [order] column value.
     *
     * @return int
     */
    public function getOrder()
    {

        return $this->order;
    }

    /**
     * Get the [id_parent] column value.
     *
     * @return int
     */
    public function getIdParent()
    {

        return $this->id_parent;
    }

    /**
     * Get the [state] column value.
     *
     * @return string
     */
    public function getState()
    {

        return $this->state;
    }

    /**
     * Get the [template] column value.
     *
     * @return string
     */
    public function getTemplate()
    {

        return $this->template;
    }

    /**
     * Get the [body] column value.
     *
     * @return string
     */
    public function getBody()
    {

        return $this->body;
    }

    /**
     * Get the [visible] column value.
     *
     * @return string
     */
    public function getVisible()
    {

        return $this->visible;
    }

    /**
     * Get the [type] column value.
     *
     * @return string
     */
    public function getType()
    {

        return $this->type;
    }

    /**
     * Get the [optionally formatted] temporal [created] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreated($format = 'Y-m-d H:i:s')
    {
        if ($this->created === null) {
            return null;
        }

        if ($this->created === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->created);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [modified] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getModified($format = 'Y-m-d H:i:s')
    {
        if ($this->modified === null) {
            return null;
        }

        if ($this->modified === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->modified);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->modified, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [user_modified] column value.
     *
     * @return int
     */
    public function getUserModified()
    {

        return $this->user_modified;
    }

    /**
     * Get the [id_enterprise] column value.
     *
     * @return int
     */
    public function getIdEnterprise()
    {

        return $this->id_enterprise;
    }

    /**
     * Set the value of [id_page] column.
     *
     * @param  int $v new value
     * @return CmsPages The current object (for fluent API support)
     */
    public function setIdPage($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_page !== $v) {
            $this->id_page = $v;
            $this->modifiedColumns[] = CmsPagesPeer::ID_PAGE;
        }


        return $this;
    } // setIdPage()

    /**
     * Set the value of [title] column.
     *
     * @param  string $v new value
     * @return CmsPages The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[] = CmsPagesPeer::TITLE;
        }


        return $this;
    } // setTitle()

    /**
     * Set the value of [slug] column.
     *
     * @param  string $v new value
     * @return CmsPages The current object (for fluent API support)
     */
    public function setSlug($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->slug !== $v) {
            $this->slug = $v;
            $this->modifiedColumns[] = CmsPagesPeer::SLUG;
        }


        return $this;
    } // setSlug()

    /**
     * Set the value of [order] column.
     *
     * @param  int $v new value
     * @return CmsPages The current object (for fluent API support)
     */
    public function setOrder($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->order !== $v) {
            $this->order = $v;
            $this->modifiedColumns[] = CmsPagesPeer::ORDER;
        }


        return $this;
    } // setOrder()

    /**
     * Set the value of [id_parent] column.
     *
     * @param  int $v new value
     * @return CmsPages The current object (for fluent API support)
     */
    public function setIdParent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_parent !== $v) {
            $this->id_parent = $v;
            $this->modifiedColumns[] = CmsPagesPeer::ID_PARENT;
        }


        return $this;
    } // setIdParent()

    /**
     * Set the value of [state] column.
     *
     * @param  string $v new value
     * @return CmsPages The current object (for fluent API support)
     */
    public function setState($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->state !== $v) {
            $this->state = $v;
            $this->modifiedColumns[] = CmsPagesPeer::STATE;
        }


        return $this;
    } // setState()

    /**
     * Set the value of [template] column.
     *
     * @param  string $v new value
     * @return CmsPages The current object (for fluent API support)
     */
    public function setTemplate($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->template !== $v) {
            $this->template = $v;
            $this->modifiedColumns[] = CmsPagesPeer::TEMPLATE;
        }

        if ($this->aCmsTemplates !== null && $this->aCmsTemplates->getTemplate() !== $v) {
            $this->aCmsTemplates = null;
        }


        return $this;
    } // setTemplate()

    /**
     * Set the value of [body] column.
     *
     * @param  string $v new value
     * @return CmsPages The current object (for fluent API support)
     */
    public function setBody($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->body !== $v) {
            $this->body = $v;
            $this->modifiedColumns[] = CmsPagesPeer::BODY;
        }


        return $this;
    } // setBody()

    /**
     * Set the value of [visible] column.
     *
     * @param  string $v new value
     * @return CmsPages The current object (for fluent API support)
     */
    public function setVisible($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->visible !== $v) {
            $this->visible = $v;
            $this->modifiedColumns[] = CmsPagesPeer::VISIBLE;
        }


        return $this;
    } // setVisible()

    /**
     * Set the value of [type] column.
     *
     * @param  string $v new value
     * @return CmsPages The current object (for fluent API support)
     */
    public function setType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->type !== $v) {
            $this->type = $v;
            $this->modifiedColumns[] = CmsPagesPeer::TYPE;
        }


        return $this;
    } // setType()

    /**
     * Sets the value of [created] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CmsPages The current object (for fluent API support)
     */
    public function setCreated($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created !== null || $dt !== null) {
            $currentDateAsString = ($this->created !== null && $tmpDt = new DateTime($this->created)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->created = $newDateAsString;
                $this->modifiedColumns[] = CmsPagesPeer::CREATED;
            }
        } // if either are not null


        return $this;
    } // setCreated()

    /**
     * Sets the value of [modified] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CmsPages The current object (for fluent API support)
     */
    public function setModified($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->modified !== null || $dt !== null) {
            $currentDateAsString = ($this->modified !== null && $tmpDt = new DateTime($this->modified)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->modified = $newDateAsString;
                $this->modifiedColumns[] = CmsPagesPeer::MODIFIED;
            }
        } // if either are not null


        return $this;
    } // setModified()

    /**
     * Set the value of [user_modified] column.
     *
     * @param  int $v new value
     * @return CmsPages The current object (for fluent API support)
     */
    public function setUserModified($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->user_modified !== $v) {
            $this->user_modified = $v;
            $this->modifiedColumns[] = CmsPagesPeer::USER_MODIFIED;
        }

        if ($this->aSysUsers !== null && $this->aSysUsers->getIdUser() !== $v) {
            $this->aSysUsers = null;
        }


        return $this;
    } // setUserModified()

    /**
     * Set the value of [id_enterprise] column.
     *
     * @param  int $v new value
     * @return CmsPages The current object (for fluent API support)
     */
    public function setIdEnterprise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_enterprise !== $v) {
            $this->id_enterprise = $v;
            $this->modifiedColumns[] = CmsPagesPeer::ID_ENTERPRISE;
        }

        if ($this->aSysEnterprises !== null && $this->aSysEnterprises->getIdEnterprise() !== $v) {
            $this->aSysEnterprises = null;
        }


        return $this;
    } // setIdEnterprise()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->id_parent !== 0) {
                return false;
            }

            if ($this->state !== 'ACTIVE') {
                return false;
            }

            if ($this->visible !== 'YES') {
                return false;
            }

            if ($this->type !== 'CMS') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_page = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->title = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->slug = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->order = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->id_parent = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->state = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->template = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->body = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->visible = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->type = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->created = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->modified = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->user_modified = ($row[$startcol + 12] !== null) ? (int) $row[$startcol + 12] : null;
            $this->id_enterprise = ($row[$startcol + 13] !== null) ? (int) $row[$startcol + 13] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 14; // 14 = CmsPagesPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CmsPages object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCmsTemplates !== null && $this->template !== $this->aCmsTemplates->getTemplate()) {
            $this->aCmsTemplates = null;
        }
        if ($this->aSysUsers !== null && $this->user_modified !== $this->aSysUsers->getIdUser()) {
            $this->aSysUsers = null;
        }
        if ($this->aSysEnterprises !== null && $this->id_enterprise !== $this->aSysEnterprises->getIdEnterprise()) {
            $this->aSysEnterprises = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CmsPagesPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSysEnterprises = null;
            $this->aCmsTemplates = null;
            $this->aSysUsers = null;
            $this->collCmsArticless = null;

            $this->collCmsFilesXPages = null;

            $this->collCmsVideosXPages = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CmsPagesQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CmsPagesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CmsPagesPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSysEnterprises !== null) {
                if ($this->aSysEnterprises->isModified() || $this->aSysEnterprises->isNew()) {
                    $affectedRows += $this->aSysEnterprises->save($con);
                }
                $this->setSysEnterprises($this->aSysEnterprises);
            }

            if ($this->aCmsTemplates !== null) {
                if ($this->aCmsTemplates->isModified() || $this->aCmsTemplates->isNew()) {
                    $affectedRows += $this->aCmsTemplates->save($con);
                }
                $this->setCmsTemplates($this->aCmsTemplates);
            }

            if ($this->aSysUsers !== null) {
                if ($this->aSysUsers->isModified() || $this->aSysUsers->isNew()) {
                    $affectedRows += $this->aSysUsers->save($con);
                }
                $this->setSysUsers($this->aSysUsers);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->cmsArticlessScheduledForDeletion !== null) {
                if (!$this->cmsArticlessScheduledForDeletion->isEmpty()) {
                    foreach ($this->cmsArticlessScheduledForDeletion as $cmsArticles) {
                        // need to save related object because we set the relation to null
                        $cmsArticles->save($con);
                    }
                    $this->cmsArticlessScheduledForDeletion = null;
                }
            }

            if ($this->collCmsArticless !== null) {
                foreach ($this->collCmsArticless as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->cmsFilesXPagesScheduledForDeletion !== null) {
                if (!$this->cmsFilesXPagesScheduledForDeletion->isEmpty()) {
                    CmsFilesXPageQuery::create()
                        ->filterByPrimaryKeys($this->cmsFilesXPagesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->cmsFilesXPagesScheduledForDeletion = null;
                }
            }

            if ($this->collCmsFilesXPages !== null) {
                foreach ($this->collCmsFilesXPages as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->cmsVideosXPagesScheduledForDeletion !== null) {
                if (!$this->cmsVideosXPagesScheduledForDeletion->isEmpty()) {
                    CmsVideosXPageQuery::create()
                        ->filterByPrimaryKeys($this->cmsVideosXPagesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->cmsVideosXPagesScheduledForDeletion = null;
                }
            }

            if ($this->collCmsVideosXPages !== null) {
                foreach ($this->collCmsVideosXPages as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CmsPagesPeer::ID_PAGE;
        if (null !== $this->id_page) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CmsPagesPeer::ID_PAGE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CmsPagesPeer::ID_PAGE)) {
            $modifiedColumns[':p' . $index++]  = '`id_page`';
        }
        if ($this->isColumnModified(CmsPagesPeer::TITLE)) {
            $modifiedColumns[':p' . $index++]  = '`title`';
        }
        if ($this->isColumnModified(CmsPagesPeer::SLUG)) {
            $modifiedColumns[':p' . $index++]  = '`slug`';
        }
        if ($this->isColumnModified(CmsPagesPeer::ORDER)) {
            $modifiedColumns[':p' . $index++]  = '`order`';
        }
        if ($this->isColumnModified(CmsPagesPeer::ID_PARENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_parent`';
        }
        if ($this->isColumnModified(CmsPagesPeer::STATE)) {
            $modifiedColumns[':p' . $index++]  = '`state`';
        }
        if ($this->isColumnModified(CmsPagesPeer::TEMPLATE)) {
            $modifiedColumns[':p' . $index++]  = '`template`';
        }
        if ($this->isColumnModified(CmsPagesPeer::BODY)) {
            $modifiedColumns[':p' . $index++]  = '`body`';
        }
        if ($this->isColumnModified(CmsPagesPeer::VISIBLE)) {
            $modifiedColumns[':p' . $index++]  = '`visible`';
        }
        if ($this->isColumnModified(CmsPagesPeer::TYPE)) {
            $modifiedColumns[':p' . $index++]  = '`type`';
        }
        if ($this->isColumnModified(CmsPagesPeer::CREATED)) {
            $modifiedColumns[':p' . $index++]  = '`created`';
        }
        if ($this->isColumnModified(CmsPagesPeer::MODIFIED)) {
            $modifiedColumns[':p' . $index++]  = '`modified`';
        }
        if ($this->isColumnModified(CmsPagesPeer::USER_MODIFIED)) {
            $modifiedColumns[':p' . $index++]  = '`user_modified`';
        }
        if ($this->isColumnModified(CmsPagesPeer::ID_ENTERPRISE)) {
            $modifiedColumns[':p' . $index++]  = '`id_enterprise`';
        }

        $sql = sprintf(
            'INSERT INTO `cms_pages` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_page`':
                        $stmt->bindValue($identifier, $this->id_page, PDO::PARAM_INT);
                        break;
                    case '`title`':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case '`slug`':
                        $stmt->bindValue($identifier, $this->slug, PDO::PARAM_STR);
                        break;
                    case '`order`':
                        $stmt->bindValue($identifier, $this->order, PDO::PARAM_INT);
                        break;
                    case '`id_parent`':
                        $stmt->bindValue($identifier, $this->id_parent, PDO::PARAM_INT);
                        break;
                    case '`state`':
                        $stmt->bindValue($identifier, $this->state, PDO::PARAM_STR);
                        break;
                    case '`template`':
                        $stmt->bindValue($identifier, $this->template, PDO::PARAM_STR);
                        break;
                    case '`body`':
                        $stmt->bindValue($identifier, $this->body, PDO::PARAM_STR);
                        break;
                    case '`visible`':
                        $stmt->bindValue($identifier, $this->visible, PDO::PARAM_STR);
                        break;
                    case '`type`':
                        $stmt->bindValue($identifier, $this->type, PDO::PARAM_STR);
                        break;
                    case '`created`':
                        $stmt->bindValue($identifier, $this->created, PDO::PARAM_STR);
                        break;
                    case '`modified`':
                        $stmt->bindValue($identifier, $this->modified, PDO::PARAM_STR);
                        break;
                    case '`user_modified`':
                        $stmt->bindValue($identifier, $this->user_modified, PDO::PARAM_INT);
                        break;
                    case '`id_enterprise`':
                        $stmt->bindValue($identifier, $this->id_enterprise, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setIdPage($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSysEnterprises !== null) {
                if (!$this->aSysEnterprises->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSysEnterprises->getValidationFailures());
                }
            }

            if ($this->aCmsTemplates !== null) {
                if (!$this->aCmsTemplates->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCmsTemplates->getValidationFailures());
                }
            }

            if ($this->aSysUsers !== null) {
                if (!$this->aSysUsers->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSysUsers->getValidationFailures());
                }
            }


            if (($retval = CmsPagesPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCmsArticless !== null) {
                    foreach ($this->collCmsArticless as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCmsFilesXPages !== null) {
                    foreach ($this->collCmsFilesXPages as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCmsVideosXPages !== null) {
                    foreach ($this->collCmsVideosXPages as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CmsPagesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdPage();
                break;
            case 1:
                return $this->getTitle();
                break;
            case 2:
                return $this->getSlug();
                break;
            case 3:
                return $this->getOrder();
                break;
            case 4:
                return $this->getIdParent();
                break;
            case 5:
                return $this->getState();
                break;
            case 6:
                return $this->getTemplate();
                break;
            case 7:
                return $this->getBody();
                break;
            case 8:
                return $this->getVisible();
                break;
            case 9:
                return $this->getType();
                break;
            case 10:
                return $this->getCreated();
                break;
            case 11:
                return $this->getModified();
                break;
            case 12:
                return $this->getUserModified();
                break;
            case 13:
                return $this->getIdEnterprise();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CmsPages'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CmsPages'][$this->getPrimaryKey()] = true;
        $keys = CmsPagesPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdPage(),
            $keys[1] => $this->getTitle(),
            $keys[2] => $this->getSlug(),
            $keys[3] => $this->getOrder(),
            $keys[4] => $this->getIdParent(),
            $keys[5] => $this->getState(),
            $keys[6] => $this->getTemplate(),
            $keys[7] => $this->getBody(),
            $keys[8] => $this->getVisible(),
            $keys[9] => $this->getType(),
            $keys[10] => $this->getCreated(),
            $keys[11] => $this->getModified(),
            $keys[12] => $this->getUserModified(),
            $keys[13] => $this->getIdEnterprise(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aSysEnterprises) {
                $result['SysEnterprises'] = $this->aSysEnterprises->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCmsTemplates) {
                $result['CmsTemplates'] = $this->aCmsTemplates->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSysUsers) {
                $result['SysUsers'] = $this->aSysUsers->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCmsArticless) {
                $result['CmsArticless'] = $this->collCmsArticless->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCmsFilesXPages) {
                $result['CmsFilesXPages'] = $this->collCmsFilesXPages->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCmsVideosXPages) {
                $result['CmsVideosXPages'] = $this->collCmsVideosXPages->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CmsPagesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdPage($value);
                break;
            case 1:
                $this->setTitle($value);
                break;
            case 2:
                $this->setSlug($value);
                break;
            case 3:
                $this->setOrder($value);
                break;
            case 4:
                $this->setIdParent($value);
                break;
            case 5:
                $this->setState($value);
                break;
            case 6:
                $this->setTemplate($value);
                break;
            case 7:
                $this->setBody($value);
                break;
            case 8:
                $this->setVisible($value);
                break;
            case 9:
                $this->setType($value);
                break;
            case 10:
                $this->setCreated($value);
                break;
            case 11:
                $this->setModified($value);
                break;
            case 12:
                $this->setUserModified($value);
                break;
            case 13:
                $this->setIdEnterprise($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CmsPagesPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdPage($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setTitle($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setSlug($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setOrder($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setIdParent($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setState($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setTemplate($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setBody($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setVisible($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setType($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setCreated($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setModified($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setUserModified($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setIdEnterprise($arr[$keys[13]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CmsPagesPeer::DATABASE_NAME);

        if ($this->isColumnModified(CmsPagesPeer::ID_PAGE)) $criteria->add(CmsPagesPeer::ID_PAGE, $this->id_page);
        if ($this->isColumnModified(CmsPagesPeer::TITLE)) $criteria->add(CmsPagesPeer::TITLE, $this->title);
        if ($this->isColumnModified(CmsPagesPeer::SLUG)) $criteria->add(CmsPagesPeer::SLUG, $this->slug);
        if ($this->isColumnModified(CmsPagesPeer::ORDER)) $criteria->add(CmsPagesPeer::ORDER, $this->order);
        if ($this->isColumnModified(CmsPagesPeer::ID_PARENT)) $criteria->add(CmsPagesPeer::ID_PARENT, $this->id_parent);
        if ($this->isColumnModified(CmsPagesPeer::STATE)) $criteria->add(CmsPagesPeer::STATE, $this->state);
        if ($this->isColumnModified(CmsPagesPeer::TEMPLATE)) $criteria->add(CmsPagesPeer::TEMPLATE, $this->template);
        if ($this->isColumnModified(CmsPagesPeer::BODY)) $criteria->add(CmsPagesPeer::BODY, $this->body);
        if ($this->isColumnModified(CmsPagesPeer::VISIBLE)) $criteria->add(CmsPagesPeer::VISIBLE, $this->visible);
        if ($this->isColumnModified(CmsPagesPeer::TYPE)) $criteria->add(CmsPagesPeer::TYPE, $this->type);
        if ($this->isColumnModified(CmsPagesPeer::CREATED)) $criteria->add(CmsPagesPeer::CREATED, $this->created);
        if ($this->isColumnModified(CmsPagesPeer::MODIFIED)) $criteria->add(CmsPagesPeer::MODIFIED, $this->modified);
        if ($this->isColumnModified(CmsPagesPeer::USER_MODIFIED)) $criteria->add(CmsPagesPeer::USER_MODIFIED, $this->user_modified);
        if ($this->isColumnModified(CmsPagesPeer::ID_ENTERPRISE)) $criteria->add(CmsPagesPeer::ID_ENTERPRISE, $this->id_enterprise);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CmsPagesPeer::DATABASE_NAME);
        $criteria->add(CmsPagesPeer::ID_PAGE, $this->id_page);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdPage();
    }

    /**
     * Generic method to set the primary key (id_page column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdPage($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdPage();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CmsPages (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTitle($this->getTitle());
        $copyObj->setSlug($this->getSlug());
        $copyObj->setOrder($this->getOrder());
        $copyObj->setIdParent($this->getIdParent());
        $copyObj->setState($this->getState());
        $copyObj->setTemplate($this->getTemplate());
        $copyObj->setBody($this->getBody());
        $copyObj->setVisible($this->getVisible());
        $copyObj->setType($this->getType());
        $copyObj->setCreated($this->getCreated());
        $copyObj->setModified($this->getModified());
        $copyObj->setUserModified($this->getUserModified());
        $copyObj->setIdEnterprise($this->getIdEnterprise());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCmsArticless() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCmsArticles($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCmsFilesXPages() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCmsFilesXPage($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCmsVideosXPages() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCmsVideosXPage($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdPage(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CmsPages Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CmsPagesPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CmsPagesPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a SysEnterprises object.
     *
     * @param                  SysEnterprises $v
     * @return CmsPages The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSysEnterprises(SysEnterprises $v = null)
    {
        if ($v === null) {
            $this->setIdEnterprise(NULL);
        } else {
            $this->setIdEnterprise($v->getIdEnterprise());
        }

        $this->aSysEnterprises = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SysEnterprises object, it will not be re-added.
        if ($v !== null) {
            $v->addCmsPages($this);
        }


        return $this;
    }


    /**
     * Get the associated SysEnterprises object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SysEnterprises The associated SysEnterprises object.
     * @throws PropelException
     */
    public function getSysEnterprises(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSysEnterprises === null && ($this->id_enterprise !== null) && $doQuery) {
            $this->aSysEnterprises = SysEnterprisesQuery::create()->findPk($this->id_enterprise, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSysEnterprises->addCmsPagess($this);
             */
        }

        return $this->aSysEnterprises;
    }

    /**
     * Declares an association between this object and a CmsTemplates object.
     *
     * @param                  CmsTemplates $v
     * @return CmsPages The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCmsTemplates(CmsTemplates $v = null)
    {
        if ($v === null) {
            $this->setTemplate(NULL);
        } else {
            $this->setTemplate($v->getTemplate());
        }

        $this->aCmsTemplates = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CmsTemplates object, it will not be re-added.
        if ($v !== null) {
            $v->addCmsPages($this);
        }


        return $this;
    }


    /**
     * Get the associated CmsTemplates object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CmsTemplates The associated CmsTemplates object.
     * @throws PropelException
     */
    public function getCmsTemplates(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCmsTemplates === null && (($this->template !== "" && $this->template !== null)) && $doQuery) {
            $this->aCmsTemplates = CmsTemplatesQuery::create()->findPk($this->template, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCmsTemplates->addCmsPagess($this);
             */
        }

        return $this->aCmsTemplates;
    }

    /**
     * Declares an association between this object and a SysUsers object.
     *
     * @param                  SysUsers $v
     * @return CmsPages The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSysUsers(SysUsers $v = null)
    {
        if ($v === null) {
            $this->setUserModified(NULL);
        } else {
            $this->setUserModified($v->getIdUser());
        }

        $this->aSysUsers = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SysUsers object, it will not be re-added.
        if ($v !== null) {
            $v->addCmsPages($this);
        }


        return $this;
    }


    /**
     * Get the associated SysUsers object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return SysUsers The associated SysUsers object.
     * @throws PropelException
     */
    public function getSysUsers(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSysUsers === null && ($this->user_modified !== null) && $doQuery) {
            $this->aSysUsers = SysUsersQuery::create()->findPk($this->user_modified, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSysUsers->addCmsPagess($this);
             */
        }

        return $this->aSysUsers;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CmsArticles' == $relationName) {
            $this->initCmsArticless();
        }
        if ('CmsFilesXPage' == $relationName) {
            $this->initCmsFilesXPages();
        }
        if ('CmsVideosXPage' == $relationName) {
            $this->initCmsVideosXPages();
        }
    }

    /**
     * Clears out the collCmsArticless collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CmsPages The current object (for fluent API support)
     * @see        addCmsArticless()
     */
    public function clearCmsArticless()
    {
        $this->collCmsArticless = null; // important to set this to null since that means it is uninitialized
        $this->collCmsArticlessPartial = null;

        return $this;
    }

    /**
     * reset is the collCmsArticless collection loaded partially
     *
     * @return void
     */
    public function resetPartialCmsArticless($v = true)
    {
        $this->collCmsArticlessPartial = $v;
    }

    /**
     * Initializes the collCmsArticless collection.
     *
     * By default this just sets the collCmsArticless collection to an empty array (like clearcollCmsArticless());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCmsArticless($overrideExisting = true)
    {
        if (null !== $this->collCmsArticless && !$overrideExisting) {
            return;
        }
        $this->collCmsArticless = new PropelObjectCollection();
        $this->collCmsArticless->setModel('CmsArticles');
    }

    /**
     * Gets an array of CmsArticles objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CmsPages is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CmsArticles[] List of CmsArticles objects
     * @throws PropelException
     */
    public function getCmsArticless($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCmsArticlessPartial && !$this->isNew();
        if (null === $this->collCmsArticless || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCmsArticless) {
                // return empty collection
                $this->initCmsArticless();
            } else {
                $collCmsArticless = CmsArticlesQuery::create(null, $criteria)
                    ->filterByCmsPages($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCmsArticlessPartial && count($collCmsArticless)) {
                      $this->initCmsArticless(false);

                      foreach ($collCmsArticless as $obj) {
                        if (false == $this->collCmsArticless->contains($obj)) {
                          $this->collCmsArticless->append($obj);
                        }
                      }

                      $this->collCmsArticlessPartial = true;
                    }

                    $collCmsArticless->getInternalIterator()->rewind();

                    return $collCmsArticless;
                }

                if ($partial && $this->collCmsArticless) {
                    foreach ($this->collCmsArticless as $obj) {
                        if ($obj->isNew()) {
                            $collCmsArticless[] = $obj;
                        }
                    }
                }

                $this->collCmsArticless = $collCmsArticless;
                $this->collCmsArticlessPartial = false;
            }
        }

        return $this->collCmsArticless;
    }

    /**
     * Sets a collection of CmsArticles objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $cmsArticless A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CmsPages The current object (for fluent API support)
     */
    public function setCmsArticless(PropelCollection $cmsArticless, PropelPDO $con = null)
    {
        $cmsArticlessToDelete = $this->getCmsArticless(new Criteria(), $con)->diff($cmsArticless);


        $this->cmsArticlessScheduledForDeletion = $cmsArticlessToDelete;

        foreach ($cmsArticlessToDelete as $cmsArticlesRemoved) {
            $cmsArticlesRemoved->setCmsPages(null);
        }

        $this->collCmsArticless = null;
        foreach ($cmsArticless as $cmsArticles) {
            $this->addCmsArticles($cmsArticles);
        }

        $this->collCmsArticless = $cmsArticless;
        $this->collCmsArticlessPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CmsArticles objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CmsArticles objects.
     * @throws PropelException
     */
    public function countCmsArticless(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCmsArticlessPartial && !$this->isNew();
        if (null === $this->collCmsArticless || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCmsArticless) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCmsArticless());
            }
            $query = CmsArticlesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCmsPages($this)
                ->count($con);
        }

        return count($this->collCmsArticless);
    }

    /**
     * Method called to associate a CmsArticles object to this object
     * through the CmsArticles foreign key attribute.
     *
     * @param    CmsArticles $l CmsArticles
     * @return CmsPages The current object (for fluent API support)
     */
    public function addCmsArticles(CmsArticles $l)
    {
        if ($this->collCmsArticless === null) {
            $this->initCmsArticless();
            $this->collCmsArticlessPartial = true;
        }

        if (!in_array($l, $this->collCmsArticless->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCmsArticles($l);

            if ($this->cmsArticlessScheduledForDeletion and $this->cmsArticlessScheduledForDeletion->contains($l)) {
                $this->cmsArticlessScheduledForDeletion->remove($this->cmsArticlessScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	CmsArticles $cmsArticles The cmsArticles object to add.
     */
    protected function doAddCmsArticles($cmsArticles)
    {
        $this->collCmsArticless[]= $cmsArticles;
        $cmsArticles->setCmsPages($this);
    }

    /**
     * @param	CmsArticles $cmsArticles The cmsArticles object to remove.
     * @return CmsPages The current object (for fluent API support)
     */
    public function removeCmsArticles($cmsArticles)
    {
        if ($this->getCmsArticless()->contains($cmsArticles)) {
            $this->collCmsArticless->remove($this->collCmsArticless->search($cmsArticles));
            if (null === $this->cmsArticlessScheduledForDeletion) {
                $this->cmsArticlessScheduledForDeletion = clone $this->collCmsArticless;
                $this->cmsArticlessScheduledForDeletion->clear();
            }
            $this->cmsArticlessScheduledForDeletion[]= $cmsArticles;
            $cmsArticles->setCmsPages(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CmsPages is new, it will return
     * an empty collection; or if this CmsPages has previously
     * been saved, it will retrieve related CmsArticless from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CmsPages.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsArticles[] List of CmsArticles objects
     */
    public function getCmsArticlessJoinSysEnterprises($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsArticlesQuery::create(null, $criteria);
        $query->joinWith('SysEnterprises', $join_behavior);

        return $this->getCmsArticless($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CmsPages is new, it will return
     * an empty collection; or if this CmsPages has previously
     * been saved, it will retrieve related CmsArticless from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CmsPages.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsArticles[] List of CmsArticles objects
     */
    public function getCmsArticlessJoinSysUsersRelatedByIdUser($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsArticlesQuery::create(null, $criteria);
        $query->joinWith('SysUsersRelatedByIdUser', $join_behavior);

        return $this->getCmsArticless($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CmsPages is new, it will return
     * an empty collection; or if this CmsPages has previously
     * been saved, it will retrieve related CmsArticless from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CmsPages.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsArticles[] List of CmsArticles objects
     */
    public function getCmsArticlessJoinSysUsersRelatedByUserModified($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsArticlesQuery::create(null, $criteria);
        $query->joinWith('SysUsersRelatedByUserModified', $join_behavior);

        return $this->getCmsArticless($query, $con);
    }

    /**
     * Clears out the collCmsFilesXPages collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CmsPages The current object (for fluent API support)
     * @see        addCmsFilesXPages()
     */
    public function clearCmsFilesXPages()
    {
        $this->collCmsFilesXPages = null; // important to set this to null since that means it is uninitialized
        $this->collCmsFilesXPagesPartial = null;

        return $this;
    }

    /**
     * reset is the collCmsFilesXPages collection loaded partially
     *
     * @return void
     */
    public function resetPartialCmsFilesXPages($v = true)
    {
        $this->collCmsFilesXPagesPartial = $v;
    }

    /**
     * Initializes the collCmsFilesXPages collection.
     *
     * By default this just sets the collCmsFilesXPages collection to an empty array (like clearcollCmsFilesXPages());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCmsFilesXPages($overrideExisting = true)
    {
        if (null !== $this->collCmsFilesXPages && !$overrideExisting) {
            return;
        }
        $this->collCmsFilesXPages = new PropelObjectCollection();
        $this->collCmsFilesXPages->setModel('CmsFilesXPage');
    }

    /**
     * Gets an array of CmsFilesXPage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CmsPages is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CmsFilesXPage[] List of CmsFilesXPage objects
     * @throws PropelException
     */
    public function getCmsFilesXPages($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCmsFilesXPagesPartial && !$this->isNew();
        if (null === $this->collCmsFilesXPages || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCmsFilesXPages) {
                // return empty collection
                $this->initCmsFilesXPages();
            } else {
                $collCmsFilesXPages = CmsFilesXPageQuery::create(null, $criteria)
                    ->filterByCmsPages($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCmsFilesXPagesPartial && count($collCmsFilesXPages)) {
                      $this->initCmsFilesXPages(false);

                      foreach ($collCmsFilesXPages as $obj) {
                        if (false == $this->collCmsFilesXPages->contains($obj)) {
                          $this->collCmsFilesXPages->append($obj);
                        }
                      }

                      $this->collCmsFilesXPagesPartial = true;
                    }

                    $collCmsFilesXPages->getInternalIterator()->rewind();

                    return $collCmsFilesXPages;
                }

                if ($partial && $this->collCmsFilesXPages) {
                    foreach ($this->collCmsFilesXPages as $obj) {
                        if ($obj->isNew()) {
                            $collCmsFilesXPages[] = $obj;
                        }
                    }
                }

                $this->collCmsFilesXPages = $collCmsFilesXPages;
                $this->collCmsFilesXPagesPartial = false;
            }
        }

        return $this->collCmsFilesXPages;
    }

    /**
     * Sets a collection of CmsFilesXPage objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $cmsFilesXPages A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CmsPages The current object (for fluent API support)
     */
    public function setCmsFilesXPages(PropelCollection $cmsFilesXPages, PropelPDO $con = null)
    {
        $cmsFilesXPagesToDelete = $this->getCmsFilesXPages(new Criteria(), $con)->diff($cmsFilesXPages);


        $this->cmsFilesXPagesScheduledForDeletion = $cmsFilesXPagesToDelete;

        foreach ($cmsFilesXPagesToDelete as $cmsFilesXPageRemoved) {
            $cmsFilesXPageRemoved->setCmsPages(null);
        }

        $this->collCmsFilesXPages = null;
        foreach ($cmsFilesXPages as $cmsFilesXPage) {
            $this->addCmsFilesXPage($cmsFilesXPage);
        }

        $this->collCmsFilesXPages = $cmsFilesXPages;
        $this->collCmsFilesXPagesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CmsFilesXPage objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CmsFilesXPage objects.
     * @throws PropelException
     */
    public function countCmsFilesXPages(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCmsFilesXPagesPartial && !$this->isNew();
        if (null === $this->collCmsFilesXPages || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCmsFilesXPages) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCmsFilesXPages());
            }
            $query = CmsFilesXPageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCmsPages($this)
                ->count($con);
        }

        return count($this->collCmsFilesXPages);
    }

    /**
     * Method called to associate a CmsFilesXPage object to this object
     * through the CmsFilesXPage foreign key attribute.
     *
     * @param    CmsFilesXPage $l CmsFilesXPage
     * @return CmsPages The current object (for fluent API support)
     */
    public function addCmsFilesXPage(CmsFilesXPage $l)
    {
        if ($this->collCmsFilesXPages === null) {
            $this->initCmsFilesXPages();
            $this->collCmsFilesXPagesPartial = true;
        }

        if (!in_array($l, $this->collCmsFilesXPages->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCmsFilesXPage($l);

            if ($this->cmsFilesXPagesScheduledForDeletion and $this->cmsFilesXPagesScheduledForDeletion->contains($l)) {
                $this->cmsFilesXPagesScheduledForDeletion->remove($this->cmsFilesXPagesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	CmsFilesXPage $cmsFilesXPage The cmsFilesXPage object to add.
     */
    protected function doAddCmsFilesXPage($cmsFilesXPage)
    {
        $this->collCmsFilesXPages[]= $cmsFilesXPage;
        $cmsFilesXPage->setCmsPages($this);
    }

    /**
     * @param	CmsFilesXPage $cmsFilesXPage The cmsFilesXPage object to remove.
     * @return CmsPages The current object (for fluent API support)
     */
    public function removeCmsFilesXPage($cmsFilesXPage)
    {
        if ($this->getCmsFilesXPages()->contains($cmsFilesXPage)) {
            $this->collCmsFilesXPages->remove($this->collCmsFilesXPages->search($cmsFilesXPage));
            if (null === $this->cmsFilesXPagesScheduledForDeletion) {
                $this->cmsFilesXPagesScheduledForDeletion = clone $this->collCmsFilesXPages;
                $this->cmsFilesXPagesScheduledForDeletion->clear();
            }
            $this->cmsFilesXPagesScheduledForDeletion[]= clone $cmsFilesXPage;
            $cmsFilesXPage->setCmsPages(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CmsPages is new, it will return
     * an empty collection; or if this CmsPages has previously
     * been saved, it will retrieve related CmsFilesXPages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CmsPages.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsFilesXPage[] List of CmsFilesXPage objects
     */
    public function getCmsFilesXPagesJoinSysFiles($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsFilesXPageQuery::create(null, $criteria);
        $query->joinWith('SysFiles', $join_behavior);

        return $this->getCmsFilesXPages($query, $con);
    }

    /**
     * Clears out the collCmsVideosXPages collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CmsPages The current object (for fluent API support)
     * @see        addCmsVideosXPages()
     */
    public function clearCmsVideosXPages()
    {
        $this->collCmsVideosXPages = null; // important to set this to null since that means it is uninitialized
        $this->collCmsVideosXPagesPartial = null;

        return $this;
    }

    /**
     * reset is the collCmsVideosXPages collection loaded partially
     *
     * @return void
     */
    public function resetPartialCmsVideosXPages($v = true)
    {
        $this->collCmsVideosXPagesPartial = $v;
    }

    /**
     * Initializes the collCmsVideosXPages collection.
     *
     * By default this just sets the collCmsVideosXPages collection to an empty array (like clearcollCmsVideosXPages());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCmsVideosXPages($overrideExisting = true)
    {
        if (null !== $this->collCmsVideosXPages && !$overrideExisting) {
            return;
        }
        $this->collCmsVideosXPages = new PropelObjectCollection();
        $this->collCmsVideosXPages->setModel('CmsVideosXPage');
    }

    /**
     * Gets an array of CmsVideosXPage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CmsPages is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CmsVideosXPage[] List of CmsVideosXPage objects
     * @throws PropelException
     */
    public function getCmsVideosXPages($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCmsVideosXPagesPartial && !$this->isNew();
        if (null === $this->collCmsVideosXPages || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCmsVideosXPages) {
                // return empty collection
                $this->initCmsVideosXPages();
            } else {
                $collCmsVideosXPages = CmsVideosXPageQuery::create(null, $criteria)
                    ->filterByCmsPages($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCmsVideosXPagesPartial && count($collCmsVideosXPages)) {
                      $this->initCmsVideosXPages(false);

                      foreach ($collCmsVideosXPages as $obj) {
                        if (false == $this->collCmsVideosXPages->contains($obj)) {
                          $this->collCmsVideosXPages->append($obj);
                        }
                      }

                      $this->collCmsVideosXPagesPartial = true;
                    }

                    $collCmsVideosXPages->getInternalIterator()->rewind();

                    return $collCmsVideosXPages;
                }

                if ($partial && $this->collCmsVideosXPages) {
                    foreach ($this->collCmsVideosXPages as $obj) {
                        if ($obj->isNew()) {
                            $collCmsVideosXPages[] = $obj;
                        }
                    }
                }

                $this->collCmsVideosXPages = $collCmsVideosXPages;
                $this->collCmsVideosXPagesPartial = false;
            }
        }

        return $this->collCmsVideosXPages;
    }

    /**
     * Sets a collection of CmsVideosXPage objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $cmsVideosXPages A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CmsPages The current object (for fluent API support)
     */
    public function setCmsVideosXPages(PropelCollection $cmsVideosXPages, PropelPDO $con = null)
    {
        $cmsVideosXPagesToDelete = $this->getCmsVideosXPages(new Criteria(), $con)->diff($cmsVideosXPages);


        $this->cmsVideosXPagesScheduledForDeletion = $cmsVideosXPagesToDelete;

        foreach ($cmsVideosXPagesToDelete as $cmsVideosXPageRemoved) {
            $cmsVideosXPageRemoved->setCmsPages(null);
        }

        $this->collCmsVideosXPages = null;
        foreach ($cmsVideosXPages as $cmsVideosXPage) {
            $this->addCmsVideosXPage($cmsVideosXPage);
        }

        $this->collCmsVideosXPages = $cmsVideosXPages;
        $this->collCmsVideosXPagesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CmsVideosXPage objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CmsVideosXPage objects.
     * @throws PropelException
     */
    public function countCmsVideosXPages(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCmsVideosXPagesPartial && !$this->isNew();
        if (null === $this->collCmsVideosXPages || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCmsVideosXPages) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCmsVideosXPages());
            }
            $query = CmsVideosXPageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCmsPages($this)
                ->count($con);
        }

        return count($this->collCmsVideosXPages);
    }

    /**
     * Method called to associate a CmsVideosXPage object to this object
     * through the CmsVideosXPage foreign key attribute.
     *
     * @param    CmsVideosXPage $l CmsVideosXPage
     * @return CmsPages The current object (for fluent API support)
     */
    public function addCmsVideosXPage(CmsVideosXPage $l)
    {
        if ($this->collCmsVideosXPages === null) {
            $this->initCmsVideosXPages();
            $this->collCmsVideosXPagesPartial = true;
        }

        if (!in_array($l, $this->collCmsVideosXPages->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCmsVideosXPage($l);

            if ($this->cmsVideosXPagesScheduledForDeletion and $this->cmsVideosXPagesScheduledForDeletion->contains($l)) {
                $this->cmsVideosXPagesScheduledForDeletion->remove($this->cmsVideosXPagesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	CmsVideosXPage $cmsVideosXPage The cmsVideosXPage object to add.
     */
    protected function doAddCmsVideosXPage($cmsVideosXPage)
    {
        $this->collCmsVideosXPages[]= $cmsVideosXPage;
        $cmsVideosXPage->setCmsPages($this);
    }

    /**
     * @param	CmsVideosXPage $cmsVideosXPage The cmsVideosXPage object to remove.
     * @return CmsPages The current object (for fluent API support)
     */
    public function removeCmsVideosXPage($cmsVideosXPage)
    {
        if ($this->getCmsVideosXPages()->contains($cmsVideosXPage)) {
            $this->collCmsVideosXPages->remove($this->collCmsVideosXPages->search($cmsVideosXPage));
            if (null === $this->cmsVideosXPagesScheduledForDeletion) {
                $this->cmsVideosXPagesScheduledForDeletion = clone $this->collCmsVideosXPages;
                $this->cmsVideosXPagesScheduledForDeletion->clear();
            }
            $this->cmsVideosXPagesScheduledForDeletion[]= clone $cmsVideosXPage;
            $cmsVideosXPage->setCmsPages(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CmsPages is new, it will return
     * an empty collection; or if this CmsPages has previously
     * been saved, it will retrieve related CmsVideosXPages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CmsPages.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CmsVideosXPage[] List of CmsVideosXPage objects
     */
    public function getCmsVideosXPagesJoinSysUsers($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CmsVideosXPageQuery::create(null, $criteria);
        $query->joinWith('SysUsers', $join_behavior);

        return $this->getCmsVideosXPages($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_page = null;
        $this->title = null;
        $this->slug = null;
        $this->order = null;
        $this->id_parent = null;
        $this->state = null;
        $this->template = null;
        $this->body = null;
        $this->visible = null;
        $this->type = null;
        $this->created = null;
        $this->modified = null;
        $this->user_modified = null;
        $this->id_enterprise = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCmsArticless) {
                foreach ($this->collCmsArticless as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCmsFilesXPages) {
                foreach ($this->collCmsFilesXPages as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCmsVideosXPages) {
                foreach ($this->collCmsVideosXPages as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aSysEnterprises instanceof Persistent) {
              $this->aSysEnterprises->clearAllReferences($deep);
            }
            if ($this->aCmsTemplates instanceof Persistent) {
              $this->aCmsTemplates->clearAllReferences($deep);
            }
            if ($this->aSysUsers instanceof Persistent) {
              $this->aSysUsers->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCmsArticless instanceof PropelCollection) {
            $this->collCmsArticless->clearIterator();
        }
        $this->collCmsArticless = null;
        if ($this->collCmsFilesXPages instanceof PropelCollection) {
            $this->collCmsFilesXPages->clearIterator();
        }
        $this->collCmsFilesXPages = null;
        if ($this->collCmsVideosXPages instanceof PropelCollection) {
            $this->collCmsVideosXPages->clearIterator();
        }
        $this->collCmsVideosXPages = null;
        $this->aSysEnterprises = null;
        $this->aCmsTemplates = null;
        $this->aSysUsers = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CmsPagesPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}

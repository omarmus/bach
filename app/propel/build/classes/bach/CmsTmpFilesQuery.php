<?php



/**
 * Skeleton subclass for performing query and update operations on the 'cms_tmp_files' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.bach
 */
class CmsTmpFilesQuery extends BaseCmsTmpFilesQuery
{
}

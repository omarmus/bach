<?php 

/**
* Class Model Mazter implements Propel ORM
*/

class BC_ModelPropel extends CI_Model 
{
	
	private $_query = NULL;
	protected $_table_name = '';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_by = '';
	protected $_timestamps = FALSE;
	protected $_autoincrement = TRUE;
	protected $rules = array();

	// Rules
	public function rules()
	{
		foreach ($this->rules as $key => $value) {
			if (isset($this->rules[$key]['label'])) {
				$this->rules[$key]['label'] = lang($this->rules[$key]['label']);
			}
		}
		return $this->rules;
	}
	
	//get new Object $_table_name
	public function getNew () {
		return new $this->_table_name();
	}

	//Select query
	public function all()
	{
		return $this->get();	
	}

	public function find($pk = NULL)
	{
		return $this->get($pk);
	}

	public function findPk($pk = NULL)
	{
		return $this->get($pk);
	}

	public function filter($where)
	{
		return $this->where($where);
	}

	// array toKeyValue($keyColumn, $valueColumn)
	public function filterArray($where)
	{
		return $this->where($where)->toArray();
	}

	public function filterOne($where)
	{
		return $this->where($where, TRUE);
	}

	public function filterOneArray($where)
	{
		return $this->where($where, TRUE)->toArray();
	}

	public function get($pk = NULL) 
	{
		if ($pk != NULL) {
			if (is_array($pk)) {
				$method = 'findPks';
			} else {
				$filter = $this->_primary_filter;
				$pk = $filter($pk);
				$method = 'findPk';
			}
		} else {
			$method = 'find';
		}

		$table = $this->_table_name.'Query';
		$this->_query = $table::create();
		$this->order_by();

		return $this->_query->$method($pk);
	}

	public function where($where, $single = FALSE) 
	{
		$table = $this->_table_name.'Query';
		$this->_query = $table::create();
		$this->_query->filterByArray($where);
		$this->order_by();
		$method = $single === TRUE?'findOne':'find';

		return $this->_query->$method();
	}

	public function save($data, $pk = NULL, $object = FALSE) 
	{
		if (CREATED || UPDATE) {
	        try {
                $con = Propel::getConnection(SysUsersPeer::DATABASE_NAME);
                $con->beginTransaction();

                // Set timestamps
                if ($this->_timestamps == TRUE) {
                	$now = date('Y-m-d H:i:s');
                	$pk || $data['Created'] = $now;
                	$data['Modified'] = $now;
                	$data['UserModified'] = ID_USER;
                }

				if ($pk === NULL) { // Insert
					if (!CREATED) {
						error_permission();
					}
					!isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
					$table = $this->_table_name;
					$obj = new $table();
				} else { // Update
					if (!UPDATE) {
						error_permission();
					}
					$filter = $this->_primary_filter;
					$pk = $filter($pk);
					$table = $this->_table_name.'Query';
					$obj = $table::create()->findPk($pk);
				}

				if ($this->_table_name != 'SysEnterprises' && method_exists($obj,'getIdEnterprise')) {
					if ($pk !== NULL && $obj->getIdEnterprise() == ID_ENTERPRISE) {
						$data['IdEnterprise'] = ID_ENTERPRISE;
						$obj->fromArray($data);
						$obj->save();
					} else {
						if ($pk === NULL) {
							$data['IdEnterprise'] = ID_ENTERPRISE;
							$obj->fromArray($data);
							$obj->save();
						}
					}
				} else {
					$obj->fromArray($data);
					$obj->save();
				}

                if ($object) {
                	$con->commit();
					return $obj;
				}

                $error = "OK";
                
                $con->commit();
	        } catch (Exception $e) {
	            $con->rollBack();
	            $error = "ERROR";
	            $mensaje = $e->getMessage();
	            echo $mensaje;
	        }
	    } else {
	        error_permission();
	    }
	    return $error;
	}
	
	public function delete($pk) 
	{
		if ( DELETE ) {
	        try {
                $con = Propel::getConnection(SysUsersPeer::DATABASE_NAME);
                $con->beginTransaction();

                $filter = $this->_primary_filter;
                $pk = $filter($pk);

                if (!$pk) {
                	return "ERROR";
                }
                $table = $this->_table_name.'Query';
                $obj = $table::create()->findPk($pk);

                if (method_exists($obj,'getDelete')) {
                	$obj->setDelete('YES');
                	$obj->save();
                } else {
                	$obj->delete();
                }

                $error = "DELETE";
                
                $con->commit();
	        } catch (Exception $e) {
	            $con->rollBack();
	            $error = "ERROR";
	            $mensaje = $e->getMessage();
	            echo $mensaje;
	        }
	    } else {
	        error_permission();
	    }
	    return $error;
	}

	public function deletePks($pks)
	{
		foreach ($pks as $pk) {
			if ($this->delete($pk) === FALSE) {
				return FALSE;
			}
		}
		return TRUE;
	}

	protected function order_by()
	{
		if ($this->_order_by != '' OR (is_array($this->_order_by) && count($this->_order_by))) {
			if (is_array($this->_order_by)) {
				foreach ($this->_order_by as $k => $v) {
					$order = "orderBy{$k}";
					$this->_query->$order($v);
				}
			} else {
				$order = 'orderBy'.$this->_order_by;
				$this->_query->$order('ASC');
			}
		}
	}

}
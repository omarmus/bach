<div class="section-buttons">
	<?php echo btn_add(lang('add_tag'), 'cms/tag/edit'); ?>
	<?php echo btn_delete('cms/tag/delete_selected'); ?>
</div>

<div class="panel panel-default">
	<div class="panel-heading filter">
		<form method="post">
			<div>
				<label><?php echo lang('name') ?></label>
				<input type="text" name="name" value="<?php echo $this->input->post('name') ?>" class="form-control">
			</div>
			<?php echo buttons_filter() ?>
		</form>
	</div>
	<div class="panel-body">		
		<table id="main-table" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th></th>
					<th class="edit"><?php echo lang('edit') ?></th>
					<th><?php echo lang('name') ?></th>
					<th><?php echo lang('description') ?></th>
					<th class="state"><?php echo lang('active') ?></th>
				</tr>
			</thead>
			<tbody>
			<?php if (count($tags)): foreach ($tags as $tag): ?>
				<tr>
					<td><?php echo $tag->id_tag ?></td>
					<td class="edit"><?php echo btn_edit('cms/tag/edit/' . $tag->id_tag) ?></td>
					<td><?php echo $tag->name; ?></td>
					<td><?php echo $tag->description; ?></td>
					<td class="edit"><?php echo btn_yes_no($tag->state, 'cms/tag/set_state/'. $tag->id_tag) ?></td>
				</tr>
			<?php endforeach; endif ?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	var DT = null;
	$(document).ready(function() {
		DT = App.datatable({noSortable : [ 1, 4 ]});
	});
</script>
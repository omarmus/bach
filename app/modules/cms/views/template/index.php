<div class="section-buttons">
	<?php echo btn_add(lang('add_template'), 'cms/template/edit'); ?>
	<?php echo btn_delete('cms/template/delete_selected'); ?>
</div>

<div class="panel panel-default">
	<div class="panel-heading filter">
		<form method="post">
			<div>
				<label><?php echo lang('name') ?></label>		
				<input type="text" name="name" value="<?php echo $this->input->post('name') ?>" class="form-control">
			</div>
			<?php echo buttons_filter() ?>
		</form>
	</div>
	<div class="panel-body">		
		<table id="main-table" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th></th>
					<th class="edit"><?php echo lang('edit') ?></th>
					<th><?php echo lang('key') ?></th>
					<th><?php echo lang('name') ?></th>
					<th><?php echo lang('type') ?></th>
					<th><?php echo lang('description') ?></th>
				</tr>
			</thead>
			<tbody>
			<?php if (count($templates)): foreach ($templates as $template): ?>
				<tr>
					<td><?php echo $template->template ?></td>
					<td class="edit"><?php echo btn_edit('cms/template/edit/' . $template->template) ?></td>
					<td><?php echo $template->template; ?></td>
					<td><?php echo $template->name; ?></td>
					<td><span class="label label-<?php echo $template->type == 'CMS' ? 'warning' : 'info' ?>"><?php echo $template->type ?></span></td>
					<td><?php echo $template->description; ?></td>
				</tr>
			<?php endforeach; endif ?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	var DT = null;
	$(document).ready(function() {
		DT = App.datatable();
	});
</script>
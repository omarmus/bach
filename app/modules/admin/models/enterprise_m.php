<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Se importará manualmente BC_ModelPropel.php de manera temporal hasta que se cambie todo el sistema a propel
include (FCPATH . APPPATH . 'core'. DIRECTORY_SEPARATOR . 'BC_ModelPropel.php');

class Enterprise_m extends BC_ModelPropel {

	protected $_table_name = 'SysEnterprises';
	protected $_primary_key = 'IdEnterprise';
	protected $_timestamps = TRUE; //created, modified and user_modified ACTIVE
	protected $rules = array(
		'Name' => array(
			'field' => 'Name',
			'label' => 'company_name', // set key on sys_lang
			'rules' => 'trim|required|xss_clean'
		),
		'NumberRegister' => array(
			'field' => 'NumberRegister',
			'label' => 'number_register',
			'rules' => 'trim|required|xss_clean'
		),
		'Address' => array(
			'field' => 'Address',
			'label' => 'address',
			'rules' => 'xss_clean'
		),
		'Phone' => array(
			'field' => 'Phone',
			'label' => 'phone',
			'rules' => 'xss_clean'
		),
		'Fax' => array(
			'field' => 'Fax',
			'label' => 'fax',
			'rules' => 'xss_clean'
		),
		'Email' => array(
			'field' => 'Email',
			'label' => 'email',
			'rules' => 'trim|valid_email|xss_clean'
		),
		'WebPage' => array(
			'field' => 'WebPage',
			'label' => 'web_page',
			'rules' => 'trim|prep_url|xss_clean'
		)
	);

}

/* End of file enterprise_m.php */
/* Location: .//C/wamp/www/bach/app/modules/admin/models/enterprise_m.php */
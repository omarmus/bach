<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Enterprise extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/enterprise_m', 'enterprise');
	}

	public function index()
	{
		//Load Javascript File
		$this->data['js_file'] = 'js/admin/enterprise.js';

		// Load enterprises
		$filters = array();

        if ($this->input->post('filter') == 'OK') {
            if ($this->input->post('Name') != '') {
                $filters['Name'] = '%' . $this->input->post('Name') . '%';
            }
            if ($this->input->post('Email') != '') {
                $filters['Email'] = '%' . $this->input->post('Email') . '%';
            }
        }

		$this->data['enterprises'] = $this->enterprise->filter($filters);

		// Load view
		$this->data['subview'] = 'admin/enterprise/index';
		$this->load->view('admin/_layout_main', $this->data);
	}

	public function edit($pk = NULL)
	{
		is_ajax();

		// Fetch a enterprise or set a new one
		if ($pk) {
			$enterprise = $this->enterprise->find($pk);
			count($enterprise) || modal_error(lang('not_exist_enterprise'));

			$this->data['data_modified'] = data_modified($enterprise->getModified(), $enterprise->getUserModified()); // Last modified
			$this->data['enterprise'] = $enterprise;
		} else {
			$this->data['enterprise'] = $this->enterprise->getNew();
		}

		// Set up the form rules
		$this->form_validation->set_rules($this->enterprise->rules());

		// Process the form
		if ($this->form_validation->run($this) == TRUE) {
			$data = $this->input->post();
			$this->enterprise->save($data, $pk);
			echo $pk ? 'UPDATE' : 'CREATE';
		} else {
			// Load the view
			$this->load->view('admin/enterprise/edit', $this->data);
		}	
	}

	public function delete_selected()
	{
		is_ajax();
		
		echo $this->enterprise->deletePks($this->input->post('pks')) ? "OK" : lang('error_deleted_records');
	}

	public function set_state($id_enterprise)
	{
		is_ajax();

		echo $this->enterprise->save(array('State' => $this->input->post('state')), $id_enterprise);
	}
}

/* End of file enterprise.php */
/* Location: .//C/wamp/www/bach/app/modules/admin/controllers/enterprise.php */
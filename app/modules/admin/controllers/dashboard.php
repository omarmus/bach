<?php
class Dashboard extends Admin_Controller {

    private $oUser;

    public function __construct(){
        parent::__construct();
        $this->oUser = $this->user->get(ID_USER);

        $this->load->model('cms/tag_m', 'tag');
        $this->load->model('cms/favorites_m', 'favorites');
        $this->load->model('cms/article_tag_m', 'article_tag');
        $this->load->model('cms/article_m', 'article');
        $this->load->model('cms/tmp_file_m', 'tmp_file');
        $this->load->model('cms/article_file_m', 'article_file');
        $this->load->model('cms/article_video_m', 'article_video');

        $this->load->library("word");
        
    }

    public function index() {

        // $this->data['simple_editor'] = TRUE;
        $this->data['mansonry'] = TRUE;
        $this->data['upload'] = TRUE;
        $this->data['chosen'] = TRUE;
        
        $this->data['tour'] = $this->oUser->tour;
        $this->data['edit'] = '';

        $this->data['list_pending'] = $this->article->filter(array('id_page' => 13, 'state' => 'INACTIVE'));
        $this->data['news'] = $this->article->filter(array('id_page' => 15, 'state' => 'ACTIVE'));

        $this->get_tmp_files();

        $this->data['total_articles'] = count($this->article->filter(array('id_page' => 13, 'state' => 'ACTIVE')));
        $this->data['total_articles_user'] = count($this->article->filter(array('id_page' => 13, 'state' => 'ACTIVE', 'id_user' => ID_USER)));
        $this->data['articles_pending'] = $this->article->filter(array('id_page' => 13, 'state' => 'PENDING', 'id_user' => ID_USER));
        $this->data['tags'] = $this->tag->all();
        $this->data['favorites'] = $this->favorites->get_favorites(ID_USER);
        $this->data['profile'] = TRUE;

        $this->data['years'] = $this->article->years();
        
        $this->data['subview'] = "admin/dashboard/index";
        $this->load->view('admin/_layout_main', $this->data);
    }

    protected function get_tmp_files($type = NULL)
    {
        $files = init_files();
        $results = $this->tmp_file->get_files(ID_USER, $type);

        foreach ($results as $item) {
            $files[$item->type][] = $item;
        }

        $this->data['files'] = $files;
        $this->data['imgs'] = get_icons();
    }

    public function load_list_files($type = '')
    {
        is_ajax();

        $this->get_tmp_files($type);
        $this->data['edit'] = '';
        $this->load->view('admin/dashboard/list_' . strtolower($type) . 's', $this->data);
    }

    public function load_add_files($type = '')
    {
        is_ajax();

        $this->data['edit'] = '';
        $this->load->view('admin/dashboard/add_' . strtolower($type) . 's', $this->data);
    }
    
    public function tour_view()
    {
        is_ajax();

        $this->user->save(array('tour' => 'NO'), ID_USER);
    }

    public function save_article($id_page = NULL)
    {
        is_ajax();

        $rules = $this->article->rules_save;

        $this->form_validation->set_rules($rules);

        $this->data['id_article'] = '';
        if ($this->form_validation->run($this) == TRUE) {

            $data = $this->input->post();
            $data['slug'] = url_title($data['title']);
            $data['pubdate'] = date('Y-m-d');
            $data['id_user'] = ID_USER;
            $data['state'] = 'ACTIVE';
            $data['id_page'] = $id_page;

            $tags = NULL;
            if (isset($data['tags'])) {
                $tags = $data['tags'];
                unset($data['tags']);
            }

            $this->db->trans_begin();

            $id_article = $this->article->save($data);

            // Save files
            $files = $this->tmp_file->filter(array('id_user' => ID_USER));           
            $data = [];
            $data['id_article'] = $id_article;

            foreach ($files as $file) {
                if ($file->type == 'PHOTO' || $file->type == 'DOCUMENT') {
                    unset($data['url']);
                    $data['type'] = $file->type;
                    $data['id_file'] = $file->id_file;
                    $this->article_file->save($data);
                    if ($file->type == 'DOCUMENT') {
                        $document = $this->article_file->get_files($id_article, 'DOCUMENT', NULL, TRUE);
                        $html = $this->word->read($document->fullpath);
                        $this->article->save(['html' => $html, 'body' => strip_html_tags($html)], $id_article);
                    }
                } else {
                    unset($data['id_file']);
                    $data['type'] = 'YOUTUBE';
                    $data['url'] = $file->description;
                    $this->article_video->save($data);
                }
            }
            $this->tmp_file->delete_temp(ID_USER);

            // Save Tags
            if (!is_null($tags)) {
                $data = ['id_article' => $id_article];
                foreach ($tags as $id_tag) {
                    $data['id_tag'] = is_numeric($id_tag) ? $id_tag : $this->tag->save(['name' => $id_tag]);
                    $this->db->insert('cms_tags_x_article', $data);
                }
            }
            
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->data['id_article'] = 'Ocurrió un error al momento de crear la publicación';
            } else {
                $this->db->trans_commit();
                $this->data['id_article'] = $id_article;
            }
        }
        
        $this->data['id_page'] = $id_page;
        $this->data['tags'] = $this->tag->all();
        $this->load->view('admin/dashboard/add_article', $this->data);
    }

    public function set_favorite($id_article)
    {
        $this->db->insert('cms_favorites', array('id_article' => $id_article, 'id_user' => ID_USER));

        $article = $this->article->find($id_article);
        echo json_encode(array('id_article' => $id_article, 'title' => $article->title));
    }

    protected function upload_files($config, $type)
    {
        $this->load->model('admin/file_m', 'file');
        $files = upload_files($config);

        $data = array('type' => $type, 'id_user' => ID_USER);

        foreach ($files as $file) {
            if($file['id_file']) {
                $data['id_file'] = $file['id_file'];
                $this->tmp_file->save($data);
            }
        }

        return json_encode($files);
    }

    protected function upload_files_edit($id_article, $config, $type)
    {
        $this->load->model('admin/file_m', 'file');
        $files = upload_files($config);

        $data = array('id_article' => $id_article, 'type' => $type);

        foreach ($files as $file) {
            if($file['id_file']) {
                $data['id_file'] = $file['id_file'];
                $this->article_file->save($data);
            }
        }

        return json_encode($files);
    }

    // Functions Photos
    public function upload_photos()
    {
        $config['field'] = 'photo';
        $config['upload_path'] = './assets/files/articles/photos/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['thumbnail'] = TRUE;
        $config['width'] = 150;
        $config['height'] = 150;

        echo $this->upload_files($config, 'PHOTO');
    }

    // Functions Documents
    public function upload_documents()
    {
        $config['field'] = 'document';
        $config['upload_path'] = './assets/files/articles/documents/';
        $config['allowed_types'] = 'doc|docx|xls|xlsx|ppt|pptx|pdf|rar|zip|apk';
        $config['encrypt_name'] = FALSE;
        $config['remove_spaces'] = FALSE;

        echo $this->upload_files($config, 'DOCUMENT');
    }

    // Functions Photos Edit
    public function upload_photos_edit($id_article)
    {
        $config['field'] = 'photo';
        $config['upload_path'] = './assets/files/articles/photos/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['thumbnail'] = TRUE;
        $config['width'] = 150;
        $config['height'] = 150;

        echo $this->upload_files_edit($id_article, $config, 'PHOTO');
    }

    // Functions Documents Edit
    public function upload_documents_edit($id_article)
    {
        $config['field'] = 'document';
        $config['upload_path'] = './assets/files/articles/documents/';
        $config['allowed_types'] = 'doc|docx|xls|xlsx|ppt|pptx|pdf|rar|zip|apk';
        $config['encrypt_name'] = FALSE;
        $config['remove_spaces'] = FALSE;

        echo $this->upload_files_edit($id_article, $config, 'DOCUMENT');
    }

    // Functions Videos
    public function add_videos($id_article = NULL)
    {
        is_ajax();

        is_null($id_article) || $this->data['id_article'] = $id_article;

        $this->data['edit'] = !is_null($id_article) ? 'edit' : '';
        $this->data['video_exist'] = !is_null($id_article) ? count($this->article_video->filter(array ('id_article' => $id_article))) :count($this->tmp_file->filter(array('type' => 'VIDEO', 'id_user' => ID_USER)));
        $this->load->view('admin/dashboard/add_videos', $this->data);
    }

    public function upload_video()
    {
        is_ajax();

        $rules = $this->article_video->rules;
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == TRUE) {
            $data['description'] = $this->input->post('url');
            $data['id_user'] = ID_USER;
            $data['type'] = 'VIDEO';

            $this->tmp_file->save($data);
            echo json_encode(array('status' => 'OK'));
        } else {
            echo json_encode(array('status' => 'ERROR', 'html' => form_error('url')));
        }   
    }

    public function upload_video_edit($id_article)
    {
        is_ajax();

        $rules = $this->article_video->rules;
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == TRUE) {
            $data = $this->input->post();
            $data['id_article'] = $id_article;
            $data['type'] = 'YOUTUBE';
            $this->article_video->save($data);
            echo json_encode(array('status' => 'OK'));
        } else {
            echo json_encode(array('status' => 'ERROR', 'html' => form_error('url')));
        }   
    }

    public function delete_file($id_file)
    {
        is_ajax();

        $this->tmp_file->delete($id_file);
    }

    public function delete_file_article($id_file)
    {
        is_ajax();

        $this->article_file->delete($id_file);
    }

    public function delete_video_article($id_video)
    {
        is_ajax();

        $this->article_video->delete($id_video);
    }

    public function delete_article($id_article)
    {
        $this->article->delete_files($id_article);
        $this->article_tag->delete_tags($id_article);
        $this->favorites->delete_favorite($id_article);
        $this->article->delete($id_article);
    }

    public function delete_favorite($id_article)
    {
        $this->db->delete('cms_favorites', array('id_article' => $id_article, 'id_user' => ID_USER));
    }

    public function save_field_article()
    {
        $value = $this->input->post('value');

        if (strlen($value)) {
            $data[$this->input->post('field')] = $value;
            $this->article->save($data, $this->input->post('id_article'));
        }
    }

    public function save_description_file()
    {
        $value = $this->input->post('value');

        if (strlen($value)) {
            $this->article_file->save(array('description' => $value), $this->input->post('id_file'));
        }
    }

    public function save_categories_article()
    {
        $id_article = $this->input->post('id_article');
        $tags = $this->input->post('tags');

        $this->article_tag->delete_tags($id_article);

        $data = array('id_article' => $id_article);
        foreach ($tags as $id_tag) {
            $data['id_tag'] = $id_tag;
            $this->db->insert('cms_tags_x_article', $data);
        }
    }

    public function verify_article($id_article)
    {
        $files = $this->article_file->filter(array('id_article' => $id_article));
        $videos = $this->article_video->filter(array('id_article' => $id_article));

        echo count($files) + count($videos) ? 'OK' : 'ERROR';
    }

    public function set_verify($id_article)
    {
        $article = $this->article->find($id_article);
        if ($article->id_user == ID_USER) {
            $this->article->save(array('state' => 'INACTIVE'), $id_article);    
        }
    }

    public function set_active($id_article)
    {
        $article = $this->article->find($id_article);
        $this->article->save(array('state' => 'ACTIVE'), $id_article);    
    }

    public function set_inactive($id_article)
    {
        $article = $this->article->find($id_article);
        $this->article->save(array('state' => 'PENDING'), $id_article);    
    }

    public function search($search = '')
    {
        $array_search = [];

        $results = $this->article->search($search, 13, 10, 'AUTHOR');
        foreach ($results as $item) {
            $article = array (
                'id_article' => $item->id_article,
                'id_user' => $item->id_user,
                'title' => $item->title,
                'author' => $item->first_name . ' ' . $item->last_name,
                'photo' => !is_null($item->filename) ? ('assets/files/users/thumbnail/'. thumb_image($item->filename)) : ('assets/img/' . ($item->gender == 'M' ? 'profile-m.jpg' : 'profile.png')),
                'type' => 'AUTHOR'
            );
            $array_search[] = $article;
        }

        $results = $this->article->search($search, 13, 10 - count($results), 'ARTICLE');
        foreach ($results as $item) {
            $article = array (
                'id_article' => $item->id_article,
                'id_user' => '',
                'title' => $item->title,
                'author' => $item->first_name . ' ' . $item->last_name,
                'photo' => '',
                'type' => 'ARTICLE'
            );
            $array_search[] = $article;
        }
        echo json_encode($array_search);
    }

    public function get_articles()
    {
        is_ajax();

        $this->load->model('cms/article_file_m', 'article_file');
        $this->load->helper('text');
        
        $articles['articles'] = [];

        $id_page = $this->input->post('id_page');
        $ini = $this->input->post('ini');

        $filter = [];

        $id_user = $this->input->post('id_user');
        if ($id_user) {
            $filter['cms_articles.id_user'] = $id_user;
        }

        $date = $this->input->post('date');
        if ($date) {
            $filter['cms_articles.created >='] = $date . '-01';
            $filter['cms_articles.created <='] = $date . '-31';
        }

        $year = $this->input->post('year');
        if ($year) {
            $filter['cms_articles.year'] = $year;
        }      

        $id_tag = $this->input->post('id_tag');
        $id_tag || $id_tag = NULL;

        $letter = $this->input->post('letter');
        $like = $letter ? ['cms_articles.title' => $letter] : [];

        $search = $this->input->post('search');
        if ($search) {
            if (count(explode(' ', $search)) == 1) {
                $results = $this->article->get_articles($id_page, 10, $ini, $filter, $search, $id_tag);
            } else {
                $results = $this->article->get_articles_search($id_page, 10, $ini, $search);
            }
            $search = explode(' ', $search);
        } else {
            $results = $this->article->get_articles($id_page, 10, $ini, $filter, $like, $id_tag);
        }
        $categorias = get_array_keys($this->tag->all(), 'id_tag', 'name');
        $favorites = get_array_keys($this->favorites->filter(array('id_user' => ID_USER)), 'id_article', 'id_user');        
        foreach ($results as $item) {
            $photo = $this->article_file->get_primary_photo($item->id_article);
            $video = FALSE;
            if (!is_object($photo)) {
                $this->load->model('cms/article_video_m', 'article_video');
                $video = $this->article_video->get_primary_video($item->id_article);
                $photo = FALSE;
            }
            $categories = [];
            $tags = $this->article_tag->filter(array('id_article' => $item->id_article));
            foreach ($tags as $tag) {
                $categories[] = array('id' => $tag->id_tag, 'name' => $categorias[$tag->id_tag]);
            }

            $document = $this->article_file->get_files($item->id_article, 'DOCUMENT', NULL, TRUE);

            $html = delimiter_text(str_replace("'", "", html_compress($item->html)), 1000, array('html' => TRUE, 'exact' => FALSE));
            $articles['articles'][] = [
                'id_article' => $item->id_article,
                'title' => $item->title,
                'body' => count($search) ? str_highlight($html, $search) : $html,
                'date' => datetime_literal($item->created),
                'date_literal' => between_two_dates_literal($item->created),
                'id_user' => $item->id_user,
                'name_user' => $item->first_name . ' ' . $item->last_name,
                'first_name' => $item->first_name,
                'number_visits' => $item->number_visits,
                'photo_user' => !is_null($item->id_photo) ? ('assets/files/users/thumbnail/'. thumb_image($item->filename)) : ('assets/img/' . ($item->gender == 'M' ? 'profile-m.jpg' : 'profile.png')),
                'photo' => $photo ? 'assets/files/articles/photos/' . $photo->filename : '',
                'width' => $photo ? $photo->image_width : 0,
                'height' => $photo ? $photo->image_height : 0,
                'video' => $video ? str_replace(array('http:', 'https:'), '', str_replace(array('youtu.be/', 'www.youtube.com/watch?v='), 'www.youtube.com/embed/', $video->url)) : '',
                'categories' => $categories,
                'favorite' => isset($favorites[$item->id_article]) ? 'SI' : 'NO',
                'description' => $item->description,
                'others' => $item->others,
                'year' => $item->year,
                'filename' => is_object($document) ? $document->filename : '',
                'filename_short' => is_object($document) ? get_filename($document->filename) : ''
            ];
        }
        echo json_encode($articles);
    }

    public function get_article($id_article)
    {
        $this->load->helper('text');
        $this->data['article'] = $this->article->get_article($id_article);
        $this->data['edit'] = $this->data['article']->state == 'PENDING' || (ID_USER < 3 && $this->data['article']->id_page == 15)? '-edit' : '';
        $this->data['id_article'] = $id_article;

        if ($this->data['article']->state == 'ACTIVE') {
            $this->data['number_visits'] = $this->data['article']->number_visits + 1;
            $this->article->save(array('number_visits' => $this->data['number_visits']), $id_article);
        }

        $files = init_files();
        $results = $this->article_file->get_files($id_article);
        foreach ($results as $item) {
            $files[$item->type]['F' . $item->id_file] = $item;
        }
        $files['VIDEO'] = $this->article_video->get_by(array('id_article' => $id_article));

        $this->data['files'] = $files;
        $this->data['imgs'] = get_icons();

        $photo = $this->article_file->get_primary_photo($id_article);
        $this->data['id_file'] = is_object($photo) ? $photo->id_file : 0;

        $this->data['tags'] = get_array_keys($this->tag->get_tags($id_article), 'id_tag', 'name');
        $this->data['tags_all'] = get_array_keys($this->tag->all(), 'id_tag', 'name');

        $this->data['favorite'] = (boolean) count($this->favorites->filter(array('id_article' => $id_article, 'id_user' => ID_USER)));
        $this->load->view('admin/dashboard/article', $this->data);
    }

    public function return_article($id_article)
    {
        $this->data['id_article'] = $id_article;
        $this->load->view('admin/dashboard/return_article', $this->data);
    }

    public function get_tags()
    {
        $query = $this->input->post('query');
        echo json_encode($this->tag->search($query));
    }

}

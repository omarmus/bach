<?php echo modal_header('Rechazar artículo') ?>
<div class="modal-body">
	<form>
		<div class="form-group" style="width: 100%">
			<label>Razón del rechazo</label>
			<textarea name="" id="comments" class="form-control"></textarea>
		</div>
	</form>
</div>
<div class="modal-footer">
	<?php echo btn_submit('btn-inactived-article', 'rechazar', 'rechazando', 'btn-primary') ?>
</div>	
<style type="text/css">
	#second-modal .modal-dialog {margin-top: 150px; width: 500px;}
</style>
<script type="text/javascript">
	$('#btn-inactived-article').on('click', function() {
		var $this = $(this);
		$this.prop({disabled : true});
		$this.find('> span:first').hide();
		$this.find('> span:last').show();
		App.loading.show('Rechazando artículo');
		$.get(base_url + 'admin/dashboard/set_inactive/' + <?php echo $id_article ?>, function(data) {
			App.loading.hide();
			App.modal.hide('second-modal');
			App.modal.hide();
			$list_pending.find('a.list-group-item').each(function () {
				var $this = $(this),
					id_article = $this.data('role');
				if (id_article == <?php echo $id_article ?>) {
					$this.parent().remove();
				}
			});
			setTimeout(function () {
				App.message.mail('Se envió un mail al autor de la publicación.');
			}, 500);
		});		
	});
</script>
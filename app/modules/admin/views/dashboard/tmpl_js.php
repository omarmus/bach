<script type="text/js-tmpl" id="article-tmpl">
    <div class="col-md-6 col-sm-6 col-xs-12 document-article" data-role="{id_article}">
        <article class="panel panel-default">
        	<div class="panel-body">
        		<div class="info-article">
        			<h3><i class="fa fa-file-word-o"></i> {filename_short}</h3>
		    		<p>
        				<em>Registrado por <a href="<?php echo base_url() ?>admin/profile/{id_user}" title="{name_user}">{first_name}</a></strong>
        				el <a href="#" title="{date}" onclick="return false;">{date_literal}</a></em>
        			</p>
	    			<div class="action-favorite">
	    				<div class="btn-group">
	    					<button type="button" class="btn btn-link btn-xs dropdown-toggle" data-toggle="dropdown">
	    						<i class="glyphicon glyphicon-chevron-down"></i>
	    					</button>
	    					<ul class="dropdown-menu" role="menu">
	    						<li><a href="assets/files/articles/documents/{filename}" class="document-download">Descagar archivo</a></li>
	    						<li><a href="#" data-role="{id_article}" class="document-delete">Eliminar</a></li>
	    					</ul>
	    				</div>
	    				<a href="#" data-role="{id_article}" title="{title_favorite}" class="{action_favorite}">
		    				<i class="glyphicon glyphicon-{class_favorite}"></i>
		    			</a>
	    			</div>
        		</div>
        		<div class="details-article" data-role="{id_article}">
        			<div>
        				<strong>Acta: </strong> {title}<br>
        				<strong>Libro: </strong> {description}<br>
        				<div class="row">
        					<div class="col-md-6"><strong>Número: </strong> {others}</div>
        					<div class="col-md-6"><strong>Año: </strong> {year}</div>
        				</div>
        				<strong>Categoría(s): </strong> {categories}
        			</div>
        			<div class="categories">
        				<strong>Contenido del documento: </strong> 
        				<iframe />
        			</div>
        			{imagen}
        		</div>
        	</div>
        </article>  
    </div>
</script>

<script type="text/js-tmpl" id="image-tmpl">
	<img class="img-responsive" src="<?php echo base_url() ?>{photo}" style="height: {height}px">
</script>

<script type="text/js-tmpl" id="video-tmpl">
	<div class="video-youtube">
		<iframe src="{url}?controls=0" width="{width}" height="{height}" frameborder="0" allowfullscreen></iframe>
	</div>
</script>

<script type="text/js-tmpl" id="loading-tmpl">
	<div class="loading text-center"><img src="<?php echo base_url() ?>assets/img/spinner.gif"></div>
</script>

<script type="text/js-tmpl" id="tmpl-tag">
	<a href="#" data-role="{id}"><label class="label label-default">{tag}</label></a>
</script>

<script type="text/tmpl-js" id="tmpl-new-article">
	<div class="list-group-item">
		<a href="#"  class="list-group-item" data-role="{id_article}">
			<span class="glyphicon glyphicon-star-empty" style="color: crimson;"></span> <strong>{title}</strong>
		</a>
		<div>
			<button title="Editar artículo" class="btn btn-default btn-xs" data-role="{id_article}"><span class="glyphicon glyphicon-pencil"></span></button>
			<button title="Eliminar artículo" class="btn btn-danger btn-xs" data-role="{id_article}"><span class="glyphicon glyphicon-trash"></span></button>
		</div>
	</div>
</script>

<script type="text/tmpl-js" id="tmpl-new-new">
	<div class="list-group-item">
		<a href="#"  class="list-group-item" data-role="{id_article}">
			<span class="glyphicon glyphicon-flag" style="color: crimson;"></span> <strong>{title}</strong>
		</a>
		<div>
			<button title="Editar noticia" class="btn btn-default btn-xs" data-role="{id_article}"><span class="glyphicon glyphicon-pencil"></span></button>
			<button title="Eliminar noticia" class="btn btn-danger btn-xs" data-role="{id_article}"><span class="glyphicon glyphicon-trash"></span></button>
		</div>
	</div>
</script>

<script type="text/tmpl-js" id="tmpl-new-favorite">
	<div class="list-group-item">
		<a href="#"  class="list-group-item" data-role="{id_article}">
			<span class="glyphicon glyphicon-star" style="color: #F0AD4E;"></span> <strong>{title}</strong>
		</a>
	<?php if ($profile) : ?>
		<a href="#" title="Eliminar favorito" class="glyphicon glyphicon-remove delete-favorite-list" data-role="{id_article}"></a>
	<?php endif; ?>
	</div>
</script>
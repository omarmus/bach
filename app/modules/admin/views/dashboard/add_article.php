<div class="form-group-content">
	<div class="form-group form-group-default required col-md-6">
		<label for="title"><?php echo 'Acta' ?></label>
		<input type="text" 
			   autocomplete="off" 
			   class="form-control" 
			   id="title" 
			   name="title" 
			   placeholder="<?php echo "Escriba el acta" ?>" 
			   value="<?php echo set_value('title') ?>" 
			   autofocus>
		<?php echo form_error('title'); ?>
	</div>
	
	<div class="form-group form-group-default required col-md-6">
		<label for="description">Libro</label>
		<input type="text" 
			   autocomplete="off" 
			   class="form-control" 
			   id="description" 
			   name="description" 
			   placeholder="<?php echo "Nombre del libro" ?>" 
			   value="<?php echo set_value('description') ?>">
		<?php echo form_error('description'); ?>
	</div>
</div>
<div class="form-group-content">
	<div class="form-group form-group-default border-right required col-md-3">
		<label for="others">Número</label>
		<input type="text" 
			   autocomplete="off" 
			   class="form-control integer" 
			   id="others" 
			   name="others" 
			   placeholder="<?php echo "Nro. de documento" ?>" 
			   value="<?php echo set_value('others') ?>">
		<?php echo form_error('others'); ?>
	</div>
	<div class="form-group required col-md-3">
		<label for="year" style="width: 100%;"><?php echo 'Año' ?><strong class="select-required">*</strong></label>
		<select class="form-control" id="year" name="year">
			<option value="-">Seleccione</option>
			<?php for ($i=date('Y'); $i >= 1950; $i--) : ?>
			<option value="<?php echo $i ?>" <?php echo set_value('year') == $i ? 'selected' : ''?>><?php echo $i; ?></option>		
			<?php endfor ?>
		</select>
		<?php echo form_error('year'); ?>
	</div>
	<div class="form-group col-md-6" style="padding: 0;">
		<label for="others">Categorías</label>
		<div id="tags" 
			 data-name="tags"
			 data-data="admin/dashboard/get_tags" 
			 data-placeholder="Escriba una categoría"></div>
		<?php echo form_error('tags'); ?>
	</div>
</div>


<input type="hidden" id="id-article" value="<?php echo isset($id_article) ? $id_article : '' ?>">
<script type="text/javascript">
	$(document).ready(function() {
		$(".chosen-select").chosen().on('change', function() {
			$('#tags_chosen').next().fadeOut(function() {
				set_height_container();
			});
		});

		var tags = new App.autocomplete('#tags', {hideTrigger: true});
	});
</script>
<?php if (count($files['DOCUMENT'])): ?>
<div class="multimedia margin-zero">
	<?php foreach ($files['DOCUMENT'] as $document): ?>
	<div class="alert document bg-primary margin-zero">
		<?php $extension = explode('.', $document->filename) ?>
		<i class="fa fa-file-<?php echo $imgs[$extension[count($extension)-1]] ?>-o"></i> 
		<strong><?php echo $document->filename ?></strong>
		<a href="#" class="delete pull-right" data-role="<?php echo $document->id_tmp_file ?>" title="Eliminar documento" data-toggle="tooltip" data-placement="bottom"><span class="glyphicon glyphicon-remove"></span></a>
	</div>
	<?php endforeach ?>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		delete_files_events('container-documents');
	});
</script>
<?php endif ?>

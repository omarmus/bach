<div class="row dashboard">
	<div class="col-md-4">
		<div class="panel panel-default clearfix dashboard-stats rounded">
			<span class="sparkline transit" id="dashboard-stats-sparkline1">
				Documentos Word
			</span>
			<i class="fa fa-file-word-o bg-primary transit stats-icon"></i>
			<h3 class="transit"><?php echo $total_articles_user ?></h3>
			<p class="text-muted transit" style="font-size: .9em;">
				Documento<?php echo $total_articles_user > 1 ? 's' : '' ?> registrado<?php echo $total_articles_user > 1 ? 's' : '' ?>.
            </p>
		</div>
		<div id="t-index" class="panel panel-default info-articles">
			<ul class="list-group">
				<li class="list-group-item"><strong>Índice alfabético</strong></li>
				<li class="list-group-item alphabet">
					<a href="#" data-role="all" class="label label-default label-primary">Todas las letras</a>
					<?php $alphabet = array ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'); ?>
					<?php foreach ($alphabet as $value) : ?>
						<a href="#"  class="label label-default" data-role="<?php echo $value ?>"><?php echo $value ?></a>
					<?php endforeach ?>
				</li>
				<li class="list-group-item"><strong>Índice por categorías</strong></li>
				<li class="list-group-item tags">
					<a href="#" data-role="all" class="label label-default label-primary">Todas las categorías</a>
					<?php foreach ($tags as $tag) : ?>
						<a href="#" class="label label-default" data-role="<?php echo $tag->id_tag ?>"><?php echo $tag->name ?></a>
					<?php endforeach ?>	
				</li>
				<li class="list-group-item"><strong>Índice por año</strong></li>
				<li class="list-group-item years">
					<a href="#" data-role="all" class="label label-default label-primary">Todas los años</a>
					<?php foreach ($years as $item): ?>
						<?php if ($item->total == 0) continue; ?>
						<a href="#" class="label label-default" data-role="<?php echo $item->year ?>"><?php echo $item->year . "(" . $item->total . ")" ?></a>
					<?php endforeach ?>
				</li>
			</ul>
		</div>
		<div id="t-favorites" class="panel panel-default info-articles">
			<div class="panel-heading">
				<?php echo 'Mis favoritos' ?>
			</div>
			<div id="favorites" class="list-group list-pending">
				<?php foreach ($favorites as $favorite) : ?>
					<div class="list-group-item">
						<a href="#" class="list-group-item" data-role="<?php echo $favorite->id_article ?>">
							<span class="glyphicon glyphicon-star"></span> <strong><?php echo $favorite->title ?></strong>
						</a>
						<a href="#" title="Eliminar favorito" class="glyphicon glyphicon-remove delete-favorite-list" data-role="<?php echo $favorite->id_article ?>"></a>
					</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="row collapse-row">
			<div class="col-md-12">
				<div class="panel panel-default container-row fixed-container" id="article-publish-container">
					<div class="panel-heading header-publish">
						<span class="glyphicon glyphicon-pencil"></span> <?php echo 'Publicar documento' ?>
					</div>
					<div class="panel-body row article-form" id="container-article">
						<form method="post" id="post-article">
							<?php $this->load->view('admin/dashboard/add_article', $this->data); ?>
						</form>
					</div>
					<div class="panel-footer text-right">
						<div class="pull-left">
							<div id="container-documents">
								<?php $this->load->view('admin/dashboard/list_documents', $this->data); ?>
							</div>
							<div class="inline-block" id="documents-article" <?php echo count($files['DOCUMENT'])? 'style="display: none;"' : '' ?>>
								<?php $this->load->view('admin/dashboard/add_documents', $this->data); ?>
							</div>
						</div>
						<?php echo btn_submit('btn-publish') ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row collapse-row">
			<form id="form-search">
				<div class="form-group">
					<div class="input-group input-search">
						<div class="input-group-addon"><strong>Buscar documento</strong></div>
						<input type="text" class="form-control" placeholder="Escriba la palabra a buscar y presione Enter">
						<button class="btn btn-primary input-group-addon" type="submit" disabled="disabled"><i class="glyphicon glyphicon-search"></i></button>
					</div>
				</div>
			</form>
		</div>
		<div id="masonry" class="row masonry"></div>
		<div class="loading text-center"><img src="<?php echo base_url() ?>assets/img/spinner.gif"></div>
		<div class="row"><div class="col-md-4 col-sm-6 col-xs-12" id="width">&nbsp;</div></div>
	</div>
</div>


<script type="text/javascript">
	var id_page = 13,
		processing = true,
 		tmpl_article = null,
 		tmpl_image = null,
 		tmpl_video = null,
 		tmpl_new_article = null,
 		tmpl_new_new = null,
 		tmpl_new_favorite = null,
 		tmpl_tag = null,
 		ini = 0,
 		FILTER = null,
 		height_description = 0,
 		scroll_events = false,
 		height_publish = 0,
 		height_pending = 0,
 		$container = null,
 		$loading = null,
 		$loading_ajax = null,
 		$articles_pending = null,
 		$articles_pending_header = null,
 		$container_article = null,
 		$articles_publish_header = null,
 		$list_pending = null,
 		$news = null,
 		$favorites = null,
 		$link_profile = null;

	$(document).ready(function() {

		var $tags = $('.tags'),
			$alphabet = $('.alphabet'),
			$years = $('.years');

		$tags.on('click', 'a', function(e) {
			e.preventDefault();
			var $el = $(this);
			$tags.find('.label').removeClass('label-primary');
			$el.addClass('label-primary');

			$alphabet.find('.label').removeClass('label-primary');
			$alphabet.find('.label:first').addClass('label-primary');

			$years.find('.label').removeClass('label-primary');
			$years.find('.label:first').addClass('label-primary');

			FILTER = $el.data('role') == 'all' ? null : {id_tag : $el.data('role')};
			get_articles(true);
		});

		$alphabet.on('click', 'a', function(e) {
			e.preventDefault();
			var $el = $(this);
			$alphabet.find('.label').removeClass('label-primary');
			$el.addClass('label-primary');

			$tags.find('.label').removeClass('label-primary');
			$tags.find('.label:first').addClass('label-primary');
			
			$years.find('.label').removeClass('label-primary');
			$years.find('.label:first').addClass('label-primary');

			FILTER = $el.data('role') == 'all' ? null : {letter : $el.data('role')};
			get_articles(true);
		});

		$years.on('click', 'a', function(e) {
			e.preventDefault();
			var $el = $(this);
			$years.find('.label').removeClass('label-primary');
			$el.addClass('label-primary');

			$tags.find('.label').removeClass('label-primary');
			$tags.find('.label:first').addClass('label-primary');

			$alphabet.find('.label').removeClass('label-primary');
			$alphabet.find('.label:first').addClass('label-primary');
			
			FILTER = $el.data('role') == 'all' ? null : {year : $el.data('role')};
			get_articles(true);
		});

		var $form = $('#post-article'),
			$btn_submit = $('#btn-publish'),
			$loading_ajax = $('#loading-tmpl').html();

		tmpl_article = $('#article-tmpl').html();
		tmpl_image = $('#image-tmpl').html();
		tmpl_video = $('#video-tmpl').html();
		tmpl_new_article = $('#tmpl-new-article').html();
		tmpl_new_new = $('#tmpl-new-new').html();
		tmpl_new_favorite = $('#tmpl-new-favorite').html();
		tmpl_tag = $('#tmpl-tag').html();

 		$container = $('#masonry');
 		$pks = $('#pks');
 		$loading = $('.loading'),
 		$link_profile = $('.link-profile');
 		$articles_pending = $('#articles-pending');
 		$list_pending = $('#list-pending');
 		$news = $('#news');
 		$favorites = $('#favorites');
 		$publish_container = $('#article-publish-container');
 		$pending_container = $('#articles-pending-container');
 		$articles_publish_header = $publish_container.find('.header-publish');
 		$articles_pending_header = $pending_container.find('.panel-heading');
 		$container_article = $('#container-article');

		get_articles();

		// Publicando artículo
 		$form.on('submit', function(event) {
 			event.preventDefault();

 			if ($('#container-documents').find('.document').length == 0) {
 				App.alert('Debe agregar un documento Word antes de poder guardar el documento.');
 				return false;
 			}
 			var form = this;
 			$btn_submit.prop({disabled : true});
 			$btn_submit.find('> span:first').hide();
			$btn_submit.find('> span:last').show();
 		
 			$.post(base_url + 'admin/dashboard/save_article/' + id_page, $form.serialize(), function(response) {
 				$btn_submit.prop({disabled : false});
 				$btn_submit.find('> span:first').show();
				$btn_submit.find('> span:last').hide();
 				$form.html(response);

 				var id_article = $('#id-article').val();
 				if (!isNaN(parseInt(id_article))) {
 					
 					var $title = $('#title'), $body = $('#body');
 					$('#container-photos, #container-videos, #container-documents').html('');
 					set_height_container();
 					if (id_page == 13) {
 						App.modal.show(base_url + 'admin/dashboard/get_article/' + id_article);
 						var $new_article = $($.parseHTML(App.nano(tmpl_new_article, {id_article : id_article, title : $title.val()})));
 						$new_article.find('a.list-group-item').on('click', function(event) {
 							event.preventDefault();
 							events_view_article_pending(this);
 						});
 						$new_article.find('.btn-default').on('click', function() {
 							events_view_article_pending(this);
 						});
 						$new_article.find('.btn-danger').on('click', function() {
 							events_delete_article(this);
 						});
 						$articles_pending.append($new_article);
 					} else {
 						message_ok('Noticia publicada');
 						var $new_new = $($.parseHTML(App.nano(tmpl_new_new, {id_article : id_article, title : $title.val()})));

 						$new_new.find('a.list-group-item').on('click', function(event) {
 							event.preventDefault();
 							events_view_article_pending(this);
 						});

 						$new_new.find('.btn-default').on('click', function() {
 							events_view_article_pending(this);
 						});

 						$new_new.find('.btn-danger').on('click', function() {
 							events_delete_new(this);
 						});
 						$news.append($new_new);
 					}
 					
 					$title.val('');
 					$body.val('');
 				} else {
 					if (id_article != '') {
 						message_error(id_article);
 						setTimeout(function () {
 							window.location = '';
 						}, 3000);
 					}
 					var error = $form.html(response).find('.input-error').get(0);
 					set_height_container();
 					setTimeout(function () {$(error).prev().focus()}, 300);
 					$form.find('input, textarea').on('keyup', function () {
 						$(this).next().fadeOut(function () {
 							set_height_container();
 						});
 					});
 				}
 			});
 		});

 		$btn_submit.on('click', function() {
 			$form.submit();
 		});

 		var width_publish = $publish_container.width(),
 			width_pending = $pending_container.width(),
 			$link_pages = $publish_container.find('a');

		height_pending = $pending_container.height();
 		// $pending_container.parent().height(height_pending);
 		
 		set_height_container();

 		var $scroll = $('section.content > div');
        var $height = $scroll.children('div');
        $scroll.on('scroll', function(e) {
            e.preventDefault();
            if (processing && $height.height() - $scroll.scrollTop() < $scroll.height() - 20) {
                processing = false;
			    get_articles();
            }
        });

        var $search = $('#form-search'),
        	$inputSearch = $search.find('input'),
        	$buttonSearch = $search.find('button');

        $inputSearch.on('keyup', function () {
        	$buttonSearch.prop('disabled', !$inputSearch.val().length);
        })
        $search.on('submit', function (e) {
        	e.preventDefault();
        	if ($inputSearch.val().length) {
        		FILTER = {search : $inputSearch.val()};
				get_articles(true);
        	}
        });

		$('#main-modal').on('shown.bs.modal', function () {
			draw_article();
		});

		$('#main-modal').on('hidden.bs.modal', function () {
			$('#main-modal .modal-content').html('');
		});

		$link_pages.on('click', function(event) {
			event.preventDefault();
			var $this = $(this);
			id_page = $this.data('role');
			$link_pages.removeClass('active-page');
			$this.addClass('active-page');
			if (id_page == 13) {
				$('#title').prop('placeholder', '<?php echo lang('write_title') ?>');
				$('#body').prop('placeholder', '<?php echo lang('write_contents') ?>');
				$btn_submit.find('.title-submit').html('<?php echo lang('publish') ?>')
				$('#documents-article').show();
				$('#tags').prop('disabled', false).parent().show();
				$('#tags_chosen').width('100%').find('.search-field').width('100%').children('input').width('100%');
			} else {
				$('#title').prop('placeholder', 'Escribe el título de la noticia aquí');
				$('#body').prop('placeholder', 'Escribe la descripción de la noticia aquí');
				$btn_submit.find('.title-submit').html('Publicar noticia');
				$('#documents-article').hide();
				$('#tags').prop('disabled', true).parent().hide();
			}
			// set_height_container();
		});

		$articles_pending.find('a.list-group-item').on('click', function(event) {
			event.preventDefault();
			events_view_article_pending(this);
		});

		$articles_pending.find('.btn-default').on('click', function() {
			events_view_article_pending(this);
		});

		$articles_pending.find('.btn-danger').on('click', function() {
			events_delete_article(this);
		});

		$list_pending.find('a.list-group-item').on('click', function(event) {
			event.preventDefault();
			events_view_article_pending(this);
		});

		$list_pending.find('.btn-default').on('click', function() {
			events_view_article_pending(this);
		});

		$list_pending.find('.btn-danger').on('click', function() {
			events_delete_article(this);
		});

		$news.find('a.list-group-item').on('click', function(event) {
			event.preventDefault();
			events_view_article_pending(this);
		});

		$news.find('.btn-default').on('click', function() {
			events_view_article_pending(this);
		});

		$news.find('.btn-danger').on('click', function() {
			events_delete_new(this);
		});

		$favorites.find('a.list-group-item').on('click', function(event) {
			event.preventDefault();
			events_view_article_pending(this);
		});

		$favorites.find('.delete-favorite-list').on('click', function() {
			events_delete_favorite(this, true);
		});

		$articles_pending_header.on('click', function() {
			if (scroll_events) {
				$articles_pending[$articles_pending.css('display') == 'none' ? 'show' : 'hide']();
			}
		});
		$articles_publish_header.on('click', function() {
			if (scroll_events) {
				$container_article[$container_article.css('display') == 'none' ? 'show' : 'hide']();
			}
		});
	});

	function set_height_container () {
		// setTimeout(function () {
		// 	height_publish = $publish_container.height();
		// 	$publish_container.parent().animate({height: height_publish + 2});
		// }, 400);
	}

	function events_view_article_pending (obj) {
		App.modal.show(base_url + 'admin/dashboard/get_article/' + $(obj).data('role'));
	}

	function events_delete_article (obj) {
		var $el = $(obj);
		App.confirm('¿Desea eliminar el documento?', function () {
			App.loading.show('Eliminando documento');
			var id_article = $el.data('role');
			$.get(base_url + 'admin/dashboard/delete_article/' + id_article, function(data) {
				App.loading.hide();
				$container.masonry( 'remove', $container.find('.document-article[data-role=' + id_article + ']') ).masonry();
				$favorites.find('.list-group-item[data-role=' + id_article + ']').parent().remove();
			});
		})
	}

	function events_delete_new (obj) {
		var $this = $(obj);
		if (App.confirm('Desea eliminar la noticia')) {
			App.loading.show('Eliminando noticia');
			$.get(base_url + 'admin/dashboard/delete_article/' + $this.data('role'), function(data) {
				App.loading.hide();
				$this.parent().parent().remove();
			});
		}
	}

	function add_favorite (obj) {
		App.loading.show('Agregando favorito');
		var $this = $(obj);
		$.get(base_url + 'admin/dashboard/set_favorite/' + $this.data('role'), function(response) {
			App.loading.hide();
			var $new_favorite = $($.parseHTML(App.nano(tmpl_new_favorite, {id_article : response.id_article, title : response.title})));
			$new_favorite.find('a.list-group-item').on('click', function(e) {
				e.preventDefault();
				events_view_article_pending(this);
			});
			$new_favorite.find('.delete-favorite-list').on('click', function(e) {
				e.preventDefault();
				events_delete_favorite(this, true);
			});
			$favorites.append($new_favorite);

			$this.off('click').on('click', function(event) {
				event.preventDefault();
				events_delete_favorite(this);
			}).prop('title', 'Quitar de favoritos').children('i').removeClass('glyphicon-star-empty').addClass('glyphicon-star');

			if ($('#options-favorite').length) {
				var $info_article = $container.find('.info-article a[data-role="' + $this.data('role') + '"]');
				$info_article.off('click').on('click', function(event) {
					event.preventDefault();
					events_delete_favorite(this);
				}).prop('title', 'Quitar de favoritos').children('i').removeClass('glyphicon-star-empty').addClass('glyphicon-star');
			};
		}, 'json');
	}

	function events_delete_favorite (obj, button) {
		var $this = $(obj);
		App.loading.show('Eliminando favorito');
		$.get(base_url + 'admin/dashboard/delete_favorite/' + $this.data('role'), function(data) {
			App.loading.hide();
			if (button) {
				var $info_article = $container.find('.info-article a[data-role="' + $this.data('role') + '"]');
				$info_article.off('click').on('click', function(event) {
					event.preventDefault();
					add_favorite(this);
				}).prop('title', 'Agregar a favoritos').children('i').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
				$this.parent().remove();
			} else {
				$this.off('click').on('click', function(event) {
					event.preventDefault();
					add_favorite(this);
				}).prop('title', 'Agregar a favoritos').children('i').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
				$favorites.find('a[data-role="'+$this.data('role')+'"]').parent().remove();

				if ($('#options-favorite').length) {
					var $info_article = $container.find('.info-article a[data-role="' + $this.data('role') + '"]');
					$info_article.off('click').on('click', function(event) {
						event.preventDefault();
						add_favorite(this);
					}).prop('title', 'Agregar a favoritos').children('i').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
				};
			}
		});
	}

	function draw_article () {
		var width_container = $('#container-files').width(),
			height_documents = $('#documents-article-modal').height();

		height_description = $('#container-description').height() - height_documents - 65;
		$('#description-article').height(height_description);

		$('#carousel-gallery img').each(function() {
			var $this = $(this),
				height = $this.data('height'),
				width = $this.data('width');
			if (width > width_container && (width_container * height / width) < 450) {
				$this.width('100%');
			}
		});
	}

	function delete_files_events (id) {
		$('#' + id + ' .delete').on('click', function(e) {
			e.preventDefault();
			var $this = $(this);
			App.loading.show('Eliminando');
			$.get(base_url + 'admin/dashboard/delete_file/' + $this.data('role'), function(data) {
				App.loading.hide();
				$this.parent().parent().parent().remove();
				set_height_container();
				$('#documents-article').show();
				if (id == 'container-videos') {
					$('#videos-article button').prop({disabled: false});
				};
			});
	    });
	}

	function get_articles (reload) {
		if (reload) {
			ini = 0;
			$container.find('.document-article').remove();
			$container.masonry('destroy');
		}

		var parameters = {id_page : id_page, ini : ini};
		if (FILTER) {
			$.extend(parameters, FILTER);
		}

		$loading.show();
		$.post(base_url + 'admin/dashboard/get_articles', parameters, function(data) {
			$loading.hide();
			ini += 10;
	    	processing = data.articles.length ? true : false;
	    	var elements = '';
	    	var articles = data.articles;
	    	for (var i = 0; i < articles.length; i++) {
	    		var article = articles[i];
	    		if (article.photo.length) {
	    			article.image = App.nano(tmpl_image, {
		    			photo : article.photo,
		    			height : parseInt(_width * article.height / article.width)
		    		});
	    		}
	    		var categories = '';
	    		for (var j = 0, l = article.categories.length; j < l; j++) {
	    			categories += App.nano(tmpl_tag, {
	    				id : article.categories[j].id, 
	    				tag : article.categories[j].name
	    			});
	    		}
	    		article.categories = categories;
	    		article.label_visits = article.number_visits != 1 ? 'veces' : 'vez';

	    		var favorite = article.favorite == 'SI';
	    		article.class_favorite = favorite ? 'star' : 'star-empty';
	    		article.label_favorite = favorite ? 'Quitar de favoritos' : 'Agregar a favoritos';
	    		article.action_favorite = favorite ? 'delete-favorite' : 'add-favorite';

	    		var $article = $($.parseHTML(App.nano(tmpl_article, article)));
	    		set_event_articles($article);
	    		$container.append($article);
	    		App.iframe($article.find('iframe')[0], article.body);
	    		if (ini > 10) {
	    			$container.masonry( 'appended' , $article );
	    		}
	    	};
	    	if (ini == 10) {
	    		$container.masonry();	
	    	}
	        
		}, 'json');
	}



	function set_event_articles ($elements) {
		$elements.find('.add-favorite').on('click', function(e) {
			e.preventDefault();
			add_favorite(this);
		});
		$elements.find('.delete-favorite').on('click', function(e) {
			e.preventDefault();
			events_delete_favorite(this);
		});
		$elements.find('.details-article').on('click', function() {
			var $this = $(this),
				$view = $this.prev().prev().find('.number-visits'),
				number = parseInt($view.find('span').html()) + 1;
			$view.html('Visto <span>'+ number +'</span> ' + (number > 1 ? 'veces' : 'vez'));
			App.modal.show(base_url + 'admin/dashboard/get_article/' + $this.data('role'));
		});
		$elements.find('.document-delete').on('click', function (e) {
			e.preventDefault();
			events_delete_article(this);
		});
	}
</script>
<?php $this->load->view('admin/dashboard/tmpl_js', $this->data); ?>

<?php if ($tour == 'YES') : ?>
<?php $this->load->view('admin/dashboard/tour', $this->data); ?>
<?php endif ?>

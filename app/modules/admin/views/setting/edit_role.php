<?php $new = ! isset($role->id_rol); ?>
<?php echo modal_header($new ? lang('new_role') : (lang('edit_role') . ': ' . $role->name)) ?>
<form onsubmit="return App.validate.form(this, '<?php echo base_url('admin/setting/edit_role'. ( $new ? '' : '/'.$role->id_rol)) ?>')">
	<div class="modal-body">
		<div class="form-group-content">
			<div class="form-group form-group-default required">
				<label><?php echo lang('name') ?></label>
				<?php echo form_input('name', set_value('name', $role->name), 'class="form-control"'); ?>
				<?php echo form_error('name'); ?>
			</div>
			<div class="form-group form-group-default">
				<label><?php echo lang('description') ?></label>
				<?php echo form_input('description', set_value('description', $role->description), 'class="form-control"'); ?>
				<?php echo form_error('description'); ?>
			</div>
		</div>
	</div>
	<?php echo $new ? modal_footer() : modal_footer_update() ?>
</form>
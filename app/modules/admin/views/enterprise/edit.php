<?php $new = is_null($enterprise->getIdEnterprise()); ?>
<?php echo modal_header($new ? lang('add_enterprise') : lang('edit_enterprise') . ' ' . $enterprise->getName()) ?>
<form data-role="<?php echo $new ? '' : $enterprise->getIdEnterprise() ?>">
	<div class="modal-body">
		<div class="form-group-content">
			<div class="form-group form-group-default required"> 
				<label><?php echo lang('company_name') ?></label>
				<?php echo form_input('Name', set_value('Name', $enterprise->getName()), 'class="form-control"'); ?>
				<?php echo form_error('Name'); ?>
			</div>
			<div class="form-group form-group-default required"> 
				<label><?php echo lang('number_register') ?></label>
				<?php echo form_input('NumberRegister', set_value('NumberRegister', $enterprise->getNumberRegister()), 'class="form-control"'); ?>
				<?php echo form_error('NumberRegister'); ?>
			</div>
		</div>
		<div class="form-group-content">
			<div class="form-group form-group-default" style="width: 100%;">
				<label><?php echo lang('address') ?></label>
				<?php echo form_input('Address', set_value('Address', $enterprise->getAddress()), 'class="form-control"'); ?>
				<?php echo form_error('Address'); ?>
			</div>
		</div>
		<div class="form-group-content">
			<div class="form-group form-group-default"> 
				<label><?php echo lang('phone') ?></label>
				<?php echo form_input('Phone', set_value('Phone', $enterprise->getPhone()), 'class="form-control"'); ?>
				<?php echo form_error('Phone'); ?>
			</div>
			<div class="form-group form-group-default">
				<label><?php echo lang('fax') ?></label>
				<?php echo form_input('Fax', set_value('Fax', $enterprise->getFax()), 'class="form-control"'); ?>
				<?php echo form_error('Fax'); ?>
			</div>
		</div>
		<div class="form-group-content">
			<div class="form-group form-group-default"> 
				<label><?php echo lang('email') ?></label>
				<?php echo form_input('Email', set_value('Email', $enterprise->getEmail()), 'class="form-control"'); ?>
				<?php echo form_error('Email'); ?>
			</div>
			<div class="form-group form-group-default"> 
				<label><?php echo lang('web_page') ?></label>
				<?php echo form_input('WebPage', set_value('WebPage', $enterprise->getWebPage()), 'class="form-control"'); ?>
				<?php echo form_error('WebPage'); ?>
			</div>
		</div>
	</div>
	<?php echo $new ? modal_footer() : modal_footer_update($data_modified) ?>
</form>

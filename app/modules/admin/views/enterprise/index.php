<div class="section-buttons">
	<?php echo btn_add(lang('add_enterprise'), 'btn-add'); ?>
	<?php echo btn_delete(); ?>
</div>

<div class="panel panel-default">
	<div class="panel-heading filter">
		<form method="post">
			<div>
				<label><?php echo lang('company_name') ?></label>
				<input type="text" name="Name" value="<?php echo $this->input->post('Name') ?>" class="form-control">
			</div>
			<div>
				<label><?php echo lang('email') ?></label>
				<input type="text" name="Email" value="<?php echo $this->input->post('Email') ?>" class="form-control">
			</div>
			<?php echo buttons_filter() ?>
		</form>
	</div>
	<div class="panel-body">		
		<table id="main-table" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th></th>
					<th class="edit"><?php echo lang('edit') ?></th>
					<th><?php echo lang('company_name') ?></th>
					<th><?php echo lang('number_register') ?></th>
					<th><?php echo lang('address') ?></th>					
					<th><?php echo lang('phone') ?>/<?php echo lang('fax') ?></th>
					<th><?php echo lang('email') ?></th>
					<th><?php echo lang('web_page') ?></th>
					<th class="state"><?php echo lang('active') ?></th>
				</tr>
			</thead>
			<tbody>
				<?php if (count($enterprises)): foreach ($enterprises as $enterprise): ?>
					<?php $pk = $enterprise->getPrimaryKey() ?>
					<tr>
						<td><?php echo $pk ?></td>
						<td class="edit"><?php echo btn_edit($pk) ?></td>
						<td><?php echo $enterprise->getName(); ?></td>
						<td><?php echo $enterprise->getNumberRegister(); ?></td>
						<td><?php echo $enterprise->getAddress() ?></td>
						<td><?php echo $enterprise->getPhone() ?></td>
						<td><?php echo $enterprise->getEmail() ?></td>
						<td><?php echo $enterprise->getWebPage() ?></td>
						<td class="edit"><?php echo btn_yes_no($enterprise->getState(), 'admin/enterprise/set_state/'. $pk) ?></td>
					</tr>
				<?php endforeach; endif ?>
			</tbody>
		</table>
	</div>
</div>
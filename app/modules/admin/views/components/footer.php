<footer class="container-fluid footer">
    <ul class="language" id="language">
        <li<?php echo $language == 'spanish' ? ' class="active"' : '' ?>><a data-role="spanish" href="#">Español</a></li>
        <li<?php echo $language == 'english' ? ' class="active"' : '' ?>><a data-role="english" href="#">English</a></li>
        <li<?php echo $language == 'portuguese' ? ' class="active"' : '' ?>><a data-role="portuguese" href="#">Portugues</a></li>
    </ul>
    <span class="text-muted credit">&copy; <?php echo $site_name_ ?> <?php echo date('Y') ?> - Disponible en <?php echo anchor('https://github.com/omarmus/bach', 'Github') ?></span>
    <a class="pull-right scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
</footer>

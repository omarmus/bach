<aside class="left-panel">
    <a href="<?php echo base_url('dashboard') ?>" class="logo"><span><?php echo $site_name_ ?></span><span>B</span></a>
    <nav class="navigation">
        <ul class="list-unstyled">
            <li <?php echo $title_ == 'Dashboard' ? 'class="active"' : '' ?>>
                <a href="<?php echo base_url('dashboard') ?>">
                    <i class="fa fa-bookmark-o"></i><span class="nav-label"><?php echo lang('dashboard') ?></span>
                </a>
            </li>

            <?php echo get_menu_admin($menu_, FALSE, $permissions_); //Menú del administrador ?>

            <?php if (isset($permissions_['admin/setting']) && $permissions_['admin/setting']['READ'] == "YES"): ?>
            <li <?php echo $title_ == 'Settings' ? 'class="active"' : '' ?>>
                <a href="<?php echo base_url('setting') ?>" >
                    <i class="glyphicon glyphicon-cog"></i><span class="nav-label"><?php echo lang('settings') ?></span>
                </a>
            </li>
            <?php endif ?>
        </ul>
    </nav>
</aside>
<header>
    <div class="top-head container-fluid margin-left">
        <button type="button" class="navbar-toggle pull-left">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <div class="page-header pull-left"><h1><?php echo $this->uri->segment(1) != 'dashboard' ? lang(strtolower(str_replace(' ', '_', $title_))) : '' ?></h1></div>
        <form role="search" class="hide navbar-left app-search pull-left hidden-xs <?php echo $this->uri->segment(1) != 'dashboard' ? 'hide' : '' ?>">
            <input type="text" placeholder="Enter keywords..." class="form-control form-control-circle">
        </form>
        <div id="t-profile" class="navbar-form navbar-right">
            <button type="submit" class="btn btn-inverse hide" ><span class="glyphicon glyphicon-search"></span></button>
            <button type="submit" class="btn btn-inverse hide" ><span class="glyphicon glyphicon-question-sign"></span></button>
            <ul class="nav-toolbar">
                <li class="dropdown"><a href="#" data-toggle="dropdown"><i class="fa fa-comments-o"></i> <span class="badge bg-warning">7</span></a>
                    <div class="dropdown-menu md arrow pull-right panel panel-default arrow-top-right">
                        <div class="panel-heading">
                            Messages
                        </div>

                        <div class="list-group">

                            <a href="#" class="list-group-item">
                                <div class="media">
                                    <div class="user-status busy pull-left">
                                        <img class="media-object img-circle pull-left" src="<?php echo base_url() ?>assets/img/avatar/user2.png" alt="user#1" width="40">
                                    </div>
                                    <div class="media-body">
                                        <h5 class="media-heading">Lorem ipsum dolor sit consect....</h5>
                                        <small class="text-muted">23 Sec ago</small>
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="list-group-item">
                                <div class="media">
                                    <div class="user-status offline pull-left">
                                        <img class="media-object img-circle pull-left" src="<?php echo base_url() ?>assets/img/avatar/user3.png" alt="user#1" width="40">
                                    </div>
                                    <div class="media-body">
                                        <h5 class="media-heading">Nunc elementum, enim vitae</h5>
                                        <small class="text-muted">23 Sec ago</small>
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="list-group-item">
                                <div class="media">
                                    <div class="user-status invisibled pull-left">
                                        <img class="media-object img-circle pull-left" src="<?php echo base_url() ?>assets/img/avatar/user4.png" alt="user#1" width="40">
                                    </div>
                                    <div class="media-body">
                                        <h5 class="media-heading">Praesent lacinia, arcu eget</h5>
                                        <small class="text-muted">23 Sec ago</small>
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="list-group-item">
                                <div class="media">
                                    <div class="user-status online pull-left">
                                        <img class="media-object img-circle pull-left" src="<?php echo base_url() ?>assets/img/avatar/user5.png" alt="user#1" width="40">
                                    </div>
                                    <div class="media-body">
                                        <h5 class="media-heading">In mollis blandit tempor.</h5>
                                        <small class="text-muted">23 Sec ago</small>
                                    </div>
                                </div>
                            </a>

                            <a href="#" class="btn btn-info btn-flat btn-block">View All Messages</a>

                        </div>

                    </div>
                </li>
                <li class="dropdown"><a href="#" data-toggle="dropdown"><i class="fa fa-bell-o"></i><span class="badge">3</span></a>
                    <div class="dropdown-menu arrow pull-right md panel panel-default arrow-top-right">
                        <div class="panel-heading">
                            Notification
                        </div>

                        <div class="list-group">

                            <a href="#" class="list-group-item">
                                <p>Installing App v1.2.1<small class="pull-right text-muted">45% Done</small></p>
                                <div class="progress progress-xs no-margn progress-striped active">
                                    <div class="progress-bar"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                        <span class="sr-only">45% Complete</span>
                                    </div>
                                </div>
                            </a>


                            <a href="#" class="list-group-item">
                                <p>Server Status</p>
                                <div class="progress progress-xs no-margn">
                                    <div class="progress-bar progress-bar-success" style="width: 35%">
                                        <span class="sr-only">35% Complete (success)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-warning" style="width: 20%">
                                        <span class="sr-only">20% Complete (warning)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-danger" style="width: 10%">
                                        <span class="sr-only">10% Complete (danger)</span>
                                    </div>
                                </div>
                            </a>

                            <a href="#" class="list-group-item">
                                <div class="media">
                                    <span class="label label-danger media-object img-circle pull-left">Danger</span>
                                    <div class="media-body">
                                        <h5 class="media-heading">Lorem ipsum dolor sit consect..</h5>
                                    </div>
                                </div>
                            </a>

                            <a href="#" class="list-group-item">
                                <p>Server Status</p>
                                <div class="progress progress-xs no-margn">
                                    <div style="width: 60%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">60% Complete (warning)</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </li>
                <li class="dropdown" id="link-profile">
                    <a data-toggle="dropdown" class="dropdown-toggle my-photo" href="#">
                        <div id="photo-user-menu" style="<?php echo $userdata_['photo'] != "" ? "background-image: url('" . base_url() . 'assets/files/users/thumbnail/'. thumb_image($userdata_['photo']) . "')" : '' ?>">
                            <?php if ($userdata_['photo'] == ""): ?>
                                <img src="<?php echo base_url('assets/img/'. ($userdata_['gender'] == 'M' ? 'profile-m.jpg' : 'profile.png')) ?>" class="img-responsive" style="background-color: #ffffff;"/>
                            <?php endif ?>
                        </div>
                    </a>
                    <div class="dropdown-menu md arrow pull-right panel panel-default arrow-top-right">
                        <div class="panel-heading">
                            <div class="my-photo">
                                <div style="<?php echo $userdata_['photo'] != "" ? "background-image: url('" . base_url() . 'assets/files/users/thumbnail/'. thumb_image($userdata_['photo']) . "')" : '' ?>">
                                    <?php if ($userdata_['photo'] == ""): ?>
                                        <img src="<?php echo base_url('assets/img/'. ($userdata_['gender'] == 'M' ? 'profile-m.jpg' : 'profile.png')) ?>" class="img-responsive" style="background-color: #ffffff;"/>
                                    <?php endif ?>
                                </div>
                            </div>
                            <div>
                                <?php echo $userdata_['first_name'] . ' ' . $userdata_['last_name'] ?><br>
                            <?php if (isset($permissions_['admin/profile']) && $permissions_['admin/profile']['READ'] == "YES"): ?>
                                <a href="<?php echo base_url('profile') ?>">
                                    <?php echo lang('edit_profile') ?>
                                </a>
                            <?php endif ?>
                            </div>
                        </div>
                        <div class="list-group">
                        <?php if (isset($permissions_['admin/setting']) && $permissions_['admin/setting']['READ'] == "YES"): ?>
                            <a class="list-group-item" href="<?php echo base_url('setting') ?>">
                                <span class="glyphicon glyphicon-cog"></span> <?php echo lang('settings') ?>
                            </a>
                        <?php endif ?>
                            <a href="<?php echo base_url('logout') ?>" class="btn btn-danger btn-flat btn-block">
                                <span class="glyphicon glyphicon-off"></span> <?php echo lang('logout') ?>
                            </a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <p class="navbar-text navbar-right"><a href="<?php echo base_url('profile') ?>" class="navbar-link"><?php echo $userdata_['first_name'] ?></a></p>
    </div>
    <ul class="breadcrumb margin-left alert bg-yellow">
        <?php if (count($page_) && !is_null($title_module_)) : ?>
            <li><?php echo lang(strtolower(str_replace(' ', '_', $title_module_))) ?></li>
        <?php endif ?>
        <?php if (isset($breadcrumb)): ?>
            <?php for ($i = 0; $i < count($breadcrumb) - 1; $i++) : ?>
                <li><a href="<?php echo base_url() . $breadcrumb[$i]['link'] ?>"><?php echo $breadcrumb[$i]['text'] ?></a></li>
            <?php endfor ?>
            <li class="active"><?php echo $breadcrumb[count($breadcrumb) - 1]['text'] ?></li>
        <?php else: ?>
            <li class="active"><?php echo lang(strtolower(str_replace(' ', '_', $title_))) ?></li>    
        <?php endif ?>
    </ul>
</header>
<!-- Header Ends -->

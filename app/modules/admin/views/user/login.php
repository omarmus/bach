<?php $cookie = isset($_COOKIE['bcuser']) && $_COOKIE['bcuser'] != '' ?>
<div class="container">
	<div class="row">
		<div class="col-lg-4 col-lg-offset-4">
			<h3 class="text-center"><?php echo $site_name_ ?></h3>
			<p class="text-center">Sign in to get in touch</p>
			<hr class="clean">
			<form role="form" action="login" method="post" class="login">
				<?php $error = $this->session->flashdata('error'); ?>
				<?php if ($error): ?>
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php echo $error ?>
					</div>	
				<?php endif ?>
				<?php $success = $this->session->flashdata('success'); ?>
				<?php if ($success): ?>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php echo $success ?>
					</div>	
				<?php endif ?>
				<div class="form-group input-group">
					<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					<input type="text" name="email_login" class="form-control" value="<?php echo $cookie ? $_COOKIE['bcuser'] : set_value('email_login') ?>" placeholder="<?php echo lang('email_login') ?>" autofocus>
				</div>
				<?php echo form_error('email_login'); ?>
				<div class="form-group input-group">
					<span class="input-group-addon"><i class="fa fa-key"></i></span>
					<input type="password" name="password_login" class="form-control" placeholder="<?php echo lang('password') ?>">
				</div>
				<?php echo form_error('password_login'); ?>
				<div style="overflow: hidden;">
					<div class="pull-left">
						<label class="cr-styled">
							<input type="checkbox" name="remember" value="remember-me" <?php echo $cookie ? 'checked' : '' ?>>
							<i class="fa"></i> 
							<?php echo lang('remember_me') ?>
						</label>
					</div>
					<div class="pull-right">
						<a href="#" id="open-reset" class="text-center"><?php echo lang('forget_password') ?></a>
					</div>
				</div>
				<button class="btn btn-primary btn-block" type="submit"><?php echo lang('login') ?></button>
			</form>
		</div>
	</div>
</div>

<script type="text/js-tmpl" id="tmpl-forgot">
	<?php echo modal_header(lang('recover_password')) ?>
	<form id="form-recover">
		<div class="modal-body">
			<div class="form-group" style="width: 100%;">
				<div class="input-group">
					<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
					<input type="text" name="email" class="form-control" placeholder="<?php echo lang('your_email') ?>">
				</div>
			</div>
		</div>
		<?php echo modal_footer('', 'recover', 'recovering') ?>
    </form>
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#open-reset').on('click', function(event) {
			event.preventDefault();
			$('#main-modal .modal-content').html($('#tmpl-forgot').html());
			set_events();
			$('#main-modal').modal();
		});

		$('#main-modal').find('input').on('keypress', function () {
			$(this).parent().next().fadeOut();
		});

		setTimeout(function () {
			$('.alert').fadeOut();
		}, 8000);

	});

	function set_events () {
		$('#form-recover').on('submit', function (event) {
			event.preventDefault();
			var $form = $(this),
				$button = $form.find('input[type=submit], button[type=submit]').prop({disabled : true});
			$button.find('> span:first').hide();
			$button.find('> span:last').show();
			$.post(base_url + 'ajax/reset_password', $form.serialize(), function (response) {
				$form.find('.input-error').remove();
				if (response.state == 'OK') {
					$('#main-modal').modal('hide');
					App.message.mail('Se ha enviadp un mail a su correo electrónico');
				} else {
					$form.find('.modal-body > div').append(response.msg);
					$button.prop({disabled : false});
					$button.find('> span:first').show();
					$button.find('> span:last').hide();
				}
				$form.find('input').get(0).focus();
			}, 'json');
		});
	}
</script>
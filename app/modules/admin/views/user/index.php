<div class="section-buttons">
	<?php echo btn_add(lang('add_user'), 'admin/user/edit', 'lg'); ?>
	<?php echo btn_delete('admin/user/delete_selected'); ?>
</div>

<div class="panel panel-default">
	<div class="panel-heading filter">
		<form method="post">
			<div>
				<label><?php echo lang('last_first_name') ?></label>		
				<input type="text" name="name" value="<?php echo $this->input->post('name') ?>" class="form-control">
			</div>
			<div>
				<label><?php echo lang('state') ?></label>
				<?php echo form_dropdown('state', array_merge(array('-' => strtoupper(lang('all'))), get_states_user()), $this->input->post('state'), 'class="form-control"'); ?>
			</div>
			<div>
				<label><?php echo lang('rol') ?></label>
				<?php echo form_dropdown('id_rol', $roles, $this->input->post('id_rol'), 'class="form-control"'); ?>
			</div>
			<?php echo buttons_filter() ?>
		</form>
	</div>
	<div class="panel-body">		
		<table id="main-table" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th></th>
					<th class="edit"><?php echo lang('edit') ?></th>
					<th class="edit"><?php echo lang('password') ?></th>
					<th><?php echo lang('last_first_name') ?></th>
					<th><?php echo lang('email') ?></th>
					<th><?php echo lang('username') ?></th>
					<th><?php echo lang('rol') ?></th>
					<th><?php echo lang('state') ?></th>
				</tr>
			</thead>
			<tbody>
				<?php if (count($users)): foreach ($users as $user): ?>
					<?php if ($user->id_rol < 3 && ID_ROL > 1) continue;  ?>
					<tr>
						<td><?php echo $user->id_user ?></td>
						<td class="edit"><?php echo btn_edit('admin/user/edit/' . $user->id_user, 'lg') ?></td>
						<td class="edit"><?php echo btn_modal('' ,'admin/user/password/' . $user->id_user, 'glyphicon-lock', NULL, UPDATE) ?></td>
						<td><a href="<?php echo base_url() . 'admin/profile/' . $user->id_user ?>"><?php echo $user->last_name . ' ' . $user->first_name ?></a></td>
						<td><?php echo $user->email; ?></td>
						<td><?php echo $user->username; ?></td>
						<td><?php echo $user->name_role; ?></td>
						<td><?php echo state_label($user->state); ?></td>
					</tr>
				<?php endforeach; endif ?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	var DT = null;
	$(document).ready(function() {
		DT = App.datatable({noSortable : [ 1, 2, 7 ]});
	});

	function toggle_password (input) {
		$('#main-modal .password')[input.checked ? 'hide' : 'show']();
	}

	function reset_password (id_user) {
		App.loading.show('Enviando mail...');
		$.post(base_url + 'admin/user/reset_password/' + id_user, function (response) {
			App.loading.hide();
			if (response.state == 'OK') {
				App.modal.hide();
				App.message.mail('Mail sent!', 300);
			} else {
				App.message.error('Error al enviar el mail');
			};
		}, 'json');
	}
</script>
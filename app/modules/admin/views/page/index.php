<div class="section-buttons">
	<?php echo btn_add(lang('add_page'), 'admin/page/edit'); ?>
	<?php echo btn_modal(lang('order_pages'), 'admin/page/order', 'glyphicon-list', NULL, UPDATE) ?>
	<?php echo btn_delete('admin/page/delete_selected', TRUE); ?>
</div>

<div class="panel panel-default">
	<div class="panel-heading filter">
		<form method="post">
			<div>
				<label><?php echo lang('name') ?>/<?php echo lang('uri') ?></label>		
				<input type="text" name="name" value="<?php echo $this->input->post('name') ?>" class="form-control">
			</div>
			<div>
				<label><?php echo lang('type') ?></label>
				<?php echo form_dropdown('type', array_merge(array(0 => strtoupper(lang('all'))), get_type_page()), $this->input->post('type'), 'class="form-control"'); ?>
			</div>
			<div style="display: none">
				<label><?php echo lang('module') ?></label>
				<?php echo form_dropdown('module', $list_modules, $this->input->post('module'), 'class="form-control"'); ?>
			</div>
			<div style="display: none">
				<label><?php echo lang('section') ?></label>
				<?php echo form_dropdown('section', array(0 => 'Select section'), $this->input->post('section'), 'class="form-control"'); ?>
			</div>
			<?php echo buttons_filter() ?>
		</form>
	</div>
	<div class="panel-body">		
		<table id="main-table" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th></th>
					<th class="edit"><?php echo lang('edit') ?></th>
					<th class="edit"><?php echo lang('permissions') ?></th>
					<th><?php echo lang('name') ?></th>
					<th><?php echo lang('uri') ?></th>
					<th><?php echo lang('module') ?></th>
					<th><?php echo lang('type') ?></th>
					<th class="state"><?php echo lang('view_menu') ?></th>
					<th class="state"><?php echo lang('active') ?></th>
				</tr>
			</thead>
			<tbody>
				<?php if (count($pages)): foreach ($pages as $page): ?>
					<tr>
						<td><?php echo $page->id_page ?></td>
						<td class="edit"><?php echo btn_edit('admin/page/edit/' . $page->id_page, 'load_sections') ?></td>
						<td class="edit"><?php echo btn_modal('' ,'admin/page/get_permissions/' . $page->id_page, 'glyphicon-lock', NULL, UPDATE) ?></td>
						<td><?php echo $page->title; ?></td>
						<td><?php echo $page->slug; ?></td>
						<td><?php echo $page->module == '' ? '' : ( $page->section == '' ? $page->module : $page->section) ?></td>
						<td>
							<span class="label label-<?php echo $page->module == '' ? 'primary' : ( $page->section == '' ? 'success' : 'info') ?>">
								<?php echo $page->module == '' ? lang('module') : ( $page->section == '' ? lang('section') : lang('subsection')); ?>
							</span>
						</td>
						<td class="edit"><?php echo $page->section != '' ? '<strong>' . $page->visible .'</strong>' : btn_yes_no($page->visible, 'admin/page/set_visible/'. $page->id_page) ?></td>
						<td class="edit"><?php echo btn_yes_no($page->state, 'admin/page/set_state/'. $page->id_page) ?></td>
					</tr>
				<?php endforeach; endif ?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">

	var DT = null;
	$(document).ready(function() {
		DT = App.datatable({noSortable : [ 1, 2, 7, 8 ]});
	});

	function page_type_change (input) {
		$('#container-module')[input.value == 'section' || input.value == 'subsection' ? 'show' : 'hide']();
		$('#container-section')[input.value == 'subsection' ? 'show' : 'hide']();
		$('#view-menu').prop({
			disabled : input.value == 'subsection', 
			checked : input.value != 'subsection'
		}).parent()[input.value == 'subsection' ? 'hide' : 'show']();
	}

	function get_sections (input, value) {
		$.post(base_url + 'admin/page/get_sections', {idModule : input.value}, function (response) {
			var select = $('#container-section select');
			select.empty();
			for (var i = 0; i < response.length; i++) {
				select.append(new Option(response[i].text, response[i].value));
			};
			if (value) {
				select.val(value);
			}			
		}, 'json');
	}

	function load_sections () {
		$('#container-module select').change();
	}

	function validate_page (form, url) {
		App.loading.show();
		var $button = $(form).find('input[type=submit], button[type=submit]');
		$button.prop({disabled : true}).children('span').hide().next('span').show();
		$.post(url, $(form).serialize(), function (response) {
			App.loading.hide();
			if (response == "UPDATE") {
				App.modal.hide();
				App.message.ok("Update success!", 500);
				setTimeout(function () {window.location = '';}, 1000);
			} else {
				if (!isNaN(response)) {
					App.modal.hide();
					App.message.ok("Create success!", 500);
					setTimeout(function () {
						App.modal.show(base_url + 'admin/page/get_permissions/' + response, '', function () {
							$('#main-modal').on('hidden.bs.modal', function () {
							  window.location = '';
							});
						});
					}, 500);
				} else {
					var error = $('#main-modal .modal-content').html(response).find('.input-error').get(0);
					setTimeout(function () {$(error).prev().focus()}, 300);
					$('#main-modal').find('input').on('keypress', function () {
						$(this).next().fadeOut();
					});
					load_sections();
				}
			}
		});
		return false;
	}
</script>
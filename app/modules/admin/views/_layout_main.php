<?php $this->load->view('admin/components/header'); ?>
<body>
    <div id="realtime" class="hide">
        <?php //$this->load->view('realtime/notification/index'); ?>
        <?php //$this->load->view('realtime/chat/index'); ?>
    </div>
    <?php if ($this->uri->segment(1) != 'dashboard' && !UPDATE): ?>
        <style type="text/css">
            .table th.edit, .table td.edit, .table th.state {display: none;}
        </style>
    <?php endif ?>
    <div id="loading-ajax"></div>

    <?php $this->load->view('admin/components/nav_main'); ?>
    <?php $this->load->view('admin/components/nav_lateral'); ?>

    <section class="content">
        <div>
            <div class="warper container-fluid">
                <?php $this->load->view($subview); // Cargando la sub vista ?>
            </div>
            <?php $this->load->view('admin/components/footer'); ?>
        </div>
    </section>
    
    <!-- Modal main -->
    <div id="main-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content"></div>
        </div>
    </div>
    <!-- Modal second -->
    <div id="second-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content"></div>
        </div>
    </div>
    <!-- Modal alert, confirm, login -->
    <div id="alert-modal" class="modal fade"></div>

    <!-- Templates Javascript -->
    <script type="text/tmpl-js" id="tmpl-alert">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{title}</h4>
                </div>
                <div class="modal-body">
                    {message}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default {button_cancel}" data-dismiss="modal" ><span class="glyphicon glyphicon-ban-circle"></span> <?php echo lang('cancel') ?></button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> <?php echo lang('ok') ?></button>
                </div>
            </div>
        </div>
    </script>

    <script src="<?php echo base_url('assets/lib/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/datepicker/js/bootstrap-datepicker.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/timepicker/js/bootstrap-timepicker.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/jquery.datatables/js/jquery.dataTables.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/jquery.gritter/js/jquery.gritter.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/nicescroll/jquery.nicescroll.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/lib/cookies/cookies.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/main.js') ?>"></script>
    <?php if (isset($js_file) && strlen($js_file)): ?>
        <script src="<?php echo base_url('assets/' . $js_file) ?>"></script>
    <?php endif ?>

    <!--<script src="<?php echo base_url('assets/lib/socket.io/socket.io.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/socket.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/chat.js') ?>"></script>-->
</body>
</html>
<?php $this->load->view('admin/components/header'); ?>
<body class="body-login">
    <div id="loading-ajax"></div>
    <div id="wrap">
		<?php $this->load->view($subview); //Subview in set in controller ?>
	</div>
	<?php $this->load->view('admin/components/footer'); ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#language a').on('click', function (event) {
				event.preventDefault();
				App.loading.show('Cambiando de idioma...');
				$.get(base_url + 'ajax/change_language/' + $(this).data('role'), function (response) {
					window.location = '';
				});
			});
		});
	</script>
	<!-- Modal main -->
    <div id="main-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content"></div>
        </div>
    </div>
	<script src="<?php echo base_url('assets/lib/bootstrap/js/bootstrap.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/lib/datepicker/js/bootstrap-datepicker.js') ?>"></script>
	<script src="<?php echo base_url('assets/lib/jquery.datatables/js/jquery.dataTables.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/lib/cookies/cookies.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/main.js') ?>"></script>
</body>
</html>
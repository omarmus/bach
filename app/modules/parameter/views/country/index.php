<div class="section-buttons">
	<?php echo btn_add(lang('add_country'), 'parameter/country/edit'); ?>
	<?php echo btn_delete('parameter/country/delete_selected'); ?>
</div>

<div class="panel panel-default">
	<div class="panel-heading filter">
		<form method="post">
			<div>
				<label><?php echo lang('name') ?></label>
				<input type="text" name="name" value="<?php echo $this->input->post('name') ?>" class="form-control">
			</div>
			<div>
				<label><?php echo lang('code') ?></label>
				<input type="text" name="code" value="<?php echo $this->input->post('code') ?>" class="form-control">
			</div>
			<?php echo buttons_filter() ?>
		</form>
	</div>
	<div class="panel-body">		
		<table id="main-table" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th></th>
					<th class="edit"><?php echo lang('edit') ?></th>
					<th><?php echo lang('name') ?></th>
					<th><?php echo lang('code') ?></th>
					<th class="state"><?php echo lang('active') ?></th>
				</tr>
			</thead>
			<tbody>
			<?php if (count($countries)): foreach ($countries as $country): ?>
				<tr>
					<td><?php echo $country->id_country ?></td>
					<td class="edit"><?php echo btn_edit('parameter/country/edit/' . $country->id_country) ?></td>
					<td><?php echo $country->name; ?></td>
					<td><?php echo $country->code; ?></td>
					<td class="edit"><?php echo btn_yes_no($country->state, 'parameter/country/set_state/'. $country->id_country) ?></td>
				</tr>
			<?php endforeach; endif ?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	var DT = null;
	$(document).ready(function() {
		DT = App.datatable({noSortable : [ 1, 4 ]});
	});
</script>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PropelORM
{
	private $propel;
    private $propel_conf_file;
    private $propel_models_dir;
    private $propel_main_script;

	public function __construct()
	{
        $this->propel =& get_instance();

        $this->propel_conf_file   = $this->propel->config->item('propel_conf_file');
        $this->propel_models_dir  = $this->propel->config->item('propel_models_dir');
        $this->propel_main_script = $this->propel->config->item('propel_main_script');

        // Add the generated 'classes' directory to the include path
        set_include_path($this->propel_models_dir . PATH_SEPARATOR);
 
        // see http://www.propelorm.org/documentation/02-buildtime.html setting up propel section
        // Include the main Propel script
        require_once $this->propel_main_script;
        // Initialize Propel with the runtime configuration

        require_once APPPATH."/third_party/propel/PropelLogger.php";

        Propel::setLogger(new PropelLogger());
        Propel::init( $this->propel_conf_file );
	}
}

/* End of file Propel.php */
/* Location: ./application/libraries/Propel.php */

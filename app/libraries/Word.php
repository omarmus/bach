<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */

require_once APPPATH . 'third_party/PhpWord/Autoloader.php';
\PhpOffice\PhpWord\Autoloader::register();

class Word extends \PhpOffice\PhpWord\PhpWord
{

	public function __construct()
	{
		parent::__construct();
	} 

	public function read($source = '')
	{
		// Se modificó la librería para que está pueda devolver el 
		// contenido del documento Word en formato HTML
		$phpWord = \PhpOffice\PhpWord\IOFactory::load($source);
		return $phpWord->save('', 'HTML', false, true);
	}
}
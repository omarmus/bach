// enterprise
App.enterprise = (function () {

	var DT = null; //Datatable

	var init = function () {

		DT = App.datatable({noSortable : [ 1, 8 ]});
		DT.btnEdit(base_url + 'admin/enterprise/edit/', addEnterprise);

		$('#btn-add').on('click', function () {
			App.modal.show(base_url + 'admin/enterprise/edit/', addEnterprise);
		});
		$('#delete-rows').on('click', function() {
			DT.deleteSelected(base_url + 'admin/enterprise/delete_selected');
		});
	}

	var addEnterprise = function () {
		$('#main-modal form').on('submit', function(e) {
			e.preventDefault();
			App.validate.form(this, base_url + 'admin/enterprise/edit/' + $(this).data('role'), addEnterprise);
		});
	}

	init(); // Init module

	// Public attributtes and methods
    var enterprise = {};

    return enterprise;

}());
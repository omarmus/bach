// Config Jquery
(function($) {
    $.ajaxSetup({
        dataFilter : function (data, type) {
            if (data == 'SESSION_EXPIRED' || data == 'ERROR_PERMISSION') {
                App.modal.hide();
                App.modal.hide("second-modal");
                $('.dataTables_processing').hide();
                if (data == 'SESSION_EXPIRED') {
                    App.$alert_modal.load(base_url + 'ajax/login_ajax', function () {
                        App.loading.hide();
                        App.$alert_modal.modal({
                            keyboard : false
                        }).modal('show')
                        .off('click.dismiss.bs.modal')
                        .on('shown.bs.modal', function () {
                            $alert_modal.find('[name=email_login]').focus();
                        });
                    });
                    throw new Error('Session expired.');
                } else {
                    App.message.error(lang.error_permission);
                    throw new Error('Error permission.');   
                }
            }
            return data;
        }
    });
    //Datepicker
    $.fn.datepicker.dates['es'] = {
        days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
        daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
        daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
        months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        today: "Hoy"
    };
    //Datatable
    $.extend( true, $.fn.dataTable.defaults, {
        "sDom" : "<'row'<'col-xs-6 col-md-6 selected-rows'><'col-xs-6 col-md-6 text-right'fl>r>t<'row'<'col-xs-6 col-md-5'i><'col-xs-6 col-md-7'p>>", 
        "bFilter" : false, 
        "sPaginationType" : "full_numbers", 
        "aoColumnDefs" : [{"bVisible" : false, "aTargets" : [0]}], 
        "aLengthMenu" : [[10, 25, 50,  - 1], [10, 25, 50, "Todos"]], 
        "oLanguage": {
            "sSearch" : "Buscar",
            "sLengthMenu": lang.datatable.rows,
            "sZeroRecords": "Nothing found - sorry",
            "sInfo": lang.datatable.info,
            "sInfgetoEmpty": lang.datatable.show_empty,
            "sProcessing" : "Cargando registros",
            "oPaginate": {
                "sNext": "&raquo;",
                "sPrevious": "&laquo;",
                "sFirst" : lang.datatable.first,
                "sLast" : lang.datatable.last
            }
        },
    });
}(jQuery));

// Main Module
var App = (function (app, $) {

    var tmpl_alert = $('#tmpl-alert').html();
    var $alert_modal = $("#alert-modal");
    var nano = function (template, data) {
        return template.replace(/\{([\w\.]*)\}/g, function (str, key) {
            var keys = key.split("."), v = data[keys.shift()];
            for (var i = 0, l = keys.length;i < l;i++)
                v = v[keys[i]];
            return (typeof v !== "undefined" && v !== null) ? v : "";
        });
    }
    
    app.$alert_modal = $alert_modal;
    app.nano = nano;   
    
    app.toType = function (obj) {
        return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
    }
    
    app.isJson = function (text) {
        return /^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
               replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
               replace(/(?:^|:|,)(?:\s*\[)+/g, ''));
    }
    
    app.alert = function (message, callback_function, title) {
        $alert_modal.html(App.nano(tmpl_alert, {
            title : title || 'Alerta', 
            message : message, 
            button_cancel : 'hide'
        })).modal();
        if (callback_function) {
            $alert_modal.find('.modal-footer .btn-primary').on('click', callback_function);
        }
    }

    app.confirm = function (message, callback_function, title) {
        $alert_modal.html(App.nano(tmpl_alert, {
            title : title || 'Confirmar', 
            message : message, 
            button_cancel : ''
        })).modal();
        if (callback_function) {
            $alert_modal.find('.modal-footer .btn-primary').on('click', callback_function);
        }
    }
    
    app.getParameters = function (p, args) {
        var first = p.first.defaultValue,
            second = p.second.defaultValue;
            
        p.first.position = p.first.position || 0;
        p.second.position = p.second.position || 1;
        
        var type = App.toType(args[p.first.position]);
        if (type != 'undefined') {          
            if (type == p.first.type) {
                first = args[p.first.position];
                second = App.toType(args[p.second.position]) == p.second.type ? args[p.second.position] : p.second.defaultValue;
            } else {
                if (type == p.second.type) {
                    first = App.toType(args[p.second.position]) == p.first.type ? args[p.second.position] : p.first.defaultValue;
                    second = args[p.first.position];
                }
            }
        }
        return {
            first : first,
            second : second
        };
    }

    app.refresh = function (time) {
        setTimeout(function () {
            window.location = '';
        }, time || 1200);
    }

    app.toggleFullScreen = function () {
        if ((document.fullScreenElement && document.fullScreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    }

    app.iframe = function (iframe, content) {
        iframe.contentWindow.document.write(content);
        iframe.contentWindow.document.close();
    }

    return app;
}(App || {}, jQuery));


App.normalize = (function() {
    var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç",
        to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
        mapping = {};

    for(var i = 0, j = from.length; i < j; i++ )
        mapping[ from.charAt( i ) ] = to.charAt( i );

    return function( str ) {
        var ret = [];
        for( var i = 0, j = str.length; i < j; i++ ) {
            var c = str.charAt( i );
            if( mapping.hasOwnProperty( str.charAt( i ) ) )
                ret.push( mapping[ c ] );
            else
                ret.push( c );
        }
        // return ret.join( '' );
        return ret.join( '' ).replace( /[^-A-Za-z0-9]+/g, '-' ).toLowerCase();
    }  
})();

// Loading
App.loading = (function () {

    var $loading_ajax = $('#loading-ajax');

    var loading = {};

    loading.show = function (text) {
        $loading_ajax.html(text || lang.loading + '...').css({
            marginLeft : '-' + $loading_ajax.outerWidth() / 2 + 'px'
        }).fadeIn(200);
    }

    loading.hide = function () {
        $loading_ajax.fadeOut(200);
    }

    return loading;

}());

// Modal
App.modal = (function () {

    var main = 'main-modal';
    var second = 'second-modal';
    var backup = [];

    var change = function (obj) {
        for (var i = 0, l = backup.length; i < l; i++) {
            if (obj.name == backup[i].name) {
                backup[i].change = backup[i].checked ? obj.checked.toString() != backup[i].checked : obj.value != backup[i].value;
                break;
            }
        }
    }

    var isChange = function () {
        for (var i = 0, l = backup.length; i < l; i++) 
            if (backup[i].change) 
                return false;
        return true;
    }
    
    var setChanges = function ($btn, obj) { 
        change(obj);
        $btn.prop({disabled: isChange()});
    }

    var modal = {};

    modal.main = main;
    modal.backup = backup;
    modal.change = change;
    modal.isChange = isChange;
    modal.setChanges = setChanges;
    
    // Parameters url, size, callback_function
    modal.show = function (url) {
        
        var $modal = $('#' + main + ' .modal-content');        
        var parameters = App.getParameters({
            first : {
                type : 'string',
                position : 1
            },
            second : {
                type : 'function',
                position : 2
            }
        }, arguments);
        var size = parameters.first, 
            callback_function = parameters.second,
            onfocus = typeof arguments[3] == 'undefined';
               
        App.loading.show();
        backup = [];
       
        if (size && size.length) {
            $modal.parent().addClass('modal-' + size);
        } else {
            $modal.parent().removeClass('modal-lg').removeClass('modal-sm');
        }
        $modal.load(url, function () {
            App.loading.hide();
            $modal = $('#' + main);
            var $form = $modal.find("form"),
                $btn_primary = $form.find("button.btn-primary");
            $btn_primary.prop({disabled: true});

            $form.find('.form-group-default input, .form-group-default select, .form-group-default textarea').on('focus', function () {
                $(this).parent().addClass('focused');
            }).on('blur', function () {
                $(this).parent().removeClass('focused');
            });
            var input = $modal.modal().find('input[type=text], textarea').get(0);
            if (input && onfocus) {
                setTimeout(function () {input.focus()}, 500);
            }

            $form.find("input[type=text], textarea").each(function () {
                backup.push({
                    name : this.name,
                    value : this.value,
                    change : false
                });
            }).on("keyup", function (e) {
                if (e == 13) return false;
                setChanges($btn_primary, this);
            });
            $form.find("input[type=radio], input[type=checkbox]").each(function () {
                backup.push({
                    name : this.name,
                    value : this.value,
                    checked : this.checked.toString(),
                    change : false
                });
            }).on("click", function () {
                setChanges($btn_primary, this);
            });
            $form.find("select").each(function () {
                backup.push({
                    name : this.name,
                    value : this.value,
                    change : false
                });
            }).on("change", function () {
                setChanges($btn_primary, this);
            });
            if (callback_function) {
                setTimeout(function () {
                    callback_function.apply(window);
                }, 200);
            }
        });
    }
    
    // Parameters url, size, callback_function
    modal.secondShow = function (url) {
        
        var $modal = $('#' + second + ' .modal-content');        
        var parameters = App.getParameters({
            first : {
                type : 'string',
                position : 1
            },
            second : {
                type : 'function',
                position : 2
            }
        }, arguments);
        var size = parameters.first, callback_function = parameters.second;
               
        App.loading.show();
       
        if (size && size.length) {
            $modal.parent().addClass('modal-' + size);
        } else {
            $modal.parent().removeClass('modal-lg').removeClass('modal-sm');
        }
        $modal.load(url, function () {
            App.loading.hide();
            $modal = $('#' + second);
            var input = $modal.modal().find('input[type=text], textarea').get(0);
            if (input) {
                setTimeout(function () {input.focus()}, 500);
            }
            if (callback_function) {
                setTimeout(function () {
                    callback_function.apply(window);
                }, 200);
            }
        });
    }

    modal.hide = function (id) {
        $('#' + (id || main)).modal('hide');
    }
    
    modal.secondHide = function () {
        $('#' + second).modal('hide');
    }

    return modal;

}());

// Validate
App.validate = (function () {

    var validate = {};

    validate.form = function (form, url, callback_error, callback_ok, id) {
        var $form = $(form),
            data = null,
            $button = $form.find('input[type=submit], button[type=submit]');

        $button.prop({disabled : true}).children('span').hide().next().show();

        if (form.body) {
            var code = $(form.body).code();
            if (code != '<p><br></p>') {
                form.body.value = code;
            }
        }

        $.post(url, $form.serialize(), function (response) {
            App.loading.hide();
            if (response.state) {
                data = response.data;
                response = response.state;
            }
            if (response == "CREATE" || response == "CREATE-AJAX" || response == 'UPDATE' || response == "UPDATE-AJAX" || response == 'CREATE-AND-MAIL') {
                App.message.ok(response == "CREATE" || response == "CREATE-AJAX"? lang.create_success : lang.update_success, 500);
                if (response == 'CREATE-AND-MAIL') {
                    App.message.mail();
                }
                if (callback_ok) {
                    callback_ok.apply(window, [data]);
                }
                if (response != "CREATE-AJAX" && response != "UPDATE-AJAX") {
                    setTimeout(function () {window.location = '';}, 1200);
                }
            } else {
                if (response == "ERROR") {
                    App.message.error(data || "Se produjo un error al momento de procesar su solicitud.");
                    App.modal.hide();
                } else {
                    var $modal = $('#' + App.modal.main);
                    var error = $modal.find('.modal-content').html(response).find('.input-error').get(0);
                    setTimeout(function () {$(error).prev().focus()}, 300);
                    var $btn_primary = $modal.find("button.btn-primary");
                    $modal.find('input[type=text], textarea').on('keyup', function (e) {
                        if (e == 13) return false;
                        $(this).next().fadeOut();
                        App.modal.setChanges($btn_primary, this);
                    });
                    $modal.find('input[type=radio], input[type=checkbox]').on('click', function () {
                        $(this).parent().parent().next().fadeOut();
                        App.modal.setChanges($btn_primary, this);
                    });
                    $modal.find('select').on('change', function () {
                        $(this).next().fadeOut();
                        App.modal.setChanges($btn_primary, this);
                    });
                    $modal.find('.input-group input').off('keyup').on('keyup', function () {
                        $(this).parent().next().fadeOut();
                        App.modal.setChanges($btn_primary, this);
                    });
                                  
                    if (callback_error) {
                        callback_error.apply(window);
                    }
                }
            }
        });
        return false;
    }

    validate.data = function (form, url, id, callback_error, callback_ok) {
        var $button = $('#' + id),
            $form = $(form);
        $button.prop({disabled : true}).children('span').hide().next().show();
        $.post(url, $form.serialize(), function (response) {
            if (response.length) {
                $form.html(response)
            }
            var error = $form.find('.input-error');
            if (error.length) {
                setTimeout(function () {$(error[0]).prev().focus()}, 500);
                $form.find('input').on('keypress', function () {
                    $(this).next().fadeOut();
                });
                if (callback_error) {
                    callback_error.apply(window);
                }
            } else {
                $button.parent().hide().prev().show();
                App.message.ok(lang.save_changes);

                if (callback_ok) {
                    callback_ok.apply(window);
                }
            }
            $button.prop({disabled : false}).children('span').show().next().hide();
        });
        return false;
    }

    validate.login = function (form, url) {
        App.loading.show('Ingresando al sistema');
        $(form.submit).prop({disabled : true});
        $.post(url, $(form).serialize(), function (response) {
            App.loading.hide();
            if (response == 'OK') {
                App.$alert_modal.modal('hide');
            } else {
                App.$alert_modal.html(response);
                if (App.$alert_modal.find('.input-error').length) {
                    App.$alert_modal.find('input').on('keypress', function () {
                        $(this).parent().next().fadeOut();
                    });
                }
            }
        });
        return false;
    }

    return validate;

}());

// Message
App.message = (function () {

    var defaults = {
        title : 'Message',
        text : '',
        sticky : false, 
        time : 4000, 
        class_name : '', 
        position : 'top-right', 
        fade_in_speed : 80, 
        fade_out_speed : 80,
        image : null
    };

    var render = function (options) {
        $.gritter.add($.extend(defaults, options));
    }

    var message = {};

    message.ok = function (text, delay) {
        setTimeout(function () {
            render({
                title : lang.success,
                text : text || lang.message_ok + '.',
                image : 'glyphicon-ok',
                class_name : 'n-success'
            });
        },
        delay || 0);
    }

    message.error = function (text, delay) {
        setTimeout(function () {
            render({
                title : lang.error,
                text : text = text || lang.message_error + '.',
                image : 'glyphicon-warning-sign',
                class_name : 'n-error'
            });
        },
        delay || 0);
    }

    message.mail = function (text, delay) {
        setTimeout(function () {
            render({
                title : lang.mail,
                text : text || lang.message_mail + '.',
                image : 'glyphicon-envelope',
                class_name : 'n-mail'
            });
        },
        delay || 0);
    }

    return message;

}());

// Datatable
App.datatable = function () {

    var oTable = null;
    var $table = null;
    var selected = [];
    var $checkAll = null;
    var settings = {
        "deferRender": true
    };
    var checkbox = {
        "fnInitComplete" : function () {
            var that = this, 
                firstTH = $(that).find('th')[0], 
                th = document.createElement('th');
            th.innerHTML = '<label class="cr-styled"><i class="fa"></i></label>';
            th.className = "check-all";
            th.onclick = function () {
                var toggle = (that.$('tr').length == that.$('tr.active').length ? 'remove' : 'add') + 'Class';
                $(firstTH).parent()[toggle]('all-selected');
                that.$('tr')[toggle]('active');
                var now = that.$('tr.active').length;
                $('#delete-rows')[(now?'remove':'add') + 'Class']('disabled');
                $($(that).parent().find('.selected-rows')[0]).html(now?'<span class="label label-warning"><strong>'+now+'</strong> ' + lang.row +(now>1?'s':'')+' ' + lang.selected + '.</span>':'');
            }
            $(firstTH).parent().prepend(th);
            if (that.$('tr').length == 0) {
                $(that).find('td').prop('colspan', $(that).find('td').prop('colspan') + 1);
            }
            that.$('tr').each(function () {
                var firstTD = this;
                var td = document.createElement('td');
                td.innerHTML = '<label class="cr-styled"><i class="fa"></i></label>';
                td.className = "check-item";
                td.onclick = function () {
                    $(firstTD).toggleClass('active');
                    var now = that.$('tr.active').length;
                    $(firstTH).parent()[(that.$('tr').length == now ? 'add' : 'remove') + 'Class']('all-selected');
                    $('#delete-rows')[(now?'remove':'add') + 'Class']('disabled');
                    $($(that).parent().find('.selected-rows')[0]).html(now ? '<span class="label label-warning"><strong>' + now + '</strong> Fila' + (now > 1 ? 's' : '') + ' Seleccionada' + (now > 1 ? 's' : '') + '.</span>' : '');
                }
                $(firstTD).prepend(td);
            })
        }
    };
    
    // Set id_table and options   
    var parameters = App.getParameters({
        first : {
            type : 'string',
            defaultValue : 'main-table'
        },
        second : {
            type : 'object',
            defaultValue : {}
        }
    }, arguments);
    var id_table = parameters.first, options = parameters.second;
       
    var init = function () {
        if (options.ajax) {
            settings = $.extend({}, settings,{
                "processing" : true,
                "serverSide" : true,
                "rowCallback": function( row, data ) {
                    if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                        $(row).addClass('active');
                    }
                 },
                "aoColumnDefs" : [
                    {"bVisible": true, "aTargets": [ 0 ]}
                ]
            });
            options.checkbox = false;
        }
        if (typeof options.checkbox === 'undefined' || options.checkbox === true) {
            settings = $.extend({}, settings, checkbox);
            delete options.checkbox;
        }
        if (options.noSortable) {
            settings = $.extend({}, settings, {
                "aoColumnDefs" : [
                    {"bVisible": false, "aTargets": [ 0 ]}, 
                    {"bSortable": false, "aTargets": options.noSortable }
                ]
            });
            delete options.noSortable;
        }
        
        settings = $.extend({}, settings, options);
        $table = $('#' + id_table);
        oTable = $table.dataTable(settings);
        
        if (options.ajax) {
            $checkAll = $table.find(".check-all");
            $checkAll.on('click', function () {
                var toggle = ($checkAll.parent().hasClass("all-selected") ? 'remove' : 'add') + 'Class';
                if (toggle == "addClass") {
                    App.loading.show("Seleccionando los registros, espere por favor");
                    $.get(options.ajax, {all : true}, function(response) {
                        App.loading.hide();
                        selected = response;
                    }, "json");
                } else {
                    selected = [];
                }
                $checkAll.parent()[toggle]('all-selected');
                oTable.$('tr')[toggle]('active');
                var now = toggle == 'addClass' ? oTable.$('table').dataTableSettings[0]._iRecordsTotal : 0;
                $($table.parent().find('.selected-rows')[0]).html(now ? '<span class="label label-warning"><strong>' + now + '</strong> ' + "Fila" + (now > 1 ? 's' : '') + ' ' + 'Seleccionada' + (now > 1 ? 's' : '') + '.</span>' : '');
            });
            $table.find('tbody').on( 'click', 'tr td.check-item', function () {           
                var id = this.parentNode.id;
                var index = $.inArray(id, selected);
                index === -1 ? selected.push(id) : selected.splice(index, 1);
                var now = selected.length;
                var total = oTable.$('table').dataTableSettings[0]._iRecordsTotal;
                $checkAll.parent()[(total == now ? 'add' : 'remove') + 'Class']('all-selected');
                $($table.parent().find('.selected-rows')[0]).html(now ? '<span class="label label-warning"><strong>' + now + '</strong> Fila' + (now > 1 ? 's' : '') + ' Seleccionada' + (now > 1 ? 's' : '') + '.</span>' : '');
                $(this).parent().toggleClass('active');
            });
        }
    }
    
    init();
    
    var deleteRow = function (obj, url, refresh) {
        App.confirm("¿Deséa eliminar el registro?", function () {
            App.loading.show("Eliminando registro");
            $.post(url, function (response) {
                if (response == 'DELETE') {
                    App.loading.hide();
                    App.message.ok("¡Registro eliminado!");
                    oTable.fnDeleteRow( $(obj).parents('tr') )
                    if (refresh) {
                        setTimeout(function () {
                            window.location = '';
                        }, 500);
                    }
                } else {
                    App.message.error(response);
                    App.loading.show("Recargando página");
                    setTimeout(function () {
                        window.location = '';
                    }, 4000);
                }
            });
        });
    }

    var deleteSelected = function (url, refresh) {
        var pks = getPks();
        if (pks.length) {
            var message = lang.delete_item.replace(/%n%/gi, pks.length);
            message = message.replace(/%s%/gi, (pks.length > 1 ? 's' : ''));
            App.confirm(message, function () {
                App.loading.show(lang.deleted_items);
                $.post(url, {pks: pks}, function (response) {
                    if (response == 'OK') {
                        App.loading.hide();
                        App.message.ok(lang.deleted_records);
                        deleteRows();
                        $('#delete-rows').addClass('disabled').blur();
                        $(oTable.find('th')[0]).parent().removeClass('all-selected');
                        $(oTable.parent().find('.selected-rows')[0]).html('');
                        if (refresh) {
                            setTimeout(function () {
                                window.location = '';
                            }, 500);
                        }
                    } else {
                        App.message.error(response);
                        App.loading.show(lang.refresh);
                        setTimeout(function () {
                            window.location = '';
                        }, 4000);
                    }
                });
            });
        }
    }

    var getPks = function () {
         var pks = [];
        if (options.ajax) {
            for (var i = 0, l = selected.length; i < l; i++) {
                pks.push(selected[i].split("_")[1]);
            }
        } else {
            oTable.$('tr.active').each(function () {
                pks.push(oTable.fnGetData(this)[0]);
            });
        }
        return pks;
    }

    var deleteRows = function () {
        oTable.$('tr.active').each(function () {
            oTable.fnDeleteRow( this );
        });
    }

    var datatable = {};
    
    datatable.deleteRow = deleteRow;
    datatable.deleteSelected = deleteSelected;
    datatable.getPks = getPks;
    
    datatable.btnEdit = function (url) {
        var parameters = App.getParameters({
            first : {
                type : 'string',
                position : 1
            },
            second : {
                type : 'function',
                position : 2
            }
        }, arguments);
        var size = parameters.first, callback_function = parameters.second;
        oTable.$('.btn-edit').on('click', function () {
            App.modal.show(url + $(this).data('role'), size || callback_function, callback_function && size ? callback_function : size, arguments[3]);
        });
    }
    
    datatable.btnDelete = function (url, refresh) {
        oTable.$('.btn-delete').on('click', function () {
            deleteRow(this, url + $(this).data('role'), refresh);
        });
    }
    
    datatable.btnView = function (search, url) {
        var parameters = App.getParameters({
            first : {
                type : 'string',
                position : 2
            },
            second : {
                type : 'function',
                position : 3
            }
        }, arguments);
        var size = parameters.first, callback_function = parameters.second;
        oTable.$(search).on('click', function (e) {
            e.preventDefault();
            App.modal.show(url + $(this).data('role'), size || callback_function, callback_function && size ? callback_function : size);
        });
    }
    
    datatable.btnViewSecond = function (query, url) {
        var parameters = App.getParameters({
            first : {
                type : 'string',
                position : 2
            },
            second : {
                type : 'function',
                position : 3
            }
        }, arguments);
        var size = parameters.first, callback_function = parameters.second;
        oTable.$(query).on('click', function (e) {
            e.preventDefault();
            App.modal.secondShow(url + $(this).data('role'), size || callback_function, callback_function && size ? callback_function : size);
        });
    }
    
    datatable.getElements = function (query) {
        return oTable.$(query);
    }

    return datatable;

};

// Autocomplete
App.autocomplete = function (query, settings) {
    settings = settings || {};
    var defaults = {}, data = {},
        $el = $(query), 
        attributes = $el[0].attributes,
        classes = $el[0].className.split(/\s+/),
        ms = null; // object MagicSuggest

    var init = function () {
        for (var i = 0, l = attributes.length; i < l; i++) {
            var value = attributes[i].value;
            data[attributes[i].name.replace(/data-/gi, '')] = value === 'true' || (value === 'false' ? false : value);
        }
        for (var i = 0, l = classes.length; i < l; i++) {
            data[classes[i]] = true;
            settings.cls = (settings.cls || '') + ' ' + classes[i];
        }
        settings = $.extend(defaults, data, settings || {});
        if (settings.multiple) {
            settings.hideTrigger = true;
        }
        if (typeof settings.search != 'undefined' && !settings.search) {
            settings.cls = (settings.cls || '') + ' no-search';
        }
        ms = $el.magicSuggest(settings);
        if (settings.integer) {
            $(ms).on('keydown', function (e,m,v) {
                App.filter.integer(v);
            });
        }
        if (settings.decimal) {
            $(ms).on('keydown', function (e,m,v) {
                App.filter.decimal(v);
            });
        }
        if (settings.required) {
            $(ms).on('selectionchange', function(e,m) {
                var $el = $(query);
                $el.closest('div.form-group').removeClass('has-error').addClass('has-success');
                $el.nextAll('.input-required').fadeOut();
            });    
        }
    }

    init();
    
    ms.disabled = function (bool) {
        bool ? ms.disable() : ms.enable();
    }

    return ms;

};

// Filters
App.filter = (function () {

    var filter = {};

    filter.integer = function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
    
    filter.decimal = function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 188, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }

    return filter;

}());

// Form
App.form = (function () {

    var form = {};

    form.integer = function (query) {
        $(query).on('keydown', function (event) {
            App.filter.integer(event);
        });
    }
    
    form.decimal = function (query) {
        $(query).on('keydown', function (event) {
            App.filter.decimal(event);
        });
    }

    form.code = function(elements) {
        $(elements).summernote({
            height: 180,
            codemirror: {
                theme: 'monokai'
            },
            toolbar: [
                ['style', ['bold', 'italic', 'underline']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link']],
                ['view', ['codeview']],
                ['help', ['help']]
            ]
        });
    }

    return form;

}());

// Array
App.array = (function () {

    var array = {};
    
    array.replace = function (text, find, replace) {
        for (var i = 0, l = find.length, regex; i < l; i++) {
            regex = new RegExp(find[i], "g");
            text = text.replace(regex, replace[i]);
        }
        return text;
    };

    return array;

}());

// Datetime
App.datetime = (function () {

    var formatDate = 'dd/MM/YYYY';
    var months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    
    var isValid = function (date) {
        if (Object.prototype.toString.call(date) !== "[object Date]")
            return false;
        return !isNaN(date.getTime());
    }
    
    var datetime = {};
    
    datetime.isValid = isValid;
    
    datetime.setFormatDate = function (format) {
        formatDate = format;
    }
    
    datetime.date = function (query, settings) {
        var defaults = {
            format: formatDate.toLowerCase(),
            language: "es",
            todayHighlight: true,
            autoclose: true
        };
        settings = $.extend(defaults, settings || {});
        $(query).datepicker(settings);
    }
    
    datetime.time = function (query) {
        $(query).timepicker({showMeridian : false});
    }
    
    datetime.dateLiteral = function (date) {
        return datetime.format(date, 'dd de MMM del YYYY');
    }
    datetime.datetimeLiteral = function (date) {
        return datetime.format(date, 'dd de MMM del YYYY a Hrs. HH:mm:ss');
    }
    
    datetime.format = function (date, format) {
        var d = new Date(date);
        if (isValid(d)) {
            format = format || formatDate;
            var monthLiteral = format.indexOf('MMM') != -1;
            return App.array.replace(format, ["dd", monthLiteral ? 'MMM' : 'MM', 'YYYY', 'HH', 'mm', 'ss'], [d.getDate(), monthLiteral ? months[d.getMonth()] : d.getMonth() + 1, d.getFullYear(), d.getHours(), d.getMinutes(), d.getSeconds()]);
        }
        return date;
    }

    return datetime;

}());

// button
App.button = (function () {

    var button = {};

    button.onOff = function (input, url) {
        $.post(url, {state: $(input).children('input').val()});
    }

    return button;

}());

// city
App.city = (function () {

    var city = {};

    city.list = function (id_country, selected, filter, id) {
        App.loading.show(lang.loading_city);
        $.get(base_url + 'parameter/city/json_list/' + id_country, filter || {}, function (response) {
            App.loading.hide();
            var obj = $('#' + (id || 'cities'));
            if (selected) {
                obj.val(selected);
            }
        }, 'json');
    }

    return city;

}());

// editor
App.editor = (function () {

	var edit = function ($this, callback_edit) {
        $this.hide().next().show();
        var $next = $this.parent().parent().next();
        $next.find('.form-control').show();
        $next.find('.input-group').css('display', 'table');
        $next.find('div.none').show();
        $next.find('label.none').show();
        $next.find('strong.none').show();
        $next.find('.form-control-static').hide();
        $next.find('.input-error').show();

        if (callback_edit) {
            callback_edit.apply(window);
        }
    }

    var cancel = function ($this, callback_cancel) {
        $this.parent().hide().prev().show();
        var $next = $this.parent().parent().parent().next();
        $next.find('.form-control').hide();
        $next.find('.input-group').hide();
        $next.find('div.none').hide();
        $next.find('label.none').hide();
        $next.find('strong.none').hide();
        $next.find('.form-control-static').show();
        $next.find('.input-error').hide();

        if (callback_cancel) {
            callback_cancel.apply(window);
        }
    }

    var editor = {};

    editor.btn = function ($buttons, callback_edit, callback_cancel) {
        $($buttons[0]).on('click', function() {
            edit($(this), callback_edit);
        });
        $($buttons[1]).on('click', function() {
            cancel($(this), callback_cancel);
        });
    }

    editor.send = function ($button, $form) {
        $button.on('click', function() {
            $form.submit();
        });
    }

    return editor;

}());

// theme Brio Web
App.theme = (function () {

    function init() {

        render('body');

        var $buttonCollapse = $('.navbar-toggle');
        var $asideActive = $('aside.left-panel .active').parent().parent();

        if($asideActive.hasClass('has-submenu')) {
            $asideActive.addClass('active');
        }

        $(window).load(function() {
            $('.loading-container').fadeOut(1000, function() {
                $(this).remove();
            });
        }); 
        
        var $leftPanel = $('aside.left-panel');
        $buttonCollapse.on('click', function(){
            $leftPanel.toggleClass('collapsed');
            var collapse = $leftPanel.hasClass('collapsed');
            $('header .margin-left').css('marginLeft', collapse ? '75px' : '230px');
            $('.left-panel .logo').toggleClass('active');
            $('.footer').toggleClass('active');
            if (collapse) {
                $('.has-submenu > ul').hide();
            }
            localStorage.setItem('navbar-collapse', collapse ? 'yes' : 'no');
            if ($('#masonry').find('.document-article').length) {
                $('#masonry').masonry();    
            }
        });

        if (localStorage.getItem('navbar-collapse') == 'yes') {
            $leftPanel.addClass('collapsed');
            $('header .margin-left').css('marginLeft', '75px');
            $('.left-panel .logo').addClass('active');
            $('.footer').addClass('active');
        }
        
        $("aside.left-panel nav.navigation > ul > li:has(ul) > a").on('click', function() {
            
            if( $("aside.left-panel").hasClass('collapsed') == false || $(window).width() < 768 ) {
                $("aside.left-panel nav.navigation > ul > li > ul").slideUp(300);
                $("aside.left-panel nav.navigation > ul > li").removeClass('active');
            
                if(!$(this).next().is(":visible")) {
                    $(this).next().slideToggle(300, function(){ $("aside.left-panel:not(.collapsed)").getNiceScroll().resize(); });
                    $(this).closest('li').addClass('active');
                }
            
                return false;
            }   
        });
        
        if( $.isFunction($.fn.niceScroll) ) {
            $(".nicescroll").niceScroll({
                cursorcolor: '#9d9ea5',
                cursorborderradius : '0px'      
            });
        }

        if( $.isFunction($.fn.niceScroll) ){
            $("aside.left-panel:not(.collapsed)").niceScroll({
                cursorcolor: '#8e909a',
                cursorborder: '0px solid #fff',
                cursoropacitymax: '0.5',
                cursorborderradius : '0px'  
            });
        }

        $('.scrollToTop').click(function() {
            $('.warper').animate({scrollTop : 0},800);
            return false;
        }); 
    }

    var render = function (container) {
        App.form.integer(container + ' .integer');
        App.form.decimal(container + ' .decimal');
        // App.form.maxlength(container);
        App.datetime.date(container + ' .datepicker');

        $(container + ' [data-toggle="tooltip"]').tooltip();
    }

    init(); // begin theme

    var theme = {};

    return theme;

}());